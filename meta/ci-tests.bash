#!/bin/bash

function extend_summary() {
    # $1: summary variable
    # $2 must have the form: "\e[92m✔ test-name: passed/failed\e[0m"
    local -n sum=$1
    cur=$(echo -e "$2" | awk '{printf("%-7s %-18s: %6s %s %s %s\n", $1, $2, $3, $4, $5, $6)}')
    sum=$(printf '%s\n%s' "$sum" "$cur")
}

function pylint_scorehandler() {
    # $1: name of test
    # $2: pylint.txt temp file storing the write-out from Pylint
    # $3: summary variable
    # $4: set for interactive mode showing warnings, unset for silent
    echo "Finished Pylint for $1. Extracting score..."
    score=$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' $2)
    rm $2
    echo -e "\e[96mPylint score was \e[1m${score}/10.00\e[0;96m. A minimum of 9.95 is required.\e[0m"
    if ( (( $(echo "$score >= 9.95" | bc -l) )); exit $? ); then
        echo -e "\e[1;92m✔ pylint-$1 passed. Minimum score fulfilled.\e[0m"
    else
        echo -e "\e[1;91m✘ pylint-$1 failed. Minimum score not fulfilled.\e[0m"
        echo -e "\e[93mPlease fix all lint errors and improve the score before continuing.\e[0m"
        [ -n "$3" ] && extend_summary $3 "\e[91m✘ pylint-$1 failed\e[0m"
        return 1
    fi

    if ( (( $(echo "$score < 10.00" | bc -l) )); exit $? ); then
        echo -e "\e[33m! Score not perfect. Please try to clean up the code as much as possible and strive for a perfect score.\e[0m"
        [ -n "$3" ] && extend_summary $3 "\e[93m✔ pylint-$1 passed (imperfect score: $score)\e[0m"
        if [ ! -z "$4" ]; then
            read -p "Would you like to halt the tests to clean up the code? [Y/n - default: Y] - " yn;\
            case $yn in
                [Nn]* )
                    echo "Fine, but please still try to clean up the code.";;
                [Yy]* )
                    echo "Excellent! Please clean up the code and run this test again.";
                    exit 2;;
                * )
                    echo "Excellent! Please clean up the code and run this test again.";
                    exit 2;;
            esac
        fi
    else
        [ -n "$3" ] && extend_summary $3 "\e[92m✔ pylint-$1 passed\e[0m"
    fi
    return 0
}


# mypy-poly-inspect
function mypy_src() {
    # $1: summary variable
    echo -e "\e[1;93mStage: Static Analysis - Job: mypy-src\e[0m"
    echo -e "\e[96mRunning Mypy for type checking on poly-inspect source code...\e[0m"
    python -m mypy polyinspect &&\
        {
            echo -e "\e[1;92m✔ mypy-src passed.\e[0m";
            [ -n "$1" ] && extend_summary $1 "\e[92m✔ mypy-src passed\e[0m";
            return 0;
        } ||\
        {
            echo -e "\e[1;91m✘ mypy-src failed.\e[0m";
            echo -e "\e[93mPlease fix all type errors before continuing.\e[0m";
            [ -n "$1" ] && extend_summary $1 "\e[91m✘ mypy-src failed\e[0m";
            return 1;
        }
}

# mypy-unittests
function mypy_unittests() {
    # $1: summary variable
    echo -e "\e[1;93mStage: Static Analysis - Job: mypy-unittests\e[0m"
    echo -e "\e[96mRunning Mypy for type checking on poly-inspect test code...\e[0m"
    python -m mypy tests &&\
        {
            echo -e "\e[1;92m✔ mypy-unittests passed.\e[0m";
            [ -n "$1" ] && extend_summary $1 "\e[92m✔ mypy-unittests passed\e[0m";
            return 0;
        } ||\
        {
            echo -e "\e[1;91m✘ mypy-unittests failed.\e[0m";
            echo -e "\e[93mPlease fix all type errors before continuing.\e[0m";
            [ -n "$1" ] && extend_summary $1 "\e[91m✘ mypy-unittests failed\e[0m";
            return 1;
        }
}

# pylint-poly-inspect
function pylint_src() {
    # $1: summary variable
    # $2: set for interactive mode showing warnings, unset for silent
    echo -e "\e[1;93mStage: Static Analysis - Job: pylint-src\e[0m"
    echo -e "\e[96mRunning Pylint to analyse poly-inspect source code...\e[0m"
    ( python -m pylint --output-format=text polyinspect || $(pwd)/meta/pylint-exit-handler.sh $? ) | tee pylint.txt
    pylint_scorehandler 'src' pylint.txt $1 $2
    return $?
}

# pylint-unittests
function pylint_unittests() {
    # $1: summary variable
    # $2: set for interactive mode showing warnings, unset for silent
    echo -e "\e[1;93mStage: Static Analysis - Job: pylint-unittests\e[0m"
    echo -e "\e[96mRunning Pylint to analyse poly-inspect test code...\e[0m"
    ( python -m pylint --output-format=text tests || $(pwd)/meta/pylint-exit-handler.sh $? ) | tee pylint.txt
    pylint_scorehandler 'unittests' pylint.txt $1 $2
    return $?
}

# pytest
function pytest_unittestscov() {
    # $1: summary variable
    trap '[ -p waitpipe.fifo ] && rm waitpipe.fifo' RETURN SIGINT
    mkfifo waitpipe.fifo
    echo -e "\e[1m\e[93mStage: Unit Tests - Job: pytest (unittests+cov)\e[0m"
    echo -e "\e[96mRunning Pytest for unit testing...\e[0m"
    regex="s:^TOTAL.+?(\d+\%)$:\1:p"
    python -m pytest -v --color=yes --cov-report=term --cov-report=html --cov=polyinspect -n auto tests/ |\
        tee >(\
                cov=$(sed -rn "s:^TOTAL.+? ([0-9]+)\%$:\1:p");\
                if [ -z $cov ]; then
                    echo -e "\e[1;91m✘ pytest-cov result not found.\e[0m"
                    echo -e "\e[93mPlease check that the pytest-cov plugin is installed.\e[0m"
                    covexit=1
                    echo $covexit > waitpipe.fifo
                    exit $covexit
                fi
                if [ $cov -lt 99 ]; then
                    echo -e "\e[1;91m✘ pytest-cov failed. Incomplete coverage of ${cov}%. A minimum coverage of 99% is required.\e[0m"
                    echo -e "\e[93mPlease check the coverage HTML report at htmlcov/index.html."
                    echo -e "\e[93mStatements that lack test calls are highlighted in \e[91mred\e[93m."
                    echo -e "Please add missing test calls at once.\e[0m"
                    covexit=1
                    echo $covexit > waitpipe.fifo
                    exit $covexit
                else
                    echo -e "\e[1;92m✔ pytest-cov passed. Sufficient coverage of ${cov}%.\e[0m"
                    covexit=0
                    echo $covexit > waitpipe.fifo
                fi\
            );

    retval=("${PIPESTATUS[@]}")
    if [ ${retval[0]} = 0 ]; then
        echo
        echo -e "\e[1;92m✔ pytest-unittests passed.\e[0m"
        [ -n "$1" ] && extend_summary $1 "\e[92m✔ pytest-unittests passed\e[0m"
        unittestsexit=0
    else
        echo
        echo -e "\e[1;91m✘ pytest-unittests failed. At least one of the tests did not succeed.\e[0m"
        echo -e "\e[93mPlease review the code and ensure that all tests pass.\e[0m"
        [ -n "$1" ] && extend_summary $1 "\e[91m✘ pytest-unittests failed\e[0m"
        echo
        unittestsexit=1
    fi

    [ -p waitpipe.fifo ] && read covexit < waitpipe.fifo
    [ $covexit = 1 ] && [ -n "$1" ] && extend_summary $1 "\e[91m✘ pytest-cov failed\e[0m"
    [ $covexit = 0 ] && [ -n "$1" ] && extend_summary $1 "\e[92m✔ pytest-cov passed\e[0m"
    [ $covexit = 0 -a $unittestsexit = 0 ]
    return $?
}
