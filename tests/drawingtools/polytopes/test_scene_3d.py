""":mod:`.test_scenes_3d` contains unit tests for classes implemented
in :mod:`src.drawingtools.polytopes.scenes_3d`.
"""

import os
import numpy as np  # type: ignore

from polyinspect.auxiliary.geometrical_space import Point, Vector
from polyinspect.auxiliary.bounded_affine_space import Segment, Contour, Face,\
        Polyhedron, VertexCollection
from polyinspect.drawingtools.polytopes.scenes_3d import Scene,\
        write_scene_to_tikz


# pylint: disable=C0103
# C0103: Variable names follow geometrical conventions.


def test_scene_cuboid_prism() -> None:
    r"""Tests for the basic construction of scenes in :math:`\mathbb{R}^3` and
    operations on them.

    We will construct a cuboid and a prism.
    """
    # pylint: disable=R0914
    # R0914: Having more local variables enhances code readability.
    A = Point(n=3)
    B = Point([2, 0, 0])
    C = Point([2, 1, 0])
    D = Point([0, 1, 0])
    E = Point([0, 0, 3])
    F = Point([2, 0, 3])
    G = Point([2, 1, 3])
    H = Point([0, 1, 3])
    face1 = Face([Contour.from_vertices([A, B, C, D])])
    face2 = Face([Contour.from_vertices([A, D, H, E])])
    face3 = Face([Contour.from_vertices([B, C, G, F])])
    face4 = Face([Contour.from_vertices([A, B, F, E])])
    face5 = Face([Contour.from_vertices([C, D, H, G])])
    face6 = Face([Contour.from_vertices([E, F, G, H])])
    face7 = Face([Contour.from_vertices([A, D, G, F])])
    face8 = Face([Contour.from_vertices([C, D, G])])
    face9 = Face([Contour.from_vertices([A, B, F])])
    cuboid = Polyhedron([face1, face2, face3, face4, face5, face6])
    prism = Polyhedron([face1, face3, face7, face8, face9])
    scene = Scene([cuboid])
    zcoord = 0.7639320225002106
    cuboid_hidden_segments =\
            sorted([Segment([A, Point([1.5, 0, 0])]),
                    Segment([Point([1.5, 0, 0]), B]),
                    Segment([A, Point([0, 0, zcoord])]),
                    Segment([Point([0, 0, zcoord]), E]),
                    Segment([A, D])])
    cuboid_visible_segments =\
            sorted([Segment([B, C]), Segment([B, F]), Segment([C, D]),
                    Segment([D, H]), Segment([C, G]), Segment([E, F]),
                    Segment([E, H]), Segment([F, G]), Segment([G, H])])
    assert scene.hidden_segments[0] == cuboid_hidden_segments
    assert scene.visible_segments[0] == cuboid_visible_segments
    assert scene.visible_vertices[0] ==\
            sorted([B, C, D, E, F, G, H])
    assert scene.hidden_vertices[0] == [A]

    scene.objects = [cuboid, prism]
    prism_visible_segments =\
            sorted([Segment([B, C]), Segment([B, F]), Segment([C, D]),
                    Segment([C, G]), Segment([D, G]), Segment([F, G])])
    assert scene.visible_segments[0] == cuboid_visible_segments
    assert scene.visible_segments[1] == prism_visible_segments

    assert scene.all_edges ==\
            sorted([Segment([A, B]), Segment([A, D]), Segment([A, E]),
                    Segment([A, F]), Segment([B, C]), Segment([B, F]),
                    Segment([C, D]), Segment([C, G]), Segment([D, G]),
                    Segment([D, H]), Segment([E, F]), Segment([E, H]),
                    Segment([F, G]), Segment([G, H])])
    assert Scene([]).all_edges == []

    scene.cabinet_angle = 4.5*np.pi
    assert scene.cabinet_angle == 0.5*np.pi
    scene.cabinet_angle = -4.5*np.pi
    assert scene.cabinet_angle == 1.5*np.pi
    assert scene.scene_axes == (Vector([1, 0, 0]),
                                Vector([0, 1, 0]),
                                Vector([0, 0, 1]))


def test_scene_cuboid_octahedron() -> None:
    r"""Tests for the basic construction of scenes in :math:`\mathbb{R}^3` and
    operations on them.
    """
    # pylint: disable=R0914
    # R0914: Having more local variables enhances code readability.

    # ----------------------------------------
    # A regular octahedron inscribed in a cube
    # ----------------------------------------

    A = Point(n=3)
    B = Point([2, 0, 0])
    C = Point([2, 2, 0])
    D = Point([0, 2, 0])
    E = Point([0, 0, 2])
    F = Point([2, 0, 2])
    G = Point([2, 2, 2])
    H = Point([0, 2, 2])
    cube = Polyhedron.convex_hull_from_vertices([A, B, C, D, E, F, G, H])

    I = Point([1, 1, 0])
    J = Point([1, 1, 2])
    K = Point([1, 0, 1])
    L = Point([1, 2, 1])
    M = Point([0, 1, 1])
    N = Point([2, 1, 1])
    octahedron = Polyhedron.convex_hull_from_vertices([I, J, K, L, M, N])

    scene2 = Scene([cube, octahedron])
    assert scene2.visible_segments[1] == []
    assert scene2.visible_vertices[1] == sorted([L, J, N])

    scene2.centre_objects()
    scene2.rotate_objects(np.pi/2, Vector([0, 1, 0]))
    Jd = Point([1, 0, 0])
    Md = Point([0, 0, 1])
    Ld = Point([0, 1, 0])
    assert scene2.visible_segments[1] == []
    assert scene2.visible_vertices[1] == sorted([Ld, Jd, Md])

    # Enlarge the cube to hide all octahedron's vertices
    Ad = 1.1*A
    Bd = 1.1*B
    Cd = 1.1*C
    Dd = 1.1*D
    Ed = 1.1*E
    Fd = 1.1*F
    Gd = 1.1*G
    Hd = 1.1*H
    cube = Polyhedron.convex_hull_from_vertices([Ad, Bd, Cd, Dd,
                                                 Ed, Fd, Gd, Hd])
    scene2.objects = [cube, octahedron]
    assert scene2.visible_vertices[1] == []

    # Move the cube behind
    cube.translate_ip(Vector([0, 0, -5]))
    scene2.refresh()
    Id = Point([-1, 0, 0])
    Kd = Point([0, -1, 0])
    assert scene2.visible_vertices[1] == sorted([Ld, Jd, Md, Id, Kd])

    # Add a faceless vertex collection
    cube_vc = VertexCollection([A, B, C, D, E, F, G, H], cutoff=2.1)
    scene2.objects = [cube, octahedron, cube_vc]
    assert scene2.visible_vertices[1] == sorted([Ld, Jd, Md, Id, Kd])

    assert scene2.polyhedra == [cube, octahedron]
    assert scene2.vertexcollections == [cube_vc]


def test_tikz_writer() -> None:
    r"""Tests for the TikZ writer to export a scene to TikZ format.
    """
    # pylint: disable=R0914
    # R0914: Having more local variables enhances code readability.

    # ----------------------------------------
    # A regular octahedron inscribed in a cube
    # ----------------------------------------

    A = Point(n=3)
    B = Point([2, 0, 0])
    C = Point([2, 2, 0])
    D = Point([0, 2, 0])
    E = Point([0, 0, 2])
    F = Point([2, 0, 2])
    G = Point([2, 2, 2])
    H = Point([0, 2, 2])
    cube = Polyhedron.convex_hull_from_vertices([A, B, C, D, E, F, G, H])

    I = Point([1, 1, 0])
    J = Point([1, 1, 2])
    K = Point([1, 0, 1])
    L = Point([1, 2, 1])
    M = Point([0, 1, 1])
    N = Point([2, 1, 1])
    octahedron = Polyhedron.convex_hull_from_vertices([I, J, K, L, M, N])
    scene3 = Scene([cube, octahedron])

    scene3_output = os.path.join(os.path.dirname(__file__), "scene3.tikz.tex")
    scene3_ref = os.path.join(os.path.dirname(__file__), "ref/scene3.tikz.tex")
    write_scene_to_tikz(scene3, scene3_output, bbox=[(-1, 3), (-1, 3)])
    with open(scene3_output, 'r') as fout, open(scene3_ref, 'r') as fref:
        diff = set(fout).symmetric_difference(fref)
    diff.discard('\n')
    try:
        assert len(diff) == 0 or len(diff) == 2
        if len(diff) == 2:
            assert all("0.0000000000000" in line for line in diff)
    finally:
        print(diff)
        os.remove(scene3_output)
