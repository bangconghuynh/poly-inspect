""":mod:`.test_state_distances` contains unit tests for functions implemented in
:mod:`analyticaltools.topology.state_distances`.
"""

import os

import numpy as np  # type: ignore
from pyscf import gto  # type: ignore
import pytest  # type: ignore

from polyinspect.auxiliary.geometrical_space import Point
from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE,\
        LOOSE_ZERO_TOLERANCE
from polyinspect.analyticaltools.topology.state_distances import _expand,\
        intersect_spheres, edmsph, density_sqdist_mat
from polyinspect.analyticaltools.wavefunctions.density1p import noci_density,\
        scf_density


def test_state_distances_helper_funcs() -> None:
    r"""Tests for helper functions in
    :mod:`analyticaltools.topology.state_distances`.
    """
    point1 = Point([1, 2, 3])
    point2 = Point([4, 5, 6])
    point3 = Point([7, 8, 9])
    assert point1.dim == 3
    assert point2[-1] == 6

    _expand([point1, point2, point3])
    assert point1.dim == 4
    assert point2[3] == 0.0
    assert point2[-1] == 0.0



def test_intersect_spheres_K2() -> None:
    r"""Tests for the solver for the solutions of sphere interesection
    in an arbitrary dimension.

    We construct cases for :math:`K = 2`.
    """
    # pylint: disable=C0103
    # C0103: Variable and function names are chosen to resemble their
    #        mathematical equivalents.

    # ---------------------------------------------------
    # Two intersecting circles in two dimensions.
    # K = 2, dim(aff(centres)) = 1
    # The intersection points lie in the K = 2 dimension.
    # ---------------------------------------------------
    sph0 = (Point([0]), 3)
    sph1 = (Point([5]), 3)
    intersections = intersect_spheres([sph0, sph1], thresh=1e-6)
    assert len(intersections) == 2
    assert intersections[0].is_same_as(Point([2.5, np.sqrt(11)/2]),
                                       COMMON_ZERO_TOLERANCE)
    assert intersections[1].is_same_as(Point([2.5, -np.sqrt(11)/2]),
                                       COMMON_ZERO_TOLERANCE)

    sph2 = (Point([0]), np.pi)
    sph3 = (Point([5]), np.pi)
    intersections = intersect_spheres([sph2, sph3], thresh=1e-6)
    assert len(intersections) == 2
    assert intersections[0].is_same_as(Point([2.5, np.sqrt(np.pi**2-2.5**2)]),
                                       COMMON_ZERO_TOLERANCE)
    assert intersections[1].is_same_as(Point([2.5, -np.sqrt(np.pi**2-2.5**2)]),
                                       COMMON_ZERO_TOLERANCE)


    # ---------------------------------------------------
    # Two osculating circles in two dimensions.
    # K = 2, dim(aff(centres)) = 1
    # The intersection point lies in the K = 1 dimension.
    # ---------------------------------------------------
    sph4 = (Point([0]), 2.5)
    sph5 = (Point([5]), 2.5)
    intersections = intersect_spheres([sph4, sph5])
    assert len(intersections) == 1
    assert intersections[0].is_same_as(Point([2.5]), COMMON_ZERO_TOLERANCE)


def test_intersect_spheres_K3_normal() -> None:
    r"""Tests for the solver for the normal solutions of sphere interesection
    in an arbitrary dimension.

    We construct cases for :math:`K = 3`.
    """
    # pylint: disable=C0103,R0914
    # C0103: Variable and function names are chosen to resemble their
    #        mathematical equivalents.

    # ---------------------------------------------------
    # Three intersecting spheres in two dimensions.
    # K = 3, dim(aff(centres)) = 2
    # The intersection point lies in the K = 3 dimension.
    # ---------------------------------------------------
    # Non-regular tetrahedron
    sph1 = (Point([0, 0]), 5)
    sph2 = (Point([8, 0]), 5)
    sph3 = (Point([4, 4]), 2)
    intersections = intersect_spheres([sph1, sph2, sph3])
    coords = [4.0, 21/8, np.sqrt(9-(21/8)**2)]
    assert len(intersections) == 2
    assert intersections[0].is_same_as(Point(coords),
                                       COMMON_ZERO_TOLERANCE)
    coords[2] = -coords[2]
    assert intersections[1].is_same_as(Point(coords),
                                       COMMON_ZERO_TOLERANCE)

    # Regular tetrahedron
    a = 8.56182
    sph1d = (Point([0, 0]), a)
    sph2d = (Point([a, 0]), a)
    sph3d = (Point([0.5*a, a*np.sqrt(3)/2.0]), a)
    intersections = intersect_spheres([sph1d, sph2d, sph3d])
    coords = [a/2, a*np.sqrt(3)/6, a*np.sqrt(2/3)]
    assert len(intersections) == 2
    assert intersections[0].is_same_as(Point(coords),
                                       COMMON_ZERO_TOLERANCE)
    coords[2] = -coords[2]
    assert intersections[1].is_same_as(Point(coords),
                                       COMMON_ZERO_TOLERANCE)


    # -----------------------------------------------------
    # Three intersecting circles/spheres in two dimensions.
    # K = 3, dim(aff(centres)) = 2
    # The intersection point lies in the K = 2 dimension.
    # -----------------------------------------------------
    sph4 = (Point([0, 0]), 3)
    sph5 = (Point([5, 0]), 3)
    sph6 = (Point([2.5, 4+np.sqrt(11)/2]), 4)
    intersections = intersect_spheres([sph4, sph5, sph6], thresh=1e-8)
    assert len(intersections) == 1
    assert intersections[0].is_same_as(Point([2.5, np.sqrt(11)/2]),
                                       COMMON_ZERO_TOLERANCE)

    sph4d = (Point([0, 0]), 3)
    sph5d = (Point([5, 0]), 3)
    sph6d = (Point([2.5, 3+np.sqrt(11)/2]), 3)
    intersections = intersect_spheres([sph4d, sph5d, sph6d], thresh=1e-8)
    assert len(intersections) == 1
    assert intersections[0].is_same_as(Point([2.5, np.sqrt(11)/2]),
                                       COMMON_ZERO_TOLERANCE)


    # ---------------------------------------------------
    # Three intersecting circles in two dimensions.
    # K = 3, dim(aff(centres)) = 1
    # The intersection point lies in the K = 2 dimension.
    # ---------------------------------------------------
    sph7 = (Point([0]), 3)
    sph8 = (Point([5]), 3)
    sph9 = (Point([2.5]), np.sqrt(11)/2)
    intersections = intersect_spheres([sph7, sph8, sph9])
    assert len(intersections) == 2
    assert intersections[0].is_same_as(Point([2.5, np.sqrt(11)/2]),
                                       COMMON_ZERO_TOLERANCE)
    assert intersections[1].is_same_as(Point([2.5, -np.sqrt(11)/2]),
                                       COMMON_ZERO_TOLERANCE)


def test_intersect_spheres_K3_special() -> None:
    r"""Tests for the solver for the special solutions of sphere interesection
    in an arbitrary dimension.

    We construct cases for :math:`K = 3`.
    """
    # pylint: disable=C0103
    # C0103: Variable and function names are chosen to resemble their
    #        mathematical equivalents.

    # ---------------------------------------------------------
    # Three non-intersecting circles/spheres in two dimensions.
    # K = 3, dim(aff(centres)) = 2
    # ---------------------------------------------------------
    sph1 = (Point([0, 0]), 3)
    sph2 = (Point([5, 0]), 3)
    sph3 = (Point([2.5, 3+np.sqrt(11)/2]), 2.9)
    intersections = intersect_spheres([sph1, sph2, sph3])
    assert len(intersections) == 0


    # -------------------------------------------------
    # Three non-intersecting circles in two dimensions.
    # K = 3, dim(aff(centres)) = 1
    # -------------------------------------------------
    sph4 = (Point([0]), 3)
    sph5 = (Point([5]), 3)
    sph6 = (Point([2.5]), 0.1+np.sqrt(11)/2)
    intersections = intersect_spheres([sph4, sph5, sph6])
    assert len(intersections) == 0


    # ---------------------------------------------------
    # Three osculating circles in two dimensions.
    # K = 3, dim(aff(centres)) = 1
    # The intersection point lies in the K = 1 dimension.
    # ---------------------------------------------------
    sph7 = (Point([0]), 3)
    sph8 = (Point([6]), 3)
    sph9 = (Point([4.5]), 1.5)
    intersections = intersect_spheres([sph7, sph8, sph9])
    assert len(intersections) == 1
    assert intersections[0].is_same_as(Point([3]),
                                       COMMON_ZERO_TOLERANCE)


def test_edmsph_K3() -> None:
    r"""Tests for the realisation of Euclidean distance matrices.

    We construct cases for :math:`K = 3`.
    """
    # pylint: disable=C0103
    # C0103: Variable and function names are chosen to resemble their
    #        mathematical equivalents.

    # ------------------------------------------------------------------
    # K = 3
    # Three points in a two-dimensional plane forming a regular triangle
    # ------------------------------------------------------------------
    a = 2.4
    edm = np.array([[0, a, a],
                    [a, 0, a],
                    [a, a, 0]])
    vertices, K = edmsph(edm, thresh=1e-14)
    assert K == 2
    assert len(vertices) == 3
    assert vertices[0] == Point([0, 0])
    assert vertices[1] == Point([a, 0])
    assert vertices[2] == Point([a/2, a*np.sqrt(3)/2])


    # --------------------------------------------------------------------
    # K = 3
    # Three points in a two-dimensional plane forming an isoceles triangle
    # --------------------------------------------------------------------
    edm = np.array([[0, 4.5, 4.5],
                    [4.5, 0, 2.0],
                    [4.5, 2.0, 0]])
    vertices, K = edmsph(edm, thresh=1e-14)
    assert K == 2
    assert len(vertices) == 3
    assert vertices[0] == Point([0, 0])
    assert vertices[1] == Point([4.5, 0])
    x = 0.5*(4.5 + (2.5*6.5/4.5))
    y = np.sqrt(4.5**2 - x**2)
    assert vertices[2] == Point([x, y])


    # ----------------------
    # K = 3
    # Three points on a line
    # ----------------------
    edm = np.array([[0, 2.1, 5.4],
                    [2.1, 0, 3.3],
                    [5.4, 3.3, 0]])
    vertices, K = edmsph(edm)
    assert K == 1
    assert len(vertices) == 3
    assert vertices[0] == Point([0])
    assert vertices[1] == Point([2.1])
    assert vertices[2] == Point([5.4])


def test_edmsph_K4() -> None:
    r"""Tests for the realisation of Euclidean distance matrices.

    We construct cases for :math:`K = 4`.
    """
    # pylint: disable=C0103,R0915
    # C0103: Variable and function names are chosen to resemble their
    #        mathematical equivalents.

    # --------------------------------------------------------------
    # K = 4
    # Four points forming a regular tetrahedron in three dimensions.
    # --------------------------------------------------------------
    a = 2.4
    edm = np.array([[0, a, a, a],
                    [a, 0, a, a],
                    [a, a, 0, a],
                    [a, a, a, 0]])
    vertices, K = edmsph(edm, thresh=1e-14)
    assert K == 3
    assert len(vertices) == 4
    assert vertices[0] == Point([0, 0, 0])
    assert vertices[1] == Point([a, 0, 0])
    assert vertices[2] == Point([a/2, a*np.sqrt(3)/2, 0])
    assert vertices[3] == Point([a/2, a*np.sqrt(3)/6, a*np.sqrt(2/3)])


    # --------------------------------------------------------------
    # K = 4
    # Four points forming a general tetrahedron in three dimensions.
    # A(0,0,0); B(4.40,0,0); C(1.94,4.80,0); D(-1.04,4.26,2)
    # --------------------------------------------------------------
    AB = 4.40
    AC = 5.177219331
    AD = 4.819668038
    BC = 5.393662948
    BD = 7.193135617
    CD = 3.629325006
    edm = np.array([[0, AB, AC, AD],
                    [AB, 0, BC, BD],
                    [AC, BC, 0, CD],
                    [AD, BD, CD, 0]])
    vertices, K = edmsph(edm, thresh=1e-14)
    assert K == 3
    assert len(vertices) == 4
    # The higher the dimension, the larger the error.
    assert vertices[0].is_same_as(Point([0, 0, 0]), thresh=1e-14)
    assert vertices[1].is_same_as(Point([4.40, 0, 0]), thresh=1e-14)
    assert vertices[2].is_same_as(Point([1.94, 4.80, 0]), thresh=1e-9)
    assert vertices[3].is_same_as(Point([-1.04, 4.26, 2.00]), thresh=1e-8)


    # --------------------------------------------------
    # K = 4
    # Four points forming a rectangle in two dimensions.
    # --------------------------------------------------
    a = 2.4
    b = 3.5
    d = np.sqrt(a**2 + b**2)
    edm = np.array([[0, b, d, a],
                    [b, 0, a, d],
                    [d, a, 0, b],
                    [a, d, b, 0]])
    vertices, K = edmsph(edm)
    assert K == 2
    assert len(vertices) == 4
    assert vertices[0] == Point([0, 0])
    assert vertices[1] == Point([b, 0])
    assert vertices[2] == Point([b, a])
    assert vertices[3] == Point([0, a])

    # Reduce the threshold
    # The K=3 component for i=3 is real and larger than thresh.
    vertices, K = edmsph(edm, thresh=1e-8)
    assert K == 3
    assert len(vertices) == 4
    assert vertices[0].is_same_as(Point([0, 0, 0]), thresh=1e-7)
    assert vertices[1].is_same_as(Point([b, 0, 0]), thresh=1e-7)
    assert vertices[2].is_same_as(Point([b, a, 0]), thresh=1e-7)
    assert vertices[3].is_same_as(Point([0, a, 0]), thresh=1e-7)

    a = 2.4
    d = a*np.sqrt(2)
    edm = np.array([[0, a, d, a],
                    [a, 0, a, d],
                    [d, a, 0, a],
                    [a, d, a, 0]])
    vertices, K = edmsph(edm)
    assert K == 2
    assert len(vertices) == 4
    assert vertices[0] == Point([0, 0])
    assert vertices[1] == Point([a, 0])
    assert vertices[2] == Point([a, a])
    assert vertices[3] == Point([0, a])

    # Reduce the threshold
    # The K=3 component for i=3 is complex and larger than thresh.
    # This therefore counts as "no solutions" and and AssertionError is raised.
    with pytest.raises(AssertionError):
        vertices, K = edmsph(edm, thresh=1e-8)


    # --------------------------------------------------------------
    # K = 4
    # Four points forming a general quadrilateral in two dimensions.
    # A(0,0); B(5,0); C(6,2); D(-1,4)
    # --------------------------------------------------------------
    AB = 5
    AC = 6.3245553203368
    AD = 4.1231056256177
    BC = 2.2360679774998
    BD = 7.2111025509280
    CD = 7.2801098892805
    edm = np.array([[0, AB, AC, AD],
                    [AB, 0, BC, BD],
                    [AC, BC, 0, CD],
                    [AD, BD, CD, 0]])
    vertices, K = edmsph(edm, tol=1e-12, thresh=1e-5)
    assert K == 2
    assert len(vertices) == 4
    assert vertices[0].is_same_as(Point([0, 0]),
                                  thresh=COMMON_ZERO_TOLERANCE)
    assert vertices[1].is_same_as(Point([5, 0]),
                                  thresh=COMMON_ZERO_TOLERANCE)
    assert vertices[2].is_same_as(Point([6, 2]),
                                  thresh=1e1*COMMON_ZERO_TOLERANCE)
    assert vertices[3].is_same_as(Point([-1, 4]),
                                  thresh=1e2*COMMON_ZERO_TOLERANCE)


def test_edmsph_K5() -> None:
    r"""Tests for the realisation of Euclidean distance matrices.

    We construct cases for :math:`K = 5`.
    """
    # pylint: disable=C0103
    # C0103: Variable and function names are chosen to resemble their
    #        mathematical equivalents.

    # --------------------------------------------------------
    # K = 5
    # Five points forming a regular 5-cell in four dimensions.
    # --------------------------------------------------------
    a = 2.3
    edm = np.array([[0, a, a, a, a],
                    [a, 0, a, a, a],
                    [a, a, 0, a, a],
                    [a, a, a, 0, a],
                    [a, a, a, a, 0]])
    vertices, K = edmsph(edm, thresh=1e-14)
    assert K == 4
    assert len(vertices) == 5
    assert vertices[0] == Point([0, 0, 0, 0])
    assert vertices[1] == Point([a, 0, 0, 0])
    assert vertices[2] == Point([a/2, a*np.sqrt(3)/2, 0, 0])
    assert vertices[3] == Point([a/2, a*np.sqrt(3)/6, a*np.sqrt(2/3), 0])
    assert vertices[4] == Point([a/2,
                                 a*np.sqrt(3)/6,
                                 a*np.sqrt(2/3)/4,
                                 a*np.sqrt(5/8)])


    # ---------------------------------------------------------------------
    # K = 5
    # Five points forming a regular trigonal bipyramid in three dimensions.
    # ---------------------------------------------------------------------
    a = 2.3
    d = 2*a*np.sqrt(2/3)
    edm = np.array([[0, a, a, a, a],
                    [a, 0, a, a, a],
                    [a, a, 0, a, a],
                    [a, a, a, 0, d],
                    [a, a, a, d, 0]])
    vertices, K = edmsph(edm)
    assert K == 3
    assert len(vertices) == 5
    assert vertices[0] == Point([0, 0, 0])
    assert vertices[1] == Point([a, 0, 0])
    assert vertices[2] == Point([a/2, a*np.sqrt(3)/2, 0])
    assert vertices[3] == Point([a/2, a*np.sqrt(3)/6, a*np.sqrt(2/3)])
    assert vertices[4] == Point([a/2, a*np.sqrt(3)/6, -a*np.sqrt(2/3)])


    # ---------------------------------------
    # K = 5
    # Five points forming a methane molecule.
    # ---------------------------------------
    a = 1.10940  # H-H distance
    b = a*np.sqrt(3/8)  # C-H distance
    edm = np.array([[0, a, a, a, b],
                    [a, 0, a, a, b],
                    [a, a, 0, a, b],
                    [a, a, a, 0, b],
                    [b, b, b, b, 0]])
    vertices, K = edmsph(edm)
    assert K == 3
    assert len(vertices) == 5
    assert vertices[0] == Point([0, 0, 0])
    assert vertices[1] == Point([a, 0, 0])
    assert vertices[2] == Point([a/2, a*np.sqrt(3)/2, 0])
    assert vertices[3] == Point([a/2, a*np.sqrt(3)/6, a*np.sqrt(2/3)])
    assert vertices[4] == Point([a/2, a*np.sqrt(3)/6, a*np.sqrt(2/3)/4])


    # ---------------------------------------------------------
    # K = 5
    # Five points forming a regular pentagon in two dimensions.
    # ---------------------------------------------------------
    a = 4
    b = np.sqrt(2*a**2*(1+np.cos(2*np.pi/5)))
    edm = np.array([[0, a, b, b, a],
                    [a, 0, a, b, b],
                    [b, a, 0, a, b],
                    [b, b, a, 0, a],
                    [a, b, b, a, 0]])
    vertices, K = edmsph(edm)
    assert K == 2
    assert len(vertices) == 5
    assert vertices[0] == Point([0, 0])
    assert vertices[1] == Point([a, 0])
    assert vertices[2] == Point([a*(1+np.cos(2*np.pi/5)),
                                 a*(np.sin(2*np.pi/5))])
    assert vertices[3] == Point([a/2, a*(np.sin(2*np.pi/5)+np.sin(np.pi/5))])
    assert vertices[4] == Point([-a*np.cos(2*np.pi/5),
                                 a*(np.sin(2*np.pi/5))])


def test_edmsph_K6() -> None:
    r"""Tests for the realisation of Euclidean distance matrices.

    We construct cases for :math:`K = 6`.
    """
    # pylint: disable=C0103
    # C0103: Variable and function names are chosen to resemble their
    #        mathematical equivalents.

    # -----------------------------------------------------------
    # K = 6
    # Five points forming a regular hexateron in five dimensions.
    # -----------------------------------------------------------
    a = 4.22301
    edm = np.array([[0, a, a, a, a, a],
                    [a, 0, a, a, a, a],
                    [a, a, 0, a, a, a],
                    [a, a, a, 0, a, a],
                    [a, a, a, a, 0, a],
                    [a, a, a, a, a, 0]])
    vertices, K = edmsph(edm, thresh=1e-14)
    assert K == 5
    assert len(vertices) == 6
    assert vertices[0] == Point([0, 0, 0, 0, 0])
    assert vertices[1] == Point([a, 0, 0, 0, 0])
    assert vertices[2] == Point([a/2, a*np.sqrt(3)/2, 0, 0, 0])
    assert vertices[3] == Point([a/2, a*np.sqrt(3)/6, a*np.sqrt(2/3), 0, 0])
    assert vertices[4] == Point([a/2,
                                 a*np.sqrt(3)/6,
                                 a*np.sqrt(2/3)/4,
                                 a*np.sqrt(5/8),
                                 0])
    assert vertices[5] == Point([a/2,
                                 a*np.sqrt(3)/6,
                                 a*np.sqrt(2/3)/4,
                                 a*np.sqrt(5/8)/5,
                                 a*np.sqrt(3/5)])


    # ------------------------------------------------------------
    # K = 6
    # Six points forming a regular octahedron in three dimensions.
    # ------------------------------------------------------------
    a = 2.34325
    d = 2*a/np.sqrt(2)
    edm = np.array([[0, a, d, a, a, a],
                    [a, 0, a, d, a, a],
                    [d, a, 0, a, a, a],
                    [a, d, a, 0, a, a],
                    [a, a, a, a, 0, d],
                    [a, a, a, a, d, 0]])
    vertices, K = edmsph(edm)
    assert K == 3
    assert len(vertices) == 6
    assert vertices[0] == Point([0, 0, 0])
    assert vertices[1] == Point([a, 0, 0])
    assert vertices[2] == Point([a, a, 0])
    assert vertices[3] == Point([0, a, 0])
    assert vertices[4] == Point([a/2, a/2, d/2])
    assert vertices[5] == Point([a/2, a/2, -d/2])


    # -------------------------------------------------------
    # K = 6
    # Six points forming a regular hexagon in two dimensions.
    # -------------------------------------------------------
    a = 0.98765
    d = a*np.sqrt(3)
    e = 2*a
    edm = np.array([[0, a, d, e, d, a],
                    [a, 0, a, d, e, d],
                    [d, a, 0, a, d, e],
                    [e, d, a, 0, a, d],
                    [d, e, d, a, 0, a],
                    [a, d, e, d, a, 0]])
    vertices, K = edmsph(edm)
    assert K == 2
    assert len(vertices) == 6
    assert vertices[0] == Point([0, 0])
    assert vertices[1] == Point([a, 0])
    assert vertices[2] == Point([3/2*a, a*np.sqrt(3)/2])
    assert vertices[3] == Point([a, a*np.sqrt(3)])
    assert vertices[4] == Point([0, a*np.sqrt(3)])
    assert vertices[5] == Point([-a/2, a*np.sqrt(3)/2])


def test_state_distances_real_H4() -> None:
    r"""Tests for the calculations of state distances between SCF and NOCI
    states.

    We take MS=0 UHF STO-3G determinants in H4 at a = 1.100 Å and b = 1.100 Å.

    The four SCF determinants below were found by metadynamics in Q-Chem 5.2.
    The NOCI results were also obtained from Q-Chem 5.2.

      1 b 2
      H───H
     a│   │
      H───H
      4   3
    """
    # pylint: disable=C0103,R0914,R0915
    # C0103: Variable names are chosen to resemble their mathematical
    #        symbols.
    # R0914, R0915: Having more local variables enhances code readability.
    aHH = 1.100
    bHH = 1.100
    mol = gto.M(atom=(f"H {-bHH/2.0} {aHH} 0;"
                      f"H {+bHH/2.0} {aHH} 0;"
                      f"H {+bHH/2.0} 0     0;"
                      f"H {-bHH/2.0} 0     0;"),
                charge=0,
                spin=0,
                basis='sto3g',
                unit='Angstrom')
    sao = mol.intor('int1e_ovlp', aosym='1')

    coeffs = []
    dens = []
    for sol in range(4):
        ca = np.loadtxt(os.path.join(os.path.dirname(__file__),
                                     f"../wavefunctions/H4/state{sol}.alpha"))
        cb = np.loadtxt(os.path.join(os.path.dirname(__file__),
                                     f"../wavefunctions/H4/state{sol}.beta"))
        coeffs.append([ca, cb])
        dens.append(scf_density([ca, cb], complexsymmetric=False))

    scf_sqdistmat = density_sqdist_mat(dens, sao, frobenius=True)
    # Consistency test only
    assert np.allclose(scf_sqdistmat,
                       np.array([[0.00000000, 2.33790524, 2.33790524, 5.33879676],
                                 [2.33790524, 0.00000000, 4.00000000, 2.33790524],
                                 [2.33790524, 4.00000000, 0.00000000, 2.33790524],
                                 [5.33879676, 2.33790524, 2.33790524, 0.00000000]]),
                       rtol=0, atol=LOOSE_ZERO_TOLERANCE)

    noci_cs = [[+0.6674623, -0.0407322, -0.0407322, -0.6674623],
               [+0.7071068, +0.0000000, +0.0000000, +0.7071068],
               [+0.0000000, +0.7071068, -0.7071068, -0.0000000],
               [+2.9174953, +2.9925951, +2.9925951, -2.9174953]]

    nocidens = [noci_density(coeffs, noci_c, sao,
                             complexsymmetric=False, thresh_zeroov=1e-20)
                for noci_c in noci_cs]

    noci_sqdistmat = density_sqdist_mat(nocidens, sao, frobenius=True)
    # Consistency test only
    assert np.allclose(noci_sqdistmat,
                       np.array([[0.000000000000, 3.80835697e-05, 2.54528468e-03, 8.34403952e-01],
                                 [3.80835697e-05, 0.000000000000, 3.20605124e-03, 8.23167803e-01],
                                 [2.54528468e-03, 3.20605124e-03, 0.000000000000, 9.29118551e-01],
                                 [8.34403952e-01, 8.23167803e-01, 9.29118551e-01, 0.000000000000]]),
                       rtol=0, atol=LOOSE_ZERO_TOLERANCE)
