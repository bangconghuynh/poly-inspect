""":mod:`.test_nonorthowavefunctions` contains unit tests for functions
implemented in :mod:`analyticaltools.wavefunctions.nonorthowavefunctions`.
"""

import os
import numpy as np  # type: ignore
from pyscf import gto, scf  # type: ignore
from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE
from polyinspect.analyticaltools.wavefunctions.nonorthowavefunctions import det_ov,\
        get_Smat, lowdin_pairing


def test_det_ov_real_H42p() -> None:
    r"""Tests for the calculations of overlap between determinants.

    We take MS=0 sigma/dashdash determinants in [H4]2+ at a = 1.100 Å and
    b = 1.540 Å. This is a very simple system, so test rigour might not be
    ideal, but this suffices for now and will be expanded later.

      1   b   2
      H───────H
     a│       │
      H───────H
      4       3
    """
    # pylint: disable=C0103,R0914
    # C0103: Variable names are chosen in accordance with their mathematical
    #        symbols.
    # R0914: Having more local variables helps enhance code readability.
    a = 1.100
    b = 1.540
    mol = gto.M(atom=(f"H {-b/2.0} {a} 0;"
                      f"H {+b/2.0} {a} 0;"
                      f"H {+b/2.0} 0   0;"
                      f"H {-b/2.0} 0   0;"),
                charge=2,
                spin=0,
                basis='sto3g',
                unit='Angstrom')
    sao = mol.intor('int1e_ovlp', aosym='1')

    css = []
    for sol in ["1a", "1b", "1c", "1d", "2a", "2b"]:
        ca = np.genfromtxt(os.path.join(os.path.dirname(__file__),
                                        f"H42p/a1.100_b1.540/{sol}.alpha"),
                           dtype=np.complex128).reshape((-1, 1))
        cb = np.genfromtxt(os.path.join(os.path.dirname(__file__),
                                        f"H42p/a1.100_b1.540/{sol}.beta"),
                           dtype=np.complex128).reshape((-1, 1))
        css.append([ca, cb])

    # Normalisation
    for cs in css:
        assert abs(det_ov(cs, cs, sao, complexsymmetric=False) - 1)\
                < 1e2*COMMON_ZERO_TOLERANCE
        assert abs(det_ov(cs, cs, sao, complexsymmetric=True) - 1)\
                < 1e2*COMMON_ZERO_TOLERANCE

    # 1a and 1b have no overlapping spatial symmetries.
    assert abs(det_ov(css[0], css[1], sao, complexsymmetric=False))\
            < 1e2*COMMON_ZERO_TOLERANCE
    assert abs(det_ov(css[0], css[1], sao, complexsymmetric=True))\
            < 1e2*COMMON_ZERO_TOLERANCE

    # 1b and 1c are time-reversal partners.
    # This alone doesn't impose orthogonality on them, but the nodal structures
    # of these two solutions do.
    assert abs(det_ov(css[1], css[2], sao, complexsymmetric=False))\
            < 1e2*COMMON_ZERO_TOLERANCE
    assert abs(det_ov(css[1], css[2], sao, complexsymmetric=True))\
            < 1e2*COMMON_ZERO_TOLERANCE

    # 1a and 1d both transform as Ag, but the individual orbitals have
    # different symmetries.
    assert abs(det_ov(css[0], css[3], sao, complexsymmetric=False))\
            < 1e2*COMMON_ZERO_TOLERANCE
    assert abs(det_ov(css[0], css[3], sao, complexsymmetric=True))\
            < 1e2*COMMON_ZERO_TOLERANCE

    # 1a and 2 share an Ag component (2 is spatial-symmetry-broken).
    # We know that they are non-orthogonal. Numerical values are verified
    # against an independent calculation in Mathematica.
    assert abs(det_ov(css[0], css[4], sao, complexsymmetric=False)
               + 0.858802541141290510) < 1e2*COMMON_ZERO_TOLERANCE
    assert abs(det_ov(css[0], css[4], sao, complexsymmetric=True)
               + 0.858802541141290510) < 1e2*COMMON_ZERO_TOLERANCE

    # 1c and 2 share an B3u component (2 is spatial-symmetry-broken).
    # We know that they are non-orthogonal. Numerical values are verified
    # against an independent calculation in Mathematica.
    assert abs(det_ov(css[2], css[4], sao, complexsymmetric=False)
               - 0.348225123261589498) < 1e2*COMMON_ZERO_TOLERANCE
    assert abs(det_ov(css[2], css[4], sao, complexsymmetric=True)
               - 0.348225123261589498) < 1e2*COMMON_ZERO_TOLERANCE

    # 2 and 3 share both Ag and B3u components (and are both symmetry-broken).
    # We know that they are non-orthogonal. Numerical values are verified
    # against an independent calculation in Mathematica.
    assert abs(det_ov(css[4], css[5], sao, complexsymmetric=False)
               - 0.0129724506008558806) < 1e2*COMMON_ZERO_TOLERANCE
    assert abs(det_ov(css[4], css[5], sao, complexsymmetric=True)
               - 0.0129724506008558806) < 1e2*COMMON_ZERO_TOLERANCE


def test_det_ov_complex() -> None:
    r"""Tests for the calculations of overlap between determinants.

    We take MS=0 sigma/dashdash determinants in [H4]2+ at a = 1.100 Å and
    b = 1.100 Å. This is a very simple system, so test rigour might not be
    ideal, but this suffices for now and will be expanded later.

      1   b   2
      H───────H
     a│       │
      H───────H
      4       3
    """
    # pylint: disable=C0103,R0914
    # C0103: Variable names are chosen in accordance with their mathematical
    #        symbols.
    # R0914: Having more local variables helps enhance code readability.
    a = 1.100
    b = 1.100
    mol = gto.M(atom=(f"H {-b/2.0} {a} 0;"
                      f"H {+b/2.0} {a} 0;"
                      f"H {+b/2.0} 0   0;"
                      f"H {-b/2.0} 0   0;"),
                basis='sto3g',
                unit='Angstrom')
    mol.charge = 2
    mol.spin = 0
    sao = mol.intor('int1e_ovlp', aosym='1')

    css = []
    for sol in ["1a", "1b", "1c", "1d", "2a", "2b"]:
        ca = np.genfromtxt(os.path.join(os.path.dirname(__file__),
                                        f"H42p/a1.100_b1.100/{sol}.alpha"),
                           dtype=np.complex128).reshape((-1, 1))
        cb = np.genfromtxt(os.path.join(os.path.dirname(__file__),
                                        f"H42p/a1.100_b1.100/{sol}.beta"),
                           dtype=np.complex128).reshape((-1, 1))
        css.append([ca, cb])

    # Normalisation
    for cs in css[0:4]:
        assert abs(det_ov(cs, cs, sao, complexsymmetric=False) - 1)\
                < 1e2*COMMON_ZERO_TOLERANCE
        assert abs(det_ov(cs, cs, sao, complexsymmetric=True) - 1)\
                < 1e2*COMMON_ZERO_TOLERANCE
    for cs in css[4:6]:
        assert abs(det_ov(cs, cs, sao, complexsymmetric=False) - 1)\
                > 1e2*COMMON_ZERO_TOLERANCE
        assert abs(det_ov(cs, cs, sao, complexsymmetric=True) - 1)\
                < 1e2*COMMON_ZERO_TOLERANCE

    # 1a and 1b have no overlapping spatial symmetries.
    assert abs(det_ov(css[0], css[1], sao, complexsymmetric=False))\
            < 1e2*COMMON_ZERO_TOLERANCE
    assert abs(det_ov(css[0], css[1], sao, complexsymmetric=True))\
            < 1e2*COMMON_ZERO_TOLERANCE

    # 1b and 1c are time-reversal partners.
    # This alone doesn't impose orthogonality on them, but the nodal structures
    # of these two solutions do.
    assert abs(det_ov(css[1], css[2], sao, complexsymmetric=False))\
            < 1e2*COMMON_ZERO_TOLERANCE
    assert abs(det_ov(css[1], css[2], sao, complexsymmetric=True))\
            < 1e2*COMMON_ZERO_TOLERANCE

    # 1a and 1d both transform as Ag, but the individual orbitals have
    # different symmetries.
    assert abs(det_ov(css[0], css[3], sao, complexsymmetric=False))\
            < 1e2*COMMON_ZERO_TOLERANCE
    assert abs(det_ov(css[0], css[3], sao, complexsymmetric=True))\
            < 1e2*COMMON_ZERO_TOLERANCE

    # 1a and 2 share an Ag component (2 is spatial-symmetry-broken).
    # We know that they are non-orthogonal. Numerical values are verified
    # against an independent calculation in Mathematica.
    assert abs(det_ov(css[4], css[0], sao, complexsymmetric=False)
               + 1.343387574820553931) < 1e2*COMMON_ZERO_TOLERANCE
    assert abs(det_ov(css[4], css[0], sao, complexsymmetric=True)
               + 1.343387574820553931) < 1e2*COMMON_ZERO_TOLERANCE

    # 1c and 2 share an B3u component (2 is spatial-symmetry-broken).
    # We know that they are non-orthogonal. Numerical values are verified
    # against an independent calculation in Mathematica.
    assert abs(det_ov(css[4], css[2], sao, complexsymmetric=False)
               + 0.679192609914004986j) < 1e2*COMMON_ZERO_TOLERANCE
    assert abs(det_ov(css[4], css[2], sao, complexsymmetric=True)
               - 0.679192609914004986j) < 1e2*COMMON_ZERO_TOLERANCE

    # 2 and 3 share both Ag and B3u components (and are both symmetry-broken).
    # We know that they are non-orthogonal. Numerical values are verified
    # against an independent calculation in Mathematica.
    assert abs(det_ov(css[4], css[5], sao, complexsymmetric=False)
               - 0.0646950376717538994) < 1e2*COMMON_ZERO_TOLERANCE
    assert abs(det_ov(css[4], css[5], sao, complexsymmetric=True)
               - 0.0646950376717538994) < 1e2*COMMON_ZERO_TOLERANCE


def test_det_ov_real_H4() -> None:
    r"""Tests for the calculations of overlap and Löwdin pairing between
    determinants.

    We take MS=0 UHF determinants in H4 at a = 1.100 Å and
    b = 1.100 Å. Results are verified against calculations in Q-Chem.

      1 b 2
      H───H
     a│   │
      H───H
      4   3
    """
    # pylint: disable=C0103,R0914
    # C0103: Variable names are chosen in accordance with their mathematical
    #        symbols.
    # R0914: Having more local variables helps enhance code readability.
    a = 1.100
    b = 1.100
    mol = gto.M(atom=(f"H {-b/2.0} {a} 0;"
                      f"H {+b/2.0} {a} 0;"
                      f"H {+b/2.0} 0   0;"
                      f"H {-b/2.0} 0   0;"),
                basis='sto3g',
                unit='Angstrom')
    mol.charge = 0
    mol.spin = 0
    scf.RHF(mol)
    sao = mol.intor('int1e_ovlp', aosym='1')

    # Solution 1
    ca1 = np.array([[+0.34371361, -0.57240233],
                    [+0.34371361, -0.57240233],
                    [+0.34371361, +0.57240233],
                    [+0.34371361, +0.57240233]])
    cb1 = np.array([[+0.34371361, -0.57240233],
                    [+0.34371361, -0.57240233],
                    [+0.34371361, +0.57240233],
                    [+0.34371361, +0.57240233]])
    c1 = [ca1, cb1]

    # Solution 2
    ca2 = np.array([[+0.34371361, +0.57240233],
                    [+0.34371361, -0.57240233],
                    [+0.34371361, -0.57240233],
                    [+0.34371361, +0.57240233]])
    cb2 = np.array([[+0.34371361, +0.57240233],
                    [+0.34371361, -0.57240233],
                    [+0.34371361, -0.57240233],
                    [+0.34371361, +0.57240233]])
    c2 = [ca2, cb2]

    # Solution 3
    ca3 = np.array([[+0.34371361, -0.57240233],
                    [+0.34371361, +0.57240233],
                    [+0.34371361, +0.57240233],
                    [+0.34371361, -0.57240233]])
    cb3 = np.array([[+0.34371361, -0.57240233],
                    [+0.34371361, -0.57240233],
                    [+0.34371361, +0.57240233],
                    [+0.34371361, +0.57240233]])
    c3 = [ca3, cb3]

    # Solution 4
    ca4 = np.array([[+0.47945829, -0.80949914],
                    [+0.19816818, +0.00000000],
                    [+0.47945829, +0.80949914],
                    [+0.19816818, +0.00000000]])
    cb4 = np.array([[+0.19816818, +0.00000000],
                    [+0.47945829, -0.80949914],
                    [+0.19816818, +0.00000000],
                    [+0.47945829, +0.80949914]])
    c4 = [ca4, cb4]


    # Overlap matrix
    Smat = get_Smat([c1, c2, c3, c4], [c1, c2, c3, c4],
                    sao, complexsymmetric=False)
    Smat_qchem = np.array([[1.0000000, +0.0000000, 0.0000000, +0.4858445],
                           [0.0000000, +1.0000000, 0.0000000, -0.4858445],
                           [0.0000000, +0.0000000, 1.0000000, +0.4858445],
                           [0.4858445, -0.4858445, 0.4858445, +1.0000000]])
    assert np.allclose(Smat, Smat_qchem, rtol=0, atol=1e7*COMMON_ZERO_TOLERANCE)

    lowdin_overlaps_a = lowdin_pairing(c1[0], c4[0], sao, False)[2]
    lowdin_overlaps_b = lowdin_pairing(c1[1], c4[1], sao, False)[2]
    assert abs(np.prod(lowdin_overlaps_a)*np.prod(lowdin_overlaps_b)
               - 0.4858445) < 1e7*COMMON_ZERO_TOLERANCE
