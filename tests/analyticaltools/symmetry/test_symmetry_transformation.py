""":mod:`.test_symmetry_transformation` contains unit tests for functions
implemented in :mod:`analyticaltools.symmetry.symmetry_transformation`.
"""

import pytest  # type: ignore
import numpy as np  # type: ignore
import scipy.linalg  # type: ignore

from polyinspect.auxiliary.chemical_structures import Molecule, Atom
from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE,\
        LOOSE_ZERO_TOLERANCE
from polyinspect.auxiliary.geometrical_space import Vector

from polyinspect.analyticaltools.symmetry.angmom3d import Rmat, Dmat_angleaxis
from polyinspect.analyticaltools.symmetry.symmetry_core import Symmetry
from polyinspect.analyticaltools.symmetry.symmetry_transformation import\
        _permute_coeffs, _transform_coeffs, unified_transform, get_symequiv


def test_permute_coeffs_dummy():
    """Tests for the permutation of coefficients of a dummy molecule.
    """
    # pylint: disable=R0914
    # R0904: Having more variables makes the test more understandable.
    bao = [("C", [("S", False, None),
                  ("P", True, None)]),
           ("F", [("S", False, None),
                  ("D", True, None)]),
           ("P", [("S", False, None),
                  ("D", True, None)]),
           ("I", [("S", False, None),
                  ("D", True, None)]),
           ("S", [("S", False, None),
                  ("D", True, None)])]

    calpha = np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0],
                       [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1],
                       [2, 2, 2], [2, 2, 2], [2, 2, 2], [2, 2, 2], [2, 2, 2], [2, 2, 2], [2, 2, 2],
                       [3, 3, 3], [3, 3, 3], [3, 3, 3], [3, 3, 3], [3, 3, 3], [3, 3, 3], [3, 3, 3],
                       [4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4]])
    cbeta = np.array([[5, 5, 5], [5, 5, 5], [5, 5, 5], [5, 5, 5],
                      [6, 6, 6], [6, 6, 6], [6, 6, 6], [6, 6, 6], [6, 6, 6], [6, 6, 6], [6, 6, 6],
                      [7, 7, 7], [7, 7, 7], [7, 7, 7], [7, 7, 7], [7, 7, 7], [7, 7, 7], [7, 7, 7],
                      [8, 8, 8], [8, 8, 8], [8, 8, 8], [8, 8, 8], [8, 8, 8], [8, 8, 8], [8, 8, 8],
                      [9, 9, 9], [9, 9, 9], [9, 9, 9], [9, 9, 9], [9, 9, 9], [9, 9, 9], [9, 9, 9]])
    csuhf = [calpha, cbeta]
    cgen = scipy.linalg.block_diag(calpha, cbeta)
    csghf = [cgen]

    perm = [0, 2, 1, 3, 4]
    csuhf_permuted, bao_permuted = _permute_coeffs(csuhf, perm, bao)
    csghf_permuted, _ = _permute_coeffs(csghf, perm, bao)

    calpha_p =\
            np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0],
                      [2, 2, 2], [2, 2, 2], [2, 2, 2], [2, 2, 2], [2, 2, 2], [2, 2, 2], [2, 2, 2],
                      [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1],
                      [3, 3, 3], [3, 3, 3], [3, 3, 3], [3, 3, 3], [3, 3, 3], [3, 3, 3], [3, 3, 3],
                      [4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4]])
    cbeta_p =\
            np.array([[5, 5, 5], [5, 5, 5], [5, 5, 5], [5, 5, 5],
                      [7, 7, 7], [7, 7, 7], [7, 7, 7], [7, 7, 7], [7, 7, 7], [7, 7, 7], [7, 7, 7],
                      [6, 6, 6], [6, 6, 6], [6, 6, 6], [6, 6, 6], [6, 6, 6], [6, 6, 6], [6, 6, 6],
                      [8, 8, 8], [8, 8, 8], [8, 8, 8], [8, 8, 8], [8, 8, 8], [8, 8, 8], [8, 8, 8],
                      [9, 9, 9], [9, 9, 9], [9, 9, 9], [9, 9, 9], [9, 9, 9], [9, 9, 9], [9, 9, 9]])
    csuhf_permuted_ref = [calpha_p, cbeta_p]
    assert all(np.all(csuhf_permuted[i] == csuhf_permuted_ref[i])
               for i in [0, 1])

    cgen_p = scipy.linalg.block_diag(calpha_p, cbeta_p)
    csghf_permuted_ref = [cgen_p]
    assert all(np.all(csghf_permuted[i] == csghf_permuted_ref[i])
               for i in [0])

    assert list(zip(*bao_permuted))[0] == ("C", "P", "F", "I", "S")


    perm = [0, 4, 3, 1, 2]
    csuhf_permuted, bao_permuted = _permute_coeffs(csuhf, perm, bao)
    csuhf_permuted_moa1, _ = _permute_coeffs(csuhf, perm, bao, mo_indices=((1, 2), None))
    csghf_permuted, _ = _permute_coeffs(csghf, perm, bao)

    calpha_p =\
            np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0],
                      [3, 3, 3], [3, 3, 3], [3, 3, 3], [3, 3, 3], [3, 3, 3], [3, 3, 3], [3, 3, 3],
                      [4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4], [4, 4, 4],
                      [2, 2, 2], [2, 2, 2], [2, 2, 2], [2, 2, 2], [2, 2, 2], [2, 2, 2], [2, 2, 2],
                      [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1]])
    calpha_p_1 =\
            np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0],
                      [1, 3, 3], [1, 3, 3], [1, 3, 3], [1, 3, 3], [1, 3, 3], [1, 3, 3], [1, 3, 3],
                      [2, 4, 4], [2, 4, 4], [2, 4, 4], [2, 4, 4], [2, 4, 4], [2, 4, 4], [2, 4, 4],
                      [3, 2, 2], [3, 2, 2], [3, 2, 2], [3, 2, 2], [3, 2, 2], [3, 2, 2], [3, 2, 2],
                      [4, 1, 1], [4, 1, 1], [4, 1, 1], [4, 1, 1], [4, 1, 1], [4, 1, 1], [4, 1, 1]])
    cbeta_p =\
            np.array([[5, 5, 5], [5, 5, 5], [5, 5, 5], [5, 5, 5],
                      [8, 8, 8], [8, 8, 8], [8, 8, 8], [8, 8, 8], [8, 8, 8], [8, 8, 8], [8, 8, 8],
                      [9, 9, 9], [9, 9, 9], [9, 9, 9], [9, 9, 9], [9, 9, 9], [9, 9, 9], [9, 9, 9],
                      [7, 7, 7], [7, 7, 7], [7, 7, 7], [7, 7, 7], [7, 7, 7], [7, 7, 7], [7, 7, 7],
                      [6, 6, 6], [6, 6, 6], [6, 6, 6], [6, 6, 6], [6, 6, 6], [6, 6, 6], [6, 6, 6]])
    csuhf_permuted_ref = [calpha_p, cbeta_p]
    csuhf_permuted_moa1_ref = [calpha_p_1, cbeta_p]
    assert all(np.all(csuhf_permuted[i] == csuhf_permuted_ref[i])
               for i in [0, 1])
    assert all(np.all(csuhf_permuted_moa1[i] == csuhf_permuted_moa1_ref[i])
               for i in [0, 1])

    cgen_p = scipy.linalg.block_diag(calpha_p, cbeta_p)
    csghf_permuted_ref = [cgen_p]
    assert all(np.all(csghf_permuted[i] == csghf_permuted_ref[i])
               for i in [0])

    assert list(zip(*bao_permuted))[0] == ("C", "I", "S", "P", "F")

    calpha_deficit = np.array([[0, 0], [0, 0], [0, 0], [0, 0],
                               [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1],
                               [2, 2], [2, 2], [2, 2], [2, 2], [2, 2], [2, 2], [2, 2],
                               [3, 3], [3, 3], [3, 3], [3, 3], [3, 3], [3, 3], [3, 3]])
    cbeta_deficit = np.array([[5, 5], [5, 5], [5, 5], [5, 5],
                              [6, 6], [6, 6], [6, 6], [6, 6], [6, 6], [6, 6], [6, 6],
                              [7, 7], [7, 7], [7, 7], [7, 7], [7, 7], [7, 7], [7, 7],
                              [8, 8], [8, 8], [8, 8], [8, 8], [8, 8], [8, 8], [8, 8]])
    csuhf_deficit = [calpha_deficit, cbeta_deficit]
    cgen_deficit = scipy.linalg.block_diag(calpha_deficit, cbeta_deficit)
    csghf_deficit = [cgen_deficit]
    with pytest.raises(ValueError):
        _permute_coeffs(csuhf_deficit, perm, bao)
    with pytest.raises(ValueError):
        _permute_coeffs(csghf_deficit, perm, bao)


def test_transform_coeffs_bf4_sqpl():
    r"""Tests for the spatial transformation of coefficients of a fictional
    :math:`\mathrm{BF}_4` molecule.
    """
    # pylint: disable=R0914,C0103
    # R0904: Having more variables makes the test more understandable.
    # C0103: cs is frequently used to denote a list of coefficient matrices.
    bao = [("B", [("S", False, None),
                  ("P", True, None),
                  ("D", True, None)]),
           ("F", [("S", False, None),
                  ("P", True, None),
                  ("D", True, None)]),
           ("F", [("S", False, None),
                  ("P", True, None),
                  ("D", True, None)]),
           ("F", [("S", False, None),
                  ("P", True, None),
                  ("D", True, None)]),
           ("F", [("S", False, None),
                  ("P", True, None)])]

    mol = Molecule([Atom("B", (0, 0, 0)),
                    Atom("F", (1, 0, 0)),
                    Atom("F", (0, 1, 0)),
                    Atom("F", (-1, 0, 0)),
                    Atom("F", (0, -1, 0))])
    mol.recentre_ip()

    calpha = np.array([[+1, +1],
                       [+1, +0], [+0, +1], [+0, +0],
                       [-1, +0], [+0, +0], [+0, +1], [+1, +0], [+0, +0], [+0, +0],
                       [+0, +0],
                       [+1, +0], [+0, +0], [+0, +0],
                       [+0, +0], [+1, +0], [+0, +0], [+0, +0], [+0, +1], [+0, +0],
                       [+0, +0],
                       [+0, +0], [+0, +1], [+0, +0],
                       [+0, +0], [+1, +0], [+0, +1], [+0, +0], [+0, +0], [+0, +0],
                       [+0, +0],
                       [+1, +0], [+0, +0], [+0, +0],
                       [+0, +0], [+1, +0], [+0, +0], [+0, +0], [+0, +1], [+0, +0],
                       [+0, +0],
                       [+0, +0], [+0, +1], [+0, +0]])

    cs = [calpha]

    sym = Symmetry(mol)
    sym.analyse()
    perm = sym.proper_permutation(4, index=0)
    R = Rmat(2*np.pi/4, sym.proper_elements[4][0])
    tcs = _transform_coeffs(cs, perm, R, bao)

    tcalpha_ref = np.array([[+1, +1],
                            [+0, -1], [+1, +0], [+0, +0],
                            [+1, +0], [+0, +0], [+0, +0], [-1, +0], [+0, +1], [+0, +0],
                            [+0, +0],
                            [+0, -1], [+0, +0], [+0, +0],
                            [+0, +0],
                            [+0, +0], [+1, +0], [+0, +0],
                            [+0, +0], [-1, +0], [+0, -1], [+0, +0], [+0, +0], [+0, +0],
                            [+0, +0],
                            [+0, -1], [+0, +0], [+0, +0],
                            [+0, +0], [-1, +0], [+0, +0], [+0, +0], [+0, +1], [+0, +0],
                            [+0, +0],
                            [+0, +0], [+1, +0], [+0, +0],
                            [+0, +0], [-1, +0], [+0, -1], [+0, +0], [+0, +0], [+0, +0]])

    tcs_ref = [tcalpha_ref]
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcs, tcs_ref))


def test_transform_coeffs_b3_spinrot():
    r"""Tests for the transformation of coefficients of a fictional
    :math:`\mathrm{B}_3` molecule, where spin rotation is also involved.
    """
    # pylint: disable=R0914,C0103
    # R0904: Having more variables makes the test more understandable.
    # C0103: The variables are named according to their mathematical symbols.
    bao = [("B", [("S", False, None),
                  ("P", True, None)]),
           ("B", [("S", False, None),
                  ("P", True, None)]),
           ("B", [("S", False, None),
                  ("P", True, None)])]

    mol = Molecule([Atom("B", (0, 0, 0)),
                    Atom("B", (1, 0, 0)),
                    Atom("B", (0.5, np.sqrt(3)/2, 0))])
    mol.recentre_ip()

    sqr = np.sqrt(3)/2
    calpha = np.array([[+1.0, +0.0],
                       [+sqr, +1.0], [-0.5, +0.0], [+0.0, +0.0],
                       [+1.0, +0.0],
                       [+0.0, +1.0], [+1.0, +0.0], [+0.0, +0.0],
                       [+1.0, +0.0],
                       [-sqr, -1.0], [-0.5, +0.0], [+0.0, +0.0]])
    cbeta = np.array([[+0.0, +0.0],
                      [-0.5, +0.0], [-sqr, +1.0], [+0.0, +0.0],
                      [+0.0, +0.0],
                      [+1.0, +0.0], [+0.0, +1.0], [+0.0, +0.0],
                      [+0.0, +0.0],
                      [-0.5, +0.0], [+sqr, -1.0], [+0.0, +0.0]])
    cgen = np.concatenate([calpha, cbeta], axis=0)
    cs = [cgen]

    sym = Symmetry(mol)
    sym.analyse()
    perm = sym.proper_permutation(3, index=0)
    R = Rmat(2*np.pi/3, sym.proper_elements[3][0])
    Dmat = Dmat_angleaxis(np.pi, Vector([0, 1, 0]))
    tcs = _transform_coeffs(cs, perm, R, bao, mo_indices=((0,),), Dmat=Dmat)

    tcalpha_ref = np.array([[+0.0, +0.0],
                            [+0.5, +1.0], [+sqr, +0.0], [+0.0, +0.0],
                            [+0.0, +0.0],
                            [-1.0, +1.0], [+0.0, +0.0], [+0.0, +0.0],
                            [+0.0, +0.0],
                            [+0.5, -1.0], [-sqr, +0.0], [+0.0, +0.0]])
    tcbeta_ref = np.array([[+1.0, +0.0],
                           [+sqr, +0.0], [-0.5, +1.0], [+0.0, +0.0],
                           [+1.0, +0.0],
                           [+0.0, +0.0], [+1.0, +1.0], [+0.0, +0.0],
                           [+1.0, +0.0],
                           [-sqr, +0.0], [-0.5, -1.0], [+0.0, +0.0]])
    tcgen_ref = np.concatenate([tcalpha_ref, tcbeta_ref], axis=0)
    tcs_ref = [tcgen_ref]

    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcs, tcs_ref))


def test_unified_transform_rotation_s4():
    r"""Tests for the transformation of coefficients of a fictional
    :math:`\mathrm{S}_4` molecule using
    :func:`~src.analyticaltools.symmetry.symmetry_transformation.unified_transform`.
    """
    # pylint: disable=R0914,C0103
    # R0904: Having more variables makes the test more understandable.
    # C0103: The variables are named according to their mathematical symbols.
    bao = [("S", [("D", False, None)]),
           ("S", [("D", False, None)]),
           ("S", [("D", False, None)]),
           ("S", [("D", False, None)])]

    mol = Molecule([Atom("S", (+1, +1, +0)),
                    Atom("S", (-1, +1, +0)),
                    Atom("S", (-1, -1, +0)),
                    Atom("S", (+1, -1, +0))])
    mol.recentre_ip()
    sym = Symmetry(mol)
    sym.analyse()

    calpha = np.array([[+1.0], [+0.0], [+0.0], [+0.0], [+0.0],
                       [-1.0], [+0.0], [+0.0], [+0.0], [+0.0],
                       [+1.0], [+0.0], [+0.0], [+0.0], [+0.0],
                       [-1.0], [+0.0], [+0.0], [+0.0], [+0.0]])
    cbeta = np.array([[+0.0], [+0.0], [+0.0], [+0.0], [+1.0],
                      [+0.0], [+0.0], [+0.0], [+0.0], [+1.0],
                      [+0.0], [+0.0], [+0.0], [+0.0], [+1.0],
                      [+0.0], [+0.0], [+0.0], [+0.0], [+1.0]])
    cs = [calpha, cbeta]
    with pytest.raises(ValueError):
        unified_transform(cs, ("C_-_-", (4, 0, 1), None), bao, sym)
    tcs = unified_transform(cs, ("C_e_-", (4, 0, 1), None), bao, sym)

    tcalpha_ref = np.array([[+1.0], [+0.0], [+0.0], [+0.0], [+0.0],
                            [-1.0], [+0.0], [+0.0], [+0.0], [+0.0],
                            [+1.0], [+0.0], [+0.0], [+0.0], [+0.0],
                            [-1.0], [+0.0], [+0.0], [+0.0], [+0.0]])
    tcbeta_ref = -np.array([[+0.0], [+0.0], [+0.0], [+0.0], [+1.0],
                            [+0.0], [+0.0], [+0.0], [+0.0], [+1.0],
                            [+0.0], [+0.0], [+0.0], [+0.0], [+1.0],
                            [+0.0], [+0.0], [+0.0], [+0.0], [+1.0]])
    tcs_ref = [tcalpha_ref, tcbeta_ref]
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcs, tcs_ref))


    calpha2 = np.array([[+0.0], [+0.0], [+0.0], [+1.0], [+0.0],
                        [+0.0], [+0.0], [+0.0], [-1.0], [+0.0],
                        [+0.0], [+0.0], [+0.0], [-1.0], [+0.0],
                        [+0.0], [+0.0], [+0.0], [+1.0], [+0.0]])
    cbeta2 = np.array([[+0.0], [+1.0], [+0.0], [+0.0], [+0.0],
                       [+0.0], [+1.0], [+0.0], [+0.0], [+0.0],
                       [+0.0], [-1.0], [+0.0], [+0.0], [+0.0],
                       [+0.0], [-1.0], [+0.0], [+0.0], [+0.0]])
    cgen2 = scipy.linalg.block_diag(calpha2, cbeta2)
    cs2 = [calpha2, cbeta2]
    csgen2 = [cgen2]
    tcs2 = unified_transform(cs2, ("C_g_-", (4, 0, -2), None), bao, sym)
    tcs2_ref = cs2
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcs2, tcs2_ref))

    tcs2 = unified_transform(cs2, ("C_e_-", (4, 0, -1), None), bao, sym)
    tcs2_ref = reversed(cs2)
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcs2, tcs2_ref))

    tcs2 = unified_transform(cs2, ("S_g_-", (1, 0, 1), None), bao, sym)
    tcs2_ref = [-c for c in cs2]
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcs2, tcs2_ref))

    tcs2 = unified_transform(cs2, ("S_e_-", (1, 2, 1), None), bao, sym)
    tcs2_ref = cs2
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcs2, tcs2_ref))

    tcs2 = unified_transform(cs2, ("S_e_-", (1, 3, 1), None), bao, sym)
    tcs2_ref = cs2
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcs2, tcs2_ref))

    with pytest.raises(AssertionError):
        unified_transform(cs2, ("C_e_sr", (2, 2, 1), None), bao, sym)
    tcsgen2 = unified_transform(csgen2, ("C_e_sr", (2, 2, 1), None), bao, sym)
    tcsgen2_ref = [np.block([[np.zeros((20, 1)), cbeta2],
                             [-calpha2, np.zeros((20, 1))]])]
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcsgen2, tcsgen2_ref))

    # sym.improper_elements[1][2] == Vector([0.000, 1.000, 0.000])
    # The inverse-associated spin rotation is C2 about the same axis,
    # which is similar to time reversal.
    tcsgen2 = unified_transform(csgen2, ("S_e_sr", (1, 2, 1), None), bao, sym)
    tcsgen2_ref = [np.block([[np.zeros((20, 1)), -cbeta2],
                             [calpha2, np.zeros((20, 1))]])]
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcsgen2, tcsgen2_ref))

    # sym.improper_elements[1][2] == Vector([1.000, 0.000, 0.000])
    # The inverse-associated spin rotation is C2 about the same axis,
    # which is not quite the same as time reversal due to some additional
    # imaginary factors.
    tcsgen2 = unified_transform(csgen2, ("S_e_sr", (1, 3, 1), None), bao, sym)
    tcsgen2_ref = [np.block([[np.zeros((20, 1)), -1j*cbeta2],
                             [-1j*calpha2, np.zeros((20, 1))]])]
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcsgen2, tcsgen2_ref))


def test_unified_transform_rotation_c2():
    r"""Tests for the transformation of coefficients of a fictional
    :math:`\mathrm{C}_2` molecule using
    :func:`~src.analyticaltools.symmetry.symmetry_transformation.unified_transform`.
    """
    # pylint: disable=C0103
    # C0103: The variables are named according to their mathematical symbols.
    bao = [("C", [("P", True, None)]),
           ("C", [("P", True, None)])]

    mol = Molecule([Atom("C", (+1, +0, +0)),
                    Atom("C", (-1, +0, +0))])
    mol.recentre_ip()
    sym = Symmetry(mol)
    sym.analyse()

    calpha = np.array([[+1.0], [+0.0], [+0.0],
                       [-1.0], [+0.0], [+0.0]])
    cbeta = np.array([[+0.0], [+1.0], [+0.0],
                      [+0.0], [+1.0], [+0.0]])
    cs = [calpha, cbeta]
    tcs = unified_transform(cs, ("C_g_-", (-1, 0, 1/4), None), bao, sym)
    sqr = 1/np.sqrt(2)
    tcalpha_ref = np.array([[+1.0], [+0.0], [+0.0],
                            [-1.0], [+0.0], [+0.0]])
    tcbeta_ref = np.array([[+0.0], [+sqr], [+sqr],
                           [+0.0], [+sqr], [+sqr]])
    tcs_ref = [tcalpha_ref, tcbeta_ref]
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcs, tcs_ref))

    tcs = unified_transform(cs, ("C_g_-", (-1, 0, -1/2), None), bao, sym)
    tcalpha_ref = np.array([[+1.0], [+0.0], [+0.0],
                            [-1.0], [+0.0], [+0.0]])
    tcbeta_ref = np.array([[+0.0], [+0.0], [-1.0],
                           [+0.0], [+0.0], [-1.0]])
    tcs_ref = [tcalpha_ref, tcbeta_ref]
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcs, tcs_ref))

    tcs = unified_transform(cs, ("C_g_-", (2, 0, 1), None), bao, sym)
    tcalpha_ref = np.array([[+1.0], [+0.0], [+0.0],
                            [-1.0], [+0.0], [+0.0]])
    tcbeta_ref = np.array([[+0.0], [-1.0], [+0.0],
                           [+0.0], [-1.0], [+0.0]])
    tcs_ref = [tcalpha_ref, tcbeta_ref]
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcs, tcs_ref))

    tcs = unified_transform(cs, ("Q_g_-", (2, 0), None), bao, sym)
    tcs_ref = cs
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcs, tcs_ref))


def test_unified_transform_spin_rotation():
    r"""Tests for the spin rotation of coefficients of a fictional
    :math:`\mathrm{C}_3` molecule and a :math:`\mathrm{O}` atom using
    :func:`~src.analyticaltools.symmetry.symmetry_transformation.unified_transform`.
    """
    # pylint: disable=R0914,C0103
    # R0904: Having more variables makes the test more understandable.
    # C0103: The variables are named according to their mathematical symbols.
    bao = [("C", [("S", False, None),
                  ("P", True, None)]),
           ("C", [("S", False, None),
                  ("P", True, None)]),
           ("C", [("S", False, None),
                  ("P", True, None)])]

    mol = Molecule([Atom("C", (+1, +0, +0)),
                    Atom("C", (+0, +1, +0)),
                    Atom("C", (+0, +0, +0))])
    mol.recentre_ip()
    sym = Symmetry(mol)
    sym.analyse()

    calpha = np.array([[+1.0, +0.0],
                       [+0.0, +1.0], [+0.0, +0.0], [+0.0, +0.0],
                       [+0.0, +0.0],
                       [+1.0, +1.0], [+0.0, +0.0], [+0.0, +0.0],
                       [+0.0, +0.0],
                       [+0.0, +1.0], [+1.0, +0.0], [+0.0, +0.0]])
    cbeta = np.array([[+0.0, +0.0],
                      [+0.0, +0.0], [+0.0, +0.0], [+1.0, +0.0],
                      [+0.0, +0.0],
                      [+0.0, +0.0], [+0.0, +0.0], [-1.0, +0.0],
                      [+0.0, +0.0],
                      [+0.0, +0.0], [+1.0, +0.0], [+1.0, +0.0]])
    cgen = scipy.linalg.block_diag(calpha, cbeta)
    cs = [calpha, cbeta]
    cgens = [cgen]
    with pytest.raises(AssertionError):
        unified_transform(cs, ("-_e_-", (2, 0, 1), None), bao, sym)

    tcs = unified_transform(cgens, ("-_-_sr", (np.pi, Vector([0, 1, 0])), None),
                            bao, sym)
    tcgens_ref = [np.block([[np.zeros((12, 2)), -cbeta],
                            [calpha, np.zeros((12, 2))]])]
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcs, tcgens_ref))

    tcs2 = unified_transform(cgens, ("-_-_sr", (np.pi, Vector([0, 1, 0])),
                                     ((0, 1),)), bao, sym)
    calpha_ref = calpha
    cbeta_ref = cbeta
    tcgens2_ref = [np.block([[np.zeros((12, 2)), np.zeros((12, 2))],
                             [calpha_ref, cbeta_ref]])]
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcs2, tcgens2_ref))

    sqr = 1/np.sqrt(2)
    tcs = unified_transform(cgens, ("-_e_sr", (2, 0, 1), None), bao, sym)
    tcgens_ref = [np.block([[np.zeros((12, 2)), -sqr*(1+1j)*cbeta],
                            [sqr*(1-1j)*calpha, np.zeros((12, 2))]])]
    assert all(np.allclose(c, c_ref, rtol=0, atol=LOOSE_ZERO_TOLERANCE)
               for c, c_ref in zip(tcs, tcgens_ref))

    tcs = unified_transform(cgens, ("-_g_sr", (2, 0, 2), None), bao, sym)
    tcgens_ref = [-scipy.linalg.block_diag(calpha, cbeta)]
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcs, tcgens_ref))


    bao = [("O", [("S", False, None),
                  ("P", True, None)])]

    mol = Molecule([Atom("O", (+0, +0, +0))])
    mol.recentre_ip()
    sym = Symmetry(mol)
    sym.analyse()
    calpha = np.array([[+1.0],
                       [+0.0], [+1.0], [+0.0]])
    cbeta = np.array([[+0.0],
                      [+1.0], [+0.0], [+0.0]])
    cgen = scipy.linalg.block_diag(calpha, cbeta)
    cgens = [cgen]
    tcs = unified_transform(cgens, ("-_g_sr", (-1, 0, 1), None), bao, sym)
    tcgens_ref = [scipy.linalg.block_diag(-1j*calpha, 1j*cbeta)]
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcs, tcgens_ref))


def test_unified_transform_complex_conjugation():
    r"""Tests for the complex conjugation of coefficients using
    :func:`~src.analyticaltools.symmetry.symmetry_transformation.unified_transform`.
    """
    # pylint: disable=C0103
    # C0103: The variables are named according to their mathematical symbols.
    bao = [("C", [("P", True, None)]),
           ("C", [("P", True, None)])]

    mol = Molecule([Atom("C", (+1, +0, +0)),
                    Atom("C", (+0, +0, +0))])
    mol.recentre_ip()
    sym = Symmetry(mol)
    sym.analyse()

    calpha = np.array([[+1.0, 1+2.5j], [+0.0, -3.4j], [+0.0, 0.2],
                       [-1.0, 1-2.5j], [+0.0, -3.4j], [+0.0, 0.2]])
    cbeta = np.array([[1+2.0j, 2+3.9j], [+1.0, +0.0], [+0.0, +0.0],
                      [2+4.0j, 2-3.9j], [+1.0, +0.0], [+0.0, +0.0]])
    cs = [calpha, cbeta]
    tcs = unified_transform(cs, ("K_-_-", None, (None, (0,))), bao, sym)

    tcalpha_ref = np.array([[+1.0, 1-2.5j], [+0.0, +3.4j], [+0.0, 0.2],
                            [-1.0, 1+2.5j], [+0.0, +3.4j], [+0.0, 0.2]])
    tcbeta_ref = np.array([[1-2.0j, 2+3.9j], [+1.0, +0.0], [+0.0, +0.0],
                           [2-4.0j, 2-3.9j], [+1.0, +0.0], [+0.0, +0.0]])
    tcs_ref = [tcalpha_ref, tcbeta_ref]
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcs, tcs_ref))

    tcs = unified_transform(cs, ("K_-_-", None, None), bao, sym)

    tcalpha_ref = np.array([[+1.0, 1-2.5j], [+0.0, +3.4j], [+0.0, 0.2],
                            [-1.0, 1+2.5j], [+0.0, +3.4j], [+0.0, 0.2]])
    tcbeta_ref = np.array([[1-2.0j, 2-3.9j], [+1.0, +0.0], [+0.0, +0.0],
                           [2-4.0j, 2+3.9j], [+1.0, +0.0], [+0.0, +0.0]])
    tcs_ref = [tcalpha_ref, tcbeta_ref]
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcs, tcs_ref))


def test_unified_transform_time_reversal():
    r"""Tests for the time reversal of coefficients using
    :func:`~src.analyticaltools.symmetry.symmetry_transformation.unified_transform`.
    """
    # pylint: disable=C0103
    # C0103: The variables are named according to their mathematical symbols.
    bao = [("C", [("P", True, None)]),
           ("C", [("P", True, None)])]

    mol = Molecule([Atom("C", (+1, +0, +0)),
                    Atom("C", (+0, +0, +0))])
    mol.recentre_ip()
    sym = Symmetry(mol)
    sym.analyse()

    calpha = np.array([[+1.0, 1+2.5j], [+0.0, -3.4j], [+0.0, 0.2],
                       [-1.0, 1-2.5j], [+0.0, -3.4j], [+0.0, 0.2]])
    cbeta = np.array([[1+2.0j, 2+3.9j], [+1.0, +0.0], [+0.0, +0.0],
                      [2+4.0j, 2-3.9j], [+1.0, +0.0], [+0.0, +0.0]])
    cgen = scipy.linalg.block_diag(calpha, cbeta)
    cs = [cgen]
    tcs = unified_transform(cs, ("T_-_-", None, None), bao, sym)
    tcs_ref = [np.block([[np.zeros((6, 2)), -cbeta.conj()],
                         [calpha.conj(), np.zeros((6, 2))]])]
    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for c, c_ref in zip(tcs, tcs_ref))


def test_gen_symequiv():
    r"""Tests for the generation of multiple symmetry-equivalent states using
    composite symmetry operations.
    """
    # pylint: disable=R0914,C0103
    # R0904: Having more variables makes the test more understandable.
    # C0103: The variables are named according to their mathematical symbols.
    bao = [("C", [("S", False, None), ("P", True, None)]),
           ("C", [("S", False, None), ("P", True, None)]),
           ("C", [("S", False, None), ("P", True, None)]),
           ("C", [("S", False, None), ("P", True, None)])]

    mol = Molecule([Atom("C", (+1, +1, +0)),
                    Atom("C", (-1, +1, +0)),
                    Atom("C", (-1, -1, +0)),
                    Atom("C", (+1, -1, +0))])
    mol.recentre_ip()
    sym = Symmetry(mol)
    sym.analyse()

    calpha = np.array([[+1.0, +0.0], [+0.0, +0.0], [+0.0, +0.0], [+0.0, +1.0],
                       [+0.0, +0.0], [+0.0, +0.0], [+0.0, +0.0], [+0.0, +0.0],
                       [-1.0, +0.0], [+0.0, +0.0], [+0.0, +0.0], [+0.0, -1.0],
                       [+0.0, +0.0], [+0.0, +0.0], [+0.0, +0.0], [+0.0, +0.0]])
    cbeta = np.array([[+0.0, +0.0], [+0.0, +1.0], [+0.0, +0.0], [+0.0, +0.0],
                      [+1.0, +0.0], [+0.0, +0.0], [+0.0, +1.0], [+0.0, +0.0],
                      [+0.0, +0.0], [+0.0, -1.0], [+0.0, +0.0], [+0.0, +0.0],
                      [-1.0, +0.0], [+0.0, +0.0], [+0.0, -1.0], [+0.0, +0.0]])
    cgens = [scipy.linalg.block_diag(calpha, cbeta)]

    comp_sym_ops = [[("C_e_-", (4, 0, 1), None)],
                    [("C_e_-", (4, 0, 1), None), ("S_e_-", (1, 2, 1), None)],
                    [("C_e_-", (4, 0, 1), None), ("T_-_-", None, None)],
                    [("T_-_-", None, None), ("T_-_-", None, None)]]

    tcss = get_symequiv(cgens, comp_sym_ops, bao, sym)

    tcss_ref = []

    tcalpha0 = np.array([[+0.0, +0.0], [+0.0, +0.0], [+0.0, +0.0], [+0.0, +0.0],
                         [+1.0, +0.0], [+0.0, +0.0], [+0.0, +0.0], [+0.0, +1.0],
                         [+0.0, +0.0], [+0.0, +0.0], [+0.0, +0.0], [+0.0, +0.0],
                         [-1.0, +0.0], [+0.0, +0.0], [+0.0, +0.0], [+0.0, -1.0]])
    tcbeta0 = np.array([[-1.0, +0.0], [+0.0, +1.0], [+0.0, +0.0], [+0.0, +0.0],
                        [+0.0, +0.0], [+0.0, +0.0], [+0.0, +1.0], [+0.0, +0.0],
                        [+1.0, +0.0], [+0.0, -1.0], [+0.0, +0.0], [+0.0, +0.0],
                        [+0.0, +0.0], [+0.0, +0.0], [+0.0, -1.0], [+0.0, +0.0]])
    cgen0s = [scipy.linalg.block_diag(tcalpha0, tcbeta0)]
    tcss_ref.append(cgen0s)

    tcalpha1 = calpha
    tcbeta1 = np.array([[+0.0, +0.0], [+0.0, +0.0], [+0.0, +1.0], [+0.0, +0.0],
                        [-1.0, +0.0], [+0.0, -1.0], [+0.0, +0.0], [+0.0, +0.0],
                        [+0.0, +0.0], [+0.0, +0.0], [+0.0, -1.0], [+0.0, +0.0],
                        [+1.0, +0.0], [+0.0, +1.0], [+0.0, +0.0], [+0.0, +0.0]])
    cgen1s = [scipy.linalg.block_diag(tcalpha1, tcbeta1)]
    tcss_ref.append(cgen1s)

    cgen2s = [np.block([[np.zeros((16, 2)), -tcbeta0],
                        [tcalpha0, np.zeros((16, 2))]])]
    tcss_ref.append(cgen2s)

    cgen3s = [-c for c in cgens]
    tcss_ref.append(cgen3s)

    assert all(np.allclose(c, c_ref, rtol=0, atol=COMMON_ZERO_TOLERANCE)
               for tcs, tcs_ref in zip(tcss, tcss_ref)
               for c, c_ref in zip(tcs, tcs_ref))
