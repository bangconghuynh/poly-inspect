""":mod:`.test_groups` contains unit tests for functions implemented in
:mod:`analyticaltools.symmetry.groups`.
"""

from textwrap import dedent
import numpy as np  # type: ignore
from polyinspect.analyticaltools.symmetry.groups import ss, SymmetrySymbol,\
        CharacterTable, chartab_cyclic, direct_product


def test_symmetry_symbol() -> None:
    r"""Tests for the construction of symmetry symbols.
    """
    eg_sym = ss("|E|_(g)")
    assert eg_sym.latex_str == r"E_{g}"

    at1g = ss("^(A)|T|_(1g)")
    assert repr(at1g) == "^(A)|T|_(1g)"
    assert at1g.main == "T"
    assert at1g.presuper == "A"
    assert at1g.presub == ""
    assert at1g.postsuper == ""
    assert at1g.postsub == "1g"

    abeg2 = ss("^(A)_(B)|E|_(g)^(2)")
    abeg2_alt = ss("^(A)_(B)|E|^(2)_(g)")
    assert repr(abeg2) == "^(A)_(B)|E|^(2)_(g)"
    assert abeg2 == abeg2_alt
    assert hash(abeg2) == hash(abeg2_alt)
    assert abeg2.main == "E"
    assert abeg2.presuper == "A"
    assert abeg2.presub == "B"
    assert abeg2.postsuper == "2"
    assert abeg2.postsub == "g"

    abeg2.main = "T"
    abeg2.presuper = "3"
    abeg2.presub = "2"
    abeg2.postsuper = "4"
    abeg2.postsub = "u"
    assert repr(abeg2) == "^(3)_(2)|T|^(4)_(u)"
    assert abeg2.latex_str == r"\prescript{3}{2}{T}^{4}_{u}"


def test_chartab_construction() -> None:
    r"""Tests for the construction of character tables.
    """
    ccsg = (("E", 1), ("T", 1))
    irreps = (ss("A"), ss("B"))
    char_arr = np.array([[+1, +1],
                         [+1, -1]])
    trev = CharacterTable.from_ndarray("T", ccsg, irreps, char_arr)
    assert trev[ss("A"), "E"] == 1
    assert trev[ss("B"), "T"] == -1

    trev_repr = """\
        ━━━━━━━━━━━━━━━━━━━━━━━━━
          T   │     1 E │     1 T
        ┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈
         |A|  │  +1.000 │  +1.000
         |B|  │  +1.000 │  -1.000
        ━━━━━━━━━━━━━━━━━━━━━━━━━\n"""
    trev_repr = dedent(trev_repr)
    assert str(trev) == trev_repr

    trev2 = CharacterTable("T", ccsg)
    assert trev2.name == "T"
    trev2.name = "T2"
    assert trev2.name == "T2"
    assert trev2.n_irreps == 0
    trev2.add_irrep(ss("A"), (1, 1))
    assert trev2.n_irreps == 1
    trev2.add_irrep(ss("B"), (1, -1))
    assert trev2.n_irreps == 2
    assert trev2[ss("B"), "T"] == -1


def test_chartab_cyclic() -> None:
    r"""Tests for the construction of character tables for cyclic groups.
    """
    # pylint: disable=C0103
    # C0103: Variable names are chosen to match their mathematical symbols.

    C7 = chartab_cyclic(7, double=False)
    assert C7.ccsg == (("E", 1), ("C_7^1", 1), ("C_7^2", 1), ("C_7^3", 1),
                       ("C_7^4", 1), ("C_7^5", 1), ("C_7^6", 1))
    assert C7.ccs == ("E", "C_7^1", "C_7^2", "C_7^3",
                      "C_7^4", "C_7^5", "C_7^6")
    g3_irrep = SymmetrySymbol("Gamma", postsub="3")
    assert C7[g3_irrep, "C_7^4"] == np.exp(2j*np.pi*3*4/7)

    C7star = chartab_cyclic(7, double=True)
    assert C7star.ccsg == (("E", 1), ("C_7^1", 1), ("C_7^2", 1), ("C_7^3", 1),
                           ("C_7^4", 1), ("C_7^5", 1), ("C_7^6", 1), ("Q", 1),
                           ("C_7^8", 1), ("C_7^9", 1), ("C_7^10", 1),
                           ("C_7^11", 1), ("C_7^12", 1), ("C_7^13", 1))
    assert C7star.ccs == ("E", "C_7^1", "C_7^2", "C_7^3",
                          "C_7^4", "C_7^5", "C_7^6", "Q",
                          "C_7^8", "C_7^9", "C_7^10",
                          "C_7^11", "C_7^12", "C_7^13")
    b_irrep = SymmetrySymbol("B")
    g5_irrep = SymmetrySymbol("Gamma", postsub="5")
    assert C7star[b_irrep] == tuple((-1)**k for k in range(2*7))
    assert C7star[g5_irrep, "C_7^11"] == np.exp(2j*np.pi*5*11/14)


def test_chartab_directproduct() -> None:
    r"""Tests for the construction of the direct product of character tables.
    """
    # pylint: disable=C0103
    # C0103: Variable names are chosen to match their mathematical symbols.
    ccsg = (("E", 1), ("T", 1))
    irreps = (ss("A"), ss("B"))
    char_arr = np.array([[+1, +1],
                         [+1, -1]])
    trev = CharacterTable.from_ndarray("T", ccsg, irreps, char_arr)

    C3 = chartab_cyclic(3, double=False)
    print("")
    print(C3)
    tC3 = direct_product("tC3", trev, C3)
    assert len(tC3.irreps) == 6
    assert len(tC3.ccs) == 6
    bg1 = tC3.irreps[4]
    assert bg1 == ss("^(B)|Gamma|_(1)")
    assert tC3[bg1, "T.E"] == -1
    print(tC3)
