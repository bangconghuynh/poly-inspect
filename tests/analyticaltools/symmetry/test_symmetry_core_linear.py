""":mod:`.test_symmetry_core_linear` contains unit tests for the classes and
functions implemented in :mod:`analyticaltools.symmetry.symmetry_core`.
These unit tests involve linear molecules.
"""

# pylint: disable=W0212
# W0212: Accessing _check_proper and _check_improper is needed to test them.

from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE
from polyinspect.auxiliary.geometrical_space import Vector
from polyinspect.auxiliary.chemical_structures import Atom, Molecule
from polyinspect.analyticaltools.symmetry.symmetry_core import Symmetry,\
        ROTSYM_SYMTOP_PROLATE_LINEAR


def test_symmetry_h2():
    """Tests for the symmetry analysis of H2.
    """
    mol_h2 = Molecule([Atom("H", (0, 0, -2)),
                       Atom("H", (0, 0, +2))])
    mol_h2.recentre_ip()
    mol_h2.reorientate_ip()
    sym = Symmetry(mol_h2,
                   moi_thresh=1e7*COMMON_ZERO_TOLERANCE,
                   dist_thresh=1e7*COMMON_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_PROLATE_LINEAR
    assert len(sym.sea_groups) == 1
    assert sym._check_proper(5, Vector([0, 0, 1]))
    assert sym._check_proper(2, Vector([1, 1, 0]))
    assert not sym._check_proper(3, Vector([2, 1, 0]))
    assert not sym._check_proper(5, Vector([-1, 1, 0]))
    assert sym._check_improper(10, Vector([0, 0, 1]))
    assert sym._check_improper(2, Vector([2, 1, 0]))
    assert not sym._check_improper(4, Vector([1, 1, 0]))
    assert not sym._check_improper(6, Vector([0, 1, 0]))
    assert sym.point_group == "D∞h"
    assert len(sym.improper_elements[2]) == 1
    assert sym.proper_generators[-1][0] == Vector([0, 0, 1])
    assert sym.proper_generators[2][0] == Vector([0, 1, 0])
    assert sym.improper_generators[1][0] == Vector([0, 0, 1])
    assert sym.sigma_generators['h'][0] == Vector([0, 0, 1])
    assert not sym._add_improper(1, Vector([0, 0, -1]), generator=True)

    assert sym.proper_permutation(10, axis=Vector([0, 0, 1])) == [0, 1]
    assert sym.proper_permutation(2, axis=Vector([0, 1, 0])) == [1, 0]


def test_symmetry_no():
    """Tests for the symmetry analysis of NO.
    """
    mol_no = Molecule([Atom("N", (0, -4.4, 0)),
                       Atom("O", (0, 0, +3.2))])
    mol_no.recentre_ip()
    mol_no.reorientate_ip()
    sym = Symmetry(mol_no,
                   moi_thresh=1e7*COMMON_ZERO_TOLERANCE,
                   dist_thresh=1e7*COMMON_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_PROLATE_LINEAR
    assert len(sym.sea_groups) == 2
    assert sym._check_proper(5, Vector([0, 0, 1]))
    assert not sym._check_proper(2, Vector([1, 1, 0]))
    assert not sym._check_improper(10, Vector([0, 0, 1]))
    assert not sym._check_improper(2, Vector([2, 1, 0]))
    assert sym.point_group == "C∞v"
    assert 2 not in sym.improper_elements
    assert sym.proper_generators[-1][0] == Vector([0, 0, 1])
    assert sym.improper_generators[1][0] == Vector([0, 1, 0])
    assert sym.sigma_generators['v'][0] == Vector([0, 1, 0])


def test_symmetry_hcch():
    """Tests for the symmetry analysis of HCCH.
    """
    mol_hcch = Molecule([Atom("H", (0, 0, 2)),
                         Atom("C", (0, 0, 3)),
                         Atom("C", (0, 0, 4)),
                         Atom("H", (0, 0, 5))])
    mol_hcch.recentre_ip()
    mol_hcch.reorientate_ip()
    sym = Symmetry(mol_hcch,
                   moi_thresh=1e7*COMMON_ZERO_TOLERANCE,
                   dist_thresh=1e7*COMMON_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_PROLATE_LINEAR
    assert len(sym.sea_groups) == 2
    assert sym._check_proper(5, Vector([0, 0, 1]))
    assert sym._check_proper(2, Vector([1, 1, 0]))
    assert sym._check_improper(10, Vector([0, 0, 1]))
    assert sym._check_improper(2, Vector([2, 1, 0]))
    assert sym.point_group == "D∞h"
    assert sym.proper_generators[-1][0] == Vector([0, 0, 1])
    assert sym.proper_generators[2][0] == Vector([0, 1, 0])
    assert len(sym.improper_elements[2]) == 1
    assert sym.improper_generators[1][0] == Vector([0, 0, 1])
    assert sym.sigma_generators['h'][0] == Vector([0, 0, 1])
    assert sym.max_proper_order == -1

    assert sym.proper_permutation(2, axis=Vector([0, 1, 0])) == [3, 2, 1, 0]


def test_symmetry_scn():
    """Tests for the symmetry analysis of [SCN]-.
    """
    mol_scn = Molecule([Atom("S", (-2, 0, 0)),
                        Atom("C", (+0, 0, 0)),
                        Atom("N", (+1, 0, 0))])
    mol_scn.recentre_ip()
    mol_scn.reorientate_ip()
    sym = Symmetry(mol_scn,
                   moi_thresh=1e7*COMMON_ZERO_TOLERANCE,
                   dist_thresh=1e7*COMMON_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_PROLATE_LINEAR
    assert len(sym.sea_groups) == 3
    assert sym._check_proper(5, Vector([0, 0, 1]))
    assert not sym._check_proper(2, Vector([1, 1, 0]))
    assert not sym._check_improper(10, Vector([0, 0, 1]))
    assert not sym._check_improper(2, Vector([2, 1, 0]))
    assert sym.point_group == "C∞v"
    assert 2 not in sym.improper_elements
    assert sym.proper_generators[-1][0] == Vector([0, 0, 1])
    assert sym.improper_generators[1][0] == Vector([0, 1, 0])
    assert sym.sigma_generators['v'][0] == Vector([0, 1, 0])
    assert sym.max_proper_order == -1
