""":mod:`.test_angmom3d` contains unit tests for functions implemented in
:mod:`analyticaltools.symmetry.angmom3d`.
"""

import numpy as np  # type: ignore
from hypothesis import given, assume, settings, strategies as st
from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE,\
        LOOSE_ZERO_TOLERANCE
from polyinspect.auxiliary.geometrical_space import Vector
from polyinspect.analyticaltools.symmetry.angmom3d import Rmat, Rlmat, Dmat_Euler,\
        Dmat_angleaxis, Dmat_Euler_gen, Dmat_angleaxis_gen, _angleaxis_to_Euler


def test_Rlmat() -> None:
    r"""Tests for the representation matrix :math:`\boldsymbol{R}` for a
    rotation in the basis of the coordinate *functions* :math:`(x, y, z)`.
    """
    # pylint: disable=C0103
    # C0103: Variables and functions are named in accordance with their
    #        mathematical symbols.

    R = Rmat(np.pi/2, Vector([0, 0, 1]))
    R2 = Rlmat(2, R, R)
    R3 = Rlmat(3, R, R2)
    assert np.allclose(R2,
                       np.array([[-1,  0, 0, 0,  0],
                                 [ 0,  0, 0, 1,  0],
                                 [ 0,  0, 1, 0,  0],
                                 [ 0, -1, 0, 0,  0],
                                 [ 0,  0, 0, 0, -1]]),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)
    assert np.allclose(R3,
                       np.array([[0,  0,  0, 0, 0,  0, -1],
                                 [0, -1,  0, 0, 0,  0,  0],
                                 [0,  0,  0, 0, 1,  0,  0],
                                 [0,  0,  0, 1, 0,  0,  0],
                                 [0,  0, -1, 0, 0,  0,  0],
                                 [0,  0,  0, 0, 0, -1,  0],
                                 [1,  0,  0, 0, 0,  0,  0]]),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)

    R = Rmat(2*np.pi/3, Vector([1, 1, 1]))
    R2 = Rlmat(2, R, R)
    sq3 = np.sqrt(3)
    assert np.allclose(R2,
                       np.array([[0, 0,     0, 1,      0],
                                 [1, 0,     0, 0,      0],
                                 [0, 0,  -0.5, 0, -sq3/2],
                                 [0, 1,     0, 0,      0],
                                 [0, 0, sq3/2, 0,   -0.5]]),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)


STFLOATS = st.floats(allow_nan=False, allow_infinity=False)
@given(STFLOATS, STFLOATS, STFLOATS)
def test_Dmat_Euler(alpha: float, beta: float, gamma: float) -> None:
    r"""Tests for the Wigner rotation matrix for :math:`j = 1/2` parametrised
    by Euler angles.
    """
    # pylint: disable=C0103
    # C0103: Variables and functions are named in accordance with their
    #        mathematical symbols.
    D1 = Dmat_Euler((0, 0, 2*np.pi))
    assert np.allclose(D1,
                       np.array([[-1, 0],
                                 [0, -1]]),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)
    D2 = Dmat_Euler((0, 2*np.pi, 0))
    assert np.allclose(D2,
                       np.array([[-1, 0],
                                 [0, -1]]),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)
    D3 = Dmat_Euler((2*np.pi, 0, 0))
    assert np.allclose(D3,
                       np.array([[-1, 0],
                                 [0, -1]]),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)

    D4 = Dmat_Euler((alpha, beta, gamma))
    assert\
        np.allclose(D4,
                    np.array([[np.exp(-1j/2*alpha-1j/2*gamma)*np.cos(beta/2),
                               -np.exp(-1j/2*alpha+1j/2*gamma)*np.sin(beta/2)],
                              [np.exp(1j/2*alpha-1j/2*gamma)*np.sin(beta/2),
                               np.exp(1j/2*alpha+1j/2*gamma)*np.cos(beta/2)]]),
                    rtol=0, atol=COMMON_ZERO_TOLERANCE)


def test_Dmat_angleaxis() -> None:
    r"""Tests for the Wigner rotation matrix for :math:`j = 1/2` parametrised
    by angle and axis of rotation.
    """
    # pylint: disable=C0103
    # C0103: Variables and functions are named in accordance with their
    #        mathematical symbols.
    D1 = Dmat_angleaxis(2*np.pi, Vector([0.5, 0.2, 0.1]))
    assert np.allclose(D1,
                       np.array([[-1, 0],
                                 [0, -1]]),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)

    D2ax = Dmat_angleaxis(2*np.pi/5, Vector([0, 1, 0]))
    D2Eu = Dmat_Euler((0, 2*np.pi/5, 0))
    assert np.allclose(D2ax, D2Eu, rtol=0, atol=COMMON_ZERO_TOLERANCE)

    D3ax = Dmat_angleaxis(2*np.pi/7, Vector([0, 0, 1]))
    D3Eugamma = Dmat_Euler((0, 0, 2*np.pi/7))
    D3Eualpha = Dmat_Euler((2*np.pi/7, 0, 0))
    assert np.allclose(D3ax, D3Eugamma, rtol=0, atol=COMMON_ZERO_TOLERANCE)
    assert np.allclose(D3ax, D3Eualpha, rtol=0, atol=COMMON_ZERO_TOLERANCE)


@given(STFLOATS, STFLOATS, STFLOATS)
def test_Dmat_Euler_gen(alpha: float, beta: float, gamma: float) -> None:
    r"""Tests for the Wigner rotation matrix for an integral or half-integral
    :math:`j` parametrised by Euler angles.
    """
    # pylint: disable=C0103
    # C0103: Variables and functions are named in accordance with their
    #        mathematical symbols.
    D1 = Dmat_Euler((0, 0, 2*np.pi))
    D1gen = Dmat_Euler_gen(0.5, (0, 0, 2*np.pi))
    assert np.allclose(D1, D1gen, rtol=0, atol=COMMON_ZERO_TOLERANCE)

    D2 = Dmat_Euler((alpha, beta, gamma))
    D2gen = Dmat_Euler_gen(0.5, (alpha, beta, gamma))
    assert np.allclose(D2, D2gen, rtol=0, atol=COMMON_ZERO_TOLERANCE)


def test_angleaxis_to_Euler():
    r"""Tests for the conversion between the angle-axis parametrisation and the
    Euler angle parametrisation of a rotation, taking into account elements in
    :math:`\mathsf{SU}(2)`.
    """
    # pylint: disable=C0103
    # C0103: Variables and functions are named in accordance with their
    #        mathematical symbols.
    Euler_angles = _angleaxis_to_Euler(2.3*np.pi, Vector([0, 0, 1]))
    assert np.allclose(Euler_angles, (0, 0, 2.3*np.pi),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)

    Euler_angles = _angleaxis_to_Euler(2.3*np.pi, Vector([0, 1, 0]))
    assert np.allclose(Euler_angles, (0, 0.3*np.pi, 2*np.pi),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)

    Euler_angles = _angleaxis_to_Euler(3*np.pi, Vector([0, 1, 0]))
    assert np.allclose(Euler_angles, (0, np.pi, 2*np.pi),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)

    Euler_angles = _angleaxis_to_Euler(np.pi, Vector([-1/2, -np.sqrt(3)/2, 0]))
    assert np.allclose(Euler_angles, (0, np.pi, 7*np.pi/3),
                       rtol=0, atol=LOOSE_ZERO_TOLERANCE)

    omega = np.arccos(1/np.sqrt(3))
    nx = np.sin(np.pi-omega)*np.cos(3*np.pi/4)
    ny = np.sin(np.pi-omega)*np.sin(3*np.pi/4)
    nz = np.cos(np.pi-omega)
    Euler_angles = _angleaxis_to_Euler(10/3*np.pi, Vector([nx, ny, nz]))
    assert np.allclose(Euler_angles, (3/2*np.pi, 1/2*np.pi, 3*np.pi),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)


def test_Dmat_angleaxis_gen_small():
    r"""Tests for the generation of Wigner rotation matrices in the angle-axis
    parametrisation.

    For :math:`j = \frac{q}{2}`, we must make sure that
    :math:`\boldsymbol{D}^{(j)}(\phi, \hat{\boldsymbol{n}}) =
    (-1)^{q}\boldsymbol{D}^{(j)}(\phi+2\pi, \hat{\boldsymbol{n}})`.

    In this small test, we consider :math:`q = 0, 1, 2` only.
    """
    # pylint: disable=C0103
    # C0103: Variables and functions are named in accordance with their
    #        mathematical symbols.

    ###############
    #### j = 0 ####
    ###############
    D1 = Dmat_angleaxis_gen(0, 0.3*np.pi, Vector([0, 0, 1]))
    D2 = Dmat_angleaxis_gen(0, 2.3*np.pi, Vector([0, 0, 1]))
    assert np.allclose(D1, D2, rtol=0, atol=COMMON_ZERO_TOLERANCE)


    ###############
    ### j = 1/2 ###
    ###############
    D1 = Dmat_angleaxis_gen(0.5, 0.3*np.pi, Vector([0, 0, 1]))
    D2 = Dmat_angleaxis_gen(0.5, 2.3*np.pi, Vector([0, 0, 1]))
    assert np.allclose(D1, -D2, rtol=0, atol=COMMON_ZERO_TOLERANCE)

    D3 = Dmat_angleaxis_gen(0.5, 0.3*np.pi, Vector([0, 1, 0]))
    D4 = Dmat_angleaxis_gen(0.5, 2.3*np.pi, Vector([0, 1, 0]))
    assert np.allclose(D3, -D4, rtol=0, atol=COMMON_ZERO_TOLERANCE)

    D5 = Dmat_angleaxis_gen(0.5, np.pi, Vector([0, 1, 0]))
    D6 = Dmat_angleaxis_gen(0.5, 3*np.pi, Vector([0, 1, 0]))
    assert np.allclose(D5, -D6, rtol=0, atol=COMMON_ZERO_TOLERANCE)

    D7 = Dmat_angleaxis_gen(0.5, np.pi, Vector([-1/2, -np.sqrt(3)/2, 0]))
    D8 = Dmat_angleaxis_gen(0.5, 3*np.pi, Vector([-1/2, -np.sqrt(3)/2, 0]))
    assert np.allclose(D7, -D8, rtol=0, atol=COMMON_ZERO_TOLERANCE)

    D10 = Dmat_angleaxis_gen(0.5, 0.9*np.pi, Vector([1, 1, 1]))
    D11 = Dmat_angleaxis_gen(0.5, 2.9*np.pi, Vector([1, 1, 1]))
    D12 = Dmat_angleaxis_gen(0.5, 4.9*np.pi, Vector([1, 1, 1]))
    D13 = Dmat_angleaxis_gen(0.5, -0.9*np.pi, Vector([1, 1, 1]))
    D13p = Dmat_angleaxis_gen(0.5, 1.1*np.pi, Vector([1, 1, 1]))
    D14 = Dmat_angleaxis_gen(0.5, -2.9*np.pi, Vector([1, 1, 1]))
    assert np.allclose(D10, -D11, rtol=0, atol=COMMON_ZERO_TOLERANCE)
    assert np.allclose(D10, D12, rtol=0, atol=COMMON_ZERO_TOLERANCE)
    assert np.allclose(D13, -D13p, rtol=0, atol=COMMON_ZERO_TOLERANCE)
    assert np.allclose(np.dot(D10, D13), np.identity(2),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)
    assert np.allclose(np.dot(D11, D14), np.identity(2),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)


STINTS = st.integers(min_value=0, max_value=20)
STFLOATS_SMALL = st.floats(min_value=-30.0, max_value=30.0, allow_nan=False)
@given(STINTS, STFLOATS_SMALL, STINTS, STINTS, STINTS)
@settings(deadline=500)
def test_Dmat_angleaxis_gen_large(q: int, factor: float,
                                  nx: int, ny: int, nz: int):
    r"""Tests for the generation of Wigner rotation matrices in the angle-axis
    parametrisation.

    For :math:`j = \frac{q}{2}`, we must make sure that
    :math:`\boldsymbol{D}^{(j)}(\phi, \hat{\boldsymbol{n}}) =
    (-1)^{q}\boldsymbol{D}^{(j)}(\phi+2\pi, \hat{\boldsymbol{n}})`.

    In this large test, we consider :math:`q = 0, 1, \ldots, 20`.
    """
    # pylint: disable=C0103
    # C0103: Variables and functions are named in accordance with their
    #        mathematical symbols.
    #################
    #### j = q/2 ####
    #################
    assume(not(nx == 0 and ny == 0 and nz == 0))
    D1 = Dmat_angleaxis_gen(q/2, factor*np.pi, Vector([nx, ny, nz]))
    D2 = Dmat_angleaxis_gen(q/2, (factor+2)*np.pi, Vector([nx, ny, nz]))
    assert np.allclose(D1, (-1)**q*D2, rtol=0, atol=100*COMMON_ZERO_TOLERANCE)
