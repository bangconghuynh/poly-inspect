""":mod:`.test_symmetry_analysis` contains unit tests for functions implemented
in :mod:`analyticaltools.symmetry.symmetry_analysis`.
"""

import os
from collections import OrderedDict
import numpy as np  # type: ignore

from polyinspect.auxiliary.chemical_structures import Atom, Molecule

from polyinspect.analyticaltools.symmetry.symmetry_core import Symmetry
from polyinspect.analyticaltools.symmetry.symmetry_transformation import\
        get_symequiv
from polyinspect.analyticaltools.symmetry.symmetry_analysis import get_Dmat,\
        get_rep, get_rep_noci, print_rep, _reduce_rep
from polyinspect.analyticaltools.symmetry.groups import CharacterTable, ss



# pylint: disable=C0301
# C0301: Extra spaces are introduced to align table elements.
ccsg =                      [("E", 1), ("C2z", 1), ("C2y", 1), ("C2x", 1), ("i", 1), ("sxy", 1), ("sxz", 1), ("syz", 1)]
chartab = [(ss("|A|_(g)"),  [       1,          1,          1,          1,        1,          1,          1,          1]),
           (ss("|B|_(1g)"), [       1,          1,         -1,         -1,        1,          1,         -1,         -1]),
           (ss("|B|_(2g)"), [       1,         -1,          1,         -1,        1,         -1,          1,         -1]),
           (ss("|B|_(3g)"), [       1,         -1,         -1,          1,        1,         -1,         -1,          1]),
           (ss("|A|_(u)" ), [       1,          1,          1,          1,       -1,         -1,         -1,         -1]),
           (ss("|B|_(1u)"), [       1,          1,         -1,         -1,       -1,         -1,          1,          1]),
           (ss("|B|_(2u)"), [       1,         -1,          1,         -1,       -1,          1,         -1,          1]),
           (ss("|B|_(3u)"), [       1,         -1,         -1,          1,       -1,          1,          1,         -1])]

d2h_chartab = CharacterTable("D2h", ccsg, OrderedDict(chartab))


def test_H4_sto3g_D2h():
    """Tests for symmetry analysis of several solutions of H4 in STO-3G
    in D2h molecular symmetry.
    """
    # pylint: disable=C0103
    # C0103: Variable and function names are chosen to resemble their
    #        mathematical symbols.

    bao = [("H", [("S", False, None)]),
           ("H", [("S", False, None)]),
           ("H", [("S", False, None)]),
           ("H", [("S", False, None)])]

    mol = Molecule([Atom("H", (+2, +1, +0)),
                    Atom("H", (-2, +1, +0)),
                    Atom("H", (-2, -1, +0)),
                    Atom("H", (+2, -1, +0))])
    mol.recentre_ip()
    sym = Symmetry(mol)
    sym.analyse()

    ca = np.array([[1+1j, -1+1j, 1-1j, -1-1j]]).T
    cb = np.array([[-1-1j, 1-1j, 1-1j, -1-1j]]).T
    cs = [ca, cb]

    sao = np.array([[1.00, 0.50, 0.25, 0.75],
                    [0.50, 1.00, 0.75, 0.25],
                    [0.25, 0.75, 1.00, 0.50],
                    [0.25, 0.25, 0.50, 0.75]])

    tcomp_sym_ops = [[("E_-_-", None, None)],
                     [("C_e_-", (2, 0, 1), None)],
                     [("C_e_-", (2, 1, 1), None)],
                     [("C_e_-", (2, 2, 1), None)],
                     [("SF_-_-", None, None)],
                     [("SF_-_-", None, None), ("C_e_-", (2, 0, 1), None)],
                     [("SF_-_-", None, None), ("C_e_-", (2, 1, 1), None)],
                     [("SF_-_-", None, None), ("C_e_-", (2, 2, 1), None)]]

    acomp_sym_ops = [[("E_-_-", None, None)],
                     [("C_e_-", (2, 0, 1), None)],
                     [("C_e_-", (2, 1, 1), None)],
                     [("C_e_-", (2, 2, 1), None)],
                     [("S_e_-", (2, 0, 1), None)],
                     [("S_e_-", (1, 0, 1), None)],
                     [("S_e_-", (1, 1, 1), None)],
                     [("S_e_-", (1, 2, 1), None)]]

    css = get_symequiv(cs, tcomp_sym_ops, bao, sym)
    rep = get_rep(css, acomp_sym_ops, bao, sym, sao, d2h_chartab, False)
    assert rep[0] == (2, ss("|B|_(1g)"))
    assert rep[1] == (2, ss("|B|_(2u)"))
    rep = get_rep(css, acomp_sym_ops, bao, sym, sao, d2h_chartab, True)
    assert rep[0] == (2, ss("|B|_(1g)"))
    assert rep[1] == (2, ss("|B|_(2u)"))

    chars_alt = [np.trace(get_Dmat(css, acomp_sym_op, bao, sym, sao, True))
                 for acomp_sym_op in acomp_sym_ops]
    rep_alt = _reduce_rep(chars_alt, d2h_chartab)
    assert rep_alt == rep

    latex_rep = print_rep(rep)
    assert latex_rep == r"2B_{1g} \oplus 2B_{2u}"

    plain_rep = print_rep(rep, fmt="plain")
    assert plain_rep == r"2|B|_(1g) + 2|B|_(2u)"


ccsg =                      [("E", 1), ("C4", 2), ("C4^2", 1), ("C2", 2), ("C2'", 2), ("i", 1), ("S4", 2), ("σh", 1), ("σv", 2), ("σd", 2)]
chartab = [(ss("|A|_(1g)"), [      +1,        +1,          +1,        +1,         +1,       +1,        +1,        +1,        +1,        +1]),
           (ss("|A|_(2g)"), [      +1,        +1,          +1,        -1,         -1,       +1,        +1,        +1,        -1,        -1]),
           (ss("|B|_(1g)"), [      +1,        -1,          +1,        +1,         -1,       +1,        -1,        +1,        +1,        -1]),
           (ss("|B|_(2g)"), [      +1,        -1,          +1,        -1,         +1,       +1,        -1,        +1,        -1,        +1]),
           (ss("|E|_(g)"),  [      +2,        +0,          -2,        +0,         +0,       +2,        +0,        -2,        +0,        +0]),
           (ss("|A|_(1u)"), [      +1,        +1,          +1,        +1,         +1,       -1,        -1,        -1,        -1,        -1]),
           (ss("|A|_(2u)"), [      +1,        +1,          +1,        -1,         -1,       -1,        -1,        -1,        +1,        +1]),
           (ss("|B|_(1u)"), [      +1,        -1,          +1,        +1,         -1,       -1,        +1,        -1,        -1,        +1]),
           (ss("|B|_(2u)"), [      +1,        -1,          +1,        -1,         +1,       -1,        +1,        -1,        +1,        -1]),
           (ss("|E|_(u)"),  [      +2,        +0,          -2,        +0,         +0,       -2,        +0,        +2,        +0,        +0])]

d4h_chartab = CharacterTable("D4h", ccsg, OrderedDict(chartab))


def test_VF63m_sto3g_D4h_noci():
    r"""Tests for symmetry analysis of SCF and NOCI solutions in
    :math:`\textrm{[VF6]}^{3-}.
    """
    # pylint: disable=C0103
    # C0103: Variable and function names are chosen to resemble their
    #        mathematical symbols.

    bao = [
        (
            "V",
            [
                ("S", True, None),
                ("S", True, None),
                ("P", True, None),
                ("S", True, None),
                ("P", True, None),
                ("D", True, None),
                ("S", True, None),
                ("P", True, None),
            ],
        ),
        ("F", [("S", True, None), ("S", True, None), ("P", True, None),]),
        ("F", [("S", True, None), ("S", True, None), ("P", True, None),]),
        ("F", [("S", True, None), ("S", True, None), ("P", True, None),]),
        ("F", [("S", True, None), ("S", True, None), ("P", True, None),]),
        ("F", [("S", True, None), ("S", True, None), ("P", True, None),]),
        ("F", [("S", True, None), ("S", True, None), ("P", True, None),]),
    ]

    mol = Molecule.from_xyz(
        os.path.join(
            os.path.dirname(__file__), "data/vf63-.xyz"
        )
    )
    mol.recentre_ip()
    sym = Symmetry(mol)
    sym.analyse()

    ca = np.genfromtxt(
        os.path.join(
            os.path.dirname(__file__), "data/vf63-_sto3g_o22s1pos_S2_libint_a"
        ), dtype=complex
    )
    cb = np.genfromtxt(
        os.path.join(
            os.path.dirname(__file__), "data/vf63-_sto3g_o22s1pos_S2_libint_b"
        ), dtype=complex
    )
    cs = [ca, cb]

    sao = np.loadtxt(
        os.path.join(
            os.path.dirname(__file__), "data/vf63-_sto3g_o22s1pos_S2_libint_ov"
        )
    )

    tcomp_sym_ops = [
        [("E_-_-", None, None)],
        [("C_e_-", (2, 0, 1), None)],
        [("C_e_-", (2, 1, 1), None)],
        [("C_e_-", (2, 2, 1), None)],
        [("C_e_-", (2, 3, 1), None)],
        [("C_e_-", (2, 4, 1), None)],
        [("C_e_-", (4, 0, 1), None)],
        [("C_e_-", (4, 0, 3), None)],
        [("S_e_-", (1, 0, 1), None)],
        [("S_e_-", (1, 1, 1), None)],
        [("S_e_-", (1, 2, 1), None)],
        [("S_e_-", (1, 3, 1), None)],
        [("S_e_-", (1, 4, 1), None)],
        [("S_e_-", (2, 0, 1), None)],
        [("S_e_-", (4, 0, 1), None)],
        [("S_e_-", (4, 0, 3), None)],
    ]

    acomp_sym_ops = [
        [("E_-_-", None, None)],
        [("C_e_-", (4, 0, 1), None)],
        [("C_e_-", (4, 0, 2), None)],
        [("C_e_-", (2, 1, 1), None)],
        [("C_e_-", (2, 2, 1), None)],
        [("S_e_-", (2, 0, 1), None)],
        [("S_e_-", (4, 0, 1), None)],
        [("S_e_-", (1, 0, 1), None)],
        [("S_e_-", (1, 1, 1), None)],
        [("S_e_-", (1, 2, 1), None)]
    ]

    css = get_symequiv(cs, tcomp_sym_ops, bao, sym)
    rep = get_rep(css, acomp_sym_ops, bao, sym, sao, d4h_chartab, False, thresh=1e-7)
    assert rep == [
        (1, ss("|A|_(1g)")),
        (1, ss("|A|_(2g)")),
        (1, ss("|B|_(1g)")),
        (1, ss("|B|_(2g)")),
        (2, ss("|E|_(g)")),
    ]
    rep = get_rep(css, acomp_sym_ops, bao, sym, sao, d4h_chartab, True, thresh=1e-7)
    assert rep == [
        (1, ss("|A|_(1g)")),
        (1, ss("|A|_(2g)")),
        (1, ss("|B|_(1g)")),
        (1, ss("|B|_(2g)")),
        (2, ss("|E|_(g)")),
    ]

    # The NOCI energies are:
    # -1520.58714039969
    # -1520.58673027860
    # -1520.58673027860
    # -1520.52632267602
    # -1520.52418974833
    # -1520.52141987886
    # -1520.52141987884
    # -1519.13559305386
    noci_coeffs = np.loadtxt(
        os.path.join(
            os.path.dirname(__file__), "data/vf63-_sto3g_o22s1pos_S2_libint_noci"
        )
    )
    noci_degenerate_groups = [[0], [1, 2], [3], [4], [5, 6], [7]]
    noci_degenerate_reps = get_rep_noci(
        css,
        noci_coeffs,
        noci_degenerate_groups,
        acomp_sym_ops,
        bao,
        sym,
        sao,
        d4h_chartab,
        False,
        thresh=1e-7, lindep_thresh=1e-7
    )
    print(noci_degenerate_reps)
    assert noci_degenerate_reps == [
        [(1, ss("|A|_(2g)"))],
        [(1, ss("|E|_(g)"))],
        [(1, ss("|B|_(2g)"))],
        [(1, ss("|A|_(1g)"))],
        [(1, ss("|E|_(g)"))],
        [(1, ss("|B|_(1g)"))],
    ]
    noci_degenerate_reps = get_rep_noci(
        css,
        noci_coeffs,
        noci_degenerate_groups,
        acomp_sym_ops,
        bao,
        sym,
        sao,
        d4h_chartab,
        True,
        thresh=1e-7, lindep_thresh=1e-7
    )
    assert noci_degenerate_reps == [
        [(1, ss("|A|_(2g)"))],
        [(1, ss("|E|_(g)"))],
        [(1, ss("|B|_(2g)"))],
        [(1, ss("|A|_(1g)"))],
        [(1, ss("|E|_(g)"))],
        [(1, ss("|B|_(1g)"))],
    ]
