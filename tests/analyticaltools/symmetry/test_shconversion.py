""":mod:`.test_shconversion` contains unit tests for functions implemented
in :mod:`analyticaltools.symmetry.shconversion`.
"""

import numpy as np  # type: ignore
from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE,\
        STRICT_ZERO_TOLERANCE
from polyinspect.analyticaltools.symmetry.shconversion import complexc,\
        complexcinv, norm_spherical_gaussian,\
        norm_cartesian_gaussian, sh_r2c_mat, sh_c2r_mat,\
        sh_cart2cl_mat, sh_cl2cart_mat, sh_cart2rl_mat,\
        sh_rl2cart_mat, sh_cart2r


def test_complexc():
    r"""Tests for the coefficients in the transformation

    .. math::
        \tilde{g}(\alpha, l, m, l_{\mathrm{cart}}, \boldsymbol{r})
        = \sum_{l_x+l_y+l_z=l_{\mathrm{cart}}}
            c(l, m, l_{\mathrm{cart}}, l_x, l_y, l_z)
            g(\alpha, l_x, l_y, l_z, \boldsymbol{r})

    for both :math:`l = l_{\mathrm{cart}}` and :math:`l < l_{\mathrm{cart}}`,
    including the Condon--Shortley phase.
    """
    # pylint: disable=C0103,R0915
    # C0103: Matrix names are capitalised. Other variable names are chosen to
    #        match their mathematical symbols.


    assert abs(complexc((0, 0), (0, 0, 0)) - 1) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((0, 0), (1, 0, 0)) - 0) < COMMON_ZERO_TOLERANCE


    assert abs(complexc((1, 0), (0, 0, 1)) - 1) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((1, 1), (1, 0, 0)) + 1/np.sqrt(2)) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((1, 1), (0, 1, 0)) + 1j*(1/np.sqrt(2))) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((1, -1), (1, 0, 0)) - 1/np.sqrt(2)) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((1, -1), (0, 1, 0)) + 1j/np.sqrt(2)) < COMMON_ZERO_TOLERANCE

    assert abs(complexc((1, -1), (1, 1, 0)) - 0) < COMMON_ZERO_TOLERANCE


    assert abs(complexc((2, 0), (0, 0, 2)) - 1) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((2, 0), (2, 0, 0)) + 1/2) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((2, 0), (0, 2, 0)) + 1/2) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((2, 1), (1, 0, 1)) + 1/np.sqrt(2)) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((2, 1), (0, 1, 1)) + 1j/np.sqrt(2)) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((2, -1), (1, 0, 1)) - 1/np.sqrt(2)) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((2, -1), (0, 1, 1)) + 1j/np.sqrt(2)) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((2, 2), (2, 0, 0)) - np.sqrt(3/8)) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((2, 2), (0, 2, 0)) + np.sqrt(3/8)) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((2, 2), (1, 1, 0)) - 1j/np.sqrt(2)) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((2, -2), (2, 0, 0)) - np.sqrt(3/8)) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((2, -2), (0, 2, 0)) + np.sqrt(3/8)) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((2, -2), (1, 1, 0)) + 1j/np.sqrt(2)) < COMMON_ZERO_TOLERANCE

    Ntilde = norm_spherical_gaussian(2, 1)
    N = norm_cartesian_gaussian((2, 0, 0), 1)
    assert abs(complexc((0, 0), (2, 0, 0))
               - Ntilde/N*np.sqrt(1/(4*np.pi))) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((0, 0), (0, 2, 0))
               - Ntilde/N*np.sqrt(1/(4*np.pi))) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((0, 0), (0, 0, 2))
               - Ntilde/N*np.sqrt(1/(4*np.pi))) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((2, -2), (0, 0, 0)) - 0) < COMMON_ZERO_TOLERANCE


    assert abs(complexc((3, 0), (0, 0, 3)) - 1) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, 0), (2, 0, 1)) + 3/(2*np.sqrt(5))) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, 0), (0, 2, 1)) + 3/(2*np.sqrt(5))) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, 1), (1, 0, 2)) + np.sqrt(3/5)) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, 1), (0, 1, 2)) + 1j*np.sqrt(3/5)) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, 1), (3, 0, 0)) - np.sqrt(3)/4) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, 1), (0, 3, 0)) - 1j*np.sqrt(3)/4) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, 1), (1, 2, 0)) - np.sqrt(3)/(4*np.sqrt(5)))\
            < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, 1), (2, 1, 0)) - 1j*np.sqrt(3)/(4*np.sqrt(5)))\
            < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, -1), (1, 0, 2)) - np.sqrt(3/5)) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, -1), (0, 1, 2)) + 1j*np.sqrt(3/5)) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, -1), (3, 0, 0)) + np.sqrt(3)/4) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, -1), (0, 3, 0)) - 1j*np.sqrt(3)/4) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, -1), (1, 2, 0)) + np.sqrt(3)/(4*np.sqrt(5)))\
            < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, -1), (2, 1, 0)) - 1j*np.sqrt(3)/(4*np.sqrt(5)))\
            < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, 2), (2, 0, 1)) - np.sqrt(3/8)) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, 2), (0, 2, 1)) + np.sqrt(3/8)) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, 2), (1, 1, 1)) - 1j/np.sqrt(2)) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, 3), (3, 0, 0)) + np.sqrt(5)/4) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, 3), (0, 3, 0)) - 1j*np.sqrt(5)/4) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, 3), (1, 2, 0)) - 3/4) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, 3), (2, 1, 0)) + 3j/4) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, -3), (3, 0, 0)) - np.sqrt(5)/4) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, -3), (0, 3, 0)) - 1j*np.sqrt(5)/4) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, -3), (1, 2, 0)) + 3/4) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, -3), (2, 1, 0)) + 3j/4) < COMMON_ZERO_TOLERANCE

    assert abs(complexc((3, 0), (0, 0, 4)) - 0) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, 0), (0, 0, 2)) - 0) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, 0), (0, 0, 1)) - 0) < COMMON_ZERO_TOLERANCE
    assert abs(complexc((3, 0), (1, 0, 0)) - 0) < COMMON_ZERO_TOLERANCE


def test_complexcinv():
    r"""Tests for the inverse coefficients in the transformation

    .. math::
        g(\alpha, l_x, l_y, l_z, \boldsymbol{r})
        = \sum_{l \le l_{\mathrm{cart}} = l_x+l_y+l_z}
            c^{-1}(l_x, l_y, l_z, l, m, l_{\mathrm{cart}})
            \tilde{g}(\alpha, l, m, l_{\mathrm{cart}}, \boldsymbol{r})

    for both :math:`l = l_{\mathrm{cart}}` and :math:`l < l_{\mathrm{cart}}`,
    including the Condon--Shortley phase.
    """
    # pylint: disable=C0103
    # C0103: Variable names are chosen to match their mathematical symbols.

    assert abs(complexcinv((0, 0, 0), (0, 0)) - 1) < COMMON_ZERO_TOLERANCE
    assert abs(complexcinv((1, 0, 0), (0, 0)) - 0) < COMMON_ZERO_TOLERANCE


    assert abs(complexcinv((0, 0, 1), (1, 0)) - 1) < COMMON_ZERO_TOLERANCE
    assert abs(complexcinv((1, 0, 0), (1, 1)) + 1/np.sqrt(2)) < COMMON_ZERO_TOLERANCE
    assert abs(complexcinv((1, 0, 0), (1, -1)) - 1/np.sqrt(2)) < COMMON_ZERO_TOLERANCE
    assert abs(complexcinv((0, 1, 0), (1, 1)) - 1j/np.sqrt(2)) < COMMON_ZERO_TOLERANCE
    assert abs(complexcinv((0, 1, 0), (1, -1)) - 1j/np.sqrt(2)) < COMMON_ZERO_TOLERANCE

    assert abs(complexcinv((1, 1, 0), (1, -1)) - 0) < COMMON_ZERO_TOLERANCE


    Ntilde = norm_spherical_gaussian(2, 1)
    N = norm_cartesian_gaussian((2, 0, 0), 1)
    assert abs(complexcinv((0, 0, 2), (2, 0)) - 2/3) < COMMON_ZERO_TOLERANCE
    assert abs(complexcinv((0, 0, 2), (0, 0))
               - 1/3*N/Ntilde*np.sqrt(4*np.pi)) < COMMON_ZERO_TOLERANCE
    assert abs(complexcinv((2, 0, 0), (0, 0))
               - 1/3*N/Ntilde*np.sqrt(4*np.pi)) < COMMON_ZERO_TOLERANCE
    assert abs(complexcinv((2, 0, 0), (2, 0)) + 1/3) < COMMON_ZERO_TOLERANCE
    assert abs(complexcinv((2, 0, 0), (2, 2)) - 1/np.sqrt(6)) < COMMON_ZERO_TOLERANCE
    assert abs(complexcinv((2, 0, 0), (2, -2)) - 1/np.sqrt(6)) < COMMON_ZERO_TOLERANCE
    assert abs(complexcinv((0, 2, 0), (0, 0))
               - 1/3*N/Ntilde*np.sqrt(4*np.pi)) < COMMON_ZERO_TOLERANCE
    assert abs(complexcinv((0, 2, 0), (2, 0)) + 1/3) < COMMON_ZERO_TOLERANCE
    assert abs(complexcinv((0, 2, 0), (2, 2)) + 1/np.sqrt(6)) < COMMON_ZERO_TOLERANCE
    assert abs(complexcinv((0, 2, 0), (2, -2)) + 1/np.sqrt(6)) < COMMON_ZERO_TOLERANCE


def test_sh_r2c():
    r"""Tests for the transformation from real to complex spherical harmonics.
    """

    sq2 = np.sqrt(2)
    r2c0 = sh_r2c_mat(0)
    assert r2c0.shape == (1, 1)
    assert r2c0[0, 0] == 1

    r2c1 = sh_r2c_mat(1)
    assert r2c1.shape == (3, 3)
    assert np.allclose(r2c1, np.array([[1j/sq2, 0,  1/sq2],
                                       [0     , 1,  0    ],
                                       [1j/sq2, 0, -1/sq2]]),
                       rtol=0, atol=STRICT_ZERO_TOLERANCE)

    r2c2 = sh_r2c_mat(2)
    assert r2c2.shape == (5, 5)
    assert np.allclose(r2c2,
                       np.array([[ 1j/sq2, 0     ,  0,  0    , 1/sq2],
                                 [ 0     , 1j/sq2,  0,  1/sq2, 0    ],
                                 [ 0     , 0     ,  1,      0, 0    ],
                                 [ 0     , 1j/sq2,  0, -1/sq2, 0    ],
                                 [-1j/sq2, 0     ,  0,  0    , 1/sq2]]),
                       rtol=0, atol=STRICT_ZERO_TOLERANCE)


def test_sh_c2r():
    r"""Tests for the transformation from complex to real spherical harmonics.
    """

    sq2 = np.sqrt(2)
    c2r0 = sh_c2r_mat(0)
    assert c2r0.shape == (1, 1)
    assert c2r0[0, 0] == 1

    c2r1 = sh_c2r_mat(1)
    assert c2r1.shape == (3, 3)
    assert np.allclose(c2r1, np.array([[-1j/sq2, 0, -1j/sq2],
                                       [  0    , 1,   0    ],
                                       [  1/sq2, 0,  -1/sq2]]),
                       rtol=0, atol=STRICT_ZERO_TOLERANCE)

    c2r1_decreasingm = sh_c2r_mat(1, increasingm=False)
    assert c2r1_decreasingm.shape == (3, 3)
    assert np.allclose(c2r1_decreasingm, np.array([[ -1/sq2, 0,   1/sq2],
                                                   [  0    , 1,   0    ],
                                                   [-1j/sq2, 0, -1j/sq2]]),
                       rtol=0, atol=STRICT_ZERO_TOLERANCE)

    c2r2 = sh_c2r_mat(2)
    assert c2r2.shape == (5, 5)
    assert np.allclose(c2r2,
                       np.array([[-1j/sq2,   0    ,  0,   0    , 1j/sq2],
                                 [  0    , -1j/sq2,  0, -1j/sq2,  0    ],
                                 [  0    ,   0    ,  1,   0    ,  0    ],
                                 [  0    ,   1/sq2,  0,  -1/sq2,  0    ],
                                 [  1/sq2,   0    ,  0,   0    ,  1/sq2]]),
                       rtol=0, atol=STRICT_ZERO_TOLERANCE)


def test_sh_cl2cart():
    r"""Tests for the transformation from a complex solid harmonic Gaussian to
    a linear combination of Cartesian Gaussians.
    """
    # pylint: disable=C0103
    # C0103: Matrix names are capitalised. Other variable names are chosen to
    #        match their mathematical symbols.

    Umat00 = sh_cl2cart_mat(0, 0)
    assert Umat00.shape == (1, 1)
    assert abs(Umat00[0, 0] - 1) < COMMON_ZERO_TOLERANCE


    sq2 = np.sqrt(2)
    Umat11 = sh_cl2cart_mat(1, 1)
    assert Umat11.shape == (3, 3)
    assert np.allclose(Umat11, np.array([[  1/sq2, 0,  -1/sq2],
                                         [-1j/sq2, 0, -1j/sq2],
                                         [  0    , 1,   0    ]]),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)
    Vmat11 = sh_cart2cl_mat(1, 1)
    assert np.allclose(np.dot(Vmat11, Umat11), np.identity(3),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)
    Umat11_alt = sh_cl2cart_mat(1, 1,
                                cartorder=[(1, 0, 0), (0, 0, 1), (0, 1, 0)])
    assert np.allclose(Umat11_alt, np.array([[  1/sq2, 0,  -1/sq2],
                                             [  0    , 1,   0    ],
                                             [-1j/sq2, 0, -1j/sq2]]),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)

    Umat21 = sh_cl2cart_mat(2, 1)
    assert np.allclose(Umat21, np.zeros(Umat21.shape),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)

    Vmat22 = sh_cart2cl_mat(2, 2)
    Umat22 = sh_cl2cart_mat(2, 2)
    assert np.allclose(np.dot(Vmat22, Umat22), np.identity(5),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)

    Vmat02 = sh_cart2cl_mat(0, 2)
    Umat20 = sh_cl2cart_mat(2, 0)
    assert np.allclose(np.dot(Vmat02, Umat20), np.identity(1),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)


def test_sh_cart2cl():
    r"""Tests for the transformation from a Cartesian Gaussian to a linear
    combination of complex solid harmonic Gaussians of a certain degree.
    """
    # pylint: disable=C0103
    # C0103: Matrix names are capitalised. Other variable names are chosen to
    #        match their mathematical symbols.

    Vmat00 = sh_cart2cl_mat(0, 0)
    assert Vmat00.shape == (1, 1)
    assert abs(Vmat00[0, 0] - 1) < COMMON_ZERO_TOLERANCE


    sq2 = np.sqrt(2)
    Vmat11 = sh_cart2cl_mat(1, 1)
    assert Vmat11.shape == (3, 3)
    assert np.allclose(Vmat11, np.array([[ 1/sq2, 1j/sq2, 0],
                                         [ 0    ,  0    , 1],
                                         [-1/sq2, 1j/sq2, 0]]),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)

    Vmat01 = sh_cart2cl_mat(0, 1)
    assert Vmat01.shape == (1, 3)
    assert np.allclose(Vmat01, np.zeros((1, 3)),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)


    sq6 = np.sqrt(6)
    Vmat22 = sh_cart2cl_mat(2, 2)
    assert Vmat22.shape == (5, 6)
    assert np.allclose\
            (Vmat22,
             np.array([[ 1/sq6,  1j/sq2,  0    , -1/sq6,  0    , 0  ],
                       [ 0    ,   0    ,  1/sq2,  0    , 1j/sq2, 0  ],
                       [-1/3  ,   0    ,  0    , -1/3  ,  0    , 2/3],
                       [ 0    ,   0    , -1/sq2,  0    , 1j/sq2, 0  ],
                       [ 1/sq6, -1j/sq2,  0    , -1/sq6,  0    , 0  ]]),
             rtol=0, atol=COMMON_ZERO_TOLERANCE)

    Vmat02 = sh_cart2cl_mat(0, 2)
    Ntilde = norm_spherical_gaussian(2, 1)
    N = norm_cartesian_gaussian((2, 0, 0), 1)
    temp = 1/3*N/Ntilde*np.sqrt(4*np.pi)
    assert Vmat02.shape == (1, 6)
    assert np.allclose(Vmat02, np.array([[temp, 0, 0, temp, 0, temp]]),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)


def test_sh_rl2cart():
    r"""Tests for the transformation from a real solid harmonic Gaussian to
    a linear combination of Cartesian Gaussians.
    """
    # pylint: disable=C0103
    # C0103: Matrix names are capitalised.

    Wmat00 = sh_rl2cart_mat(0, 0)
    assert Wmat00.shape == (1, 1)
    assert abs(Wmat00[0, 0] - 1) < COMMON_ZERO_TOLERANCE


    Wmat11 = sh_rl2cart_mat(1, 1)
    assert np.allclose(Wmat11, np.array([[0, 0, 1],
                                         [1, 0, 0],
                                         [0, 1, 0]]),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)

    Xmat11 = sh_cart2rl_mat(1, 1)
    assert np.allclose(np.dot(Xmat11, Wmat11), np.identity(3),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)

    Wmat22 = sh_rl2cart_mat(2, 2)
    assert np.allclose(Wmat22,
                       np.array([[0, 0, -0.5, 0,  np.sqrt(3)/2],
                                 [1, 0,    0, 0,           0  ],
                                 [0, 0,    0, 1,           0  ],
                                 [0, 0, -0.5, 0, -np.sqrt(3)/2],
                                 [0, 1,    0, 0,           0  ],
                                 [0, 0,    1, 0,           0  ]]),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)

    Wmat20 = sh_rl2cart_mat(2, 0)
    assert np.allclose(Wmat20,
                       np.array([[1/np.sqrt(5)],
                                 [0           ],
                                 [0           ],
                                 [1/np.sqrt(5)],
                                 [0           ],
                                 [1/np.sqrt(5)]]),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)


def test_sh_cart2rl():
    r"""Tests for the transformation from a Cartesian Gaussian to a linear
    combination of real solid harmonic Gaussians.
    """
    # pylint: disable=C0103
    # C0103: Matrix names are capitalised. Other variable names are chosen to
    #        match their mathematical symbols.

    Xmat00 = sh_cart2rl_mat(0, 0)
    assert Xmat00.shape == (1, 1)
    assert abs(Xmat00[0, 0] - 1) < COMMON_ZERO_TOLERANCE


    Xmat11 = sh_cart2r(1)[0]
    assert np.allclose(Xmat11, np.array([[0, 1, 0],
                                         [0, 0, 1],
                                         [1, 0, 0]]),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)


    Xmat22, Xmat02 = sh_cart2r(2)[0], sh_cart2r(2)[1]
    sq3 = np.sqrt(3)
    assert np.allclose(Xmat22,
                       np.array([[ 0    , 1, 0,  0    , 0, 0  ],
                                 [ 0    , 0, 0,  0    , 1, 0  ],
                                 [-1/3  , 0, 0, -1/3  , 0, 2/3],
                                 [ 0    , 0, 1,  0    , 0, 0  ],
                                 [ 1/sq3, 0, 0, -1/sq3, 0, 0  ]]),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)

    sq5 = np.sqrt(5)
    assert np.allclose(Xmat02, np.array([[sq5/3, 0, 0,  sq5/3, 0, sq5/3]]),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)
