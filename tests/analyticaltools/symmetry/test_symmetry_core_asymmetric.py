""":mod:`.test_symmetry_core_symmetric` contains unit tests for the classes and
functions implemented in :mod:`analyticaltools.symmetry.symmetry_core`.
These unit tests involve asymmetric molecules.
"""

# pylint: disable=W0212
# W0212: Accessing _check_proper and _check_improper is needed to test them.

from polyinspect.auxiliary.common_standards import LOOSE_ZERO_TOLERANCE
from polyinspect.auxiliary.geometrical_space import Vector
from polyinspect.auxiliary.chemical_structures import Atom, Molecule
from polyinspect.analyticaltools.symmetry.symmetry_core import Symmetry,\
        ROTSYM_ASYMTOP, ROTSYM_ASYMTOP_PLANAR


def test_symmetry_cholesterol():
    """Tests for the symmetry analysis of meso-tartaric acid.
    """
    mol_cholesterol = Molecule([Atom("C", (-5.1538119, -5.7192891, +2.1665506)),
                                Atom("C", (-4.2296432, -5.1972395, +1.0635476)),
                                Atom("C", (-3.1359097, -4.3184715, +1.6694004)),
                                Atom("C", (-5.7715520, -4.5737800, +2.9817347)),
                                Atom("C", (-4.7202026, -3.6349146, +3.5120466)),
                                Atom("C", (-4.7672897, -3.2289272, +4.7881298)),
                                Atom("C", (-3.8069398, -2.2609029, +5.4021207)),
                                Atom("C", (-2.9828575, -1.4942482, +4.3496970)),
                                Atom("C", (-1.7758542, -0.8059178, +4.9983333)),
                                Atom("C", (-2.0262790, +0.2108362, +6.1229376)),
                                Atom("C", (-0.7497205, +1.0835798, +6.1408967)),
                                Atom("C", (+0.1000108, +0.7047759, +4.8919157)),
                                Atom("C", (+0.8598605, +1.9086605, +4.2882339)),
                                Atom("C", (+1.7296709, +2.6011508, +5.3636927)),
                                Atom("C", (+2.3418770, +3.9150084, +4.8578647)),
                                Atom("C", (+2.9966242, +4.7069627, +5.9966601)),
                                Atom("C", (+3.7093637, +5.9794306, +5.4907362)),
                                Atom("C", (+2.7052489, +7.0256229, +4.9890107)),
                                Atom("C", (+4.5808089, +6.5719187, +6.6074548)),
                                Atom("C", (+1.7384893, +1.4594826, +3.1124357)),
                                Atom("C", (-0.9336847, -0.0033308, +3.9521360)),
                                Atom("C", (-1.7689874, +1.0290388, +3.1794867)),
                                Atom("C", (-0.3430805, -1.0428055, +2.9805354)),
                                Atom("C", (-1.4625771, -1.8424675, +2.2946131)),
                                Atom("C", (-2.4718227, -2.4875855, +3.2699307)),
                                Atom("C", (-3.6829445, -3.1314051, +2.5101402)),
                                Atom("C", (-4.3816023, -2.1042362, +1.5994829)),
                                Atom("O", (-6.1935780, -6.4193373, +1.4467871)),
                                Atom("H", (-4.6413126, -6.4518654, +2.8265503)),
                                Atom("H", (-3.7901519, -6.0478896, +0.5086091)),
                                Atom("H", (-4.8260118, -4.6378275, +0.3149312)),
                                Atom("H", (-2.4778794, -4.9413161, +2.3069978)),
                                Atom("H", (-2.4926147, -3.9285548, +0.8575440)),
                                Atom("H", (-6.4735384, -4.0028380, +2.3346874)),
                                Atom("H", (-6.3952087, -4.9819697, +3.7991877)),
                                Atom("H", (-5.5279182, -3.5993870, +5.4742463)),
                                Atom("H", (-4.3546185, -1.5377454, +6.0393704)),
                                Atom("H", (-3.1339512, -2.8144612, +6.0909502)),
                                Atom("H", (-3.6394725, -0.7327075, +3.8664245)),
                                Atom("H", (-1.1201256, -1.6111373, +5.4201401)),
                                Atom("H", (-2.9213681, +0.8217022, +5.9208022)),
                                Atom("H", (-2.1974062, -0.2837816, +7.0904028)),
                                Atom("H", (-1.0167673, +2.1532217, +6.1303936)),
                                Atom("H", (-0.1705965, +0.9145055, +7.0622326)),
                                Atom("H", (+0.8584772, -0.0522239, +5.2010241)),
                                Atom("H", (+0.1170801, +2.6544665, +3.9137101)),
                                Atom("H", (+1.1199326, +2.8091798, +6.2633843)),
                                Atom("H", (+2.5301227, +1.9154387, +5.6969819)),
                                Atom("H", (+3.0903824, +3.7017702, +4.0702900)),
                                Atom("H", (+1.5616224, +4.5332275, +4.3723820)),
                                Atom("H", (+2.2376035, +4.9787217, +6.7539619)),
                                Atom("H", (+3.7263573, +4.0625939, +6.5240566)),
                                Atom("H", (+4.3780903, +5.6962317, +4.6396663)),
                                Atom("H", (+2.1147061, +6.6422850, +4.1499637)),
                                Atom("H", (+2.0021550, +7.3199963, +5.7756549)),
                                Atom("H", (+3.2137516, +7.9334223, +4.6479812)),
                                Atom("H", (+5.3234083, +5.8507502, +6.9641050)),
                                Atom("H", (+5.1241953, +7.4557644, +6.2569464)),
                                Atom("H", (+3.9783913, +6.8794596, +7.4689866)),
                                Atom("H", (+1.1486693, +0.9443970, +2.3409976)),
                                Atom("H", (+2.2295460, +2.3137321, +2.6346159)),
                                Atom("H", (+2.5242311, +0.7694838, +3.4376254)),
                                Atom("H", (-2.6216625, +0.5592049, +2.6752166)),
                                Atom("H", (-2.1716742, +1.7984797, +3.8464268)),
                                Atom("H", (-1.1714285, +1.5318416, +2.4128319)),
                                Atom("H", (+0.2809915, -0.5381507, +2.2148875)),
                                Atom("H", (+0.3381812, -1.7259044, +3.5194971)),
                                Atom("H", (-2.0010561, -1.1757394, +1.5898094)),
                                Atom("H", (-1.0057226, -2.6334254, +1.6695361)),
                                Atom("H", (-1.9394480, -3.3120371, +3.8071101)),
                                Atom("H", (-5.1998337, -2.5632249, +1.0340275)),
                                Atom("H", (-4.8051561, -1.2817992, +2.1876604)),
                                Atom("H", (-3.6848060, -1.6687912, +0.8745890)),
                                Atom("H", (-6.8425472, -6.8065699, +2.0578202))])
    mol_cholesterol.recentre_ip()
    mol_cholesterol.reorientate_ip()
    sym = Symmetry(mol_cholesterol,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_ASYMTOP
    assert sym.point_group == "C1"
    assert len(sym.proper_elements[1]) == 1
    assert len(sym.proper_generators[1]) == 1


def test_symmetry_meso_tartaric_acid():
    """Tests for the symmetry analysis of meso-tartaric acid.
    """
    mol_meso_tartaric_acid = Molecule([Atom("C", (+0.0040483, -0.1883768, +1.9461870)),
                                       Atom("C", (-0.1936784, +0.4612842, +0.5908447)),
                                       Atom("C", (+0.1936784, -0.4612842, -0.5908447)),
                                       Atom("C", (-0.0040483, +0.1883768, -1.9461870)),
                                       Atom("O", (-0.4655242, +1.3153292, -2.0438755)),
                                       Atom("O", (+0.3299405, -0.4793584, -3.0708870)),
                                       Atom("O", (+0.4655242, -1.3153292, +2.0438755)),
                                       Atom("O", (-0.3299405, +0.4793584, +3.0708870)),
                                       Atom("O", (+0.5851412, +1.6304233, +0.5376393)),
                                       Atom("O", (-0.5851412, -1.6304233, -0.5376393)),
                                       Atom("H", (-1.2773532, +0.7065816, +0.4800304)),
                                       Atom("H", (+1.2773532, -0.7065816, -0.4800304)),
                                       Atom("H", (+0.7038490, -1.3710226, -3.0412541)),
                                       Atom("H", (-0.7038490, +1.3710226, +3.0412541)),
                                       Atom("H", (+0.0116250, +2.3817296, +0.8417740)),
                                       Atom("H", (-0.0116250, -2.3817296, -0.8417740))])
    mol_meso_tartaric_acid.recentre_ip()
    mol_meso_tartaric_acid.reorientate_ip()
    sym = Symmetry(mol_meso_tartaric_acid,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_ASYMTOP
    assert len(sym.sea_groups) == 8
    assert sym.point_group == "Ci"
    assert len(sym.improper_elements[2]) == 1
    assert len(sym.improper_generators[2]) == 1


def test_symmetry_phenol():
    """Tests for the symmetry analysis of phenol.
    """
    mol_phenol = Molecule([Atom("O", (+0.0470507, +0.0000000, -2.4631437)),
                           Atom("C", (+0.0238787, +0.0000000, -0.8735895)),
                           Atom("C", (+0.0136795, -1.2047750, -0.1780642)),
                           Atom("H", (+0.0216518, -2.1528100, -0.7252862)),
                           Atom("C", (-0.0067020, -1.2047300, +1.2128665)),
                           Atom("H", (-0.0148310, -2.1527550, +1.7602061)),
                           Atom("C", (-0.0167972, +0.0000000, +1.9084632)),
                           Atom("H", (-0.0328528, +0.0000000, +3.0029855)),
                           Atom("C", (-0.0067020, +1.2047300, +1.2128665)),
                           Atom("H", (-0.0148310, +2.1527550, +1.7602061)),
                           Atom("C", (+0.0136795, +1.2047750, -0.1780642)),
                           Atom("H", (+0.0216518, +2.1528100, -0.7252862)),
                           Atom("H", (-0.9783216, +0.0000000, -2.9685973))])
    mol_phenol.recentre_ip()
    mol_phenol.reorientate_ip()
    sym = Symmetry(mol_phenol,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_ASYMTOP
    assert len(sym.sea_groups) == 9
    assert sym.point_group == "Cs"
    assert len(sym.improper_elements[1]) == 1
    assert len(sym.improper_generators[1]) == 1
    assert len(sym.sigma_generators[""]) == 1


def test_symmetry_trans_cyclooctene():
    """Tests for the symmetry analysis of trans-cyclooctene.
    """
    mol_trans_cyclooctene = Molecule([Atom("H", (-0.2411732, -1.0661137, -2.7508024)),
                                      Atom("C", (-0.3719785, -0.5873119, -1.7611035)),
                                      Atom("H", (-1.4645847, -0.5874131, -1.5730131)),
                                      Atom("C", (+0.1278192, +0.8656879, -1.8533026)),
                                      Atom("H", (+1.0112215, +0.9348901, -2.5155907)),
                                      Atom("H", (-0.6466783, +1.5101875, -2.3106067)),
                                      Atom("C", (+0.4731059, +1.2911890, -0.4733967)),
                                      Atom("H", (+1.5369041, +1.3430867, -0.2117833)),
                                      Atom("C", (-0.4731059, +1.2911890, +0.4733967)),
                                      Atom("H", (-1.5369041, +1.3430867, +0.2117833)),
                                      Atom("C", (-0.1278192, +0.8656879, +1.8533026)),
                                      Atom("H", (-1.0112215, +0.9348901, +2.5155907)),
                                      Atom("H", (+0.6466783, +1.5101875, +2.3106067)),
                                      Atom("C", (+0.3719785, -0.5873119, +1.7611035)),
                                      Atom("H", (+0.2411732, -1.0661137, +2.7508024)),
                                      Atom("H", (+1.4645847, -0.5874131, +1.5730131)),
                                      Atom("C", (-0.3243071, -1.4428078, +0.6961959)),
                                      Atom("H", (-0.3295112, -2.4893114, +1.0614977)),
                                      Atom("H", (-1.3922081, -1.1558124, +0.6223825)),
                                      Atom("C", (+0.3243071, -1.4428078, -0.6961959)),
                                      Atom("H", (+1.3922081, -1.1558124, -0.6223825)),
                                      Atom("H", (+0.3295112, -2.4893114, -1.0614977))])
    mol_trans_cyclooctene.recentre_ip()
    mol_trans_cyclooctene.reorientate_ip()
    sym = Symmetry(mol_trans_cyclooctene,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_ASYMTOP
    assert len(sym.sea_groups) == 11
    assert sym.point_group == "C2"
    assert len(sym.proper_elements[2]) == 1
    assert len(sym.proper_generators[2]) == 1


def test_symmetry_h2o():
    """Tests for the symmetry analysis of H2O.
    """
    mol_h2o = Molecule([Atom("O", (+0.0000000, -0.0155252, +0.0000000)),
                        Atom("H", (+0.0000000, +0.4944556, -0.7830366)),
                        Atom("H", (+0.0000000, +0.4944556, +0.7830366))])
    mol_h2o.recentre_ip()
    mol_h2o.reorientate_ip()
    sym = Symmetry(mol_h2o,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_ASYMTOP_PLANAR
    assert len(sym.sea_groups) == 2
    assert sym._check_proper(2, Vector([0, 1, 0]))
    assert not sym._check_proper(2, Vector([1, 1, 0]))
    assert sym._check_improper(1, Vector([1, 0, 0]))
    assert sym._check_improper(1, Vector([0, 0, 1]))
    assert not sym._check_improper(1, Vector([0, 1, 0]))
    assert sym.point_group == "C2v"
    assert len(sym.proper_elements[2]) == 1
    assert len(sym.proper_generators[2]) == 1
    assert len(sym.improper_elements[1]) == 2
    assert len(sym.sigma_elements["v"]) == 2
    assert len(sym.improper_generators[1]) == 1
    assert len(sym.sigma_generators["v"]) == 1


def test_symmetry_trans_decalin():
    """Tests for the symmetry analysis of trans-decalin.
    """
    mol_trans_decalin = Molecule([Atom("C", (-0.2711110, +0.7177980, -2.5257988)),
                                  Atom("C", (+0.1935891, +1.4668744, -1.2690816)),
                                  Atom("C", (-0.2728175, +0.7266591, +0.0000000)),
                                  Atom("C", (+0.1935891, +1.4668744, +1.2690816)),
                                  Atom("C", (-0.2711110, +0.7177980, +2.5257988)),
                                  Atom("C", (+0.2711110, -0.7177980, -2.5257988)),
                                  Atom("C", (-0.1935891, -1.4668744, -1.2690816)),
                                  Atom("C", (+0.2728175, -0.7266591, +0.0000000)),
                                  Atom("C", (-0.1935891, -1.4668744, +1.2690816)),
                                  Atom("C", (+0.2711110, -0.7177980, +2.5257988)),
                                  Atom("H", (+0.0867420, +1.2517631, -3.4321096)),
                                  Atom("H", (-1.3829750, +0.6937315, -2.5531008)),
                                  Atom("H", (-0.2231186, +2.4973551, -1.2828847)),
                                  Atom("H", (+1.3040643, +1.5397334, -1.2767251)),
                                  Atom("H", (-1.3870963, +0.6973294, +0.0000000)),
                                  Atom("H", (-0.2231186, +2.4973551, +1.2828847)),
                                  Atom("H", (+1.3040643, +1.5397334, +1.2767251)),
                                  Atom("H", (-1.3829750, +0.6937315, +2.5531008)),
                                  Atom("H", (+0.0867420, +1.2517631, +3.4321096)),
                                  Atom("H", (+1.3829750, -0.6937315, -2.5531008)),
                                  Atom("H", (-0.0867420, -1.2517631, -3.4321096)),
                                  Atom("H", (+0.2231186, -2.4973551, -1.2828847)),
                                  Atom("H", (-1.3040643, -1.5397334, -1.2767251)),
                                  Atom("H", (+1.3870963, -0.6973294, +0.0000000)),
                                  Atom("H", (+0.2231186, -2.4973551, +1.2828847)),
                                  Atom("H", (-1.3040643, -1.5397334, +1.2767251)),
                                  Atom("H", (+1.3829750, -0.6937315, +2.5531008)),
                                  Atom("H", (-0.0867420, -1.2517631, +3.4321096))])
    mol_trans_decalin.recentre_ip()
    mol_trans_decalin.reorientate_ip()
    sym = Symmetry(mol_trans_decalin,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_ASYMTOP
    assert len(sym.sea_groups) == 8
    assert sym.point_group == "C2h"
    assert len(sym.proper_elements[2]) == 1
    assert len(sym.proper_generators[2]) == 1
    assert len(sym.improper_elements[1]) == 1
    assert len(sym.sigma_elements["h"]) == 1
    assert len(sym.improper_generators[1]) == 1
    assert len(sym.sigma_generators["h"]) == 1
    assert len(sym.improper_elements[2]) == 1


def test_symmetry_twistane():
    """Tests for the symmetry analysis of twistane.
    """
    mol_twistane = Molecule([Atom("C", (-0.2087241, -0.7392251, -1.8985013)),
                             Atom("C", (-0.6219509, -1.1186250, -0.4696988)),
                             Atom("C", (-1.5650031, +0.0000000, -0.0000000)),
                             Atom("C", (+0.2087241, +0.7392251, -1.8985013)),
                             Atom("C", (+0.6219509, +1.1186250, -0.4696988)),
                             Atom("C", (-0.6219509, +1.1186250, +0.4696988)),
                             Atom("C", (-0.2087241, +0.7392251, +1.8985013)),
                             Atom("C", (+0.2087241, -0.7392251, +1.8985013)),
                             Atom("C", (+0.6219509, -1.1186250, +0.4696988)),
                             Atom("C", (+1.5650031, -0.0000000, +0.0000000)),
                             Atom("H", (-1.0450000, -0.9116245, -2.6020027)),
                             Atom("H", (+0.6157721, -1.3892013, -2.2480520)),
                             Atom("H", (-1.1207258, -2.1111002, -0.4489223)),
                             Atom("H", (-2.2378525, -0.3343978, +0.8128010)),
                             Atom("H", (-2.2378525, +0.3343978, -0.8128010)),
                             Atom("H", (-0.6157721, +1.3892013, -2.2480520)),
                             Atom("H", (+1.0450000, +0.9116245, -2.6020027)),
                             Atom("H", (+1.1207258, +2.1111002, -0.4489223)),
                             Atom("H", (-1.1207258, +2.1111002, +0.4489223)),
                             Atom("H", (-1.0450000, +0.9116245, +2.6020027)),
                             Atom("H", (+0.6157721, +1.3892013, +2.2480520)),
                             Atom("H", (-0.6157721, -1.3892013, +2.2480520)),
                             Atom("H", (+1.0450000, -0.9116245, +2.6020027)),
                             Atom("H", (+1.1207258, -2.1111002, +0.4489223)),
                             Atom("H", (+2.2378525, +0.3343978, +0.8128010)),
                             Atom("H", (+2.2378525, -0.3343978, -0.8128010))])
    mol_twistane.recentre_ip()
    mol_twistane.reorientate_ip()
    sym = Symmetry(mol_twistane,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_ASYMTOP
    assert len(sym.sea_groups) == 7
    assert sym.point_group == "D2"
    assert len(sym.proper_elements[2]) == 3
    assert len(sym.proper_generators[2]) == 2
    assert abs(sym.proper_generators[2][1].dot(sym.proper_generators[2][0]))\
            < LOOSE_ZERO_TOLERANCE


def test_symmetry_pyrene():
    """Tests for the symmetry analysis of pyrene.
    """
    mol_pyrene = Molecule([Atom("C", (-0.0000000, -1.2138777, +2.8131721)),
                           Atom("C", (-0.0000000, -1.2247845, +1.4103571)),
                           Atom("C", (-0.0000000, +0.0000000, +0.7052978)),
                           Atom("C", (-0.0000000, +0.0000000, +3.5075065)),
                           Atom("C", (+0.0000000, +1.2138777, +2.8131721)),
                           Atom("C", (+0.0000000, +1.2247845, +1.4103571)),
                           Atom("C", (+0.0000000, -0.0000000, -0.7052978)),
                           Atom("C", (+0.0000000, +1.2247845, -1.4103571)),
                           Atom("C", (+0.0000000, +2.4346425, -0.6995343)),
                           Atom("C", (+0.0000000, +2.4346425, +0.6995343)),
                           Atom("C", (-0.0000000, -2.4346425, +0.6995343)),
                           Atom("C", (-0.0000000, -2.4346425, -0.6995343)),
                           Atom("C", (-0.0000000, -1.2247845, -1.4103571)),
                           Atom("C", (-0.0000000, -1.2138777, -2.8131721)),
                           Atom("C", (+0.0000000, -0.0000000, -3.5075065)),
                           Atom("C", (+0.0000000, +1.2138777, -2.8131721)),
                           Atom("H", (-0.0000000, -2.1420277, +3.3721799)),
                           Atom("H", (-0.0000000, +0.0000000, +4.5898957)),
                           Atom("H", (+0.0000000, +2.1420277, +3.3721799)),
                           Atom("H", (+0.0000000, +3.3816993, -1.2259371)),
                           Atom("H", (+0.0000000, +3.3816993, +1.2259371)),
                           Atom("H", (-0.0000000, -3.3816993, +1.2259371)),
                           Atom("H", (-0.0000000, -3.3816993, -1.2259371)),
                           Atom("H", (-0.0000000, -2.1420277, -3.3721799)),
                           Atom("H", (+0.0000000, -0.0000000, -4.5898957)),
                           Atom("H", (+0.0000000, +2.1420277, -3.3721799))])
    mol_pyrene.recentre_ip()
    mol_pyrene.reorientate_ip()
    sym = Symmetry(mol_pyrene,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_ASYMTOP_PLANAR
    assert len(sym.sea_groups) == 8
    assert sym.point_group == "D2h"
    assert len(sym.proper_elements[2]) == 3
    assert len(sym.proper_generators[2]) == 2
    assert abs(sym.proper_generators[2][1].dot(sym.proper_generators[2][0]))\
            < LOOSE_ZERO_TOLERANCE
    assert len(sym.improper_elements[1]) == 3
    assert len(sym.sigma_elements[""]) == 3
    assert len(sym.sigma_generators[""]) == 1
    assert len(sym.improper_elements[2]) == 1


# def test_symmetry_chfbrcl():
#     """Tests for the symmetry analysis of CHFBrCl.
#     """
#     mol_chfbrcl = Molecule([Atom("C", (+0.0000000, +0.0000000, +0.0000000)),
#                             Atom("F", (+0.6405128, -0.6405128, +0.6405128)),
#                             Atom("Cl", (+0.6405128, +0.6405128, -0.6405128)),
#                             Atom("Br", (-0.6405128, +0.6405128, +0.6405128)),
#                             Atom("H", (-0.6405128, -0.6405128, -0.6405128))])
#     mol_chfbrcl.recentre_ip()
#     mol_chfbrcl.reorientate_ip()
#     sym = Symmetry(mol_chfbrcl,
#                    moi_thresh=LOOSE_ZERO_TOLERANCE,
#                    dist_thresh=LOOSE_ZERO_TOLERANCE)
#     assert sym.rotational_symmetry == ROTSYM_ASYMTOP
#     assert len(sym.sea_groups) == 5


# def test_symmetry_h2o():
#     """Tests for the symmetry analysis of H2O.
#     """
#     mol_h2o = Molecule([Atom("O", (+0.0000000, -0.0155252, +0.0000000)),
#                         Atom("H", (+0.0000000, +0.4944556, -0.7830366)),
#                         Atom("H", (+0.0000000, +0.4944556, +0.7830366))])
#     mol_h2o.recentre_ip()
#     mol_h2o.reorientate_ip()
#     sym = Symmetry(mol_h2o,
#                    moi_thresh=LOOSE_ZERO_TOLERANCE,
#                    dist_thresh=LOOSE_ZERO_TOLERANCE)
#     assert sym.rotational_symmetry == ROTSYM_ASYMTOP_PLANAR
#     assert len(sym.sea_groups) == 2
#     assert sym._check_proper(2, Vector([0, 1, 0]))
#     assert not sym._check_proper(2, Vector([1, 1, 0]))
#     assert sym._check_improper(1, Vector([1, 0, 0]))
#     assert sym._check_improper(1, Vector([0, 0, 1]))
#     assert not sym._check_improper(1, Vector([0, 1, 0]))
#     sym._analyse_symmetric()
#     assert sym.point_group == "C2v"
