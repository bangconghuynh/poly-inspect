""":mod:`.test_symmetry_core_symmetric` contains unit tests for the classes and
functions implemented in :mod:`analyticaltools.symmetry.symmetry_core`.
These unit tests involve symmetric top molecules.
"""

# pylint: disable=W0212,C0302
# W0212: Accessing _check_proper and _check_improper is needed to test them.
# C0302: There are quite a lot of symmetric top groups to be tested.

from polyinspect.auxiliary.common_standards import LOOSE_ZERO_TOLERANCE,\
        COMMON_ZERO_TOLERANCE
from polyinspect.auxiliary.geometrical_space import Vector
from polyinspect.auxiliary.chemical_structures import Atom, Molecule
from polyinspect.analyticaltools.symmetry.symmetry_core import Symmetry,\
        ROTSYM_SYMTOP_OBLATE, ROTSYM_SYMTOP_OBLATE_PLANAR,\
        ROTSYM_SYMTOP_PROLATE


# ===============
# Dihedral groups
# ===============

def test_symmetry_s4n4():
    """Tests for the symmetry analysis of S4N4.
    """
    mol_s4n4 = Molecule([Atom("N", (+0.0000000, +1.2727245, +0.9627024)),
                         Atom("N", (+1.2727245, -0.0000000, -0.9627024)),
                         Atom("N", (-0.0000000, -1.2727245, +0.9627024)),
                         Atom("N", (-1.2727245, +0.0000000, -0.9627024)),
                         Atom("S", (+1.4807251, +1.4807251, +0.0000000)),
                         Atom("S", (-1.4807251, -1.4807251, +0.0000000)),
                         Atom("S", (+1.4807251, -1.4807251, +0.0000000)),
                         Atom("S", (-1.4807251, +1.4807251, +0.0000000))])
    mol_s4n4.recentre_ip()
    mol_s4n4.reorientate_ip()
    sym = Symmetry(mol_s4n4,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_OBLATE
    assert len(sym.sea_groups) == 2
    assert sym.point_group == "D2d"
    assert len(sym.proper_elements[2]) == 3
    assert len(sym.improper_elements[1]) == 2
    assert len(sym.sigma_elements["d"]) == 2
    assert len(sym.improper_elements[4]) == 1
    assert len(sym.proper_generators[2]) == 2
    assert len(sym.improper_generators[1]) == 1
    assert len(sym.sigma_generators["d"]) == 1
    assert abs(sym.proper_generators[2][0].dot(sym.proper_generators[2][1]))\
            < COMMON_ZERO_TOLERANCE


def test_symmetry_trisoxalatoiron():
    """Tests for the symmetry analysis of tris(oxalato)iron(III).
    """
    mol_trisoxalatoiron = Molecule([Atom("Fe", (+0.0000000, -0.0000000, +0.0000000)),
                                    Atom("O", (-1.3362075, -0.8052002, +1.0695926)),
                                    Atom("O", (+1.3654276, +0.7545896, -1.0695926)),
                                    Atom("O", (-0.0292201, -1.5597897, -1.0695926)),
                                    Atom("O", (-0.0292201, +1.5597897, +1.0695926)),
                                    Atom("O", (+1.3654276, -0.7545896, +1.0695926)),
                                    Atom("O", (-1.3362075, +0.8052002, -1.0695926)),
                                    Atom("C", (+2.5796938, -0.4218448, +0.5978529)),
                                    Atom("C", (+2.5796938, +0.4218448, -0.5978529)),
                                    Atom("C", (-0.9245186, -2.4450028, -0.5978529)),
                                    Atom("C", (-1.6551752, -2.0231580, +0.5978529)),
                                    Atom("C", (-0.9245186, +2.4450028, +0.5978529)),
                                    Atom("C", (-1.6551752, +2.0231580, -0.5978529)),
                                    Atom("O", (+3.6040555, +0.8043119, -1.1397336)),
                                    Atom("O", (+3.6040555, -0.8043119, +1.1397336)),
                                    Atom("O", (-2.4985823, -2.7190476, +1.1397336)),
                                    Atom("O", (-1.1054732, -3.5233595, -1.1397336)),
                                    Atom("O", (-2.4985823, +2.7190476, -1.1397336)),
                                    Atom("O", (-1.1054732, +3.5233595, +1.1397336))])
    mol_trisoxalatoiron.recentre_ip()
    mol_trisoxalatoiron.reorientate_ip()
    sym = Symmetry(mol_trisoxalatoiron,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=1e1*LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_OBLATE
    assert len(sym.sea_groups) == 4
    assert sym.point_group == "D3"
    assert len(sym.proper_elements[2]) == 3
    assert len(sym.proper_elements[3]) == 1
    assert len(sym.proper_generators[2]) == 1
    assert len(sym.proper_generators[3]) == 1
    assert abs(sym.proper_generators[2][0].dot(sym.proper_generators[3][0]))\
            < 1e1*LOOSE_ZERO_TOLERANCE


def test_symmetry_bh3():
    """Tests for the symmetry analysis of BH3.
    """
    mol_bh3 = Molecule([Atom("B", (-2.5474597, +0.5655867, 0.0)),
                        Atom("H", (-2.6944018, +1.7375269, 0.0)),
                        Atom("H", (-1.4590587, +0.1068721, 0.0)),
                        Atom("H", (-3.4889187, -0.1476390, 0.0))])
    mol_bh3.recentre_ip()
    mol_bh3.reorientate_ip()
    sym = Symmetry(mol_bh3,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_OBLATE_PLANAR
    assert len(sym.sea_groups) == 2
    assert sym._check_proper(3, Vector([0, 0, 1]))
    assert sym._check_proper(2, mol_bh3[0].position\
                                        .get_vector_to(mol_bh3[1].position))
    assert sym._check_proper(2, mol_bh3[0].position\
                                        .get_vector_to(mol_bh3[2].position))
    assert sym._check_proper(2, mol_bh3[0].position\
                                        .get_vector_to(mol_bh3[3].position))
    assert sym._check_improper(1, Vector([0, 0, 1]))
    assert sym.point_group == "D3h"
    assert len(sym.proper_elements[2]) == 3
    assert len(sym.proper_elements[3]) == 1
    assert len(sym.improper_elements[1]) == 4
    assert len(sym.sigma_elements["h"]) == 1
    assert len(sym.sigma_elements["v"]) == 3
    assert len(sym.improper_elements[3]) == 1
    assert len(sym.proper_generators[2]) == 1
    assert len(sym.proper_generators[3]) == 1
    assert len(sym.improper_generators[1]) == 1
    assert len(sym.sigma_generators["h"]) == 1
    assert abs(sym.proper_generators[2][0].dot(sym.proper_generators[3][0]))\
            < COMMON_ZERO_TOLERANCE


def test_symmetry_pcl5():
    """Tests for the symmetry analysis of PCl5.
    """
    mol_pcl5 = Molecule([Atom("P", (+0.0000000, +0.0000000, +0.0000000)),
                         Atom("Cl", (+0.0000000, +0.0000000, -2.0739975)),
                         Atom("Cl", (+0.0000000, +0.0000000, +2.0739975)),
                         Atom("Cl", (+1.0369984, -1.7961338, +0.0000000)),
                         Atom("Cl", (+1.0369984, +1.7961338, +0.0000000)),
                         Atom("Cl", (-2.0739967, +0.0000000, +0.0000000))])
    mol_pcl5.recentre_ip()
    mol_pcl5.reorientate_ip()
    sym = Symmetry(mol_pcl5,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_PROLATE
    assert len(sym.sea_groups) == 3
    assert sym._check_proper(3, Vector([1, 0, 0]))
    assert sym._check_proper(2, mol_pcl5[0].position\
                                        .get_vector_to(mol_pcl5[3].position))
    assert sym._check_proper(2, mol_pcl5[0].position\
                                        .get_vector_to(mol_pcl5[4].position))
    assert sym._check_proper(2, mol_pcl5[0].position\
                                        .get_vector_to(mol_pcl5[5].position))
    assert sym._check_improper(1, Vector([1, 0, 0]))
    assert sym.point_group == "D3h"
    assert len(sym.proper_elements[2]) == 3
    assert len(sym.proper_elements[3]) == 1
    assert len(sym.improper_elements[1]) == 4
    assert len(sym.sigma_elements["h"]) == 1
    assert len(sym.sigma_elements["v"]) == 3
    assert len(sym.improper_elements[3]) == 1
    assert len(sym.proper_generators[2]) == 1
    assert len(sym.proper_generators[3]) == 1
    assert len(sym.improper_generators[1]) == 1
    assert len(sym.sigma_generators["h"]) == 1
    assert abs(sym.proper_generators[2][0].dot(sym.proper_generators[3][0]))\
            < COMMON_ZERO_TOLERANCE


def test_symmetry_cyclohexane_chair():
    """Tests for the symmetry analysis of cyclohexane (chair).
    """
    mol_cyclohexane_chair = Molecule([Atom("C", (+1.2605196, +0.7277613, +0.2448386)),
                                      Atom("C", (+1.2605196, -0.7277613, -0.2448386)),
                                      Atom("C", (-0.0000000, -1.4555226, +0.2448386)),
                                      Atom("C", (+0.0000000, +1.4555226, -0.2448386)),
                                      Atom("C", (-1.2605196, +0.7277613, +0.2448386)),
                                      Atom("C", (-1.2605196, -0.7277613, -0.2448386)),
                                      Atom("H", (+2.1653402, +1.2501597, -0.1331892)),
                                      Atom("H", (+1.2886542, +0.7440049, +1.3568491)),
                                      Atom("H", (+1.2886542, -0.7440049, -1.3568491)),
                                      Atom("H", (+2.1653402, -1.2501597, +0.1331892)),
                                      Atom("H", (-0.0000000, -1.4880097, +1.3568491)),
                                      Atom("H", (-0.0000000, -2.5003195, -0.1331892)),
                                      Atom("H", (+0.0000000, +1.4880097, -1.3568491)),
                                      Atom("H", (+0.0000000, +2.5003195, +0.1331892)),
                                      Atom("H", (-1.2886542, +0.7440049, +1.3568491)),
                                      Atom("H", (-2.1653402, +1.2501597, -0.1331892)),
                                      Atom("H", (-1.2886542, -0.7440049, -1.3568491)),
                                      Atom("H", (-2.1653402, -1.2501597, +0.1331892))])
    mol_cyclohexane_chair.recentre_ip()
    mol_cyclohexane_chair.reorientate_ip()
    sym = Symmetry(mol_cyclohexane_chair,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=1e1*LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_OBLATE
    assert len(sym.sea_groups) == 3
    assert sym.point_group == "D3d"
    assert len(sym.proper_elements[2]) == 3
    assert len(sym.proper_elements[3]) == 1
    assert len(sym.improper_elements[1]) == 3
    assert len(sym.sigma_elements["d"]) == 3
    assert len(sym.improper_elements[2]) == 1
    assert len(sym.improper_elements[6]) == 1
    assert len(sym.proper_generators[2]) == 1
    assert len(sym.proper_generators[3]) == 1
    assert len(sym.improper_generators[1]) == 1
    assert len(sym.sigma_generators["d"]) == 1
    assert abs(sym.proper_generators[2][0].dot(sym.proper_generators[3][0]))\
            < COMMON_ZERO_TOLERANCE


def test_symmetry_ttcd():
    """Tests for the symmetry analysis of tetrathiacyclododecane.
    """
    mol_ttcd = Molecule([Atom("S", (+2.2296497, +2.2296497, +0.0000000)),
                         Atom("S", (+2.2296497, -2.2296497, +0.0000000)),
                         Atom("S", (-2.2296497, -2.2296497, -0.0000000)),
                         Atom("S", (-2.2296497, +2.2296497, -0.0000000)),
                         Atom("C", (+2.3249669, +0.5048733, -0.5629894)),
                         Atom("C", (+2.3249669, -0.5048733, +0.5629894)),
                         Atom("C", (+0.5048733, -2.3249669, -0.5629894)),
                         Atom("C", (-0.5048733, -2.3249669, +0.5629894)),
                         Atom("C", (-2.3249669, -0.5048733, -0.5629894)),
                         Atom("C", (-2.3249669, +0.5048733, +0.5629894)),
                         Atom("C", (-0.5048733, +2.3249669, -0.5629894)),
                         Atom("C", (+0.5048733, +2.3249669, +0.5629894)),
                         Atom("H", (+3.1341438, +0.3959636, -1.0676507)),
                         Atom("H", (+1.5698050, +0.3283710, -1.1281499)),
                         Atom("H", (+3.1341438, -0.3959636, +1.0676507)),
                         Atom("H", (+1.5698050, -0.3283710, +1.1281499)),
                         Atom("H", (+0.3959636, -3.1341438, -1.0676507)),
                         Atom("H", (+0.3283710, -1.5698050, -1.1281499)),
                         Atom("H", (-0.3959636, -3.1341438, +1.0676507)),
                         Atom("H", (-0.3283710, -1.5698050, +1.1281499)),
                         Atom("H", (-3.1341438, -0.3959636, -1.0676507)),
                         Atom("H", (-1.5698050, -0.3283710, -1.1281499)),
                         Atom("H", (-3.1341438, +0.3959636, +1.0676507)),
                         Atom("H", (-1.5698050, +0.3283710, +1.1281499)),
                         Atom("H", (-0.3959636, +3.1341438, -1.0676507)),
                         Atom("H", (-0.3283710, +1.5698050, -1.1281499)),
                         Atom("H", (+0.3959636, +3.1341438, +1.0676507)),
                         Atom("H", (+0.3283710, +1.5698050, +1.1281499))])
    mol_ttcd.recentre_ip()
    mol_ttcd.reorientate_ip()
    sym = Symmetry(mol_ttcd,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_OBLATE
    assert len(sym.sea_groups) == 4
    assert sym.point_group == "D4"
    assert len(sym.proper_elements[2]) == 5
    assert len(sym.proper_elements[4]) == 1
    assert len(sym.proper_generators[2]) == 1
    assert len(sym.proper_generators[4]) == 1
    assert abs(sym.proper_generators[2][0].dot(sym.proper_generators[4][0]))\
            < COMMON_ZERO_TOLERANCE


def test_symmetry_vf6_d4h():
    """Tests for the symmetry analysis of tetragonal VF6.
    """
    mol_vf6_d4h = Molecule([Atom("V", (+0, +0, +0)),
                            Atom("F", (+1, +0, +0)),
                            Atom("F", (-1, +0, +0)),
                            Atom("F", (+0, +1, +0)),
                            Atom("F", (+0, -1, +0)),
                            Atom("F", (+0, +0, +0.5)),
                            Atom("F", (+0, +0, -0.5))])
    mol_vf6_d4h.recentre_ip()
    mol_vf6_d4h.reorientate_ip()
    sym = Symmetry(mol_vf6_d4h,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_OBLATE
    assert len(sym.sea_groups) == 3
    assert sym._check_proper(4, Vector([0, 0, 1]))
    assert sym._check_proper(2, Vector([0, 0, 1]))
    assert sym._check_proper(2, Vector([0, 1, 0]))
    assert sym._check_proper(2, Vector([1, 0, 0]))
    assert sym._check_proper(2, Vector([1, 1, 0]))
    assert sym._check_proper(2, Vector([-1, 1, 0]))
    assert sym._check_improper(1, Vector([0, 0, 1]))
    assert sym._check_improper(1, Vector([0, 1, 0]))
    assert sym._check_improper(1, Vector([1, 0, 0]))
    assert sym._check_improper(1, Vector([1, 1, 0]))
    assert sym._check_improper(1, Vector([1, -1, 0]))
    assert sym._check_improper(2, Vector([2, 4, 8]))
    assert sym._check_improper(2, Vector([1, 0, 5]))
    assert sym.point_group == "D4h"
    assert len(sym.proper_elements[2]) == 5
    assert len(sym.proper_elements[4]) == 1
    assert len(sym.improper_elements[1]) == 5
    assert len(sym.sigma_elements["h"]) == 1
    assert len(sym.sigma_elements["v"]) == 4
    assert len(sym.improper_elements[4]) == 1
    assert len(sym.proper_generators[2]) == 1
    assert len(sym.proper_generators[4]) == 1
    assert len(sym.improper_generators[1]) == 1
    assert len(sym.sigma_generators["h"]) == 1
    assert abs(sym.proper_generators[2][0].dot(sym.proper_generators[4][0]))\
            < COMMON_ZERO_TOLERANCE


def test_symmetry_wcn8():
    """Tests for the symmetry analysis of [W(CN)8]2-.
    """
    mol_wcn8 = Molecule([Atom("W", (-0.0000000, -0.0000000, +0.0000000)),
                         Atom("N", (+1.9755943, -1.9755943, -1.7585068)),
                         Atom("N", (+1.9755943, +1.9755943, -1.7585068)),
                         Atom("N", (-1.9755943, +1.9755943, -1.7585068)),
                         Atom("N", (-1.9755943, -1.9755943, -1.7585068)),
                         Atom("N", (+2.7939122, -0.0000000, +1.7585068)),
                         Atom("N", (+0.0000000, +2.7939122, +1.7585068)),
                         Atom("N", (-2.7939122, +0.0000000, +1.7585068)),
                         Atom("N", (-0.0000000, -2.7939122, +1.7585068)),
                         Atom("C", (+1.2859995, -1.2859995, -1.1784046)),
                         Atom("C", (+1.2859995, +1.2859995, -1.1784046)),
                         Atom("C", (-1.2859995, +1.2859995, -1.1784046)),
                         Atom("C", (-1.2859995, -1.2859995, -1.1784046)),
                         Atom("C", (+1.8186779, -0.0000000, +1.1784046)),
                         Atom("C", (+0.0000000, +1.8186779, +1.1784046)),
                         Atom("C", (-1.8186779, +0.0000000, +1.1784046)),
                         Atom("C", (-0.0000000, -1.8186779, +1.1784046))])
    mol_wcn8.recentre_ip()
    mol_wcn8.reorientate_ip()
    sym = Symmetry(mol_wcn8,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_OBLATE
    assert len(sym.sea_groups) == 3
    assert sym.point_group == "D4d"
    assert len(sym.proper_elements[2]) == 5
    assert len(sym.proper_elements[4]) == 1
    assert len(sym.improper_elements[1]) == 4
    assert len(sym.sigma_elements["d"]) == 4
    assert len(sym.improper_elements[8]) == 1
    assert len(sym.proper_generators[2]) == 1
    assert len(sym.proper_generators[4]) == 1
    assert len(sym.improper_generators[1]) == 1
    assert len(sym.sigma_generators["d"]) == 1
    assert abs(sym.proper_generators[2][0].dot(sym.proper_generators[4][0]))\
            < COMMON_ZERO_TOLERANCE


# def test_symmetry_chfbrcl():
#     """Tests for the symmetry analysis of CHFBrCl.
#     """
#     mol_chfbrcl = Molecule([Atom("C", (+0.0000000, +0.0000000, +0.0000000)),
#                             Atom("F", (+0.6405128, -0.6405128, +0.6405128)),
#                             Atom("Cl", (+0.6405128, +0.6405128, -0.6405128)),
#                             Atom("Br", (-0.6405128, +0.6405128, +0.6405128)),
#                             Atom("H", (-0.6405128, -0.6405128, -0.6405128))])
#     mol_chfbrcl.recentre_ip()
#     mol_chfbrcl.reorientate_ip()
#     sym = Symmetry(mol_chfbrcl,
#                    moi_thresh=LOOSE_ZERO_TOLERANCE,
#                    dist_thresh=LOOSE_ZERO_TOLERANCE)
#     assert sym.rotational_symmetry == ROTSYM_ASYMTOP
#     assert len(sym.sea_groups) == 5


def test_symmetry_ybi2thf5():
    """Tests for the symmetry analysis of YbI2(THF)5.
    """
    mol_ybi2thf5 = Molecule([Atom("Yb", (-0.0000000, +0.0000000, +0.0000000)),
                             Atom("I", (-0.0000000, -0.0000000, +2.9361965)),
                             Atom("O", (+1.9020536, +1.3819229, +0.0000000)),
                             Atom("O", (+1.9020536, -1.3819229, +0.0000000)),
                             Atom("O", (-0.7265198, -2.2359981, +0.0000000)),
                             Atom("C", (+2.2550356, +2.3556575, +1.0385500)),
                             Atom("H", (+2.5642077, +1.9004269, +1.8550910)),
                             Atom("H", (+1.4809545, +2.9188846, +1.2668515)),
                             Atom("C", (+3.3514601, +3.1835360, +0.4539694)),
                             Atom("H", (+3.9511818, +3.5241819, +1.1582017)),
                             Atom("H", (+2.9919585, +3.9417969, -0.0584598)),
                             Atom("C", (+2.9372077, -1.4167281, +1.0385500)),
                             Atom("H", (+2.5997971, -1.8514423, +1.8550910)),
                             Atom("H", (+3.2336643, -0.5064865, +1.2668515)),
                             Atom("C", (+4.0633808, -2.2036613, +0.4539694)),
                             Atom("H", (+4.5726785, -2.6687651, +1.1582017)),
                             Atom("H", (+4.6734376, -1.6274394, -0.0584598)),
                             Atom("C", (+3.3514601, -3.1835360, -0.4539694)),
                             Atom("H", (+3.9511818, -3.5241819, -1.1582017)),
                             Atom("H", (+2.9919585, -3.9417969, +0.0584598)),
                             Atom("C", (+2.2550356, -2.3556575, -1.0385500)),
                             Atom("H", (+2.5642077, -1.9004269, -1.8550910)),
                             Atom("H", (+1.4809545, -2.9188846, -1.2668515)),
                             Atom("C", (-0.4397414, -3.2312436, +1.0385500)),
                             Atom("H", (-0.9574448, -3.0446811, +1.8550910)),
                             Atom("H", (+0.5175600, -3.2319104, +1.2668515)),
                             Atom("C", (-0.8401527, -4.5454736, +0.4539694)),
                             Atom("H", (-0.1036152, -4.9476098, -0.0584598)),
                             Atom("H", (-1.1251111, -5.1735695, +1.1582017)),
                             Atom("C", (-1.9920645, -4.1711947, -0.4539694)),
                             Atom("H", (-2.8243056, -4.0636039, +0.0584598)),
                             Atom("H", (-2.1307138, -4.8468293, -1.1582017)),
                             Atom("C", (-1.5435191, -2.8726045, -1.0385500)),
                             Atom("H", (-1.0150296, -3.0259707, -1.8550910)),
                             Atom("H", (-2.3183841, -2.3104563, -1.2668515)),
                             Atom("I", (+0.0000000, +0.0000000, -2.9361965)),
                             Atom("C", (+4.0633808, +2.2036613, -0.4539694)),
                             Atom("C", (+2.9372077, +1.4167281, -1.0385500)),
                             Atom("H", (+2.5997971, +1.8514423, -1.8550910)),
                             Atom("H", (+3.2336643, +0.5064865, -1.2668515)),
                             Atom("H", (+4.5726785, +2.6687651, -1.1582017)),
                             Atom("H", (+4.6734376, +1.6274394, +0.0584598)),
                             Atom("O", (-0.7265198, +2.2359981, +0.0000000)),
                             Atom("C", (-0.4397414, +3.2312436, -1.0385500)),
                             Atom("H", (-0.9574448, +3.0446811, -1.8550910)),
                             Atom("H", (+0.5175600, +3.2319104, -1.2668515)),
                             Atom("C", (-0.8401527, +4.5454736, -0.4539694)),
                             Atom("H", (-1.1251111, +5.1735695, -1.1582017)),
                             Atom("H", (-0.1036152, +4.9476098, +0.0584598)),
                             Atom("C", (-1.9920645, +4.1711947, +0.4539694)),
                             Atom("H", (-2.1307138, +4.8468293, +1.1582017)),
                             Atom("H", (-2.8243056, +4.0636039, -0.0584598)),
                             Atom("C", (-1.5435191, +2.8726045, +1.0385500)),
                             Atom("H", (-1.0150296, +3.0259707, +1.8550910)),
                             Atom("H", (-2.3183841, +2.3104563, +1.2668515)),
                             Atom("O", (-2.3510676, +0.0000000, +0.0000000)),
                             Atom("C", (-3.2089828, +0.5802903, -1.0385500)),
                             Atom("H", (-3.1915305, +0.0302741, -1.8550910)),
                             Atom("H", (-2.9137946, +1.4909440, -1.2668515)),
                             Atom("C", (-4.5826237, +0.6055959, -0.4539694)),
                             Atom("H", (-4.7374754, +1.4303516, +0.0584598)),
                             Atom("H", (-5.2680354, +0.5286766, -1.1582017)),
                             Atom("C", (-4.5826237, -0.6055959, +0.4539694)),
                             Atom("H", (-4.7374754, -1.4303516, -0.0584598)),
                             Atom("H", (-5.2680354, -0.5286766, +1.1582017)),
                             Atom("C", (-3.2089828, -0.5802903, +1.0385500)),
                             Atom("H", (-3.1915305, -0.0302741, +1.8550910)),
                             Atom("H", (-2.9137946, -1.4909440, +1.2668515))])
    mol_ybi2thf5.recentre_ip()
    mol_ybi2thf5.reorientate_ip()
    sym = Symmetry(mol_ybi2thf5,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=1e2*LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_OBLATE
    assert len(sym.sea_groups) == 9
    assert sym.point_group == "D5"
    assert len(sym.proper_elements[2]) == 5
    assert len(sym.proper_elements[5]) == 1
    assert len(sym.proper_generators[2]) == 1
    assert len(sym.proper_generators[5]) == 1
    assert abs(sym.proper_generators[2][0].dot(sym.proper_generators[5][0]))\
            < COMMON_ZERO_TOLERANCE


def test_symmetry_ferrocene_staggered():
    """Tests for the symmetry analysis of ferrocene (staggered).
    """
    mol_fecp2stag = Molecule([Atom("C", (+0.9923419, +0.7209786, -1.7378500)),
                              Atom("C", (+0.9923419, -0.7209786, -1.7378500)),
                              Atom("C", (-0.3790409, -1.1665679, -1.7378500)),
                              Atom("C", (-1.2266021, +0.0000000, -1.7378500)),
                              Atom("C", (-0.3790409, +1.1665679, -1.7378500)),
                              Atom("H", (+1.8667695, +1.3562874, -1.7301600)),
                              Atom("H", (+1.8667695, -1.3562874, -1.7301600)),
                              Atom("H", (-0.7130425, -2.1945191, -1.7301600)),
                              Atom("H", (-2.3074540, +0.0000000, -1.7301600)),
                              Atom("H", (-0.7130425, +2.1945191, -1.7301600)),
                              Atom("Fe", (+0.0000000, -0.0000000, +0.0000000)),
                              Atom("C", (-0.9923419, -0.7209786, +1.7378500)),
                              Atom("C", (-0.9923419, +0.7209786, +1.7378500)),
                              Atom("C", (+0.3790409, +1.1665679, +1.7378500)),
                              Atom("C", (+1.2266021, -0.0000000, +1.7378500)),
                              Atom("C", (+0.3790409, -1.1665679, +1.7378500)),
                              Atom("H", (-1.8667695, -1.3562874, +1.7301600)),
                              Atom("H", (-1.8667695, +1.3562874, +1.7301600)),
                              Atom("H", (+0.7130425, +2.1945191, +1.7301600)),
                              Atom("H", (+2.3074540, -0.0000000, +1.7301600)),
                              Atom("H", (+0.7130425, -2.1945191, +1.7301600))])
    mol_fecp2stag.recentre_ip()
    mol_fecp2stag.reorientate_ip()
    sym = Symmetry(mol_fecp2stag,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_PROLATE
    assert len(sym.sea_groups) == 3
    assert sym._check_proper(5, Vector([1, 0, 0]))
    assert sym._check_improper(1, Vector([0, 0, 1]))
    assert sym._check_improper(10, Vector([1, 0, 0]))
    assert sym.point_group == "D5d"
    assert len(sym.proper_elements[2]) == 5
    assert len(sym.proper_elements[5]) == 1
    assert len(sym.improper_elements[1]) == 5
    assert len(sym.sigma_elements["d"]) == 5
    assert len(sym.improper_elements[10]) == 1
    assert len(sym.proper_generators[2]) == 1
    assert len(sym.proper_generators[5]) == 1
    assert len(sym.improper_generators[1]) == 1
    assert len(sym.sigma_generators["d"]) == 1
    assert abs(sym.proper_generators[2][0].dot(sym.proper_generators[5][0]))\
            < COMMON_ZERO_TOLERANCE


def test_symmetry_ferrocene_eclipsed():
    """Tests for the symmetry analysis of ferrocene (eclipsed).
    """
    mol_fecp2ecli = Molecule([Atom("Fe", (+0.0000000, -0.0000000, +0.0000000)),
                              Atom("C", (+0.3756053, -1.1559941, +1.6578997)),
                              Atom("C", (+0.3756053, +1.1559941, +1.6578997)),
                              Atom("C", (+1.2154841, -0.0000000, +1.6578997)),
                              Atom("C", (+0.3756053, -1.1559941, -1.6578997)),
                              Atom("C", (+0.3756053, +1.1559941, -1.6578997)),
                              Atom("C", (+1.2154841, -0.0000000, -1.6578997)),
                              Atom("H", (+0.7093478, -2.1831479, +1.6582799)),
                              Atom("H", (+0.7093478, +2.1831479, +1.6582799)),
                              Atom("H", (+2.2954976, -0.0000000, +1.6582799)),
                              Atom("H", (+0.7093478, -2.1831479, -1.6582799)),
                              Atom("H", (+0.7093478, +2.1831479, -1.6582799)),
                              Atom("H", (+2.2954976, -0.0000000, -1.6582799)),
                              Atom("C", (-0.9833473, +0.7144436, +1.6578997)),
                              Atom("C", (-0.9833473, -0.7144436, +1.6578997)),
                              Atom("C", (-0.9833473, +0.7144436, -1.6578997)),
                              Atom("C", (-0.9833473, -0.7144436, -1.6578997)),
                              Atom("H", (-1.8570965, +1.3492596, +1.6582799)),
                              Atom("H", (-1.8570965, -1.3492596, +1.6582799)),
                              Atom("H", (-1.8570965, +1.3492596, -1.6582799)),
                              Atom("H", (-1.8570965, -1.3492596, -1.6582799))])
    mol_fecp2ecli.recentre_ip()
    mol_fecp2ecli.reorientate_ip()
    sym = Symmetry(mol_fecp2ecli,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=1e1*LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_PROLATE
    assert len(sym.sea_groups) == 3
    assert sym.point_group == "D5h"
    assert len(sym.proper_elements[2]) == 5
    assert len(sym.proper_elements[5]) == 1
    assert len(sym.improper_elements[1]) == 6
    assert len(sym.sigma_elements["h"]) == 1
    assert len(sym.sigma_elements["v"]) == 5
    assert len(sym.improper_elements[5]) == 1
    assert len(sym.proper_generators[2]) == 1
    assert len(sym.proper_generators[5]) == 1
    assert len(sym.improper_generators[1]) == 1
    assert len(sym.sigma_generators["h"]) == 1
    assert abs(sym.proper_generators[2][0].dot(sym.proper_generators[5][0]))\
            < COMMON_ZERO_TOLERANCE


def test_symmetry_coronene():
    """Tests for the symmetry analysis of coronene.
    """
    mol_coronene = Molecule([Atom("C", (+1.2505676, +0.7220156, +0.0000000)),
                             Atom("C", (+1.2505676, -0.7220156, +0.0000000)),
                             Atom("C", (-0.0000000, -1.4440311, -0.0000000)),
                             Atom("C", (-1.2505676, -0.7220156, -0.0000000)),
                             Atom("C", (-1.2505676, +0.7220156, -0.0000000)),
                             Atom("C", (+0.0000000, +1.4440311, +0.0000000)),
                             Atom("C", (+2.4699128, +1.4260048, +0.0000000)),
                             Atom("C", (+2.4699128, -1.4260048, +0.0000000)),
                             Atom("C", (+0.0000000, +2.8520097, +0.0000000)),
                             Atom("C", (-0.0000000, -2.8520097, -0.0000000)),
                             Atom("C", (-2.4699128, +1.4260048, -0.0000000)),
                             Atom("C", (-2.4699128, -1.4260048, -0.0000000)),
                             Atom("C", (+3.6981235, +0.6859821, +0.0000000)),
                             Atom("C", (+2.4431396, +2.8596778, +0.0000000)),
                             Atom("C", (+3.6981235, -0.6859821, +0.0000000)),
                             Atom("C", (+2.4431396, -2.8596778, +0.0000000)),
                             Atom("C", (+1.2549838, +3.5456599, +0.0000000)),
                             Atom("C", (-1.2549838, +3.5456599, -0.0000000)),
                             Atom("C", (+1.2549838, -3.5456599, +0.0000000)),
                             Atom("C", (-1.2549838, -3.5456599, -0.0000000)),
                             Atom("C", (-2.4431396, +2.8596778, -0.0000000)),
                             Atom("C", (-3.6981235, +0.6859821, -0.0000000)),
                             Atom("C", (-2.4431396, -2.8596778, -0.0000000)),
                             Atom("C", (-3.6981235, -0.6859821, -0.0000000)),
                             Atom("H", (+4.6359383, +1.2174157, +0.0000000)),
                             Atom("H", (+3.3722821, +3.4061325, +0.0000000)),
                             Atom("H", (+4.6359383, -1.2174157, +0.0000000)),
                             Atom("H", (+3.3722821, -3.4061325, +0.0000000)),
                             Atom("H", (+1.2636563, +4.6235482, +0.0000000)),
                             Atom("H", (-1.2636563, +4.6235482, -0.0000000)),
                             Atom("H", (+1.2636563, -4.6235482, +0.0000000)),
                             Atom("H", (-1.2636563, -4.6235482, -0.0000000)),
                             Atom("H", (-3.3722821, +3.4061325, -0.0000000)),
                             Atom("H", (-4.6359383, +1.2174157, -0.0000000)),
                             Atom("H", (-3.3722821, -3.4061325, -0.0000000)),
                             Atom("H", (-4.6359383, -1.2174157, -0.0000000))])
    mol_coronene.recentre_ip()
    mol_coronene.reorientate_ip()
    sym = Symmetry(mol_coronene,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=1e1*LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_OBLATE_PLANAR
    assert len(sym.sea_groups) == 4
    assert sym.point_group == "D6h"
    sym.analyse(moi_thresh=LOOSE_ZERO_TOLERANCE,
                dist_thresh=LOOSE_ZERO_TOLERANCE)
    assert len(sym.sea_groups) == 7
    assert sym.point_group == "D6h"
    assert len(sym.proper_elements[2]) == 7
    assert len(sym.proper_elements[6]) == 1
    assert len(sym.improper_elements[1]) == 7
    assert len(sym.sigma_elements["h"]) == 1
    assert len(sym.sigma_elements["v"]) == 6
    assert len(sym.improper_elements[3]) == 1
    assert len(sym.improper_elements[6]) == 1
    assert len(sym.proper_generators[2]) == 1
    assert len(sym.proper_generators[6]) == 1
    assert len(sym.improper_generators[1]) == 1
    assert len(sym.sigma_generators["h"]) == 1
    assert abs(sym.proper_generators[2][0].dot(sym.proper_generators[6][0]))\
            < COMMON_ZERO_TOLERANCE


def test_symmetry_au26():
    """Tests for the symmetry analysis of Au26 cluster.
    """
    mol_au26 = Molecule([Atom("Au", (+2.4210019, +1.3977661, +3.5475440)),
                         Atom("Au", (+2.4210019, -1.3977661, +3.5475440)),
                         Atom("Au", (-0.0000000, -2.7955322, +3.5475440)),
                         Atom("Au", (-2.4210019, -1.3977661, +3.5475440)),
                         Atom("Au", (-2.4210019, +1.3977661, +3.5475440)),
                         Atom("Au", (+0.0000000, +2.7955322, +3.5475440)),
                         Atom("Au", (+2.8051267, -0.0000000, +1.1720040)),
                         Atom("Au", (+1.4025633, -2.4293110, +1.1720040)),
                         Atom("Au", (-1.4025633, -2.4293110, +1.1720040)),
                         Atom("Au", (-2.8051267, +0.0000000, +1.1720040)),
                         Atom("Au", (-1.4025633, +2.4293110, +1.1720040)),
                         Atom("Au", (+1.4025633, +2.4293110, +1.1720040)),
                         Atom("Au", (-0.0000000, -0.0000000, +3.5328200)),
                         Atom("Au", (+2.4293110, +1.4025633, -1.1720040)),
                         Atom("Au", (+2.4293110, -1.4025633, -1.1720040)),
                         Atom("Au", (-0.0000000, -2.8051267, -1.1720040)),
                         Atom("Au", (-2.4293110, -1.4025633, -1.1720040)),
                         Atom("Au", (-2.4293110, +1.4025633, -1.1720040)),
                         Atom("Au", (+0.0000000, +2.8051267, -1.1720040)),
                         Atom("Au", (+2.7955322, -0.0000000, -3.5475440)),
                         Atom("Au", (+1.3977661, -2.4210019, -3.5475440)),
                         Atom("Au", (-1.3977661, -2.4210019, -3.5475440)),
                         Atom("Au", (-2.7955322, +0.0000000, -3.5475440)),
                         Atom("Au", (-1.3977661, +2.4210019, -3.5475440)),
                         Atom("Au", (+1.3977661, +2.4210019, -3.5475440)),
                         Atom("Au", (+0.0000000, +0.0000000, -3.5328200))])
    mol_au26.recentre_ip()
    mol_au26.reorientate_ip()
    sym = Symmetry(mol_au26,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_PROLATE
    assert len(sym.sea_groups) == 3
    assert sym.point_group == "D6d"
    assert len(sym.proper_elements[2]) == 7
    assert len(sym.proper_elements[3]) == 1
    assert len(sym.proper_elements[6]) == 1
    assert len(sym.improper_elements[1]) == 6
    assert len(sym.sigma_elements["d"]) == 6
    assert len(sym.improper_elements[4]) == 1
    assert len(sym.improper_elements[12]) == 1
    assert len(sym.proper_generators[2]) == 1
    assert len(sym.proper_generators[6]) == 1
    assert len(sym.improper_generators[1]) == 1
    assert len(sym.sigma_generators["d"]) == 1
    assert abs(sym.proper_generators[2][0].dot(sym.proper_generators[6][0]))\
            < COMMON_ZERO_TOLERANCE


# ===================
# Non-dihedral groups
# ===================

def test_symmetry_pph3():
    """Tests for the symmetry analysis of PPh3.
    """
    mol_pph3 = Molecule([Atom("P", (+0.0000000, +0.0000000, -1.2940022)),
                         Atom("C", (-1.5196164, -0.6547108, -0.4442647)),
                         Atom("C", (-2.1924704, +0.0239057, +0.5792609)),
                         Atom("H", (-1.8586413, +0.9795604, +0.9380763)),
                         Atom("C", (-3.3305709, -0.5262302, +1.1737532)),
                         Atom("H", (-3.8441623, +0.0000000, +1.9631506)),
                         Atom("C", (-3.8129783, -1.7508841, +0.7568604)),
                         Atom("H", (-4.6952342, -2.1735369, +1.2198380)),
                         Atom("C", (-3.1643085, -2.4329697, -0.2563246)),
                         Atom("H", (-3.5464251, -3.3880182, -0.5791761)),
                         Atom("C", (-2.0251457, -1.8885980, -0.8560660)),
                         Atom("H", (-1.5327539, -2.4346754, -1.6460810)),
                         Atom("C", (+1.3268044, -0.9886710, -0.4442647)),
                         Atom("C", (+1.0755323, -1.9106879, +0.5792609)),
                         Atom("H", (+0.0809965, -2.0994107, +0.9380763)),
                         Atom("C", (+2.1210142, -2.6212439, +1.1737532)),
                         Atom("H", (+1.9220812, -3.3291422, +1.9631506)),
                         Atom("C", (+3.4227992, -2.4266940, +0.7568604)),
                         Atom("H", (+4.2299552, -2.9794236, +1.2198380)),
                         Atom("C", (+3.6891678, -1.5238867, -0.2563246)),
                         Atom("H", (+4.7073224, -1.3772852, -0.5791761)),
                         Atom("C", (+2.6481467, -0.8095287, -0.8560660)),
                         Atom("H", (+2.8748677, -0.1100661, -1.6460810)),
                         Atom("C", (+0.1928120, +1.6433818, -0.4442647)),
                         Atom("C", (+1.1169382, +1.8867822, +0.5792609)),
                         Atom("H", (+1.7776448, +1.1198504, +0.9380763)),
                         Atom("C", (+1.2095567, +3.1474741, +1.1737532)),
                         Atom("H", (+1.9220812, +3.3291422, +1.9631506)),
                         Atom("C", (+0.3901790, +4.1775781, +0.7568604)),
                         Atom("H", (+0.4652790, +5.1529605, +1.2198380)),
                         Atom("C", (-0.5248593, +3.9568564, -0.2563246)),
                         Atom("H", (-1.1608972, +4.7653034, -0.5791761)),
                         Atom("C", (-0.6230010, +2.6981266, -0.8560660)),
                         Atom("H", (-1.3421138, +2.5447415, -1.6460810))])
    mol_pph3.recentre_ip()
    mol_pph3.reorientate_ip()
    sym = Symmetry(mol_pph3,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=1e1*LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_OBLATE
    assert len(sym.sea_groups) == 12
    assert sym.point_group == "C3"
    assert len(sym.proper_elements[3]) == 1
    assert len(sym.proper_generators[3]) == 1


def test_symmetry_hcoco4():
    """Tests for the symmetry analysis of HCo(CO)4.
    """
    mol_hcoco4 = Molecule([Atom("Co", (-0.0000000, -0.0000000, -0.4139416)),
                           Atom("C", (+0.0000000, -0.0000000, +1.5249367)),
                           Atom("C", (+0.9692513, +1.6787926, -0.4139373)),
                           Atom("C", (+0.9692513, -1.6787926, -0.4139373)),
                           Atom("O", (+1.5332921, +2.6557399, -0.4139622)),
                           Atom("O", (+1.5332921, -2.6557399, -0.4139622)),
                           Atom("O", (+0.0000000, -0.0000000, +2.6529859)),
                           Atom("C", (-1.9385027, +0.0000000, -0.4139373)),
                           Atom("O", (-3.0665842, +0.0000000, -0.4139622)),
                           Atom("H", (-0.0000000, +0.0000000, -1.5699801))])
    mol_hcoco4.recentre_ip()
    mol_hcoco4.reorientate_ip()
    sym = Symmetry(mol_hcoco4,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_OBLATE
    assert len(sym.sea_groups) == 6
    assert sym.point_group == "C3v"
    assert len(sym.proper_elements[3]) == 1
    assert len(sym.improper_elements[1]) == 3
    assert len(sym.sigma_elements["v"]) == 3
    assert len(sym.proper_generators[3]) == 1
    assert len(sym.improper_generators[1]) == 1
    assert len(sym.sigma_generators["v"]) == 1


def test_symmetry_boric_acid():
    """Tests for the symmetry analysis of boric acid.
    """
    mol_boric_acid = Molecule([Atom("B", (-0.0000000, +0.0000000, +0.0000000)),
                               Atom("O", (+0.0323754, +0.0000000, -1.4656406)),
                               Atom("O", (-1.2854697, +0.0000000, +0.7047823)),
                               Atom("O", (+1.2530942, +0.0000000, +0.7608582)),
                               Atom("H", (-1.9816712, +0.0000000, +0.0000000)),
                               Atom("H", (+0.9908356, +0.0000000, -1.7161776)),
                               Atom("H", (+0.9908356, +0.0000000, +1.7161776))])
    mol_boric_acid.recentre_ip()
    mol_boric_acid.reorientate_ip()
    sym = Symmetry(mol_boric_acid,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=1e1*LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_OBLATE_PLANAR
    assert len(sym.sea_groups) == 3
    assert sym.point_group == "C3h"
    assert len(sym.proper_elements[3]) == 1
    assert len(sym.improper_elements[1]) == 1
    assert len(sym.sigma_elements["h"]) == 1
    assert len(sym.improper_elements[3]) == 1
    assert len(sym.proper_generators[3]) == 1
    assert len(sym.improper_generators[1]) == 1
    assert len(sym.sigma_generators["h"]) == 1


def test_symmetry_tetraazacopper():
    """Tests for the symmetry analysis of tetraazacopper(II).
    """
    mol_tetraazacopper = Molecule([Atom("Cu", (+0.0000000, +0.0000000, +0.3247251)),
                                   Atom("N", (+0.5890221, -1.8823411, -0.1746016)),
                                   Atom("O", (+0.0000000, +0.0000000, +2.4678198)),
                                   Atom("N", (+1.8823411, +0.5890221, -0.1746016)),
                                   Atom("N", (-0.5890221, +1.8823411, -0.1746016)),
                                   Atom("N", (-1.8823411, -0.5890221, -0.1746016)),
                                   Atom("C", (+1.0147646, -2.7607853, +0.9203949)),
                                   Atom("C", (+1.6562171, -1.6562171, -1.1308031)),
                                   Atom("C", (+2.5113563, -0.5993094, -0.8373027)),
                                   Atom("C", (+2.7607853, +1.0147646, +0.9203949)),
                                   Atom("C", (+1.6562171, +1.6562171, -1.1308031)),
                                   Atom("C", (+0.5993094, +2.5113563, -0.8373027)),
                                   Atom("C", (-1.0147646, +2.7607853, +0.9203949)),
                                   Atom("C", (-1.6562171, +1.6562171, -1.1308031)),
                                   Atom("C", (-2.5113563, +0.5993094, -0.8373027)),
                                   Atom("C", (-2.7607853, -1.0147646, +0.9203949)),
                                   Atom("C", (-1.6562171, -1.6562171, -1.1308031)),
                                   Atom("C", (-0.5993094, -2.5113563, -0.8373027))])
    mol_tetraazacopper.recentre_ip()
    mol_tetraazacopper.reorientate_ip()
    sym = Symmetry(mol_tetraazacopper,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_OBLATE
    assert len(sym.sea_groups) == 6
    assert sym.point_group == "C4"
    assert len(sym.proper_elements[4]) == 1
    assert len(sym.proper_generators[4]) == 1


def test_symmetry_ge9():
    """Tests for the symmetry analysis of nido-[Ge9]4-.
    """
    mol_ge9 = Molecule([Atom("Ge", (+0.0000000, -0.0000000, +2.4176476)),
                        Atom("Ge", (+1.3970992, +1.3970992, +0.7606456)),
                        Atom("Ge", (+1.3970992, -1.3970992, +0.7606456)),
                        Atom("Ge", (-1.3970992, -1.3970992, +0.7606456)),
                        Atom("Ge", (-1.3970992, +1.3970992, +0.7606456)),
                        Atom("Ge", (+0.0000000, +1.8291982, -1.3650575)),
                        Atom("Ge", (+1.8291982, -0.0000000, -1.3650575)),
                        Atom("Ge", (-0.0000000, -1.8291982, -1.3650575)),
                        Atom("Ge", (-1.8291982, +0.0000000, -1.3650575))])
    mol_ge9.recentre_ip()
    mol_ge9.reorientate_ip()
    sym = Symmetry(mol_ge9,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_PROLATE
    assert len(sym.sea_groups) == 3
    assert sym.point_group == "C4v"
    assert len(sym.proper_elements[4]) == 1
    assert len(sym.improper_elements[1]) == 4
    assert len(sym.proper_generators[4]) == 1
    assert len(sym.improper_generators[1]) == 1
    assert len(sym.sigma_generators["v"]) == 1


def test_symmetry_tetraazidocopper():
    """Tests for the symmetry analysis of tetraazidocopper(II).
    """
    mol_tetraazidocopper = Molecule([Atom("Cu", (-0.0000000, +0.0000000, +0.0000000)),
                                     Atom("N", (+1.8874083, +0.5303184, +0.0000000)),
                                     Atom("N", (+2.2647239, +1.6466005, +0.0000000)),
                                     Atom("N", (+2.6981358, +2.6981358, +0.0000000)),
                                     Atom("N", (+0.5303184, -1.8874083, +0.0000000)),
                                     Atom("N", (+1.6466005, -2.2647239, +0.0000000)),
                                     Atom("N", (+2.6981358, -2.6981358, +0.0000000)),
                                     Atom("N", (-0.5303184, +1.8874083, +0.0000000)),
                                     Atom("N", (-1.6466005, +2.2647239, +0.0000000)),
                                     Atom("N", (-2.6981358, +2.6981358, +0.0000000)),
                                     Atom("N", (-1.8874083, -0.5303184, +0.0000000)),
                                     Atom("N", (-2.2647239, -1.6466005, +0.0000000)),
                                     Atom("N", (-2.6981358, -2.6981358, +0.0000000))])
    mol_tetraazidocopper.recentre_ip()
    mol_tetraazidocopper.reorientate_ip()
    sym = Symmetry(mol_tetraazidocopper,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_OBLATE_PLANAR
    assert len(sym.sea_groups) == 4
    assert sym.point_group == "C4h"
    assert len(sym.proper_elements[2]) == 1
    assert len(sym.proper_elements[4]) == 1
    assert len(sym.improper_elements[1]) == 1
    assert len(sym.improper_elements[2]) == 1
    assert len(sym.proper_generators[4]) == 1
    assert len(sym.improper_generators[1]) == 1
    assert len(sym.sigma_generators["h"]) == 1


def test_symmetry_tetraphenylmethane():
    """Tests for the symmetry analysis of tetraphenylmethane.
    """
    mol_tetraphenylmethane = Molecule([Atom("C", (+0.0000000, +0.0000000, +0.0000000)),
                                       Atom("C", (+0.0239436, +1.2075608, -0.9723105)),
                                       Atom("C", (+1.1707975, +1.4079863, -1.7776929)),
                                       Atom("H", (+2.0224470, +0.7449347, -1.6907996)),
                                       Atom("C", (+1.2428759, +2.4635103, -2.6873394)),
                                       Atom("H", (+2.1365253, +2.6049504, -3.2813321)),
                                       Atom("C", (+0.1645169, +3.3309954, -2.8353808)),
                                       Atom("H", (+0.2196458, +4.1447633, -3.5468090)),
                                       Atom("C", (-0.9886340, +3.1421854, -2.0750080)),
                                       Atom("H", (-1.8310936, +3.8083446, -2.2056665)),
                                       Atom("C", (-1.0640534, +2.0936617, -1.1519134)),
                                       Atom("H", (-1.9875257, +1.9792528, -0.6053569)),
                                       Atom("C", (+1.2075608, -0.0239436, +0.9723105)),
                                       Atom("C", (+2.0936617, +1.0640534, +1.1519134)),
                                       Atom("H", (+1.9792528, +1.9875257, +0.6053569)),
                                       Atom("C", (+3.1421854, +0.9886340, +2.0750080)),
                                       Atom("H", (+3.8083446, +1.8310936, +2.2056665)),
                                       Atom("C", (+3.3309954, -0.1645169, +2.8353808)),
                                       Atom("H", (+4.1447633, -0.2196458, +3.5468090)),
                                       Atom("C", (+2.4635103, -1.2428759, +2.6873394)),
                                       Atom("H", (+2.6049504, -2.1365253, +3.2813321)),
                                       Atom("C", (+1.4079863, -1.1707975, +1.7776929)),
                                       Atom("H", (+0.7449347, -2.0224470, +1.6907996)),
                                       Atom("C", (-0.0239436, -1.2075608, -0.9723105)),
                                       Atom("C", (-1.1707975, -1.4079863, -1.7776929)),
                                       Atom("H", (-2.0224470, -0.7449347, -1.6907996)),
                                       Atom("C", (-1.2428759, -2.4635103, -2.6873394)),
                                       Atom("H", (-2.1365253, -2.6049504, -3.2813321)),
                                       Atom("C", (-0.1645169, -3.3309954, -2.8353808)),
                                       Atom("H", (-0.2196458, -4.1447633, -3.5468090)),
                                       Atom("C", (+0.9886340, -3.1421854, -2.0750080)),
                                       Atom("H", (+1.8310936, -3.8083446, -2.2056665)),
                                       Atom("C", (+1.0640534, -2.0936617, -1.1519134)),
                                       Atom("H", (+1.9875257, -1.9792528, -0.6053569)),
                                       Atom("C", (-1.2075608, +0.0239436, +0.9723105)),
                                       Atom("C", (-1.4079863, +1.1707975, +1.7776929)),
                                       Atom("H", (-0.7449347, +2.0224470, +1.6907996)),
                                       Atom("C", (-2.4635103, +1.2428759, +2.6873394)),
                                       Atom("H", (-2.6049504, +2.1365253, +3.2813321)),
                                       Atom("C", (-3.3309954, +0.1645169, +2.8353808)),
                                       Atom("H", (-4.1447633, +0.2196458, +3.5468090)),
                                       Atom("C", (-3.1421854, -0.9886340, +2.0750080)),
                                       Atom("H", (-3.8083446, -1.8310936, +2.2056665)),
                                       Atom("C", (-2.0936617, -1.0640534, +1.1519134)),
                                       Atom("H", (-1.9792528, -1.9875257, +0.6053569))])
    mol_tetraphenylmethane.recentre_ip()
    mol_tetraphenylmethane.reorientate_ip()
    sym = Symmetry(mol_tetraphenylmethane,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_PROLATE
    assert len(sym.sea_groups) == 12
    assert sym.point_group == "S4"
    assert len(sym.proper_elements[2]) == 1
    assert len(sym.improper_elements[4]) == 1
    assert len(sym.improper_generators[4]) == 1


def test_symmetry_65coronane():
    """Tests for the symmetry analysis of [6.5]coronane.
    """
    mol_65coronane = Molecule([Atom("C", (+1.5459875, -0.0899052, +0.1887992)),
                               Atom("C", (+0.6951336, -1.3838171, -0.1887992)),
                               Atom("C", (-0.8508539, -1.2939119, +0.1887992)),
                               Atom("C", (+0.8508539, +1.2939119, -0.1887992)),
                               Atom("C", (-0.6951336, +1.3838171, +0.1887992)),
                               Atom("C", (-1.5459875, +0.0899052, -0.1887992)),
                               Atom("C", (-1.4084150, -2.5263792, -0.5779154)),
                               Atom("C", (+0.6598146, -1.8478039, -1.6752250)),
                               Atom("C", (-0.6344565, -2.6438412, -1.8840643)),
                               Atom("C", (+2.8921161, +0.0434664, -0.5779154)),
                               Atom("C", (+1.2703378, +1.4953182, -1.6752250)),
                               Atom("C", (+2.6068619, +0.7724651, -1.8840643)),
                               Atom("C", (-1.4837011, +2.4829128, -0.5779154)),
                               Atom("C", (-1.9301524, +0.3524857, -1.6752250)),
                               Atom("C", (-1.9724054, +1.8713761, -1.8840643)),
                               Atom("C", (+1.4837011, -2.4829128, +0.5779154)),
                               Atom("C", (+1.9301524, -0.3524857, +1.6752250)),
                               Atom("C", (+1.9724054, -1.8713761, +1.8840643)),
                               Atom("C", (-1.2703378, -1.4953182, +1.6752250)),
                               Atom("C", (-2.8921161, -0.0434664, +0.5779154)),
                               Atom("C", (-2.6068619, -0.7724651, +1.8840643)),
                               Atom("C", (-0.6598146, +1.8478039, +1.6752250)),
                               Atom("C", (+1.4084150, +2.5263792, +0.5779154)),
                               Atom("C", (+0.6344565, +2.6438412, +1.8840643)),
                               Atom("H", (-2.5068104, -2.6412755, -0.6776515)),
                               Atom("H", (-1.0887514, -3.4255969, -0.0071427)),
                               Atom("H", (+1.5716595, -2.3589584, -2.0526684)),
                               Atom("H", (+0.5879209, -1.0183088, -2.3621767)),
                               Atom("H", (-0.4611560, -3.7129832, -2.1360815)),
                               Atom("H", (-1.2069735, -2.1999941, -2.7261924)),
                               Atom("H", (+3.5408169, -0.8503237, -0.6776515)),
                               Atom("H", (+3.5110297, +0.7699121, -0.0071427)),
                               Atom("H", (+1.2570882, +2.5405763, -2.0526684)),
                               Atom("H", (+0.5879209, +1.0183088, -2.3621767)),
                               Atom("H", (+3.4461158, +1.4571188, -2.1360815)),
                               Atom("H", (+2.5087376, +0.0547273, -2.7261924)),
                               Atom("H", (-1.0340065, +3.4915993, -0.6776515)),
                               Atom("H", (-2.4222783, +2.6556848, -0.0071427)),
                               Atom("H", (-2.8287477, -0.1816178, -2.0526684)),
                               Atom("H", (-1.1758417, +0.0000000, -2.3621767)),
                               Atom("H", (-2.9849598, +2.2558644, -2.1360815)),
                               Atom("H", (-1.3017640, +2.1452668, -2.7261924)),
                               Atom("H", (+2.4222783, -2.6556848, +0.0071427)),
                               Atom("H", (+1.0340065, -3.4915993, +0.6776515)),
                               Atom("H", (+1.1758417, -0.0000000, +2.3621767)),
                               Atom("H", (+2.8287477, +0.1816178, +2.0526684)),
                               Atom("H", (+1.3017640, -2.1452668, +2.7261924)),
                               Atom("H", (+2.9849598, -2.2558644, +2.1360815)),
                               Atom("H", (-0.5879209, -1.0183088, +2.3621767)),
                               Atom("H", (-1.2570882, -2.5405763, +2.0526684)),
                               Atom("H", (-3.5110297, -0.7699121, +0.0071427)),
                               Atom("H", (-3.5408169, +0.8503237, +0.6776515)),
                               Atom("H", (-2.5087376, -0.0547273, +2.7261924)),
                               Atom("H", (-3.4461158, -1.4571188, +2.1360815)),
                               Atom("H", (-0.5879209, +1.0183088, +2.3621767)),
                               Atom("H", (-1.5716595, +2.3589584, +2.0526684)),
                               Atom("H", (+1.0887514, +3.4255969, +0.0071427)),
                               Atom("H", (+2.5068104, +2.6412755, +0.6776515)),
                               Atom("H", (+0.4611560, +3.7129832, +2.1360815)),
                               Atom("H", (+1.2069735, +2.1999941, +2.7261924))])
    mol_65coronane.recentre_ip()
    mol_65coronane.reorientate_ip()
    sym = Symmetry(mol_65coronane,
                   moi_thresh=LOOSE_ZERO_TOLERANCE,
                   dist_thresh=1e1*LOOSE_ZERO_TOLERANCE)
    sym.analyse()
    assert sym.rotational_symmetry == ROTSYM_SYMTOP_OBLATE
    assert len(sym.sea_groups) == 10
    assert sym.point_group == "S6"
    assert len(sym.proper_elements[3]) == 1
    assert len(sym.improper_elements[2]) == 1
    assert len(sym.improper_elements[6]) == 1
    assert len(sym.improper_generators[6]) == 1
