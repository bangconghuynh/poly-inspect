""":mod:`.test_affine_space` contains unit tests for classes implemented in
:mod:`src.auxiliary.affine_space`.
"""

import numpy as np  # type: ignore
import pytest  # type: ignore

from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE
from polyinspect.auxiliary.geometrical_space import Point, Vector
from polyinspect.auxiliary.affine_space import AffineSubspace, Line, Hyperplane


# pylint: disable=C0103
# C0103: Variable names follow geometrical conventions.


def test_affine_space_basic():
    r"""Tests for the basic construction of affine spaces in
    :math:`\mathbb{R}^n` and operations on them.
    """
    # pylint: disable=R0914,R0915
    # R0914: Each point is explicitly defined as a separate variable for ease
    #        of checking.

    # -----------------------------------------------
    # 1-d affine subspaces in R^3, i.e., lines in 3-d
    # -----------------------------------------------
    point1 = Point([1.0, 2.0, 4.9])
    dir1a = Vector([1.2, 4.5, 7.7])
    direction1 = [dir1a]
    aff1 = AffineSubspace(point1, direction1)
    assert aff1.anchor == point1
    assert aff1.direction == [dir1a.normalise()]
    assert aff1.__repr__() ==\
            "AffineSubspace[\n" +\
            "  Anchor: Point(1.000, 2.000, 4.900)\n" +\
            "  Direction:\n" +\
            "    Vector(0.133, 0.500, 0.856)\n" +\
            "]"

    point2 = Point([-5.0, 4.0, -7.9])
    dir2a = Vector([-2.4, -9.0, -15.4])
    direction2 = [dir2a]
    aff2 = AffineSubspace(point2, direction2)
    assert aff2.get_point([0.0]) == point2
    assert aff2.get_point([2.0]) == point2\
                                    + 2.0*Point.from_vector(-dir2a.normalise())
    assert aff2.is_parallel(aff1)
    assert aff2.contains_point(point2)
    assert not aff2.contains_point(point1)

    # ------------------------------------------------
    # 2-d affine subspaces in R^3, i.e., planes in 3-d
    # ------------------------------------------------
    point3 = Point(n=3)
    dir3a = Vector([1, 0, 0])
    dir3b = Vector([0, 1, 0])
    dir3 = [dir3a, dir3b]
    aff3 = AffineSubspace(point3, dir3)  # xy-plane
    assert aff3.__repr__() ==\
            "AffineSubspace[\n" +\
            "  Anchor: Point(0.000, 0.000, 0.000)\n" +\
            "  Direction:\n" +\
            "    Vector(1.000, 0.000, 0.000)\n" +\
            "    Vector(0.000, 1.000, 0.000)\n" +\
            "]"
    assert aff3.contains_point(point3)
    assert aff3 != aff2
    assert aff3 != aff1
    assert not aff3 == dir3  # pylint: disable=C0113

    point4 = Point([1, 2, 3])
    dir4a = Vector([1, 1, 0])
    dir4b = Vector([-0.5, 1, 0])
    dir4 = [dir4a, dir4b]
    aff4 = AffineSubspace(point4, dir4)
    assert aff4.is_parallel(aff3)
    assert aff4 != aff3
    assert not aff3.contains_point(point4)
    assert aff4.contains_point(point4)
    assert aff4.find_parameters(point4) == [0, 0]
    point4b = aff4.get_point([-0.4, 2.789])
    assert np.allclose(aff4.find_parameters(point4b), [-0.4, 2.789],
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)

    point5 = Point([-3, 4, -2])
    dir5a = Vector([1, 1, 1])
    dir5b = Vector([-1.5, 1, 0])
    dir5 = [dir5a, dir5b]
    aff5 = AffineSubspace(point5, dir5)
    assert not aff5.is_parallel(aff4)
    assert not aff5.contains_point(point4)
    point5b = aff5.get_point([0, np.pi])
    assert np.allclose(aff5.find_parameters(point5b), [0, np.pi],
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)
    assert aff5.find_parameters(point4) is None

    point6 = Point([-5.0, 4.0, -7.9])
    dir6a = Vector([2.0, -1.5, 0])
    direction6 = [dir6a]
    aff6 = AffineSubspace(point6, direction6)
    assert aff6.is_parallel(aff4)
    dir6a = Vector([2.0, -1.5, 1])
    direction6 = [dir6a]
    aff6 = AffineSubspace(point6, direction6)
    assert not aff6.is_parallel(aff4)


def test_affine_space_intersection():
    r"""Tests for the calculation of intersections of affine subspaces
    :math:`\mathbb{R}^n`.
    """
    # pylint: disable=R0914
    # R0914: Each point is explicitly defined as a separate variable for ease
    #        of checking.
    # -----------------------------------------------
    # 1-d affine subspaces in R^3, i.e., lines in 3-d
    # -----------------------------------------------
    point1 = Point([1.0, 2.0, 4.9])
    dir1a = Vector([1.2, 4.5, 7.7])
    direction1 = [dir1a]
    aff1 = AffineSubspace(point1, direction1)

    point2 = Point([-5.0, 4.0, -7.9])
    dir2a = Vector([-2.4, -9.0, -15.4])
    direction2 = [dir2a]
    aff2 = AffineSubspace(point2, direction2)
    assert aff2.intersects(aff1) == (0, None)  # parallel lines

    point2d = Point([0, 9.0, -2.9])
    dir2da = Vector([1, 1, 1])
    direction2d = [dir2da]
    aff2d = AffineSubspace(point2d, direction2d)
    assert aff2d.intersects(aff2) == (1, point2)

    point2dd = Point([0, 2.0, 4.9])
    dir2dda = Vector([0, 1, 0])
    direction2dd = [dir2dda]
    aff2dd = AffineSubspace(point2dd, direction2dd)
    assert aff2dd.intersects(aff1) == (0, None)  # skew lines


    # ------------------------------------------------
    # 2-d affine subspaces in R^3, i.e., planes in 3-d
    # ------------------------------------------------
    point3 = Point(n=3)
    dir3a = Vector([1, 0, 0])
    dir3b = Vector([0, 1, 0])
    dir3 = [dir3a, dir3b]
    aff3 = AffineSubspace(point3, dir3)  # xy-plane

    point4 = Point([1, 2, 3])
    dir4a = Vector([1, 1, 0])
    dir4b = Vector([-0.5, 1, 0])
    dir4 = [dir4a, dir4b]
    aff4 = AffineSubspace(point4, dir4)
    assert aff4.intersects(aff3) == (0, None)

    point5 = Point([2, 0, -10])
    dir5a = Vector([1, 0, 1])
    dir5b = Vector([1, 0, -1])
    dir5 = [dir5a, dir5b]
    aff5 = AffineSubspace(point5, dir5)  # xz-plane
    assert aff3.intersects(aff5) == (np.inf, AffineSubspace(point3, [dir3a]))
    assert aff5.intersects(aff3) == (np.inf, AffineSubspace(point3, [dir3a]))

    point6 = Point([-5.0, 4.0, 0])
    dir6a = Vector([2.0, -1.5, 0])
    direction6 = [dir6a]
    aff6 = AffineSubspace(point6, direction6)  # A line in the xy-plane
    assert aff6.intersects(aff3) == (np.inf, aff6)
    assert aff3.intersects(aff6) == (np.inf, aff6)


def test_line_basic():
    r"""Tests for the basic construction of lines in :math:`\mathbb{R}^n` and
    operations on them.
    """
    anchor1 = Point([2.111, 4.329, 6.546, 8.976, 1.544])
    dir1 = Vector([-1, -2, 0, 1, -3])
    line1 = Line(anchor1, dir1)
    assert line1.anchor == anchor1
    assert line1.get_a_point() == anchor1
    assert line1.direction[0] == -dir1.normalise()
    assert line1.dim == 5
    assert line1.__repr__() == "Line[Point(2.111, 4.329, 6.546, 8.976, 1.544)"\
            + " + lambda*Vector(0.258, 0.516, 0.000, -0.258, 0.775)]"

    dir2 = Vector([1, 1, 0, 2, -1])
    line2 = Line(anchor1, dir2)
    assert line2.direction[0] == dir2.normalise()
    assert line2 != line1

    with pytest.raises(AssertionError):
        Line(Point(n=3), Vector([1, 2, 3, 4, 5]))
    with pytest.raises(AssertionError):
        Line(Point(n=5), Vector(n=5))

    anchor3 = Point([3, 4, 6, 8, 9, 7])
    dir3 = Vector([1, 1, 1, 1, 1, 1])
    line3 = Line(anchor3, dir3)
    pointon3 = line3.get_point(params=[5.0])
    assert pointon3 == anchor3 + (5.0/np.sqrt(6))*Point([1, 1, 1, 1, 1, 1])
    assert abs(line3.find_parameters(pointon3)[0] - 5.0)\
            < COMMON_ZERO_TOLERANCE
    pointnoton3 = anchor3 + Point([1, 1, 1, 1, 1, 4])
    assert line3.find_parameters(pointnoton3) is None


def test_line_intersection():
    r"""Tests for the intersection of lines in :math:`\mathbb{R}^n`.
    """
    # pylint: disable=R0914
    # R0914: Each point is explicitly defined as a separate variable for ease
    #        of checking.
    anchor3 = Point([3, 4, 6, 8, 9, 7])
    dir3 = Vector([1, 1, 1, 1, 1, 1])
    line3 = Line(anchor3, dir3)
    anchor4 = Point([9, 6, 12, 10, 15, 13])
    dir4 = Vector([1, 0, 1, 0, 1, 1])
    line4 = Line(anchor4, dir4)
    intersection_pt = line4.get_point(params=[-8])
    line4d = Line(anchor4+Point([0.001, 0, 0, 0, 0, 0]), dir4)
    line4dd = Line(anchor4, dir4+Vector([0, 0, 0.002, 0.0001, 1, 1]))
    line4td = Line(intersection_pt, dir4)

    # 3 and 4 intersect. 3 and 4d or 4dd are skew lines.
    # 4 and 4d are parallel.
    # 4 and 4td coincide.
    assert line3.intersects_line(line4, thresh=1e1*COMMON_ZERO_TOLERANCE)\
            == (1, intersection_pt)
    assert line3.intersects_line(line4d) == (0, None)
    assert line3.intersects_line(line4dd) == (0, None)
    assert line4.intersects_line(line4d) == (0, None)
    assert line4.intersects_line(line4td)[0] == np.inf
    assert line4 == line4td
    assert line4 != line3
    assert not line4 == dir4  # pylint: disable=C0113

    with pytest.raises(AssertionError):
        line4.get_cabinet_projection()

    anchor5 = Point([4.5, 5.4])
    dir5 = Vector([1, 2])
    line5 = Line(anchor5, dir5)
    anchor6 = Point([2.2, 3.1])
    dir6 = Vector([1.1, 2])
    line6 = Line(anchor6, dir6)
    intersection = line6.intersects_line(line5)
    assert intersection[0] == 1
    assert intersection[1].is_same_as(Point([2.2, 3.1])+23/2*Point([1.1, 2]),
                                      thresh=1e1*COMMON_ZERO_TOLERANCE)
    line7 = Line(anchor5, dir6)
    intersection = line7.intersects_line(line5)
    assert intersection[0] == 1
    assert intersection[1].is_same_as(anchor5, thresh=COMMON_ZERO_TOLERANCE)


def test_hyperplane_basic():
    r"""Tests for the basic construction of hyperplanes in :math:`\mathbb{R}^n`
    and operations on them.
    """
    anchor1 = Point(n=3)
    normal1 = Vector([0, 0, -1])
    hplane1 = Hyperplane(anchor1, normal1)
    assert hplane1.__repr__() ==\
        "Hyperplane[r·Vector(0.000, 0.000, 1.000)\n" +\
        "           = Vector(0.000, 0.000, 0.000)·Vector(0.000, 0.000, 1.000)\n" +\
        "           = 0.000]"
    assert hplane1.get_front_normal(Vector([-1, -1, 0.5])) == -normal1
    assert hplane1.get_front_normal(Vector([-1, 1, -1])) == normal1
    with pytest.raises(AssertionError):
        hplane1.get_front_normal(Vector([-1, -1]))

    with pytest.raises(AssertionError):
        Hyperplane(anchor1, Vector([0, 0, 1, 0]))

    anchor2 = Point([3, 2, 4, 5])
    dir2a = Vector([1, 1, 1, 0])
    dir2b = Vector([1, 1, -1, 0])
    dir2c = Vector([1, -1, 1, 0])
    direction2 = [dir2a, dir2b]
    aff2 = AffineSubspace(anchor2, direction2)
    with pytest.raises(AssertionError):
        Hyperplane.from_affinesubspace(aff2)
    direction2 = [dir2a, dir2b, dir2c]
    aff2 = AffineSubspace(anchor2, direction2)
    hplane2 = Hyperplane.from_affinesubspace(aff2)
    assert hplane2.normal == Vector([0, 0, 0, 1])
    assert hplane2 != hplane1


def test_hyperplane_intersection():
    r"""Tests for the intersection of hyperplanes in :math:`\mathbb{R}^n`.
    """
    # -----------------------------------
    # Intersection with other hyperplanes
    # -----------------------------------
    anchor1 = Point(n=3)
    normal1 = Vector([1, 0, 0])
    hplane1 = Hyperplane(anchor1, normal1)  # yz-plane

    point2 = Point([0, 3, -2])
    anchor2 = point2 + 3*Point([0, 1, 0]) - 5/np.sqrt(2)*Point([1, 0, 1])
    normal2 = Vector([1, 0, -1])  # null space of [0, 1, 0] and [1, 0, 1]
    hplane2 = Hyperplane(anchor2, normal2)

    # hplane1 and hplane2 intersect in a line parallel to the y-axis passing
    # through point2.
    assert hplane2.intersects_hyperplane(hplane1)\
            == (np.inf, Line(point2, Vector([0, 1, 0])))

    hplane3 = Hyperplane(point2, Vector([-2, 0, 0]))
    assert hplane3.intersects_hyperplane(hplane1) == (np.inf, hplane1)

    # -----------------------
    # Intersection with lines
    # -----------------------
    anchor4a = Point([-2, -4, -1])
    dir4a = Vector([-0.5, 1, 2])
    line4a = Line(anchor4a, dir4a)
    assert hplane1.intersects_line(line4a) == (1, Point([0, -8, -9]))
    dir4b = Vector([0, 1, -1])
    line4ab = Line(anchor4a, dir4b)
    assert hplane1.intersects_line(line4ab) == (0, None)
    anchor4b = Point([0, 2, 3])
    line4b = Line(anchor4b, dir4b)
    assert hplane1.intersects_line(line4b) == (np.inf, line4b)
