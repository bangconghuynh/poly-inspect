""":mod:`.test_geometrical_space` contains unit tests for classes implemented in
:mod:`src.auxiliary.geometrical_space`.
"""

import numpy as np  # type: ignore
import pytest  # type: ignore

from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE
from polyinspect.auxiliary.geometrical_space import Point, Vector,\
        check_regular_polygon, get_anticlockwise_angle


def test_point_basic():
    r"""Tests for the basic construction of points in :math:`\mathbb{F}^n` and
    operations on them.
    """
    point1 = Point([1.0, 2.0, 4.9, 8.7, np.e])
    point2 = Point(n=5)
    vec21 = point2.get_vector_to(point1)
    assert vec21 == Vector.from_point(point1)
    assert Point.from_vector(vec21) == point1
    assert point1 != point2
    assert point1.__repr__() == "Point(1.000, 2.000, 4.900, 8.700, 2.718)"

    with pytest.raises(ValueError):
        point2.coordinates[1] = 1

    with pytest.raises(ValueError):
        Point([np.nan, 1, 2, 4])
    with pytest.raises(ValueError):
        Point([np.inf, 1, 2, 4])

    assert not point1 != vec21  # pylint: disable=C0113
    assert (point1.is_same_as(vec21)) == NotImplemented

    point3 = Point([1, -2, 3, -4, 9, -7.4324])
    assert point3.bounding_box == [(x, x) for x in point3]

    point4 = Point(n=3)
    point5 = Point([8.9, 2.6, 7.0])
    point6 = Point([8.9, 2.4, 7.0])
    assert point4 < point6 <= point5
    assert point5 > point6 >= point4
    assert point5 >= Point([8.9, 2.6, 7.0])


def test_point_operations() -> None:
    r"""Test for the effects of unary and binary operations on
    :class:`~src.auxiliary.geometrical_space.Point`.
    """
    point5 = Point([8.9, 2.6, 7.0])
    point6 = Point([8.9, 2.4, 7.0])
    assert point5 + point6 == Point([17.8, 5.0, 14.0])
    assert point5.__radd__(point6) == point5 + point6

    alpha = np.arctan(2)
    assert point5.get_cabinet_projection() == Point([8.9-0.5*7.0*np.cos(alpha),
                                                     2.6-0.5*7.0*np.sin(alpha)])

    with pytest.raises(AssertionError):
        Point([1j, 2.6, 7.0])  # type: ignore

    point7 = Point([1, 4, 9, 8])
    with pytest.raises(AssertionError):
        point7.get_cabinet_projection()

    tvec = Vector([1, -1, 4, -2])
    point8 = point7.translate(tvec)
    point7.translate_ip(tvec, None)
    point7.translate_ip(tvec, [id(point7)])
    assert point7 == Point([1, 4, 9, 8])
    point7.translate_ip(tvec, [])
    assert point7 == point8

    with pytest.raises(NotImplementedError):
        point7.rotate(0.5, Vector([0, 0, 1, 0]))

    with pytest.raises(NotImplementedError):
        point7.rotate_ip(0.5, Vector([0, 0, 1, 0]), [])

    point9 = point5.rotate(np.pi/2, Vector([0, 0, 1]))
    assert point9 == Point([-2.6, 8.9, 7.0])

    point5.rotate_ip(np.pi/2, Vector([0, 0, 1]), None)
    assert point5 == Point([8.9, 2.6, 7.0])
    point5.rotate_ip(np.pi/2, Vector([0, 0, 1]), [id(point5)])
    assert point5 == Point([8.9, 2.6, 7.0])
    point5.rotate_ip(np.pi/2, Vector([0, 0, 1]), [])
    assert point5 == point9


def test_vector_basic() -> None:
    r"""Tests for the construction of vectors in :math:`\mathbb{F}^n`.
    """
    vec1 = Vector(n=5)
    assert vec1.dim == 5
    assert np.all(np.abs(vec1.components - np.array([0]*5))
                  < COMMON_ZERO_TOLERANCE)
    assert vec1.__repr__() == "Vector(0.000, 0.000, 0.000, 0.000, 0.000)"

    vec2 = Vector([1.0, 0.99, 1.4, 5.0, 0.6, 9.666, 10])
    assert vec2.dim == 7

    vec3 = Vector([1, 2, 4, 3, 5], n=5)
    assert vec3.dim == 5

    with pytest.raises(ValueError):
        Vector([np.inf, 1, 4, 5])

    with pytest.raises(AssertionError):
        Vector([0, 1, 2], n=2)

    with pytest.raises(AssertionError):
        Vector([1j, 2.6, 7.0])  # type: ignore


def test_vector_operations() -> None:
    r"""Test for the effects of unary and binary operations on
    :class:`~src.auxiliary.geometrical_space.Vector`.
    """
    vec1 = Vector([4.99, np.pi/2, 1, np.exp(1)])
    vec2 = Vector([9.98/2, 0.5*np.pi, 1, np.exp(1)])
    assert vec1 == vec2
    assert vec1 != 2*vec2
    assert -vec1 == vec2*(-1)
    assert vec1 + vec2 == 2*vec1
    assert vec1 - vec2 == Vector(n=4)
    assert vec2.__radd__(vec1) == 2*vec1
    assert vec2/2 == Vector([2.495, np.pi/4, 0.5, 0.5*np.exp(1)])
    with pytest.raises(AssertionError):
        assert vec2/(1e-1*COMMON_ZERO_TOLERANCE)

    vec3 = Vector(n=3)
    with pytest.raises(AssertionError):
        assert vec3 == vec2
    with pytest.raises(AssertionError):
        assert vec3 + vec2
    with pytest.raises(AssertionError):
        assert vec3 - vec2
    assert not vec3 == 1  # pylint: disable=C0113
    assert (vec3.is_same_as(1)) == NotImplemented  # type: ignore

    assert vec1.norm == 5.979678686953254
    assert vec3.norm == 0.0

    # pylint: disable=C0113
    vec4 = Vector([1, 1, 3])
    vec5 = Vector([4, 2, 2])
    assert vec4 <= vec5
    assert not vec4 > vec5
    assert vec5 >= vec4
    assert not vec5 < vec4
    assert not vec5 > Vector([4, 2, 2])
    assert not vec5 < Vector([4, 2, 2])
    assert vec4.dot(vec5) == 12
    assert vec5.dot(vec4) == 12

    with pytest.raises(ZeroDivisionError):
        vec3.normalise()
    assert vec4.normalise() == 1/(np.sqrt(11))*vec4

    vec6 = Vector([0, 2, -3, 4])
    vec7 = Vector([2, 8, 1, 0])
    assert np.allclose(vec6.outer(vec7),
                       np.array([[ 0,   0,  0, 0],
                                 [ 4,  16,  2, 0],
                                 [-6, -24, -3, 0],
                                 [ 8,  32,  4, 0]]),
                       rtol=0, atol=COMMON_ZERO_TOLERANCE)

    vec8 = Vector([-2, 3.4, 6, 2])
    vec9 = Vector([4, -6.8, -12, -4])
    vec10 = Vector([1.1, 2, 0, 1])
    vec10b = Vector([-2, 3.4, 6, -2])
    assert vec8.is_parallel(vec9)
    assert not vec8.is_parallel(vec10)
    assert not vec8.is_parallel(vec10b)


def test_vector3d():
    r"""Tests for the construction of vectors in :math:`\mathbb{R}^3` and
    operations on them.
    """
    vec1 = Vector([1, 2, 3])
    vec2 = Vector([4, 5, 6])
    assert vec1.cross(vec2) == Vector([-3, 6, -3])

    vec1d = Vector([1, 0])
    vec2d = Vector([0, 1])
    assert vec1d.cross(vec2d) == Vector([0, 0, 1])

    vec3 = Vector([1, 0, 0])
    vec3.rotate(2*np.pi/3, Vector([1, 1, 1]))
    assert vec3 == Vector([0, 1, 0])
    with pytest.raises(NotImplementedError):
        vec3.rotate(2*np.pi/3, Vector([1, 1]))

    zcomp = 1
    alpha = np.pi/4
    viewing_vec = Vector([0.5*zcomp*np.cos(alpha),
                          0.5*zcomp*np.sin(alpha),
                          zcomp])
    assert viewing_vec.get_cabinet_projection(alpha) == Vector(n=2)

    vec4 = Vector([1, 0])
    with pytest.raises(NotImplementedError):
        vec4.rotate(2*np.pi/3, Vector([1, 1, 1]))
    with pytest.raises(AssertionError):
        vec4.get_cabinet_projection()


def test_get_anticlockwise_angle() -> None:
    r"""Tests for
    :func:`~src.auxiliary.geometrical_space.get_anticlockwise_angle` function.
    """
    vec1 = Vector([1, 1])
    vec2 = Vector([0, 1])
    normal = Vector([0, 0, 1])
    assert get_anticlockwise_angle(vec1, vec2, normal) == np.pi/4
    assert get_anticlockwise_angle(vec2, vec1, normal) == 2*np.pi - np.pi/4

    vec3 = Vector([1, 0, 0])
    vec4 = Vector([0, 1, 0])
    vec5 = Vector([-1, 0, 0])
    assert get_anticlockwise_angle(vec3, vec4, normal) == np.pi/2
    assert get_anticlockwise_angle(vec4, vec3, normal) == 2*np.pi - np.pi/2
    assert get_anticlockwise_angle(vec3, vec5, normal) == np.pi
    assert get_anticlockwise_angle(vec5, vec3, normal) == np.pi


def test_check_regular_polygon() -> None:
    r"""Tests for :func:`~src.auxiliary.geometrical_space.check_regular_polygon`
    function.
    """
    # pylint: disable=C0103
    # C0103: A, B, C, etc. are standard notations for points.
    A = Point([1, 0])
    B = Point([0, 1])
    C = Point([-1, 0])
    D = Point([0, -1])
    assert check_regular_polygon([A, B, C, D])
    assert check_regular_polygon([A, C, B, D])
    assert check_regular_polygon([B, A, C, D])
    D = Point([0, 0])
    assert not check_regular_polygon([A, B, C, D])

    G = Point([0.5, np.sqrt(3)/2])
    assert check_regular_polygon([D, A, G])
    G = Point([0, 1])
    assert not check_regular_polygon([D, A, G])

    I = Point([4, 0])
    J = Point([4*(1 + np.cos(2*np.pi/5)), 4*np.sin(2*np.pi/5)])
    K = Point([2, 4*(np.sin(2*np.pi/5) + np.sin(np.pi/5))])
    L = Point([-4*np.cos(2*np.pi/5), 4*np.sin(2*np.pi/5)])
    assert check_regular_polygon([D, I, J, K, L])

    M = Point([0, 0, 1])
    Mp = Point([0, 0, -1])
    N = Point([0, 1, 0])
    Np = Point([0, -1, 0])
    O = Point([1, 0, 0])
    Op = Point([-1, 0, 0])
    assert not check_regular_polygon([M, N, O, Mp, Np, Op])
