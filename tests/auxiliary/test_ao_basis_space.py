""":mod:`.test_ao_basis_space` contains unit tests for classes and functions
implemented in :mod:`src.auxiliary.ao_basis_space`.
"""

from polyinspect.auxiliary.ao_basis_space import get_shell_indices,\
        get_atom_basis_indices


def test_get_indices():
    r"""Tests for the retrieval of shell and atom indices in a basis set.
    """
    bao = [("V", [("S", False, None),
                  ("S", False, None),
                  ("P", True, None),
                  ("S", False, None),
                  ("P", True, None),
                  ("S", False, None),
                  ("P", True, None),
                  ("S", False, None),
                  ("P", True, None),
                  ("D", True, None),
                  ("D", True, None),
                  ("F", False, None)]),
           ("F", [("S", False, None),
                  ("S", False, None),
                  ("P", True, None),
                  ("S", False, None),
                  ("P", True, None),
                  ("D", True, None)])]

    shell_indices = get_shell_indices(bao)
    assert len(shell_indices) == 18
    assert shell_indices == [(0, 0, 0, False, None),
                             (1, 1, 0, False, None),
                             (2, 4, 1, True, None),
                             (5, 5, 0, False, None),
                             (6, 8, 1, True, None),
                             (9, 9, 0, False, None),
                             (10, 12, 1, True, None),
                             (13, 13, 0, False, None),
                             (14, 16, 1, True, None),
                             (17, 22, 2, True, None),
                             (23, 28, 2, True, None),
                             (29, 35, 3, False, None),
                             (36, 36, 0, False, None),
                             (37, 37, 0, False, None),
                             (38, 40, 1, True, None),
                             (41, 41, 0, False, None),
                             (42, 44, 1, True, None),
                             (45, 50, 2, True, None)]

    atom_basis_indices = get_atom_basis_indices(bao)
    assert len(atom_basis_indices) == 2
    assert atom_basis_indices == [(0, 35, "V"),
                                  (36, 50, "F")]
