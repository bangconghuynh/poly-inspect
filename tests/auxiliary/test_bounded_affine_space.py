""":mod:`.test_bounded_affine_space` contains unit tests for classes implemented
in :mod:`src.auxiliary.bounded_affine_space`.
"""
# pylint: disable=C0302
# C0302: bounded_affine_space is a large module.

import numpy as np  # type: ignore
import pytest  # type: ignore

from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE
from polyinspect.auxiliary.geometrical_space import Point, Vector
from polyinspect.auxiliary.affine_space import AffineSubspace, Hyperplane
from polyinspect.auxiliary.bounded_affine_space import Segment, Contour, Face,\
        Polyhedron, VertexCollection


# pylint: disable=C0103
# C0103: Variable names follow geometrical conventions.


def test_segment_basic():
    r"""Tests for the basic construction of segments in :math:`\mathbb{R}^n` and
    operations on them.
    """
    # pylint: disable=R0914,R0915
    # R0914: Each point is explicitly defined as a separate variable for ease
    #        of checking.

    # ---------------------------------
    # Construction and point retrievals
    # ---------------------------------

    A = Point([0.0, -4.4, 9.3, 0.2, 3.7, -2.8])
    B = Point([1.2, 3.4, -5.8, 9.1, -0.2, 7.6])
    AB = Segment([A, B])
    assert not AB == AB.vector
    assert (AB.is_same_as(AB.vector)) == NotImplemented
    assert AB == Segment([B, A])
    assert abs(AB.length - 22.20067566539361) < COMMON_ZERO_TOLERANCE
    assert AB.midpoint == Point([0.6, -0.5, 1.75, 4.65, 1.75, 2.4])
    assert AB.get_a_point() == Point([0.6, -0.5, 1.75, 4.65, 1.75, 2.4])
    assert repr(AB) ==\
            "\n" +\
            "Segment[" +\
            "Point(0.000, -4.400, 9.300, 0.200, 3.700, -2.800), " +\
            "Point(1.200, 3.400, -5.800, 9.100, -0.200, 7.600)" +\
            "]"

    ## Non-segments in R^3
    A = Point(n=3)
    B = Point([2, 0, 0])
    Bd = Point([3, 0, 0])
    C = Point([2, 2, 0])
    with pytest.warns(UserWarning):
        Segment([A, B, Bd])
    with pytest.raises(AssertionError):
        Segment([A, B, C])

    # ---------------------
    # Fractions of segments
    # ---------------------

    B = Point([0.0, 0.0, 0.0])
    C = Point([5.0, 7.0, 9.0])
    BC = Segment([B, C])
    P, BP = BC.get_fraction_of_segment(0.5, B)
    PB = Segment([P, B])
    assert P == BC.midpoint
    assert P == (B + C)/2
    assert BP == Segment([B, BC.midpoint])
    assert BC.shares_an_endpoint(BP)[0]
    assert BC.shares_an_endpoint(PB)[0]

    assert BC.find_fraction(B, P) == 0.5
    Pd = Point([1.0, 1.1, 2.2])
    PPd = Segment([P, Pd])
    with pytest.raises(AssertionError):
        BC.find_fraction(B, Pd)
    with pytest.raises(AssertionError):
        PPd.find_fraction(B, C)

    Pdd, BPdd = BC.get_fraction_of_segment(0, B)
    assert Pdd == B
    assert BPdd is None

    P2, _ = BC.get_fraction_of_segment(0.3, B)
    assert P2 == 0.3*C
    P3, CP3 = BC.get_fraction_of_segment(0.3, C)
    P3C = Segment([P3, C])
    assert P3 == 0.7*C
    assert abs(BC.find_fraction(C, P3) - 0.3) < COMMON_ZERO_TOLERANCE
    assert abs(CP3.find_fraction(C, P3) - 1) < COMMON_ZERO_TOLERANCE
    assert BC.shares_an_endpoint(CP3)[0]
    assert BC.shares_an_endpoint(P3C)[0]
    assert not BC.shares_an_endpoint(PPd)[0]

    with pytest.raises(AssertionError):
        BC.get_fraction_of_segment(1.1, C)


    # ------------------
    # Segment comparison
    # ------------------
    D = Point(n=4)
    E1 = Point([1, 2, 3, 4])
    E2 = Point([-1, -2, -3, -4])
    DE1 = Segment([D, E1])
    E1D = Segment([E1, D])
    DE2 = Segment([D, E2])
    E2D = Segment([E2, D])
    E1E2 = Segment([E1, E2])
    assert DE2 < DE1 < E1E2
    assert E1D > E2D
    assert E1D != E2D
    assert DE1 >= E1D
    assert DE2 <= E2D


    # -------------------
    # Cabinet projections
    # -------------------
    F = Point(n=3)
    G = Point([3, 4, 5])
    FG = Segment([F, G])
    Fd = Point(n=2)
    a = np.arctan(2)
    x = 3 - 0.5*5*np.cos(a)
    y = 4 - 0.5*5*np.sin(a)
    Gd = Point([x, y])
    assert FG.get_cabinet_projection() == Segment([Fd, Gd])


    # -------------------
    # Segment translation
    # -------------------
    FG.translate_ip(Vector([1, 1, 1]))
    assert FG == Segment([Point([1, 1, 1]), Point([4, 5, 6])])


def test_segment_intersection():
    r"""Tests for the intersection of segments in :math:`\mathbb{R}^n`.
    """
    # pylint: disable=R0914,R0915
    # R0914: Each point is explicitly defined as a separate variable for ease
    #        of checking.

    # ------------------
    # Collinear segments
    # B--P--C--D
    # ------------------

    B = Point([0.0, 0.0, 0.0])
    C = Point([5.0, 7.0, 9.0])
    BC = Segment([B, C])
    P, BP = BC.get_fraction_of_segment(0.5, B)
    D = BC.midpoint + C + (-B)
    DB = Segment([D, B])
    CD = Segment([C, D])
    PC = Segment([P, C])
    PD = Segment([P, D])
    # Case a.
    assert BP.intersects_segment(CD) == (0, None)
    # Case b.
    assert BC.intersects_segment(CD) == (1, C)
    # Case c.
    assert PD.intersects_segment(BC) == (np.inf, Segment([P, C]))
    # Case d.
    assert DB.intersects_segment(PC) == (np.inf, PC)
    # Case e.
    assert BP.intersects_segment(BC) == (np.inf, BP)
    assert PC.intersects_segment(BC) == (np.inf, PC)
    # Case f.
    assert BC.intersects_segment(BC) == (np.inf, BC)


    # ----------------------
    # Non-collinear segments
    # ----------------------

    E = Point([4.0, 8.0, 9.0])

    # Parallel
    F = E + 2*C - B
    EF = Segment([E, F])
    assert EF.intersects_segment(BC) == (0, None)

    # Non-parallel, non-intersecting
    G = Point([4.0, 9.0, 9.0])
    EG = Segment([E, G])
    assert EG.intersects_segment(BC) == (0, None)

    # Intersecting at end-point
    CE = Segment([E, C])
    assert CE.intersects_segment(BC) == (1, C)

    # Intersecting at non-end-point
    H = Point([0.0, 0.0, 4.0])
    G = Point([5.0, 7.0, 4.0])
    HG = Segment([H, G])
    x = 4/9
    I = Point([5.0*x, 7.0*x, 4.0])
    assert HG.intersects_segment(BC) == (1, I)

    # Intersection point lies outside segment
    y = 3/9
    Gd = Point([5.0*y, 7.0*y, 4.0])
    HGd = Segment([H, Gd])
    assert HGd.intersects_segment(BC) == (0, None)
    assert BC.intersects_segment(HGd) == (0, None)


    # -----------------------------
    # Intersection with hyperplanes
    # -----------------------------
    anchor1 = Point(n=3)
    normal1 = Vector([1, 0, 0])
    hplane1 = Hyperplane(anchor1, normal1)  # yz-plane

    A = Point([2, -3, 5])
    B = Point([-2, 4, 1])
    AB = Segment([A, B])
    assert AB.intersects_hyperplane(hplane1) == (1, AB.midpoint)

    C = Point([1, -4, 2])
    AC = Segment([C, A])
    assert AC.intersects_hyperplane(hplane1) == (0, None)

    D = Point([2, -4, 3])
    AD = Segment([A, D])
    assert AD.intersects_hyperplane(hplane1) == (0, None)

    E = Point([0, -4, 5])
    F = Point([0, 4, -5])
    EF = Segment([F, E])
    assert EF.intersects_hyperplane(hplane1) == (np.inf, EF)


def test_contour_basic():
    r"""Tests for the basic construction of contours in :math:`\mathbb{R}^n` and
    operations on them.
    """
    # pylint: disable=R0914,R0915
    # R0914: Each point is explicitly defined as a separate variable for ease
    #        of checking.

    # ------------
    # Construction
    # ------------

    ## Contour in the xy-plane embedded in R^2
    A = Point(n=2)
    B = Point([2, 0])
    C = Point([3, 1])
    D = Point([1, 3])
    ABCD = Contour.from_vertices([A, B, C, D])
    assert ABCD.__repr__() ==\
            "\n" +\
            "Contour[\n" +\
            "    Point(0.000, 0.000)\n" +\
            "    Point(2.000, 0.000)\n" +\
            "    Point(3.000, 1.000)\n" +\
            "    Point(1.000, 3.000)\n" +\
            "]"
    # A two-dimensional plane in R^2 has no third dimension.
    assert ABCD.get_front_normal(Vector([0, 0, -1])) is None

    E1 = Point([1, 1])
    E2 = Point([1, 2])
    E3 = Point([1/3, 1])
    E4 = Point([1.2, 0])
    assert ABCD.contains_point(C)
    assert ABCD.contains_point(E1)
    assert ABCD.contains_point(E2)
    assert ABCD.contains_point(E3)
    assert ABCD.contains_point(E4)
    E1E2E3E4 = Contour.from_vertices([E1, E2, E3, E4])
    assert E1E2E3E4 <= ABCD

    F1 = Point([2.1, 2.1])
    F2 = Point([0.2, 1])
    F3 = Point([-0.1, -0.2])
    F4 = Point([2.5, 0])
    assert not ABCD.contains_point(F1)
    assert not ABCD.contains_point(F2)
    assert not ABCD.contains_point(F3)
    assert not ABCD.contains_point(F4)
    F1F2F3 = Contour.from_vertices([F1, F2, F3])
    F1F2F3F4 = Contour.from_vertices([F1, F2, F3, F4])
    assert F1F2F3 <= ABCD
    assert F1F2F3F4 <= ABCD
    assert F1F2F3F4 >= F1F2F3

    F5 = Point([-1, -1])
    F6 = Point([-1, 4])
    F7 = Point([4, 4])
    F8 = Point([4, -1])
    F5F6F7F8 = Contour.from_vertices([F5, F6, F7, F8])
    assert F5F6F7F8 >= ABCD

    ## Non-convex contour parallel to the xy-plane embedded in R^3
    G = Point([-1, -2, 0])
    H = Point([-3, -1, 0])
    I = Point([1, -1, 0])
    J = Point([1, 4, 0])
    GHIJ = Contour.from_vertices([G, H, I, J])
    assert not GHIJ.contains_point(Point([-1, 0, 0]))
    assert not GHIJ.contains_point(Point([0, -2, 0]))
    assert GHIJ.contains_point(Point(n=3))
    assert GHIJ.contains_point(Point([-1.5, -1.5, 0]))

    Gd = Point([-1, -2, 1])
    Hd = Point([-3, -1, 1])
    Id = Point([1, -1, 1])
    Jd = Point([1, 4, 1])
    GdHdIdJd = Contour.from_vertices([Gd, Hd, Id, Jd])
    assert GdHdIdJd.get_front_normal(Vector([0, 0.5, -0.5]))\
            == Vector([0, 0, -1])
    assert not GdHdIdJd.is_coplanar(GHIJ)
    GdHdIdJd.translate_ip(Vector([1, 2, 3]))
    std_contour = Contour.from_vertices([Point([0, 0, 4]),
                                         Point([-2, 1, 4]),
                                         Point([2, 1, 4]),
                                         Point([2, 6, 4])])
    assert GdHdIdJd == std_contour
    assert GdHdIdJd.associated_plane == std_contour.associated_plane
    with pytest.raises(AssertionError):
        GdHdIdJd.rotate_ip(np.pi/2, Vector([0, 1]))
    GdHdIdJd.rotate_ip(-np.pi/2, Vector([0, 1, 0]))
    std_contour = Contour.from_vertices([Point([-4, 0, 0]),
                                         Point([-4, 1, -2]),
                                         Point([-4, 1, 2]),
                                         Point([-4, 6, 2])])
    assert GdHdIdJd == std_contour
    assert GdHdIdJd.associated_plane == std_contour.associated_plane


    ## Contour in R^3
    anchor1 = Point([1, 2, 4])
    normal1 = Vector([1, 1, 1])
    plane1 = Hyperplane(anchor1, normal1)
    assert plane1.affdim == 2

    A = plane1.get_point([0, 0])
    B = plane1.get_point([5, 0])
    C = plane1.get_point([4, 2])
    D = plane1.get_point([5, 4])
    E = plane1.get_point([1, 5])
    AB = Segment([A, B])
    BC = Segment([B, C])
    CD = Segment([C, D])
    DE = Segment([D, E])
    EA = Segment([E, A])
    AE = Segment([A, E])
    with pytest.raises(AssertionError):
        Contour([AB, BC, CD, DE, AE])
    ABCDE = Contour([AB, BC, CD, DE, EA])
    CDEAB = Contour([CD, DE, EA, AB, BC])
    assert ABCDE == CDEAB
    assert CDEAB.vertices == [C, D, E, A, B]
    ABDCE = Contour.from_vertices([A, B, D, C, E])
    assert ABCDE != ABDCE
    assert not ABCDE != AB  # pylint: disable=C0113
    assert not ABCDE == AB  # pylint: disable=C0113
    ver_proj = [ver.get_cabinet_projection() for ver in [A, B, C, D, E]]
    assert ABCDE.get_cabinet_projection() == Contour.from_vertices(ver_proj)

    F1 = plane1.get_point([1, 2])
    F2 = plane1.get_point([4.5, 1])
    assert ABCDE.contains_point(F1)
    assert ABCDE.contains_point(F2)

    G1 = plane1.get_point([4.5, 2])
    G2 = plane1.get_point([0.5, 4])
    G3 = Point(n=3)
    assert not ABCDE.contains_point(G1)
    assert not ABCDE.contains_point(G2)
    assert not ABCDE.contains_point(G3)

    assert not GdHdIdJd.is_coplanar(ABCDE)
    assert GdHdIdJd != ABCDE

    Ad = plane1.get_point([-1, -1])
    Bd = plane1.get_point([-1, -2])
    Cd = plane1.get_point([-5, -2])
    Dd = plane1.get_point([-9, -9])
    Ed = plane1.get_point([1, 1])
    AdBdDdCdEd = Contour.from_vertices([Ad, Bd, Dd, Cd, Ed])
    assert AdBdDdCdEd.is_coplanar(ABCDE)
    assert AdBdDdCdEd.contains_point(AdBdDdCdEd.get_a_point())

    ## Non-contours in R^3
    A = Point(n=3)
    B = Point([2, 0, 0])
    C = Point([2, 2, 0])
    D = Point([2, 2, 2])
    with pytest.raises(AssertionError):
        Contour.from_vertices([A, B, C, D])


def test_contour_convexhull():
    r"""Tests for the construction of contours in :math:`\mathbb{R}^2` as
    convex hulls of vertices.
    """
    # ----------
    # A pentagon
    # ----------

    ## Contour in the xy-plane embedded in R^2
    A = Point([-2, -2])
    B = Point([0, 2])
    C = Point([2, -2])
    D = Point([-1, 1])
    E = Point([1, 1])
    pentagon = Contour.from_vertices([A, D, B, E, C])
    pentagon_convexhull = Contour.convex_hull_from_vertices([A, B, C, D, E])
    assert pentagon == pentagon_convexhull


def test_contour_intersection():
    r"""Tests for the intersection of contours in :math:`\mathbb{R}^n`.
    """
    # pylint: disable=R0914,R0915
    # R0914: Each point is explicitly defined as a separate variable for ease
    #        of checking.
    # ---------------------------------------------
    # Intersections between a contour and a segment
    # ---------------------------------------------

    ## Non-convex contour parallel to the xy-plane embedded in R^3
    A = Point([-1, -2, 0])
    B = Point([-3, -1, 0])
    C = Point([1, -1, 0])
    D = Point([1, 4, 0])
    ABCD = Contour.from_vertices([A, B, C, D])

    E1 = Point([-1, 0, 1])
    F1 = Point([-1, 0, -1])
    E1F1 = Segment([E1, F1])
    assert E1F1.intersects_contour(ABCD) == (0, [], [], [(E1F1, 0, 1)])
    E2 = Point([-2, 0, 0])
    F2 = Point([-1, 0, 0])
    E2F2 = Segment([E2, F2])
    assert E2F2.intersects_contour(ABCD) == (0, [], [], [(E2F2, 0, 1)])
    E3 = Point([-2, 0, -1])
    F3 = Point([0, 2, 1.5])
    E3F3 = Segment([E3, F3])
    assert E3F3.intersects_contour(ABCD) == (0, [], [], [(E3F3, 0, 1)])
    E4 = Point([2, 0, -1])
    F4 = Point([3, 2, 2.5])
    E4F4 = Segment([E4, F4])
    assert E4F4.intersects_contour(ABCD) == (0, [], [], [(E4F4, 0, 1)])


    G1 = Point([0, 0, 1])
    H1 = Point([0, 0, -1])
    G1H1 = Segment([G1, H1])
    O = Point(n=3)
    OH1 = Segment([H1, O])
    OG1 = Segment([G1, O])
    assert G1H1.intersects_contour(ABCD) == (1, [(Point(n=3), 0.5)],
                                             [], [(OG1, 0, 0.5),
                                                  (OH1, 0.5, 1)])

    G2 = Point([-3, -3, 0])
    H2 = Point([3, 0, 0])
    H2d = Point([-3, 1, 0])
    G2H2 = Segment([G2, H2])
    G2H2d = Segment([G2, H2d])
    G2H2intABCD = G2H2.intersects_contour(ABCD)
    assert G2H2intABCD[0] == 2
    assert G2H2intABCD[1][0][0] == A
    assert abs(G2H2intABCD[1][0][1] - 1/3)\
            < COMMON_ZERO_TOLERANCE
    assert G2H2intABCD[1][1][0] == C
    assert abs(G2H2intABCD[1][1][1] - 2/3)\
            < COMMON_ZERO_TOLERANCE
    assert len(G2H2intABCD[2]) == 0
    assert [seg[0] for seg in G2H2intABCD[3]]\
            == [(Segment([G2, A])), (Segment([A, C])), (Segment([C, H2]))]
    G2H2dintABCD = G2H2d.intersects_contour(ABCD)
    assert G2H2dintABCD[0] == 1
    assert G2H2dintABCD[1][0][0] == B
    assert G2H2dintABCD[2] == []
    assert G2H2dintABCD[3][0][0] == Segment([G2, B])
    assert G2H2dintABCD[3][0][1] == 0
    assert G2H2dintABCD[3][0][2] == 0.5
    assert G2H2dintABCD[3][1][0] == Segment([H2d, B])
    assert G2H2dintABCD[3][1][1] == 0.5
    assert G2H2dintABCD[3][1][2] == 1

    H3 = Segment([C, D]).midpoint
    BH3 = Segment([B, H3])
    H3d = BH3.intersects_segment(Segment([A, D]))[1]
    BH3intABCD = BH3.intersects_contour(ABCD)
    assert BH3intABCD[0] == np.inf
    assert len(BH3intABCD[1]) == 3
    assert BH3intABCD[1][0] == (B, 0.0)
    assert BH3intABCD[1][1][0] == H3d
    assert abs(BH3intABCD[1][1][1] - 0.7368421052631581) < COMMON_ZERO_TOLERANCE
    assert BH3intABCD[1][2][0] == H3
    assert abs(BH3intABCD[1][2][1] -  1.0) < COMMON_ZERO_TOLERANCE
    assert len(BH3intABCD[2]) == 1
    assert BH3intABCD[2][0][0] == Segment([H3, H3d])
    assert len(BH3intABCD[3]) == 1
    assert BH3intABCD[3][0][0] == Segment([B, H3d])

    G4 = Point([-3, 1, 0])
    H4 = Point([3, 1, 0])
    G4H4 = Segment([G4, H4])
    G4H4intABCD = G4H4.intersects_contour(ABCD)
    assert G4H4intABCD[0] == np.inf
    assert len(G4H4intABCD[1]) == 2
    assert G4H4intABCD[1][0][0] == Point([0, 1, 0])
    assert G4H4intABCD[1][1][0] == Point([1, 1, 0])
    assert len(G4H4intABCD[2]) == 1
    assert G4H4intABCD[2][0][0] == Segment([Point([0, 1, 0]), Point([1, 1, 0])])
    assert len(G4H4intABCD[3]) == 2
    assert G4H4intABCD[3][0][0] == Segment([G4, Point([0, 1, 0])])
    assert G4H4intABCD[3][1][0] == Segment([H4, Point([1, 1, 0])])

    AB = Segment([A, B])
    ABintABCD = AB.intersects_contour(ABCD)
    assert ABintABCD[0] == np.inf
    assert len(ABintABCD[1]) == 2
    assert ABintABCD[1][0][0] == A
    assert ABintABCD[1][1][0] == B
    assert len(ABintABCD[2]) == 1
    assert ABintABCD[2][0][0] == AB
    assert len(ABintABCD[3]) == 0


def test_face_basic():
    r"""Tests for the basic construction of faces in :math:`\mathbb{R}^n` and
    operations on them.
    """
    # pylint: disable=R0914,R0915
    # R0914, R0915: Having more local variables enhances code readability.

    # ------------
    # Construction
    # ------------

    ## Face in the xy-plane embedded in R^2
    A = Point(n=2)
    B = Point([2, 0])
    C = Point([3, 1])
    D = Point([1, 3])
    ABCD = Contour.from_vertices([A, B, C, D])
    E = Point([1, 1])
    F = Point([0, 2])
    G = Point([3, 2])
    H = Point([1, -1])
    EFGH = Contour.from_vertices([E, F, G, H])

    fABCDEFGH = Face([ABCD, EFGH])
    assert fABCDEFGH.get_front_normal(Vector([0, 0, 1])) is None
    xy_plane = AffineSubspace(Point(n=2), [Vector([1, 0]), Vector([0, 1])])
    assert fABCDEFGH.associated_plane == xy_plane
    assert fABCDEFGH.__repr__() ==\
        "\n" +\
        "Face[\n" +\
        "    Contour[\n" +\
        "        Point(0.000, 0.000)\n" +\
        "        Point(2.000, 0.000)\n" +\
        "        Point(3.000, 1.000)\n" +\
        "        Point(1.000, 3.000)\n" +\
        "    ]\n" +\
        "    Contour[\n" +\
        "        Point(1.000, 1.000)\n" +\
        "        Point(0.000, 2.000)\n" +\
        "        Point(3.000, 2.000)\n" +\
        "        Point(1.000, -1.000)\n" +\
        "    ]\n" +\
        "]"

    I1 = Point([0.5, 0.5])
    I2 = Point([2.1, 2])
    assert fABCDEFGH.contains_point(I1)
    assert fABCDEFGH.contains_point(I2)
    assert fABCDEFGH.contains_point(H)

    J1 = Point([1.5, 1])
    J2 = Point([2, 2])
    assert not fABCDEFGH.contains_point(J1)
    assert not fABCDEFGH.contains_point(J2)
    assert not fABCDEFGH.contains_point(E)
    assert not fABCDEFGH.contains_point(E)

    vec = Vector([-1, -2])
    Ad = A.translate(vec)
    Bd = B.translate(vec)
    Cd = C.translate(vec)
    Dd = D.translate(vec)
    AdBdCdDd = Contour.from_vertices([Ad, Bd, Cd, Dd])
    Ed = E.translate(vec)
    Fd = F.translate(vec)
    Gd = G.translate(vec)
    Hd = H.translate(vec)
    EdFdGdHd = Contour.from_vertices([Ed, Fd, Gd, Hd])
    assert fABCDEFGH != Face([AdBdCdDd, EdFdGdHd])
    fABCDEFGH.translate_ip(vec)
    assert fABCDEFGH == Face([AdBdCdDd, EdFdGdHd])
    assert fABCDEFGH.bounding_box == Face([AdBdCdDd, EdFdGdHd]).bounding_box

    ## Star-shaped face in R^3
    plane = AffineSubspace(Point([3, -0.5, -0.7]), [Vector([1, 2, 1]),
                                                    Vector([2, 3, -1])])
    A = plane.get_point([-2, -2])
    B = plane.get_point([0, 2])
    C = plane.get_point([2, -2])
    D = plane.get_point([-1, 1])
    E = plane.get_point([1, 1])
    star = Contour.from_vertices([A, B, C, D, E])
    pentagon = Contour.from_vertices([A, D, B, E, C])
    face = Face([star, pentagon])
    assert face.get_front_normal(Vector([0, 0, 1]))\
            == Vector([5, -3, 1])/np.sqrt(35)
    assert face.non_adj_vertices == sorted([A, B, C, D, E])
    assert face.associated_plane == plane
    assert face != Face([star, star])
    assert face != Face([star, star, pentagon])

    F1 = plane.get_point([0, 0])
    G1 = plane.get_point([0, -2])
    assert not face.contains_point(F1)
    assert face.contains_point(G1)

    star_projected = star.get_cabinet_projection()
    pentagon_projected = pentagon.get_cabinet_projection()
    assert face.get_cabinet_projection() == Face([pentagon_projected,
                                                  star_projected])
    assert face >= fABCDEFGH
    assert fABCDEFGH <= face

    face_star = Face([star])
    face_pentagon = Face([pentagon])
    assert face_star != face_pentagon
    assert face > face_pentagon
    assert face_star < face
    assert face_star > face_pentagon
    assert face_pentagon < face_star

    ## Face rotation
    H = Point([-2, 4, 1])
    I = Point([2, 0, 1])
    J = Point([3, 1, 1])
    K = Point([1, 3, 1])
    HIJK = Contour.from_vertices([H, I, J, K])
    fHIJK = Face([HIJK])
    fHIJK.rotate_ip(-np.pi/2, Vector([0, 1, 0]))
    Hd = Point([-1, 4, -2])
    Id = Point([-1, 0, 2])
    Jd = Point([-1, 1, 3])
    Kd = Point([-1, 3, 1])
    HdIdJdKd = Contour.from_vertices([Hd, Id, Jd, Kd])
    fHdIdJdKd = Face([HdIdJdKd])
    assert fHIJK.associated_plane == fHdIdJdKd.associated_plane


def test_face_higherdim():
    r"""Tests for the construction of faces in :math:`\mathbb{R}^n` and
    operations on them.
    """
    # ------------
    # Construction
    # ------------

    plane = AffineSubspace(Point(n=5), [Vector([1, 2, 1, 1, 4]),
                                        Vector([0, 3, -1, 2, 1])])
    A = plane.get_point([-2, 0])
    B = plane.get_point([0, -2])
    C = plane.get_point([0, 2])
    D = plane.get_point([2, 0])
    ABCD = Contour.from_vertices([A, B, C, D])
    E = plane.get_point([-1, 0])
    F = plane.get_point([0, 1])
    G = plane.get_point([1, 0])
    H = plane.get_point([0, -1])
    EFGH = Contour.from_vertices([E, F, G, H])

    fABCDEFGH = Face([ABCD, EFGH])
    assert fABCDEFGH.get_front_normal(Vector([0, 0, 1, 0, 0])) is None
    assert fABCDEFGH.non_adj_vertices == sorted([A, B, C, D, E, F, G, H])
    assert fABCDEFGH.associated_plane == plane

    I1 = plane.get_point([0.5, 0.5])
    J1 = plane.get_point([-0.5, 0.5])
    assert not fABCDEFGH.contains_point(I1)
    assert fABCDEFGH.contains_point(J1)


def test_face_intersection():
    r"""Tests for the intersection of faces in :math:`\mathbb{R}^n`.
    """
    # pylint: disable=R0914,R0915
    # R0914: Each point is explicitly defined as a separate variable for ease
    #        of checking.
    # ------------------------------------------
    # Intersections between a face and a segment
    # ------------------------------------------

    ## Two overlapping polygons, one convex, one non-convex
    A = Point(n=3)
    B = Point([2, 0, 0])
    C = Point([3, 1, 0])
    D = Point([1, 3, 0])
    ABCD = Contour.from_vertices([A, B, C, D])
    E = Point([1, 1, 0])
    F = Point([0, 2, 0])
    G = Point([3, 2, 0])
    H = Point([1, -1, 0])
    EFGH = Contour.from_vertices([E, F, G, H])

    fABCDEFGH = Face([ABCD, EFGH])

    I1 = Point([2, 1, 1])
    I2 = Point([2, 1, -1])
    I1I2 = Segment([I1, I2])
    assert I1I2.intersects_face(fABCDEFGH) == (0, [], [], [(I1I2, 0, 1)])

    J1 = Point([0.5, 0.5, 0.2])
    J2 = Point([0.5, 0.5, -0.8])
    J1J2 = Segment([J1, J2])
    O = Point([0.5, 0.5, 0])
    J1O = Segment([J1, O])
    J2O = Segment([J2, O])
    J1J2intfABCDEFGH = J1J2.intersects_face(fABCDEFGH)
    assert J1J2intfABCDEFGH[0] == 1
    assert J1J2intfABCDEFGH[1][0][0] == O
    assert (J1J2intfABCDEFGH[1][0][1] - 0.2) < COMMON_ZERO_TOLERANCE
    assert J1J2intfABCDEFGH[2] == []
    assert J1J2intfABCDEFGH[3][0][0] == J1O
    assert J1J2intfABCDEFGH[3][0][1] == 0
    assert (J1J2intfABCDEFGH[3][0][2] - 0.2) < COMMON_ZERO_TOLERANCE
    assert J1J2intfABCDEFGH[3][1][0] == J2O
    assert (J1J2intfABCDEFGH[3][1][1] - 1) < COMMON_ZERO_TOLERANCE
    assert (J1J2intfABCDEFGH[3][1][2] - 1) < COMMON_ZERO_TOLERANCE

    K1 = Point([-1, 1, 0])
    K2 = Point([4, 1, 0])
    K1K2 = Segment([K1, K2])
    K1K2intfABCDEFGH = K1K2.intersects_face(fABCDEFGH)
    assert K1K2intfABCDEFGH[0] == np.inf
    assert len(K1K2intfABCDEFGH[1]) == 4
    assert K1K2intfABCDEFGH[2][0][0] == Segment([E, Point([1/3, 1, 0])])
    assert K1K2intfABCDEFGH[2][1][0] == Segment([C, Point([7/3, 1, 0])])
    assert K1K2intfABCDEFGH[3][0][0] == Segment([K1, Point([1/3, 1, 0])])
    assert K1K2intfABCDEFGH[3][1][0] == Segment([E, Point([7/3, 1, 0])])
    assert K1K2intfABCDEFGH[3][2][0] == Segment([C, K2])

    L1 = Point([0, 4, 0])
    L2 = Point([4, 4, 0])
    L1L2 = Segment([L1, L2])
    L1L2intfABCDEFGH = L1L2.intersects_face(fABCDEFGH)
    assert L1L2intfABCDEFGH == (0, [], [], [(L1L2, 0, 1)])

    L3 = Point([0.5, 2.5, 0])
    L4 = Point([0.5, 3.5, 0])
    L3L4 = Segment([L3, L4])
    L3L4intfABCDEFGH = L3L4.intersects_face(fABCDEFGH)
    assert L3L4intfABCDEFGH == (0, [], [], [(L3L4, 0, 1)])


    ## A rhombus in a square
    A = Point([0, 0, 0])
    B = Point([2, 0, 0])
    C = Point([2, 2, 0])
    D = Point([0, 2, 0])
    ABCD = Contour.from_vertices([A, B, C, D])
    E = Point([1, 0, 0])
    F = Point([2, 1, 0])
    G = Point([1, 2, 0])
    H = Point([0, 1, 0])
    EFGH = Contour.from_vertices([E, F, G, H])
    fABCDEFGH = Face([ABCD, EFGH])

    I1 = Point([1, 3, 0])
    I2 = Point([1, 1, 0])
    I1I2 = Segment([I1, I2])
    I1I2intfABCDEFGH = I1I2.intersects_face(fABCDEFGH)
    assert I1I2intfABCDEFGH[0] == 1
    assert I1I2intfABCDEFGH[1][0][0] == G
    assert I1I2intfABCDEFGH[2] == []
    assert I1I2intfABCDEFGH[3][0][0] == Segment([I1, G])
    assert I1I2intfABCDEFGH[3][1][0] == Segment([I2, G])


def test_polyhedron_basic():
    r"""Tests for the basic construction of polyhedra in :math:`\mathbb{R}^n` and
    operations on them.
    """
    # pylint: disable=R0914
    # R0914: Having more local variables enhances code readability.

    # ---------------
    # A cuboid in 3-d
    # ---------------

    A = Point(n=3)
    B = Point([2, 0, 0])
    C = Point([2, 1, 0])
    D = Point([0, 1, 0])
    E = Point([0, 0, 3])
    F = Point([2, 0, 3])
    G = Point([2, 1, 3])
    H = Point([0, 1, 3])
    face1 = Face([Contour.from_vertices([A, B, C, D])])
    face2 = Face([Contour.from_vertices([A, D, H, E])])
    face3 = Face([Contour.from_vertices([B, C, G, F])])
    face4 = Face([Contour.from_vertices([A, B, F, E])])
    face5 = Face([Contour.from_vertices([C, D, H, G])])
    face6 = Face([Contour.from_vertices([E, F, G, H])])
    face7 = Face([Contour.from_vertices([A, D, G, F])])
    face8 = Face([Contour.from_vertices([C, D, G])])
    face9 = Face([Contour.from_vertices([A, B, F])])
    cuboid = Polyhedron([face1, face2, face3, face4, face5, face6])
    prism = Polyhedron([face1, face3, face7, face8, face9])
    assert cuboid != prism
    assert prism <= cuboid
    assert cuboid >= prism
    assert cuboid.__repr__() ==\
            "\n" +\
            "Polyhedron[\n" +\
            "    Face[\n" +\
            "        Contour[\n" +\
            "            Point(0.000, 0.000, 0.000)\n" +\
            "            Point(2.000, 0.000, 0.000)\n" +\
            "            Point(2.000, 1.000, 0.000)\n" +\
            "            Point(0.000, 1.000, 0.000)\n" +\
            "        ]\n" +\
            "    ]\n" +\
            "    Face[\n" +\
            "        Contour[\n" +\
            "            Point(0.000, 0.000, 0.000)\n" +\
            "            Point(0.000, 1.000, 0.000)\n" +\
            "            Point(0.000, 1.000, 3.000)\n" +\
            "            Point(0.000, 0.000, 3.000)\n" +\
            "        ]\n" +\
            "    ]\n" +\
            "    Face[\n" +\
            "        Contour[\n" +\
            "            Point(2.000, 0.000, 0.000)\n" +\
            "            Point(2.000, 1.000, 0.000)\n" +\
            "            Point(2.000, 1.000, 3.000)\n" +\
            "            Point(2.000, 0.000, 3.000)\n" +\
            "        ]\n" +\
            "    ]\n" +\
            "    Face[\n" +\
            "        Contour[\n" +\
            "            Point(0.000, 0.000, 0.000)\n" +\
            "            Point(2.000, 0.000, 0.000)\n" +\
            "            Point(2.000, 0.000, 3.000)\n" +\
            "            Point(0.000, 0.000, 3.000)\n" +\
            "        ]\n" +\
            "    ]\n" +\
            "    Face[\n" +\
            "        Contour[\n" +\
            "            Point(2.000, 1.000, 0.000)\n" +\
            "            Point(0.000, 1.000, 0.000)\n" +\
            "            Point(0.000, 1.000, 3.000)\n" +\
            "            Point(2.000, 1.000, 3.000)\n" +\
            "        ]\n" +\
            "    ]\n" +\
            "    Face[\n" +\
            "        Contour[\n" +\
            "            Point(0.000, 0.000, 3.000)\n" +\
            "            Point(2.000, 0.000, 3.000)\n" +\
            "            Point(2.000, 1.000, 3.000)\n" +\
            "            Point(0.000, 1.000, 3.000)\n" +\
            "        ]\n" +\
            "    ]\n" +\
            "]"

    assert cuboid.edges == sorted([Segment([A, B]), Segment([A, D]),
                                   Segment([A, E]), Segment([B, C]),
                                   Segment([B, F]), Segment([C, D]),
                                   Segment([C, G]), Segment([D, H]),
                                   Segment([E, F]), Segment([E, H]),
                                   Segment([F, G]), Segment([G, H])])

    [Ap, Bp, Cp, Dp, Ep, Fp, Gp, Hp] = [vertex.get_cabinet_projection()
                                        for vertex in [A, B, C, D, E, F, G, H]]
    assert cuboid.get_cabinet_projection() ==\
            Face([Contour.from_vertices([Ap, Bp, Cp, Dp]),
                  Contour.from_vertices([Ap, Dp, Hp, Ep]),
                  Contour.from_vertices([Bp, Cp, Gp, Fp]),
                  Contour.from_vertices([Ap, Bp, Fp, Ep]),
                  Contour.from_vertices([Cp, Dp, Hp, Gp]),
                  Contour.from_vertices([Ep, Fp, Gp, Hp])])

    Ad = A.rotate(np.pi/3, Vector([0, 2, 1]))
    Bd = B.rotate(np.pi/3, Vector([0, 2, 1]))
    Cd = C.rotate(np.pi/3, Vector([0, 2, 1]))
    Dd = D.rotate(np.pi/3, Vector([0, 2, 1]))
    Ed = E.rotate(np.pi/3, Vector([0, 2, 1]))
    Fd = F.rotate(np.pi/3, Vector([0, 2, 1]))
    Gd = G.rotate(np.pi/3, Vector([0, 2, 1]))
    Hd = H.rotate(np.pi/3, Vector([0, 2, 1]))
    face1d = Face([Contour.from_vertices([Ad, Bd, Cd, Dd])])
    face2d = Face([Contour.from_vertices([Ad, Dd, Hd, Ed])])
    face3d = Face([Contour.from_vertices([Bd, Cd, Gd, Fd])])
    face4d = Face([Contour.from_vertices([Ad, Bd, Fd, Ed])])
    face5d = Face([Contour.from_vertices([Cd, Dd, Hd, Gd])])
    face6d = Face([Contour.from_vertices([Ed, Fd, Gd, Hd])])
    cuboidd = Polyhedron([face1d, face2d, face3d, face4d, face5d, face6d])
    cuboid.rotate_ip(np.pi/3, Vector([0, 2, 1]))
    assert cuboid == cuboidd
    assert cuboid.faces[0].associated_plane == cuboidd.faces[0].associated_plane


def test_polyhedron_convexhull():
    r"""Tests for the construction of polyhedra in :math:`\mathbb{R}^3` as
    convex hulls of vertices.
    """
    # pylint: disable=R0914
    # R0914: Having more local variables enhances code readability.

    # ---------------------------
    # A cuboid and a prism in 3-d
    # ---------------------------

    A = Point(n=3)
    B = Point([2, 0, 0])
    C = Point([2, 1, 0])
    D = Point([0, 1, 0])
    E = Point([0, 0, 3])
    F = Point([2, 0, 3])
    G = Point([2, 1, 3])
    H = Point([0, 1, 3])
    face1 = Face([Contour.from_vertices([A, B, C, D])])
    face2 = Face([Contour.from_vertices([A, D, H, E])])
    face3 = Face([Contour.from_vertices([B, C, G, F])])
    face4 = Face([Contour.from_vertices([A, B, F, E])])
    face5 = Face([Contour.from_vertices([C, D, H, G])])
    face6 = Face([Contour.from_vertices([E, F, G, H])])
    face7 = Face([Contour.from_vertices([A, D, G, F])])
    face8 = Face([Contour.from_vertices([C, D, G])])
    face9 = Face([Contour.from_vertices([A, B, F])])
    face10 = Face([Contour.from_vertices([D, G, H])])
    face11 = Face([Contour.from_vertices([A, F, E])])
    cuboid = Polyhedron([face1, face2, face3, face4, face5, face6])
    cuboid_convexhull = Polyhedron.convex_hull_from_vertices([A, B, C, D,
                                                              E, F, G, H])
    prism = Polyhedron([face1, face3, face7, face8, face9])
    prism_convexhull = Polyhedron.convex_hull_from_vertices([A, B, C, D, F, G])
    assert cuboid == cuboid_convexhull
    assert prism == prism_convexhull
    prism2 = Polyhedron([face2, face6, face7, face10, face11])
    assert prism2 != prism
    assert prism < prism2
    assert prism2 > prism
    assert not prism == face11  # pylint: disable=C0113
    assert not prism != face11  # pylint: disable=C0113


def test_vertexcollection_basic():
    r"""Tests for the basic construction of vertex collections in
    :math:`\mathbb{R}^n` and operations on them.
    """
    # pylint: disable=R0914,R0915
    # R0914, R0915: Having more local variables enhances code readability.
    A = Point(n=3)
    B = Point([2, 0, 0])
    C = Point([2, 1, 0])
    D = Point([0, 1, 0])
    E = Point([0, 0, 3])
    F = Point([2, 0, 3])
    G = Point([2, 1, 3])
    H = Point([0, 1, 3])
    ABC = VertexCollection([A, B, C], cutoff=3)
    assert ABC.affdim == 2
    assert ABC.edges == sorted([Segment([A, B]), Segment([A, C]),
                                Segment([B, C])])
    assert ABC.vertices == [A, B, C]

    ABCD = VertexCollection([A, B, C, D], cutoff=3)
    AABCD = VertexCollection([A, A, B, C, D], cutoff=3)
    assert ABCD.affdim == 2
    assert ABCD.edges == sorted([Segment([A, B]), Segment([A, C]),
                                 Segment([A, D]), Segment([B, C]),
                                 Segment([B, D]), Segment([C, D])])
    assert ABCD.vertices == sorted([A, B, C, D])
    assert AABCD == ABCD
    assert AABCD.vertices == ABCD.vertices
    assert ABCD != ABC
    assert ABCD >= ABC
    assert ABC <= ABCD
    assert not ABC == Segment([A, B])  # pylint: disable=C0113
    assert not ABC != Segment([A, B])  # pylint: disable=C0113

    Ad = A.rotate(0.4, Vector([1, 2, 3]))
    Bd = B.rotate(0.4, Vector([1, 2, 3]))
    Cd = C.rotate(0.4, Vector([1, 2, 3]))
    Dd = D.rotate(0.4, Vector([1, 2, 3]))
    AdBdCdDd = VertexCollection([Ad, Bd, Cd, Dd], cutoff=3)
    ABCD.rotate_ip(0.4, Vector([1, 2, 3]))
    assert ABCD == AdBdCdDd

    EFGH = VertexCollection([E, F, G, H], cutoff=3)
    assert EFGH > ABCD
    assert ABCD < EFGH

    # Note that ABCD.rotate_ip mutates A, B, C, D as well.
    # These points are no longer the same as defined above.
    # We re-define them here.
    A = Point(n=3)
    B = Point([2, 0, 0])
    C = Point([2, 1, 0])
    D = Point([0, 1, 0])
    E = Point([0, 0, 3])
    F = Point([2, 0, 3])
    G = Point([2, 1, 3])
    H = Point([0, 1, 3])
    ABCD = VertexCollection([A, B, C, D], cutoff=2)
    assert ABCD.affdim == 2
    assert ABCD.cutoff == 2
    assert ABCD.edges == sorted([Segment([A, B]), Segment([A, D]),
                                 Segment([B, C]), Segment([C, D])])
    assert ABCD.vertices == sorted([A, B, C, D])

    ABCDEFGH = VertexCollection([A, B, C, D, E, F, G, H], cutoff=3)
    assert ABCDEFGH.affdim == 3
    assert len(ABCDEFGH.edges) == 16
    # Due to cutoff, some face diagonals are also included.
    assert ABCDEFGH.__repr__() ==\
            "\n" +\
            "VertexCollection[\n" +\
            "    Point(0.000, 0.000, 0.000)\n" +\
            "    Point(0.000, 0.000, 3.000)\n" +\
            "    Point(0.000, 1.000, 0.000)\n" +\
            "    Point(0.000, 1.000, 3.000)\n" +\
            "    Point(2.000, 0.000, 0.000)\n" +\
            "    Point(2.000, 0.000, 3.000)\n" +\
            "    Point(2.000, 1.000, 0.000)\n" +\
            "    Point(2.000, 1.000, 3.000)\n" +\
            "]"
    vertices_projected = [vertex.get_cabinet_projection()
                          for vertex in [A, B, C, D, E, F, G, H]]
    assert ABCDEFGH.get_cabinet_projection() ==\
            VertexCollection(vertices_projected, cutoff=3)
