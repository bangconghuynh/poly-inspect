""":mod:`.test_chemical_structures` contains unit tests for classes implemented
in :mod:`src.auxiliary.chemical_structures`.
"""

import pytest  # type: ignore
import numpy as np  # type: ignore

from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE
from polyinspect.auxiliary.geometrical_space import Point, Vector
from polyinspect.auxiliary.chemical_structures import Atom, Molecule


def test_atom() -> None:
    r"""Tests for the basic construction and manipulation of atoms.
    """
    atom1 = Atom('V', (1.0, 2.0, 4.9))
    assert repr(atom1) == "Atom[V, (+1.000, +2.000, +4.900)]"

    with pytest.raises(ValueError):
        Atom('G', (1.0, 2.0, 4.9))

    with pytest.raises(AssertionError):
        Atom(24, (1, 1, 1, 1))

    atom1.rotate_ip(np.pi/2, Vector([0, 1, 0]))
    assert atom1.position == Point([4.9, 2.0, -1.0])
    atom1.translate_ip(Vector([-2.9, 3.1, 0.5]))
    assert atom1.position == Point([2.0, 5.1, -0.5])


def test_molecule() -> None:
    r"""Tests for the basic construction and manipulation of molecules.
    """
    atom1 = Atom('H', (2.413, 0, 0))
    atom2 = Atom('H', (-1, 2.2, 1.1))
    atom3 = Atom('He', (2.413, 0, 0))

    with pytest.raises(AssertionError):
        Molecule([atom1, atom2, atom3])

    mol_propane = Molecule([Atom('C', (+0.0000000, -0.2745724, -1.2595561)),
                            Atom('C', (+0.0000000, +0.5863476, -0.0000000)),
                            Atom('C', (+0.0000000, -0.2745724, +1.2595561)),
                            Atom('H', (+0.0000000, +0.3792162, -2.1570471)),
                            Atom('H', (-0.9055041, -0.9175565, -1.2877191)),
                            Atom('H', (+0.9055041, -0.9175565, -1.2877191)),
                            Atom('H', (-0.9029885, +1.2342351, -0.0000000)),
                            Atom('H', (+0.9029885, +1.2342351, -0.0000000)),
                            Atom('H', (-0.9055041, -0.9175565, +1.2877191)),
                            Atom('H', (-0.0000000, +0.3792162, +2.1570471)),
                            Atom('H', (+0.9055041, -0.9175565, +1.2877191))])

    assert abs(mol_propane.mass - 44.09700000000002) < COMMON_ZERO_TOLERANCE
    assert abs(mol_propane.mass_ma - 44.062600257600025) < COMMON_ZERO_TOLERANCE
    assert mol_propane.com.is_same_as(Point([0, -6.15833277e-07, 0]),
                                      thresh=COMMON_ZERO_TOLERANCE)
    assert mol_propane.com_ma.is_same_as(Point([0, -8.14341137e-06, 0]),
                                         thresh=COMMON_ZERO_TOLERANCE)

    atom_he1 = Atom('He', (1, 1, 0))
    atom_he2 = Atom('He', (-1, 1, 0))
    atom_he3 = Atom('He', (-1, -1, 0))
    atom_he4 = Atom('He', (1, -1, 0))
    mol_he4 = Molecule([atom_he1, atom_he2, atom_he3, atom_he4])
    assert mol_he4.com == Point(n=3)
    moi, moi_axes = mol_he4.moi
    assert abs(moi[0] - 4*atom_he1.mass) < COMMON_ZERO_TOLERANCE
    assert abs(moi[1] - 4*atom_he1.mass) < COMMON_ZERO_TOLERANCE
    assert abs(moi[2] - 8*atom_he1.mass) < COMMON_ZERO_TOLERANCE
    assert moi_axes[0] == Vector([0, 1, 0])
    assert moi_axes[1] == Vector([1, 0, 0])
    assert moi_axes[2] == Vector([0, 0, 1])


def test_molecule_reorientation() -> None:
    r"""Tests for the reorientation and recentring of molecules.
    """
    atom_he1 = Atom('He', (2, 3, 0))
    atom_he2 = Atom('He', (0, 3, 0))
    atom_he3 = Atom('He', (0, 1, 0))
    atom_he4 = Atom('He', (2, 1, 0))
    mol_he4 = Molecule([atom_he1, atom_he2, atom_he3, atom_he4])
    assert mol_he4.com == Point([1, 2, 0])
    assert repr(mol_he4) ==\
            "\n" +\
            "Molecule[\n" +\
            "    Atom[He, (+2.000, +3.000, +0.000)]\n" +\
            "    Atom[He, (+0.000, +3.000, +0.000)]\n" +\
            "    Atom[He, (+0.000, +1.000, +0.000)]\n" +\
            "    Atom[He, (+2.000, +1.000, +0.000)]\n" +\
            "]"
    mol_he4.recentre_ip()
    assert mol_he4.com == Point(n=3)

    atom_h1 = Atom('H', (0, 0, 0))
    atom_h2 = Atom('H', (3, 3, 0))
    atom_h3 = Atom('H', (2, 4, 0))
    atom_h4 = Atom('H', (-1, 1, 0))
    mol_h4 = Molecule([atom_h1, atom_h2, atom_h3, atom_h4])
    mol_h4.rotate_ip(np.pi/2, Vector([0, 1, 0]))
    assert mol_h4.atoms[0].position == Point([0, 0, 0])
    assert mol_h4.atoms[1].position == Point([0, 3, -3])
    assert mol_h4.atoms[2].position == Point([0, 4, -2])
    assert mol_h4.atoms[3].position == Point([0, 1, 1])
    mol_h4.reorientate_ip()
    sq2 = np.sqrt(2)
    assert mol_h4.atoms[0].position == Point([0-3*sq2/2, 2-sq2/2, -1])
    assert mol_h4.atoms[1].position == Point([0+3*sq2/2, 2-sq2/2, -1])
    assert mol_h4.atoms[2].position == Point([0+3*sq2/2, 2+sq2/2, -1])
    assert mol_h4.atoms[3].position == Point([0-3*sq2/2, 2+sq2/2, -1])

    atom_c1 = Atom('C', (0, 0, 0))
    atom_c2 = Atom('C', (1, 0, 0))
    mol_c2 = Molecule([atom_c1, atom_c2])
    assert mol_c2[1] == atom_c2
    mol_c2.recentre_ip()
    mol_c2.rotate_ip(np.pi/3, Vector([1, -1, 1]))
    mol_c2.reorientate_ip(thresh=1e7*COMMON_ZERO_TOLERANCE)
    assert mol_c2.atoms[0].position == Point([0, 0, 0.5])
    assert mol_c2.atoms[1].position == Point([0, 0, -0.5])
