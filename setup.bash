#!/usr/bin/env bash

echo -n -e "Initialising git hooks... ";
git config --local core.hooksPath meta/githooks &&\
    echo -e "\e[92mSuccess.\e[0m" ||\
    {
        echo -e "\e[91mFailed.\e[0m";
        echo -e "\e[91mPlease troubleshoot and then execute 'git config --local core.hooksPath meta/githooks' manually. Exiting...\e[0m";
        exit 1;
    }

echo -e "Running initial git hooks...";
./meta/githooks/post-merge
echo
echo -e "Running initial git hooks... \e[92mSuccess.\e[0m";
