#!/bin/bash

source $(pwd)/meta/ci-tests.bash

# Default argument values
unset HALT_ON_ERROR

function usage() {
    printf '%s\n' "poly-inspect Routine Tests 0.0.1"
    printf '%s\n' "Execute CI test jobs for poly-inspect locally."
    printf '\n'
    printf '%s\n' "USAGE:"
    printf '%4s%s\n' "" "routine-test.bash [FLAGS]"
    printf '\n'
    printf '%s\n' "FLAGS:"
    printf '%4s%-30s%s\n' "" "-e, --halt-on-error" "Stop the test script on errors. Pause the test script and wait for user input to advance on warnings."
    printf '%4s%-30s%s\n' "" "-h, --help" "Print this help message."
}

# Argument parsing
for arg in "$@"
do
    case $arg in
        -e|--halt-on-error)
            HALT_ON_ERROR=1
            shift
            ;;
        -h|--help)
            usage
            exit 0
            ;;
        *)
            echo "Unrecognised flag $1. Use -h or --help for more information."
            exit 1
            ;;
    esac
done

# Main test execution
(
    trap 'echo; echo; echo "$summary"' EXIT;
    summary=$(printf '\e[1m%s\e[0m' "---------- summary ----------");
    err=0;
    mypy_src summary || { err=$?; [ -z "$HALT_ON_ERROR" ] && true || exit $err; };
    echo; echo;
    mypy_unittests summary || { err=$?; [ -z "$HALT_ON_ERROR" ] && true || exit $err; };
    echo; echo;
    pylint_src summary $HALT_ON_ERROR || { err=$?; [ -z "$HALT_ON_ERROR" ] && true || exit $err; };
    echo; echo;
    pylint_unittests summary $HALT_ON_ERROR || { err=$?; [ -z "$HALT_ON_ERROR" ] && true || exit $err; };
    echo; echo;
    pytest_unittestscov summary || { err=$?; [ -z "$HALT_ON_ERROR" ] && true || exit $err; };
    exit $err;
) &&\
    {
        echo;
        echo -e "\e[1;92m✔✔ All test jobs passed.\e[0m";
        echo -e "\e[92mThis may now be pushed upstream and checked through the CI pipeline.\e[0m" ;
    } ||\
    { [ $? = 1 ] &&\
        {
            echo;
            echo -e "\e[1;91m✘✘ Some test jobs failed.\e[0m";
            echo -e "\e[91mPlease fix all errors before pushing upstream.\e[0m" ;
        } ;
    }
