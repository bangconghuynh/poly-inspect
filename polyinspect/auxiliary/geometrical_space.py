""":mod:`.geometrical_space` contains classes for general n-dimensional real
geometrical and linear-algebraic objects of dimensions two and below.
"""

from __future__ import annotations

from typing import Optional, Sequence, List, Tuple, Any, Union, cast
from abc import ABC, abstractmethod
from math import ceil
import numpy as np # type: ignore
from scipy.spatial.transform import Rotation # type: ignore

from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE
from polyinspect.auxiliary.common_types import Scalar, RealScalar


class FiniteObject(ABC):
    r"""An abstract class describing a finite geometrical object in
    :math:`\mathbb{R}^n`, *i.e.*, an
    :math:`n`-dimensional coordinate space over :math:`\mathbb{R}`.
    Each finite object must have a bounding box.
    """

    @property
    def bounding_box(self) ->List[Tuple[RealScalar, RealScalar]]:
        r"""A list of :math:`n` tuples for the :math:`n` dimensions.
        Each tuple (min, max) contains the minimum and maximum coordinates in the
        corresponding dimension.
        """
        return self._bounding_box

    @bounding_box.setter
    def bounding_box(self,
                     bbox: Sequence[Tuple[RealScalar, RealScalar]]) -> None:
        assert all(isinstance(pair, tuple) for pair in bbox),\
                "Each (min, max) pair must be a tuple."
        self._bounding_box = list(bbox)

    # @property
    # def bounding_box_planes(self) -> List[Tuple[Plane, Plane]]:
    #     r"""A list of :math:`n` tuples for the :math:`n`
    #     dimensions. Each tuple contains the planes at the minimum and maximum
    #     coordinates in the corresponding dimension.
    #     """
    #     [(xmin,xmax),(ymin,ymax),(zmin,zmax)] = self.bounding_box
    #     bb = self.bounding_box
    #     bb_planes = []
    #     for dim, (coord_min, coord_max) in enumerate(bb):
    #         coord_min_plane

    #     xmin_plane = Plane3D(Vector3D([1,0,0]), Point3D([xmin,0,0]))
    #     xmax_plane = Plane3D(Vector3D([1,0,0]), Point3D([xmax,0,0]))
    #     ymin_plane = Plane3D(Vector3D([0,1,0]), Point3D([0,ymin,0]))
    #     ymax_plane = Plane3D(Vector3D([0,1,0]), Point3D([0,ymax,0]))
    #     zmin_plane = Plane3D(Vector3D([0,0,1]), Point3D([0,0,zmin]))
    #     zmax_plane = Plane3D(Vector3D([0,0,1]), Point3D([0,0,zmax]))
    #     return [(xmin_plane,xmax_plane),(ymin_plane,ymax_plane),
    #             (zmin_plane,zmax_plane)]

    @abstractmethod
    def _find_bounding_box(self) -> None:
        r"""Find the bounding box of the current geometrical object.
        """

    def intersects_bounding_box(self,
                                other: FiniteObject,
                                thresh: float = COMMON_ZERO_TOLERANCE) -> bool:
        r"""Check if the current bounding box intersects with `other`'s
        bounding box.

        :param other: An object that has a bounding box.
        :param thresh: The bounding box of `other` is increased by this amount
                in each dimension.

        :returns: A tuple of booleans for the real and imaginary components.
                For each boolean, :const:`True` if the two bounding boxes in the
                respective component intersect, :const:`False` if not.
        """
        self_bb = self.bounding_box
        other_bb = other.bounding_box
        assert len(self_bb) == len(other_bb),\
                "Bounding boxes do not have the same dimensions."
        for dim, _ in enumerate(self_bb):
            if self_bb[dim][0] > other_bb[dim][1]+thresh\
            or self_bb[dim][1] < other_bb[dim][0]-thresh:
                return False
        return True


class Point(FiniteObject):
    r"""A class describing a point in :math:`\mathbb{R}^n`, *i.e.*, an
    :math:`n`-dimensional coordinate space over :math:`\mathbb{R}`.

    Two points are geometrically equal if and only if the Euclidean distance
    between them is smaller than the global constant
    :const:`~.common_standards.COMMON_ZERO_TOLERANCE`.

    Two points can be compared using lexicographical comparison of their
    coordinates with respect to a threshold of
    :const:`~.common_standards.COMMON_ZERO_TOLERANCE`.
    """

    def __init__(self, coordinates: Optional[Sequence[RealScalar]] = None,
                 n: Optional[int] = None) -> None:
        """
        :param coordinates: A sequence of :math:`n` coordinate values. If this
                is :const:`None`, `n` must be provided so that an
                :math:`n`-coordinate point at the origin will be created.
        :param n: The dimensionality :math:`n` of the coordinate space.
        """
        if coordinates is None:
            assert n is not None
            coordinates = [0]*n
        if n is not None:
            assert len(coordinates) == n
        self.coordinates = np.array(coordinates)
        self._find_bounding_box()

    @property
    def coordinates(self) -> np.ndarray:
        """An immutable one-dimensional :class:`~numpy.ndarray` containing
        :math:`n` coordinate values.

        :raises ValueError: when non-finite elements are encountered.
        """
        return self._coordinates

    @coordinates.setter
    def coordinates(self, coords: np.ndarray) -> None:
        assert len(coords.shape) == 1, "Please supply a one-dimensional ndarray."
        if not np.all(np.isfinite(coords)):
            raise ValueError("Non-finite elements encountered.")
        assert np.all(np.isreal(coords)),\
                "Complex coordinates are not supported."
        self._coordinates = coords
        self._coordinates.flags.writeable = False

    @property
    def dim(self) -> int:
        r"""The dimensionality :math:`n` of the coordinate space
        :math:`\mathbb{R}^n` in which this point lives.
        """
        return self._coordinates.shape[0]

    @property
    def affdim(self) -> int:
        r"""The affine dimension of this point, which is always zero.

        This property has no setter.
        """
        return 0

    @classmethod
    def from_vector(cls, vector: Vector) -> Point:
        """Create a point from a position vector.

        :param vector: Position vector of the desired point.

        :returns: A :class:`Point` corresponding to `vector`.
        """
        return cls(vector.components.tolist())

    def __str__(self) -> str:
        # Add zero so that -0.000 becomes 0.000.
        str_coordinates = [f"{coord + 0:.3f}" for coord in self]
        return f"Point({', '.join(str_coordinates)})"


    __repr__ = __str__

    def __getitem__(self, key: Any) -> RealScalar:
        """Return the zero-based `key`-th coordinate of this point.
        """
        return self.coordinates[key]

    def __iter__(self):
        """The iterator iterates over the coordinates of this point.
        """
        for i in range(self.dim):
            yield self[i]

    def __add__(self, other: Point) -> Point:
        assert self.dim == other.dim,\
                "`self` and `other` must have the same dimensionality."
        return Point(self.coordinates + other.coordinates)

    def __radd__(self, other: Union[int, Point]) -> Point:
        if other == 0:
            return self
        assert isinstance(other, Point)
        return self.__add__(other)

    def __sub__(self, other: Point) -> Point:
        assert self.dim == other.dim,\
                "`self` and `other` must have the same dimensionality."
        return Point((self.coordinates - other.coordinates).tolist())

    def __neg__(self) -> Point:
        return Point((-self.coordinates).tolist())

    def __mul__(self, other: RealScalar) -> Point:
        return Point((other*self.coordinates).tolist())

    __rmul__ = __mul__

    def __truediv__(self, scalar: RealScalar) -> Point:
        assert np.abs(scalar) > COMMON_ZERO_TOLERANCE
        return Point((self.coordinates/scalar).tolist())

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Point):
            return NotImplemented
        return self.is_same_as(other, thresh=COMMON_ZERO_TOLERANCE)

    def __ne__(self, other: object) -> bool:
        return not self.__eq__(other)

    def __lt__(self, other: Point) -> bool:
        # Find the index of the first non-matching coordinate
        idx_array = np.where(np.abs(self.coordinates - other.coordinates)
                             >= COMMON_ZERO_TOLERANCE)[0]
        if idx_array.shape[0] > 0:
            idx = idx_array[0]
            return self[idx] < other[idx]
        return False

    def __gt__(self, other: Point) -> bool:
        idx_array = np.where(np.abs(self.coordinates - other.coordinates)
                             >= COMMON_ZERO_TOLERANCE)[0]
        if idx_array.shape[0] > 0:
            idx = idx_array[0]
            return self[idx] > other[idx]
        return False

    def __le__(self, other: Point) -> bool:
        return self < other or self == other

    def __ge__(self, other: Point) -> bool:
        return self > other or self == other

    def get_vector_to(self: Point, other: Point) -> Vector:
        """Find the displacement vector to another point.

        :param other: A point in the same space.

        :returns: Displacement vector from the current point to `other`.
        """
        return Vector((other.coordinates-self.coordinates).tolist())

    def is_same_as(self, other: Point,
                   thresh: float = COMMON_ZERO_TOLERANCE) -> bool:
        """Check if the current point is the same as `other`.

        :param point: A point in the same Euclidean space.
        :param thresh: Threshold to consider if the distance between the two
                       points is zero.

        :returns: `True` if the two points are the same, `False` if not.
        """
        if not isinstance(other, Point):
            return NotImplemented
        return self.get_vector_to(other).norm < thresh

    def _find_bounding_box(self) -> None:
        """Find the bounding box of the current point.
        """
        self.bounding_box = [(self[i], self[i]) for i in range(self.dim)]

    def translate(self: Point, vec: Vector) -> Point:
        r"""Translate the current point by the displacement vector `vec` to
        give a new point.

        :param vec: The displacement vector for the translation.

        :returns: The translated point.
        """
        assert self.dim == vec.dim
        translated_point = self + Point.from_vector(vec)
        return translated_point

    def translate_ip(self, vec: Vector,
                     translated: Optional[List[int]]) -> None:
        r"""Translate the current point by the displacement vector `vec`.

        The translation occurs in-place.

        :param vec: The displacement vector for the translation.
        :param translated: A list of the identifiers of objects that have
                already been translated in the current, potentially recursive,
                call to :meth:`translate_ip`.
        """
        if translated is None:
            return None
        if id(self) in translated:
            return None
        assert self.dim == vec.dim
        translated_point = self + Point.from_vector(vec)
        self.coordinates = translated_point.coordinates
        self._find_bounding_box()
        return None

    def rotate(self, angle: float, axis: Vector) -> Point:
        r"""Rotate the current point through `angle` about `axis` about the
        origin to give a new point.

        At the moment, only rotations in :math:`\mathbb{R}^3` are implemented.

        :param angle: The angle of rotation (radians).
        :param axis: A vector indicating the rotation axis. This vector will be
                normalised.

        :returns: The rotated result for the current point.
        """
        if self.dim != 3 or axis.dim != 3:
            raise NotImplementedError("Only rotations in R^3 are supported.")
        axis = axis.normalise()
        axis_array = axis.components
        rotmat = Rotation.from_rotvec(angle*axis_array).as_matrix()
        return self.transform(rotmat)

    def rotate_ip(self, angle: float, axis: Vector,
                  rotated: Optional[List[int]]) -> None:
        r"""Rotate the current point through `angle` about `axis` about the
        origin.

        The rotation occurs in-place.
        At the moment, only rotations in :math:`\mathbb{R}^3` are implemented.

        :param angle: The angle of rotation (radians).
        :param axis: A vector indicating the rotation axis. This vector will be
                normalised.
        :param rotated: A list of the identifiers of objects that have already
                been rotated in the current, potentially recursive, call to
                :meth:`rotate_ip`.
        """
        if self.dim != 3 or axis.dim != 3:
            raise NotImplementedError("Only rotations in R^3 are supported.")
        axis = axis.normalise()
        axis_array = axis.components
        rotmat = Rotation.from_rotvec(angle*axis_array).as_matrix()
        self.transform_ip(rotmat, rotated)

    def transform(self, mat: np.ndarray) -> Point:
        r"""Transform the current point about the origin by the transformation
        matrix `mat`.

        At the moment, only transformations in :math:`\mathbb{R}^3` are
        implemented.

        :param mat: The orthogonal transformation matrix.

        :returns: The transformed result for the current point.
        """
        if mat.shape != (3, 3):
            raise NotImplementedError("Only transformations in R^3 "
                                      + "are supported.")
        assert np.all(np.isreal(mat))
        assert abs(abs(np.linalg.det(mat)) - 1) < COMMON_ZERO_TOLERANCE
        transformed_coordinates = np.dot(mat, self.coordinates)
        return Point(transformed_coordinates.tolist())

    def transform_ip(self, mat: np.ndarray,
                     transformed: Optional[List[int]]) -> None:
        r"""Transform the current point about the origin by the transformation
        matrix `mat`.

        The transformation occurs in-place.
        At the moment, only transformations in :math:`\mathbb{R}^3` are
        implemented.

        :param mat: The orthogonal transformation matrix.
        :param transformed: A list of the identifiers of objects that have
                already been rotated in the current, potentially recursive, call
                to :meth:`transform_ip`.
        """
        if transformed is None:
            return None
        if id(self) in transformed:
            return None
        if mat.shape != (3, 3):
            raise NotImplementedError("Only transformations in R^3 "
                                      + "are supported.")
        assert np.all(np.isreal(mat))
        assert abs(abs(np.linalg.det(mat)) - 1) < COMMON_ZERO_TOLERANCE
        self.coordinates = np.dot(mat, self.coordinates)
        self._find_bounding_box()
        return None

    def get_cabinet_projection(self, alpha: float = np.arctan(2)) -> Point:
        r"""The cabinet projection of the current point onto the
        :math:`xy`-plane.

        This is only defined if the current point is in :math:`\mathbb{R}^3`.

        Let `alpha` = :math:`\alpha` such that :math:`0 \leq \alpha < 2\pi`.

        In a right-handed coordinate system, the cabinet projection projects
        the point :math:`(x,y,z)` onto
        :math:`(x-\frac{1}{2}z\cos\alpha, y-\frac{1}{2}z\sin\alpha, 0)`.
        Orthogonality between the :math:`x`- and :math:`y`-axes is maintained,
        while the projected :math:`z`-axis makes an angle of :math:`\alpha`
        w.r.t. the math:`x`-axis. In addition, the length of the receding lines
        is cut in half, hence the factor of :math:`\frac{1}{2}`.

        The viewing vector is
        :math:`(\frac{1}{2} z\cos\alpha, \frac{1}{2} z\sin\alpha, z)`,
        which is projected onto the origin. The projection lines are all
        parallel to this vector since this projection is a parallel projection.

        :param alpha: Angle :math:`\alpha` of projection (radians).
                :math:`0 \leq \alpha < 2\pi`.

        :returns: The projected point as a :class:`Point` in
                :math:`\mathbb{R}^2`.
        """
        # pylint: disable=C0103
        # C0103: x, y, z are okay as variable names since they denote
        #        coordinates.
        assert self.dim == 3,\
                "The cabinet projection is only defined for points in R^3."
        x, y, z = self[0], self[1], self[2]
        return Point([x-0.5*z*np.cos(alpha), y-0.5*z*np.sin(alpha)])


class Vector():
    r"""A class describing a vector in :math:`\mathbb{R}^n`, *i.e.*, an
    :math:`n`-dimensional coordinate space over a field :math:`\mathbb{R}`.
    """

    def __init__(self, components: Optional[Sequence[RealScalar]] = None,
                 n: Optional[int] = None) -> None:
        """
        :param components: A sequence of :math:`n` component values. If this is
            :const:`None`, `n` must be provided so that an :math:`n`-component
            zero vector will be created.
        :param n: The dimensionality :math:`n` of the coordinate space.
        """
        if components is None:
            assert n is not None
            components = [0]*n
        if n is not None:
            assert len(components) == n
        self.components = np.array(components)

    @property
    def components(self) -> np.ndarray:
        """An immutable one-dimensional :class:`~numpy.ndarray` containing
        :math:`n` coordinate values.
        """
        return self._components

    @components.setter
    def components(self, comps: np.ndarray) -> None:
        assert len(comps.shape) == 1, "Please supply a one-dimensional ndarray."
        if not np.all(np.isfinite(comps)):
            raise ValueError("Non-finite elements encountered.")
        assert np.all(np.isreal(comps)),\
                "Complex components are not supported."
        self._components = comps
        self._components.flags.writeable = False

    @property
    def dim(self) -> int:
        r"""The dimensionality :math:`n` of the coordinate space
        :math:`\mathbb{F}^n` in which this vector lives.
        """
        return self._components.shape[0]

    @property
    def norm(self) -> float:
        r"""Frobenius norm of the current vector.

        If :math:`\boldsymbol{v} = (v_1, v_2, \ldots, v_n)`, then its Frobenius
        norm is given by :math:`\sqrt{\sum_{i=1}^{n} \lvert v_i \rvert^2}`.

        .. warning::
            Care must be taken when associating the Frobenius norm of a vector
            :math:`\boldsymbol{v} \in \mathbb{F}^n` with its length.
            This is only possible if :math:`\boldsymbol{v}` contains the
            components of a vector in a Euclidean :math:`n`-space.
        """
        max_element = np.max(np.abs(self.components))
        if max_element < COMMON_ZERO_TOLERANCE:
            return np.linalg.norm(self.components)
        scaled_components = self.components/max_element
        return np.linalg.norm(scaled_components)*max_element

    @classmethod
    def from_point(cls, point: Point) -> Vector:
        """Create a position vector from a point.

        :param point: Point whose position vector will be returned.

        :returns: The position vector corresponding to `point`.
        """
        return cls(point.coordinates.tolist())

    def __str__(self) -> str:
        # Add zero so that -0.000 becomes 0.000.
        str_components = [f"{comp + 0:.3f}" for comp in self]
        return f"Vector({', '.join(str_components)})"

    __repr__ = __str__

    def __getitem__(self, key: Any) -> RealScalar:
        """Return the zero-based `key`-th component of this vector.
        """
        return self.components[key]

    def __iter__(self):
        """The iterator iterates over the components of this vector.
        """
        for i in range(self.dim):
            yield self[i]

    def __add__(self: Vector, other: Vector) -> Vector:
        assert self.dim == other.dim,\
                "`self` and `other` must have the same dimensionality."
        return self.__class__(self.components + other.components)

    def __radd__(self, other: Union[int, Vector]) -> Vector:
        if other == 0:
            return self
        assert isinstance(other, Vector)
        return self.__add__(other)

    def __sub__(self, other: Vector) -> Vector:
        assert self.dim == other.dim,\
                "`self` and `other` must have the same dimensionality."
        return Vector(self.components - other.components)

    def __neg__(self: Vector) -> Vector:
        return Vector(-self.components)

    def __mul__(self: Vector, other: Scalar) -> Vector:
        return Vector(other*self.components)

    __rmul__ = __mul__

    def __truediv__(self, scalar: Scalar) -> Vector:
        assert np.abs(scalar) > COMMON_ZERO_TOLERANCE
        return Vector(self.components/scalar)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Vector):
            return NotImplemented
        return self.is_same_as(other, COMMON_ZERO_TOLERANCE)

    def __ne__(self, other: object) -> bool:
        return not self.__eq__(other)

    def __lt__(self, other: Vector) -> bool:
        # Find the index of the first non-matching coordinate
        idx_array = np.where(np.abs(self.components - other.components)
                             >= COMMON_ZERO_TOLERANCE)[0]
        if idx_array.shape[0] > 0:
            idx = idx_array[0]
            return self[idx] < other[idx]
        return False

    def __gt__(self, other: Vector) -> bool:
        idx_array = np.where(np.abs(self.components - other.components)
                             >= COMMON_ZERO_TOLERANCE)[0]
        if idx_array.shape[0] > 0:
            idx = idx_array[0]
            return self[idx] > other[idx]
        return False

    def __le__(self, other: Vector) -> bool:
        return self < other or self == other

    def __ge__(self, other: Vector) -> bool:
        return self > other or self == other

    def is_same_as(self: Vector, other: Vector,
                   thresh: float = COMMON_ZERO_TOLERANCE) -> bool:
        r"""Check if the current vector is equal to `other`.

        Two vectors can only be compared if they have the same dimensionality.
        :math:`\boldsymbol{u}` and :math:`\boldsymbol{v}` in
        :math:`\mathbb{F}^n` are equal if and only if :math:`\lvert u_i - v_i \rvert
        \le \epsilon, \ i = 1, 2, \ldots, n`, where :math:`\epsilon` is given by
        `thresh`.

        :param other: A vector in the same coordinate space.
        :param thresh: Threshold to consider if the difference between the
                       corresponding components is zero.

        :returns: `True` if the two vectors are the same, `False` if not.
        """
        if not isinstance(other, self.__class__):
            return NotImplemented
        assert self.dim == other.dim,\
                "`self` and `other` must have the same dimensionality."
        return np.allclose(self.components,
                           other.components,
                           rtol=0, atol=thresh)

    def is_parallel(self, other: Vector,
                    thresh: float = COMMON_ZERO_TOLERANCE) -> bool:
        r"""Check if the current vector is parallel to `other`.

        Let :math:`\boldsymbol{v}_1` and :math:`\boldsymbol{v}_2` be the two
        vectors. They are parallel if and only if
        :math:`\frac{\boldsymbol{v}_1}{\lVert \boldsymbol{v}_1 \rVert}
        \cdot \frac{\boldsymbol{v}_2}{\lVert \boldsymbol{v}_2 \rVert} = \pm 1`.

        :param other: A vector to check for parallelism with the current vector.
        :param thresh: Threshold to determine if the above dot product is
                equal to :math:`\pm 1`.

        :returns: :const:`True` if the two vector are parallel,
                :const:`False` if not.
        """
        if abs(self.normalise().dot(other.normalise()) - 1) < thresh\
            or abs(self.normalise().dot(other.normalise()) + 1) < thresh:
            return True
        return False

    def dot(self: Vector, other: Vector) -> RealScalar:
        r"""Dot product between the current vector and `other`.

        Let :math:`\boldsymbol{u} \in \mathbb{F}^n` be `self` and
        :math:`\boldsymbol{v} \in \mathbb{F}^n` be `other`.
        The dot product between :math:`\boldsymbol{u}` and
        :math:`\boldsymbol{v}` is given by

        .. math::
            \boldsymbol{u} \cdot \boldsymbol{v} \equiv \sum_{i=1}^{n} u_iv_i.

        :param other: Another vector with which the dot product is to be
                      calculated.

        :returns: The dot product between `self` and `other`.
        """
        return np.dot(self.components, other.components)

    # def vdot(self: Vector, other: Vector) -> Scalar:
    #     r"""Complex dot product between the current vector and `other`, in that
    #     order.

    #     Let :math:`\boldsymbol{u} \in \mathbb{F}^n` be `self` and
    #     :math:`\boldsymbol{v} \in \mathbb{F}^n` be `other`.
    #     The complex dot product between :math:`\boldsymbol{u}` and
    #     :math:`\boldsymbol{v}` is given by

    #     .. math::
    #         \boldsymbol{u}^{\star} \cdot \boldsymbol{v} \equiv \sum_{i=1}^{n}
    #                 u_i^{\star} v_i,

    #     where :math:`\star` denotes complex conjugation.

    #     At the moment, only vectors in :math:`\mathbb{R}^n` are supported, so
    #     :meth:`vdot` and :meth:`dot` coincide.

    #     :param other: Another vector with which the dot product is to be
    #                   calculated.

    #     :returns: The dot product between `self` and `other`.
    #     """
    #     return np.vdot(self.components, other.components)

    def outer(self: Vector, other: Vector) -> np.ndarray:
        r"""Outer product between the current vector and `other`, in that order.

        Let :math:`\boldsymbol{u} \in \mathbb{F}^n` be `self` and
        :math:`\boldsymbol{v} \in \mathbb{F}^n` be `other`.
        The outer product between :math:`\boldsymbol{u}` and
        :math:`\boldsymbol{v}` is a :math:`n \times n` two-dimensional
        :class:`~numpy.ndarray` with elements

        .. math::
            (\boldsymbol{u} \otimes \boldsymbol{v})_{ij} = u_i v_j.

        :param other: Another vector with which the outer product is to be
                calculated.

        :returns: The outer product between `self` and `other`.
        """
        return np.outer(self.components, other.components)

    def normalise(self, thresh: float = COMMON_ZERO_TOLERANCE) -> Vector:
        r"""Normalise the current vector by

        .. math::
            \bar{\boldsymbol{v}} =
                \frac{1}{\sqrt{\sum_{i=1}^{n} \lvert v_i \rvert^2}}
                \boldsymbol{v}.

        :raises ZeroDivisionError: if the current vector is a null vector.

        :param thresh: If the Frobenius norm of the current vector is smaller
                than `thresh`, then it is considered a null vector.

        :returns: A unit vector parallel to the current vector.
        """
        norm = self.norm
        if norm < thresh:
            raise ZeroDivisionError(f"{self} has Frobenius norm {norm} "
                                    + f"< {thresh} and is a null vector.")
        return self/self.norm

    def positise(self, thresh: float = COMMON_ZERO_TOLERANCE) -> Vector:
        r"""Positise the current vector.

        :returns: The same vector if the number of its negative components
                does not exist the number of positive components, the negative
                of this vector otherwise.
        """
        assert thresh > 0
        pos_count = len([x for x in self if x > thresh])
        neg_count = len([x for x in self if x < -thresh])
        if pos_count >= neg_count:
            return self
        return -self

    def cross(self, other: Vector) -> Vector:
        r"""Cross product between the current vector and `other`, in that order.

        The cross product is only defined for vectors in :math:`\mathbb{R}^2`
        and :math:`\mathbb{R}^3`.

        :param other: Another vector with which the cross product is to be
                calculated.

        :returns: The cross product between `self` and `other`.
        """
        assert 2 <= self.dim == other.dim <= 3
        assert np.all(np.isreal(self.components)),\
            "The cross product is only defined for vectors in R^2 or R^3."
        if self.dim == 2 and other.dim == 2:
            return Vector([0, 0,
                           np.cross(self.components, other.components).item()])
        return Vector(np.cross(self.components, other.components).tolist())

    def rotate(self, angle: float, axis: Vector) -> None:
        r"""Rotate the current vector through `angle` about `axis`.

        The rotation occurs in-place.
        At the moment, only rotations in :math:`\mathbb{R}^3` are implemented.

        :param angle: The angle of rotation (radians).
        :param axis: A vector indicating the rotation axis. This vector will be
                normalised.

        :returns: The rotated result for the current vector.
        """
        if self.dim != 3 or axis.dim != 3:
            raise NotImplementedError("Only rotations in R^3 are supported.")
        axis = axis.normalise()
        axis_array = axis.components
        vector_array = self.components
        rot = Rotation.from_rotvec(angle*axis_array)
        rotated_vector_array = rot.apply(vector_array)
        self.components = rotated_vector_array

    def get_cabinet_projection(self, alpha: float = np.arctan(2)) -> Vector:
        r"""The cabinet projection of the current vector onto the
        :math:`xy`-plane.

        This is only defined if the current vector is in :math:`\mathbb{R}^3`.

        For a description of the cabinet projection, see
                :meth:`Point.get_cabinet_projection`.

        :param alpha: Angle :math:`\alpha` of projection (radians).
                :math:`0 \leq \alpha < 2\pi`.

        :returns: The projected vector as a :class:`Vector` in
                :math:`\mathbb{R}^2`.
        """
        # pylint: disable=C0103
        # C0103: x, y, z are okay as variable names since they denote
        #        coordinates.
        assert self.dim == 3,\
                "The cabinet projection is only defined for vectors in R^3."
        x, y, z = self[0], self[1], self[2]
        return Vector([x-0.5*z*np.cos(alpha), y-0.5*z*np.sin(alpha)])


def proper_rotation_matrix(angle: float, axis: Vector, power: int = 1)\
        -> np.ndarray:
    r"""Return a :math:`3 \times 3` rotation matrix in :math:`\mathbb{R}^3`
    corresponding to a rotation through `angle` about `axis`.

    :param angle: The angle of rotation (radians). A positive rotation angle
            is an anticlockwise rotation when looking down `axis`.
    :param axis: A vector indicating the rotation axis. This vector will be
            normalised.
    :param power: The power to which the rotation operation is raised.

    :returns: The matrix corresponding to this rotation.
    """
    assert isinstance(power, int)
    axis = axis.normalise()
    rotmat = Rotation.from_rotvec(angle*power*axis.components).as_matrix()
    return rotmat


def improper_rotation_matrix(angle: float, axis: Vector, power: int = 1)\
        -> np.ndarray:
    r"""Return a :math:`3 \times 3` improper rotation matrix in
    :math:`\mathbb{R}^3` corresponding to an improper rotation through `angle`
    about `axis`.

    :param angle: The angle of rotation (radians). A positive rotation angle
            is an anticlockwise rotation when looking down `axis`.
    :param axis: A vector indicating the improper rotation axis. This vector
            will be normalised.
    :param power: The power to which the improper operation is raised.

    :returns: The matrix corresponding to this improper rotation.
    """
    assert isinstance(power, int)
    axis = axis.normalise()
    rotmat = proper_rotation_matrix(angle, axis, power)
    refmat = np.identity(n=3) - 2*(power % 2)*axis.outer(axis)
    return np.dot(refmat, rotmat)


def check_regular_polygon(points: Sequence[Point],
                          thresh: float = COMMON_ZERO_TOLERANCE) -> bool:
    r"""Check if a sequence of points are vertices of a regular polygon.

    :param points: A sequence of points to be tested.
    :param thresh: Threshold for comparisons of angles and distances.

    :returns: :const:`True` if `points` form the vertices of a regular polygon,
            :const:`False` otherwise.
    """
    assert len(points) >= 3
    assert thresh > 0
    digits = -int(ceil(np.log10(thresh)))

    # Checking if all points are equidistant from the geometric centre
    centre = 1/len(points)*cast(Point, sum(points))
    radial_dists = [Vector.from_point(point - centre).norm for point in points]
    radial_dists_rounded = np.around(radial_dists, decimals=digits)
    if not len(set(radial_dists_rounded)) == 1:
        return False

    regular_angle = 2*np.pi/len(points)
    rad_vectors = [Vector.from_point(point - centre) for point in points]
    vec0 = rad_vectors[0]
    normal = vec0.cross(rad_vectors[1])
    while normal.norm < thresh:
        vec = next(iter(rad_vectors[2:]))
        normal = vec0.cross(vec)

    rad_vectors_sorted = np.array(
        sorted(
            rad_vectors,
            key=lambda vec: get_anticlockwise_angle(vec0, vec, normal)
        )
    ).tolist()
    vector_pairs = zip(rad_vectors_sorted, np.roll(rad_vectors_sorted, -1))
    angles = [get_anticlockwise_angle(vec1, vec2, normal)
              for (vec1, vec2) in vector_pairs]
    angles.append(regular_angle)
    angles_rounded = np.around(angles, decimals=digits)
    if not len(set(angles_rounded)) == 1:
        return False
    return True


def get_anticlockwise_angle(vec1: Vector, vec2: Vector, normal: Vector,
                            thresh: float = COMMON_ZERO_TOLERANCE) -> float:
    r"""Return the anticlockwise angle :math:`\phi` from `vec1` to `vec2` when
    viewed down the `normal` vector.

    This is only well defined in :math:`\mathbb{R}^3`. The range of the
    anticlockwise angle is such that :math:`0 \le \phi \le 2\pi`.

    :param vec1: The first vector.
    :param vec2: The second vector.
    :param normal: The normal vector. "Anticlockwise" is with respect to the
            view down this vector.
    :param thresh: Threshold for an angle to be considered as zero.

    :returns: The anticlockwise angle from `vec1` to `vec2` when viewed down
            `normal`.
    """
    assert thresh > 0
    assert vec1.dim <= 3
    assert vec2.dim <= 3
    assert vec1.norm >= thresh
    assert vec2.norm >= thresh
    dot = vec1.dot(vec2)
    det = normal.normalise().dot(vec1.cross(vec2))
    angle = np.arctan2(det, dot)
    if angle < -thresh:
        angle += 2*np.pi
    return angle
