""":mod:`.chemical_structures` contains classes for describing atomic and
molecular structures of chemical compounds.
"""

from __future__ import annotations

from typing import Union, List, Sequence, Tuple, Iterator, cast
from math import ceil
import numpy as np
import mendeleev  # type: ignore
import sqlalchemy  # type: ignore

from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE
from polyinspect.auxiliary.common_types import RealScalar
from polyinspect.auxiliary.geometrical_space import Point, Vector



class Atom():
    r"""A class describing an atom and its position in a three-dimensional
    real space.
    """
    def __init__(self, elem: Union[str, int], pos: Sequence[float]) -> None:
        r"""
        :param elem: Either a string specifying the element name or its
                atomic symbol, or an integer specifying the atomic number.
        :param pos: The coordinates of the atom in units of
                :math:`\unicode{x212B}`.
        """
        self.element = elem
        self.position = Point(pos)

    @staticmethod
    def from_xyz(line: str) -> Atom:
        r"""Construct an atom from a line in an ``xyz`` file.

        :param line: A line in an ``xyz`` file.

        :returns: An atom.
        """
        split_line = line.replace("\t", " ").strip().split()
        assert len(split_line) == 4, "Invalid atom line."
        pos = [float(coord) for coord in split_line[1:]]
        return Atom(split_line[0], pos)

    @property
    def element(self) -> mendeleev.Element:
        r"""An :class:`~mendeleev.tables.Element` object containing the
        information of this element.

        This property can be set by either a string specifying the element
        name or its atomic symbol, or an integer specifying the atomic number.
        """
        return self._element

    @element.setter
    def element(self, elem: Union[str, int]) -> None:
        """
        :param elem: Either a string specifying the element name or its
                atomic symbol, or an integer specifying the atomic number.

        :raises ValueError: if `elem` is not a valid chemical symbol or atomic
                number.
        """
        try:
            self._element = mendeleev.element(elem)
        except sqlalchemy.exc.NoResultFound as err:
            raise ValueError("Element not found.") from err

    @property
    def position(self) -> Point:
        r"""A :class:`~.geometrical_space.Point` object specifying the
        coordinates of the atom in a three-dimensional real space.
        """
        return self._position

    @position.setter
    def position(self, pos: Point) -> None:
        assert pos.dim <= 3,\
                "Only spaces of up to three dimensions are permitted."
        self._position = pos

    @property
    def mass(self) -> RealScalar:
        """The relative atomic mass of this element.

        This property has no setter.
        """
        return self.element.atomic_weight

    @property
    def mass_ma(self) -> RealScalar:
        """The mass of the most abundant isotope of this element.

        This property has no setter.
        """
        ma_iso = max(self.element.isotopes,
                     key=lambda iso: iso.abundance\
                             if iso.abundance is not None else 0)
        return ma_iso.mass

    def __str__(self) -> str:
        str_element = self.element.symbol
        str_coordinates = ', '.join([f"{(coord + 0):+.3f}"
                                     for coord in self.position])
        return f"{type(self).__name__}[{str_element}, ({str_coordinates})]"

    __repr__ = __str__

    def rotate_ip(self, angle: float, axis: Vector) -> None:
        r"""Rotate the current atom through `angle` about `axis` about the
        origin.

        The rotation occurs in-place.
        At the moment, only rotations in :math:`\mathbb{R}^3` are implemented.

        :param angle: The angle of rotation (radians).
        :param axis: A vector indicating the rotation axis. This vector will be
                normalised.
        """
        self.position.rotate_ip(angle, axis, [])

    def transform_ip(self, mat: np.ndarray) -> None:
        r"""Transform the current atom about the origin by the transformation
        matrix `mat`.

        The transformation occurs in-place.
        At the moment, only transformations in :math:`\mathbb{R}^3` are
        implemented.

        :param mat: The orthogonal transformation matrix.
        """
        self.position.transform_ip(mat, [])

    def translate_ip(self, vec: Vector) -> None:
        r"""Translate the current atom by the displacement vector `vec`.

        The translation occurs in-place.

        :param vec: The displacement vector for the translation.
        """
        self.position.translate_ip(vec, [])

    def get_distance_to(self, other: Atom) -> RealScalar:
        r"""Calculate the distance between the current atom and `other`.

        :param other: Another atom to which the distance from this atom is to
                be found.

        :returns: The distance between the two atoms.
        """
        return self.position.get_vector_to(other.position).norm

    def write_to_xyz(self, precision: int = 3) -> str:
        r"""Write the current atom to an ``xyz`` line.

        :params precision: The number of decimal places for the coordinates.

        :returns: A string containing the ``xyz`` line for the current atom.
        """
        coords = " ".join([f"{(coord + 0):+.{precision}f}"
                           for coord in self.position])
        line = f"{self.element.symbol} {coords}\n"
        return line


class Molecule():
    r"""A class describing an molecule and its constituting atoms.
    """
    def __init__(self, atoms: Sequence[Atom]) -> None:
        """
        :param atoms: A sequence of the atoms constituting this molecule.
        """
        self.atoms = atoms  # type: ignore

    @staticmethod
    def from_xyz(file: str) -> Molecule:
        r"""Construct a molecule from an ``xyz`` file.

        :param file: File name for an ``xyz`` file.

        :returns: A molecule.
        """
        n_atoms = 0
        atoms: list[Atom] = []
        with open(file, "r") as xyz:
            for i, line in enumerate(xyz):
                if i == 0:
                    n_atoms_parsed = float(line.strip())
                    assert n_atoms_parsed.is_integer()
                    n_atoms = int(n_atoms_parsed)
                elif i > 1:
                    atoms.append(Atom.from_xyz(line))
        assert len(atoms) == n_atoms
        return Molecule(atoms)

    @property
    def atoms(self) -> List[Atom]:
        r"""The list of atoms constituting this molecule.
        """
        return list(self._atoms)

    @atoms.setter
    def atoms(self, atoms: Sequence[Atom]) -> None:
        r"""The list of atoms constituting this molecule.
        """
        assert len(atoms) > 0, "Empty molecule detected."
        digits = -int(ceil(np.log10(COMMON_ZERO_TOLERANCE)))
        all_coords = np.array([atom.position.coordinates for atom in atoms])
        all_coords = np.around(all_coords, digits)
        _, unique_counts = np.unique(all_coords, axis=0, return_counts=True)
        assert np.all(unique_counts == 1), "Ill-defined geometry detected."
        self._atoms = atoms

    @property
    def mass(self) -> RealScalar:
        r"""The relative molecular mass of this molecule, expressed in units of
        :math:`\mathrm{u}`, the atomic mass unit.

        This property has no setter.
        """
        return sum(atom.mass for atom in self)

    @property
    def mass_ma(self) -> RealScalar:
        r"""The mass of the most abundant isotopologue of this molecule,
        expressed in units of :math:`\mathrm{u}`, the atomic mass unit.

        This property has no setter.
        """
        return sum(atom.mass_ma for atom in self)

    @property
    def com(self) -> Point:
        """The centre of (relative atomic) mass of this molecule.

        This property has no setter.
        """
        com = 1/self.mass*cast(Point,
                               sum(atom.mass*atom.position for atom in self))
        return com

    @property
    def com_ma(self) -> Point:
        """The centre of mass of the most abundant isotopologue of this
        molecule.

        This property has no setter.
        """
        com_ma = 1/self.mass_ma*cast(Point,
                                     sum(atom.mass_ma*atom.position
                                         for atom in self))
        return com_ma

    @property
    def moi(self) -> Tuple[Tuple[RealScalar, RealScalar, RealScalar],
                           Tuple[Vector, Vector, Vector]]:
        r"""The principal moments of inertia and the principal axes of the
        current fragment, with respect to the axes of `self`, *i.e.*, the
        current fragment, and passing through its centre of mass.

        The moments of inertia are expressed in units of
        :math:`\mathrm{u}\ \unicode{x212B}^2`, where :math:`\mathrm{u}`
        is the atomic mass unit.

        This property has no setter.

        Returns:
            - A tuple of the principal moments of inertia in ascending order
            - A tuple of the corresponding right-handed principal axes
        """
        maxdtype = max([atom.position.coordinates.dtype for atom in self]
                       + [np.dtype(np.float64)], key=lambda dtype: dtype.num)
        moi_tensor = np.zeros((3, 3), dtype=maxdtype)
        identity = np.identity(3, dtype=maxdtype)
        com = self.com
        for atom in self:
            rel_pos = com.get_vector_to(atom.position)
            moi_tensor += atom.mass*((rel_pos.norm**2)*identity
                                     - rel_pos.outer(rel_pos))
        principal_moi, principal_axes_mat = np.linalg.eigh(moi_tensor)
        principal_axes = [Vector(principal_axes_mat[:, i].tolist())\
                                .normalise().positise()
                          for i in range(principal_axes_mat.shape[1])]
        # We generate the axis with the smallest MoI ourselves to ensure
        # right-handedness.
        principal_axes[0] = principal_axes[1].cross(principal_axes[2])
        # We sort the MoIs in ascending order.
        # For degenerate MoIs, we then sort the corresponding axes in
        #   ascending lexicographical order of their components.
        principal_moi, principal_axes = zip(*sorted(zip(principal_moi,
                                                        principal_axes)))
        return ((principal_moi[0], principal_moi[1], principal_moi[2]),
                (principal_axes[0], principal_axes[1], principal_axes[2]))

    def __getitem__(self, key: int) -> Atom:
        """Return the zero-based `key`-th atom of this molecule.
        """
        return self.atoms[key]

    def __iter__(self) -> Iterator[Atom]:
        r"""The iterator iterates over the atoms in this molecule.
        """
        for atom in self.atoms:
            yield atom

    def rotate_ip(self, angle: float, axis: Vector) -> None:
        r"""Rotate the current molecule through `angle` about `axis` about the
        origin.

        The rotation occurs in-place.
        At the moment, only rotations in :math:`\mathbb{R}^3` are implemented.

        :param angle: The angle of rotation (radians).
        :param axis: A vector indicating the rotation axis. This vector will be
                normalised.
        """
        for atom in self:
            atom.rotate_ip(angle, axis)

    def transform_ip(self, mat: np.ndarray) -> None:
        r"""Transform the current molecule about the origin by the
        transformation matrix `mat`.

        The transformation occurs in-place.
        At the moment, only transformations in :math:`\mathbb{R}^3` are
        implemented.

        :param mat: The orthogonal transformation matrix.
        """
        for atom in self:
            atom.transform_ip(mat)

    def translate_ip(self, vec: Vector) -> None:
        r"""Translate the current molecule by the displacement vector `vec`.

        The translation occurs in-place.

        :param vec: The displacement vector for the translation.
        """
        for atom in self:
            atom.translate_ip(vec)

    def recentre_ip(self) -> None:
        r"""Recentre the molecule so that its centre of mass :attr:`.com` sits
        at the origin.
        """
        tvec = -Vector.from_point(self.com)
        self.translate_ip(tvec)

    def reorientate_ip(self, thresh: float = COMMON_ZERO_TOLERANCE) -> None:
        r"""Reorientate the molecule into a canonical alignment with the axes
        of the coordinate system.

        if the molecule has a unique principal axis, then this axis becomes
        aligned with the :math:`z`-axis and the other two degenerate axes become
        aligned with the :math:`x`- and :math:`y`-axes of the coordinate system.
        If the molecule has no unique principal axes, then the axes are aligned
        with :math:`x`-, :math:`y`-,  and :math:`z`-axes in ascending order of
        moments of inertia.

        :param thresh: Threshold for comparing moments of inertia.
        """
        moi, axes = self.moi
        if abs(moi[0] - moi[1]) > thresh > abs(moi[1] - moi[2]):
            # axes[0] is unique.
            rotmat = np.array([axes[1].components,
                               axes[2].components,
                               axes[0].components])
        else:
            # axes[2] is unique
            # no unique axis
            # isotropic
            rotmat = np.array([axes[0].components,
                               axes[1].components,
                               axes[2].components])
        tvec = -Vector.from_point(self.com)
        self.recentre_ip()
        self.transform_ip(rotmat)
        self.translate_ip(-tvec)

    def __str__(self) -> str:
        atom_str = "\n    ".join([str(atom) for atom in self])
        return f"{type(self).__name__}[\n    {atom_str}\n]"

    def __repr__(self) -> str:
        return "\n" + str(self)
