""":mod:`.bounded_affine_space` contains classes for general bounded real
affine subspaces of a Euclidean space.
"""

# pylint: disable=C0302
# C0302: All of the classes in this module are bounded affine spaces. Splitting
#        them into a different module would result in the loss of logical
#        arrangement.


from __future__ import annotations

from typing import Optional, Sequence, List, Tuple, Union, TypeVar, Generic, Iterable, cast
from itertools import chain
from warnings import warn
import numpy as np  # type: ignore
from scipy.linalg import orth  # type: ignore
from pyhull import qconvex  # type: ignore

from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE
from polyinspect.auxiliary.common_types import RealScalar, inf
from polyinspect.auxiliary.geometrical_space import Point, Vector, FiniteObject
from polyinspect.auxiliary.affine_space import AffineSubspace, Line, Hyperplane


BoundaryT = TypeVar("BoundaryT", bound=Union["BoundedAffineSubspace", Point])


def _winding_number(edge: Segment, point: Point,
                    origin: Point, basis_vecs: Tuple[Vector, Vector],
                    thresh: float = COMMON_ZERO_TOLERANCE) -> int:
    r"""Auxiliary function to determine the winding number of `point` relative
    to `edge` in the two-dimensional affine subspace spanned by the orthonormal
    vectors in `basis_vecs` and containing `origin`.

    `point` and `edge` are assumed to be coplanar here. Assertion of this
    property should be done elsewhere before calling this function.

    This implementation is based on the algorithm described
    `here <http://geomalgorithms.com/a03-_inclusion.html>`_.

    :param edge: A directed edge. The direction of this edge is that of the
            vector from endpoint[0] to endpoint[1].
    :param point: A point whose relationship with `edge` is to be tested.
    :param origin: A point in the two-dimensional affine subspace.
    :param basis_vecs: A tuple of two orthonormal basis vectors in
            :math:`\mathbb{R}^n` spanning the two-dimensional affine subspace
            containing `edge` and `point`.
    :param thresh: Threshold for determining of the :math:`z`-component of the
            cross-product is positive, negative, or zero.

    :returns: :const:`1` if `point` is to the left of an upward edge,
        :const:`-1` if `point` is to the right of a downward edge,
        or :const:`0` otherwise.
    """
    # pylint: disable=C0103
    # C0103: AB, AP, etc. okay as variable names since they denote geometric
    #        segments.
    # Express the coordinates of all points in the given basis.
    # "r" stands for "relative" to the origin in the plane.
    # "t" stands for "transformed" as though the plane has now become the
    # xy-plane.
    assert thresh >= 0
    tpoints = []
    for pnt in edge.endpoints + [point]:
        rpnt_vec = Vector.from_point(pnt - origin)
        rpntx, rpnty = rpnt_vec.dot(basis_vecs[0]), rpnt_vec.dot(basis_vecs[1])
        tpnt = Point([rpntx, rpnty])
        tpoints.append(tpnt)

    # tpoints now contains A, B, and P.
    # We check the sign of the cross product between AB and AP.
    # zcomp > 0: P to the left of AB.
    # zcomp < 0: P to the right of AB.
    # zcomp = 0: P on AB.
    AB = tpoints[0].get_vector_to(tpoints[1])
    AP = tpoints[0].get_vector_to(tpoints[2])
    zcomp = AB.cross(AP)[2]
    if zcomp > thresh and tpoints[0][1] <= tpoints[2][1] < tpoints[1][1]:
        # P to the left of AB; AB upward (A is included, crossing rule 1).
        return 1
    if zcomp < -thresh and tpoints[0][1] > tpoints[2][1] >= tpoints[1][1]:
        # P to the right of AB; AB downward (B is included, crossing rule 2).
        return -1
    return 0


class BoundedAffineSubspace(FiniteObject, Generic[BoundaryT]):
    r"""A generic class describing a bounded affine subspace in
    :math:`\mathbb{R}^n`, *i.e.*, an :math:`n`-dimensional coordinate space
    over :math:`\mathbb{R}`.


    Each bounded affine subspace consists of one or more boundaries, each of
    which is a :class:`~.geometrical_space.FiniteObject` subclass, and an
    associated affine subspace which is a :class:`AffineSubspace` subclass.
    """

    def __init__(self, boundaries: Sequence[BoundaryT]) -> None:
        r"""
        :param boundaries: A sequence of :class:`~geometrical_space.Point`
                or :class:`.BoundedAffineSubspace` objects.
        """
        self.boundaries = boundaries  # type: ignore
        self._find_bounding_box()

    @property
    def boundaries(self) -> List[BoundaryT]:
        r"""A list of the :class:`~geometrical_space.Point` or
        :class:`~.BoundedAffineSubspace` objects serving as the boundaries.

        If each boundary has an affine dimension of :math:`K-1`, then at least
        :math:`K+1` affinely independent boundaries are required to form a
        bounded affine subspace of affine dimension :math:`K`, *i.e.*, a
        :math:`K`-simplex.
        """
        return self._boundaries

    @boundaries.setter
    def boundaries(self, boundaries: Sequence[BoundaryT]) -> None:
        boundary_dims = {boundary.dim for boundary in boundaries}
        assert len(boundary_dims) == 1
        boundary_affdims = {boundary.affdim for boundary in boundaries}
        assert len(boundary_affdims) == 1
        boundary_affdim = next(iter(boundary_affdims))
        assert len(boundaries) >= boundary_affdim + 2,\
            f"The affine dimension of the boundaries is {boundary_affdim}. "\
            + f"At least {boundary_affdim+2} boundaries are needed, while "\
            + f"only {len(boundaries)} are given."

        # Construct the the affine subspace associated with this bounded
        # affine subspace.
        if all(isinstance(bdr, Point) for bdr in boundaries):
            anchor = cast(Point, boundaries[0])
            raw_direction =\
                    [Vector.from_point(bdr - anchor)
                     for bdr in cast(Iterable[Point], boundaries[1:])]
        else:
            vertices = list(chain.from_iterable([bdr.get_subdim_boundaries(0)
                                                 for bdr in boundaries]))
            assert all(isinstance(ver, Point) for ver in vertices)
            anchor = cast(Point, vertices[0])
            raw_direction = [Vector.from_point(vertex - anchor)
                             for vertex in cast(Iterable[Point], vertices[1:])]

        directionmatrix = orth(np.hstack([np.reshape(vec.components, (-1, 1))
                                          for vec in raw_direction]))
        direction = [Vector(col) for col in directionmatrix.T]
        affdim = len(direction)
        assert affdim >= boundary_affdim + 1,\
            "The affine dimension of the bounded affine subspace to be "\
            + f"constructed, {affdim}, is not at least one higher than that "\
            + f"of the boundaries, {boundary_affdim}."
        self._associated_affinesubspace = AffineSubspace(anchor, direction)
        self._boundaries = list(boundaries)

    @property
    def associated_affinesubspace(self) -> AffineSubspace:
        r"""The affine subspace whose bounded region gives this object.

        This property has no setter and can only be set when :attr:`boundaries`
        is set.
        """
        return self._associated_affinesubspace

    @property
    def dim(self) -> int:
        r"""The dimensionality :math:`n` of the coordinate space
        :math:`\mathbb{R}^n` in which this bounded affine subsspace lives.
        """
        return self.get_a_point().dim

    @property
    def affdim(self) -> int:
        r"""The affine dimension :math:`K` of this bounded affine subspace.

        This property has no setter.
        """
        return self.associated_affinesubspace.affdim

    @property
    def non_adj_vertices(self) -> List[Point]:
        r"""Return the unique vertices of this bounded affine subspace.

        .. note::
            These vertices have no neighbouring relationships to one another.
            One-dimensional adjacent vertices can only be defined for
            :class:`Contour`.

        :returns: A list of the unique vertices of this bounded affine subspace.
        """
        non_unique_vertices = cast(List[Point],
                                   sorted(self.get_subdim_boundaries(0)))
        assert len(non_unique_vertices) >= 2
        unique_vertices = [non_unique_vertices[0]]
        for vertex in non_unique_vertices[1:]:
            if vertex.is_same_as(unique_vertices[-1], COMMON_ZERO_TOLERANCE):
                continue
            unique_vertices.append(vertex)
        return unique_vertices

    def get_common_vertices(self, other: BoundedAffineSubspace,
                            thresh: float = COMMON_ZERO_TOLERANCE)\
                                    -> List[Point]:
        r"""Return a list of the common vertices between the current bounded
        affine subspace and `other`.

        :param other: Another bounded affine subspace whose common vertices
                with this current bounded affine subspace are to be found.
        :param thresh: Threshold to determine of two vertices are the same.

        :returns: A list of the common vertices.
        """
        both_vertices = sorted(self.non_adj_vertices + other.non_adj_vertices)
        common_vertices = [both_vertices[0]]
        for vertex in both_vertices[1:]:
            if vertex.is_same_as(common_vertices[-1], thresh):
                continue
            common_vertices.append(vertex)
        return common_vertices

    def get_subdim_boundaries(self, subdim: int)\
            -> List[Union[Point, BoundedAffineSubspace]]:
        r"""Return the sub-dimensional boundaries.

        This only returns references to the appropriate objects without
        deep-copying them.

        Let :math:`K` be the current affine dimension and :math:`k` `subdim`
        such that :math:`0 \le k \le K-1`. This method returns the list of
        sub-dimensional boundaries with affine dimension :math:`k`.

        :param subdim: The affine sub-dimension for which the corresponding
                boundaries are to be returned.

        :returns: A list of the boundaries with affine dimension :math:`k`.
        """
        assert 0 <= subdim <= self.affdim-1
        if subdim == self.affdim-1:
            return list(self.boundaries)
        return [subdim_bdr
                for bdr in self.boundaries
                for subdim_bdr in bdr.get_subdim_boundaries(subdim)]

    def get_a_point(self) -> Point:
        r"""Return a point in this bounded affine subspace.

        This method is useful when a point in the associated affine subspace
        is required, but it does not matter which one.

        .. note::
            It is not certain that this point will lie *inside* the
            bounded region. Tests for boundedness can be done easily in two
            dimensions, but are rather challenging in higher dimensions, and so
            will be implemented later.

        :returns: A point in the associated affine subspace.
        """
        if all(isinstance(boundary, Point) for boundary in self.boundaries):
            return cast(Point, sum(self.boundaries))/len(self.boundaries)
        return cast(Point, sum([bdr.get_a_point() for bdr in self.boundaries])\
                            /len(self.boundaries))

    def translate_ip(self, vec: Vector,
                     translated: Optional[List[int]] = None) -> None:
        r"""Translate all points in the current bounded affine subspace by a
        displacement `vec`.

        The translation occurs in-place. However, the vertices are duplicated.

        :param vec: The displacement vector of the translation.
        :param translated: A list of the identifiers of objects that have
                already been translated in the current, potentially recursive,
                call to :meth:`translate_ip`.
        """
        assert vec.dim == self.dim,\
                "The displacement vector has incompatible dimensions."
        if translated is None:
            translated = []
        for boundary in self.boundaries:
            if id(boundary) not in translated:
                boundary.translate_ip(vec, translated)
                translated.append(id(boundary))
        self.boundaries = self.boundaries  # To update the assoc. aff.
        self._find_bounding_box()

    def rotate_ip(self, angle: float, axis: Vector,
                  rotated: Optional[List[int]] = None) -> None:
        r"""Rotate all points in the current bounded affine subspace through
        `angle` about `axis` about the origin.

        The rotation occurs **in-place**.
        At the moment, only rotations in :math:`\mathbb{R}^3` are implemented.

        :param angle: The angle of rotation (radians).
        :param axis: A vector indicating the rotation axis. This vector will be
                normalised.
        :param rotated: A list of the identifiers of objects that have already
                been rotated in the current, potentially recursive, call to
                :meth:`rotated_ip`.
        """
        assert axis.dim == self.dim == 3,\
                "Only three-dimensional rotations are supported at the moment."
        if rotated is None:
            rotated = []
        for boundary in self.boundaries:
            if id(boundary) not in rotated:
                boundary.rotate_ip(angle, axis, rotated)
                rotated.append(id(boundary))
        self.boundaries = self.boundaries  # To update the assoc. aff.
        self._find_bounding_box()

    def _find_bounding_box(self) -> None:
        """Find the bounding box of the current bounded affine subspace.
        """
        bbox = []
        for dim in range(self.dim):
            coords = sorted([coord for boundary in self.boundaries
                             for coord in boundary.bounding_box[dim]])
            bbox.append((min(coords), max(coords)))
        self.bounding_box = bbox


# pylint: disable=E1136
# E1136: BoundedAffineSubspace is subscriptable as it is a Generic type.
class Segment(BoundedAffineSubspace[Point]):
    r"""A class describing a segment in :math:`\mathbb{R}^n`, *i.e.*, an
    :math:`n`-dimensional coordinate space over :math:`\mathbb{R}`.

    Mathematically, this segment is a collection of all points satisfying
    the vector equation

    .. math:: \boldsymbol{r} = \boldsymbol{r}_0 + \lambda \boldsymbol{d}

    where :math:`\lambda \in \mathbb{R}` is a real scalar parameter in a
    specified interval.

    Two segments are equal if and only if their endpoints are equal.
    """

    def __init__(self, endpoints: Sequence[Point]) -> None:
        r"""
        :param endpoints: A sequence of two points defining the endpoints of the
                segment. The order does not matter as segments are
                non-directional.
        """
        if len(endpoints) > 2:
            warn(f"A {len(endpoints)}-endpoint segment is to be constructed. "\
                 + "Checking for affine dependence...", UserWarning)
        super().__init__(endpoints)
        assert self.affdim == 1,\
            f"Affine dimension of {self.affdim} detected. "\
            + "A segment can only exist in a one-dimensional affine subspace."
        self._find_bounding_box()

    @property
    def endpoints(self) -> List[Point]:
        r"""A list of points defining the endpoints of the segment.
        """
        return self.boundaries

    @property
    def midpoint(self) -> Point:
        r"""Midpoint of the segment.
        """
        return 0.5*(self.endpoints[0] + self.endpoints[1])

    @property
    def length(self) -> RealScalar:
        r"""Length of the current segment.
        """
        return self.vector.norm

    @property
    def vector(self) -> Vector:
        r"""Vector corresponding to the current segment.

        The direction of this vector is chosen to be from the first endpoint
        towards the second endpoint.
        """
        return self.endpoints[0].get_vector_to(self.endpoints[1])

    @property
    def associated_line(self) -> Line:
        r"""The line containing this segment.
        """
        return Line.from_affinesubspace(self.associated_affinesubspace)

    def __str__(self) -> str:
        endpt_str = ", ".join([endpt.__str__().replace("\n", "\n    ")
                               for endpt in self.endpoints])
        return f"Segment[{endpt_str}]"

    def __repr__(self) -> str:
        return "\n" + self.__str__()

    def get_fraction_of_segment(self: Segment, mu: RealScalar, point: Point,
                                thresh: float = COMMON_ZERO_TOLERANCE)\
                               -> Tuple[Point, Optional[Segment]]:
        r"""Return a fraction of this segment.

        Let :math:`\mathrm{A}` be `point`. :math:`\mathrm{A}` must be one of
        the endpoints of the segment.
        Let :math:`\mathrm{B}` be the other endpoint of the segment, and let
        `mu` :math:`\equiv\mu \in \mathbb{R}`.
        The segment :math:`\mathrm{AP}` is returned such that
        :math:`\mathrm{AP}/\mathrm{AB} = \mu`.

        :param mu: Fraction of the segment length. :math:`0 \leq \mu \leq 1`.
        :param point: The endpoint from which the fraction length is measured.
        :param thresh: Threshold to determine if `point` corresponds to one of
                the two endpoints.

        Returns
        -------
        P
            Point :math:`\mathrm{P}`.
        AP
            Fraction :math:`\mathrm{AP}` of this segment as defined above.
            If :math:`\mathrm{P}` coincides with :math:`\mathrm{A}`,
            :const:`None` will be returned instead.
        """
        # pylint: disable=C0103
        # C0103: mu okay as a variable name since it denotes the corresponding
        #        Greek variable.
        assert point.is_same_as(self.endpoints[0], thresh) or\
               point.is_same_as(self.endpoints[1], thresh),\
                f"{point} does not correspond to either endpoint of {self}."

        assert -thresh <= mu <= 1+thresh, f"{mu} lies outside [0, 1]."

        for endpoint in self.endpoints:
            if point.is_same_as(endpoint, thresh):
                A = endpoint
            else:
                B = endpoint
        P = A + mu*(B - A)
        if mu < thresh:
            return (P, None)
        return (P, Segment([A, P]))

    def get_subsegment(self: Segment, mu_start: RealScalar, mu_end: RealScalar,
                       point: Point, thresh: float = COMMON_ZERO_TOLERANCE)\
                    -> Segment:
        r"""Return a subsegment of this segment.

        Let :math:`\mathrm{A}` be `point`. :math:`\mathrm{A}` must be one of
        the endpoints of the segment.
        Let :math:`\mathrm{B}` be the other endpoint of the segment, and let
        `mu` :math:`\equiv\mu \in \mathbb{R}`.
        The segment :math:`\mathrm{P}_1\mathrm{P}_2` is returned such that
        :math:`\mathrm{AP}_1/\mathrm{AB} = \mu_{\mathrm{start}}` and
        :math:`\mathrm{AP}_2/\mathrm{AB} = \mu_{\mathrm{end}}`.

        :param mu_start: Fraction of the segment length to the starting point
                of the subsegment. :math:`0 \leq \mu_{\mathrm{start}} \leq 1`.
        :param mu_end: Fraction of the segment length to the ending point
                of the subsegment. :math:`0 \leq \mu_{\mathrm{end}} \leq 1`.
        :param point: The endpoint from which the fraction length is measured.
        :param thresh: Threshold to determine if `point` corresponds to one of
                the two endpoints.

        :raises AssertionError: if `mu_start` and `mu_end` are closer than
                `thresh`.

        :returns: The segment :math:`\mathrm{P}_1\mathrm{P}_2`.
        """
        # pylint: disable=C0103
        # C0103: mu okay as a variable name since it denotes the corresponding
        #        Greek variable.
        assert point.is_same_as(self.endpoints[0], thresh) or\
               point.is_same_as(self.endpoints[1], thresh),\
                f"{point} does not correspond to either endpoint of {self}."

        assert -thresh <= mu_start <= 1+thresh,\
                f"{mu_start} lies outside [0, 1]."
        assert -thresh <= mu_end <= 1+thresh,\
                f"{mu_end} lies outside [0, 1]."

        for endpoint in self.endpoints:
            if point.is_same_as(endpoint, thresh):
                A = endpoint
            else:
                B = endpoint
        assert abs(mu_start - mu_end) >= thresh
        P1 = A + mu_start*(B - A)
        P2 = A + mu_end*(B - A)
        return Segment([P1, P2])

    def find_fraction(self, anchor: Point, point: Point,
                      thresh: float = COMMON_ZERO_TOLERANCE) -> RealScalar:
        r"""Return the fraction between `point` and `anchor` relative to the
        current segment. `anchor` must be one of the endpoints of the current
        segment, and `point` must lie within the segment.

        Let :math:`\mathrm{AB}` be the current segment. Assume that
        :math:`\mathrm{A}` is `anchor` and :math:`\mathrm{P}` is `point` which
        must lie on :math:`\mathrm{AB}`. We seek
        :math:`\mu = \mathrm{AP}/\mathrm{AB}`.

        :param anchor: One of the two endpoints of the segment.
        :param point: A point on the current segment.
        :param thresh: Threshold to determine if `anchor` corresponds to either
                endpoint, and if `point` lies on the current segment.

        :returns: The value of :math:`\mu` as defined above.
        """
        # pylint: disable=C0103
        # C0103: A, B, etc. denote points and segments mathematically.
        assert any(anchor.is_same_as(endpt) for endpt in self.endpoints),\
                f"{anchor} does not correspond to either endpoint."
        assert self.contains_point(point, thresh),\
                f"{point} does not lie within {self}."
        return anchor.get_vector_to(point).norm/self.length

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Segment):
            return NotImplemented
        return self.is_same_as(other, COMMON_ZERO_TOLERANCE)

    def __ne__(self, other: object) -> bool:
        return not self.__eq__(other)

    def _create_comparison_tuples(self, other: Segment)\
            -> Tuple[Tuple[Point, Point],
                     Tuple[Point, Point]]:
        r"""Return comparison tuples for `self` and `other` which can be compared
        lexicographically.

        Each comparison tuple has the form :math:`(l, \mathrm{A}, \mathrm{B})`,
        where :math:`l` is the length of the segment, and :math:`\mathrm{A}` and
        :math:`\mathrm{B}` are its endpoints sorted such that
        :math:`\mathrm{A} < \mathrm{B}` lexicographically, provided that they
        are points in :math:`\mathbb{R}^n`.

        :param other: Another :class:`Segment`.

        :returns: A tuple of comparison tuples.
        """
        self_endpts_sorted = sorted(self.endpoints)
        other_endpts_sorted = sorted(other.endpoints)
        comparison_tuple_self = (self_endpts_sorted[0],
                                 self_endpts_sorted[1])
        comparison_tuple_other = (other_endpts_sorted[0],
                                  other_endpts_sorted[1])
        return (comparison_tuple_self, comparison_tuple_other)

    def __lt__(self, other: Segment) -> bool:
        if abs(self.length - other.length) >= COMMON_ZERO_TOLERANCE:
            return self.length < other.length
        comparison_tuple_self, comparison_tuple_other =\
                self._create_comparison_tuples(other)
        return comparison_tuple_self < comparison_tuple_other

    def __gt__(self, other: Segment) -> bool:
        if abs(self.length - other.length) >= COMMON_ZERO_TOLERANCE:
            return self.length > other.length
        comparison_tuple_self, comparison_tuple_other =\
                self._create_comparison_tuples(other)
        return comparison_tuple_self > comparison_tuple_other

    def __le__(self, other: Segment) -> bool:
        return self < other or self == other

    def __ge__(self, other: Segment) -> bool:
        return self > other or self == other

    def is_same_as(self, other: Segment,
                   thresh: float = COMMON_ZERO_TOLERANCE) -> bool:
        r"""Check if the current segment is the same as `other`.

        :param other: A segment to check for identicality with the current one.
        :param thresh: Threshold to determine if two points are the same.

        :returns: :const:`True` if `other` is the same as the current segment,
                :const:`False` if not.
        """
        if not isinstance(other, Segment):
            return NotImplemented
        if len(self.get_common_vertices(other, thresh))\
                == len(self.endpoints) == len(other.endpoints):
            return True
        return False

    def contains_point(self, point: Point,
                       thresh: float = COMMON_ZERO_TOLERANCE) -> bool:
        r"""Check if `point` lies within the current segment.

        :param point: A point to check for membership of the current segment.
        :param thresh: Threshold to determine if the associated line of the
                current segment contains `point`.

        :returns: :const:`True` if `point` lies on the current segment,
                :const:`False` if not.
        """
        # pylint: disable=C0103
        # C0103: A, B, etc. denote points and segments mathematically.
        if self.associated_line.contains_point(point, thresh):
            AP = self.endpoints[0].get_vector_to(point)
            BP = self.endpoints[1].get_vector_to(point)
            if AP.norm < thresh or BP.norm < thresh:
                # Point P coincides with either endpoint.
                return True
            if abs(self.length - AP.norm - BP.norm) < thresh:
                # Point P lies between the two endpoints.
                return True
        return False

    def shares_an_endpoint(self, other: Segment,
                           thresh: float = COMMON_ZERO_TOLERANCE)\
                                -> Tuple[bool, Optional[Point]]:
        r"""Check if `other` shares one and only one endpoint with the current
        segment.

        :param other: Another segment to check for the end-point-sharing
                property.
        :param thresh: Threshold to determine if two endpoints are the same.

        :returns: A tuple in which the first element is :const:`True` if the
                above condition holds, :const:`False` if not, and the
                second element is the shared endpoint.
        """
        assert self != other, "Two segments cannot be identical!"
        if self.endpoints[0].is_same_as(other.endpoints[0], thresh):
            return (True, self.endpoints[0])
        if self.endpoints[0].is_same_as(other.endpoints[1], thresh):
            return (True, self.endpoints[0])
        if self.endpoints[1].is_same_as(other.endpoints[0], thresh):
            return (True, self.endpoints[1])
        if self.endpoints[1].is_same_as(other.endpoints[1], thresh):
            return (True, self.endpoints[1])
        return (False, None)

    def shares_connecting_endpoint(self, other: Segment,
                                   thresh: float = COMMON_ZERO_TOLERANCE)\
                                -> Tuple[bool, Optional[Point]]:
        r"""Check if the second endpoint of the current segment coincides with
        the first endpoint of other.

        This function is *not* reflexive:
        ``self.shares_connecting_endpoint(other) == True`` implies that
        ``other.shares_connecting_endpoint(self) == False``,

        :param other: Another segment to check for the
                connecting-end-point-sharing property.
        :param thresh: Threshold to determine if two endpoints are the same.

        :returns: A tuple in which the first element is :const:`True` if the
                above condition holds, :const:`False` if not, and the
                second element is the shared endpoint.
        """
        assert self != other, "Two segments cannot be identical!"
        if self.endpoints[1].is_same_as(other.endpoints[0], thresh):
            return (True, self.endpoints[1])
        return (False, None)

    def intersects_line(self, line: Line,
                        thresh: float = COMMON_ZERO_TOLERANCE)\
                       -> Tuple[int, Union[None, Point, Segment]]:
        r"""Check if the current segment intersects `line`.

        :param line: A line to check for intersection with the current segment.
        :param thresh: Threshold to determine if the shortest distance between
                the line and the current segment is zero.

        Returns
        -------
        n:
            Number of points of intersection.
        intersection:
            :const:`None` if `n` is zero, :class:`Point` if `n` = 1,
            :class:`Segment` if `n` = :const:`numpy.inf`.
        """
        n_int_line, intersection_line =\
                self.associated_line.intersects_line(line, thresh)
        if n_int_line == 0:
            return (0, None)
        if n_int_line == 1:
            assert isinstance(intersection_line, Point)
            if self.contains_point(intersection_line, thresh):
                return (1, intersection_line)
            return (0, None)
        return (inf, self)

    def intersects_segment(self: Segment, other: Segment,
                           thresh: float = COMMON_ZERO_TOLERANCE)\
                          -> Tuple[int, Union[None, Point, Segment]]:
        r"""Check if the current segment intersects `other`.

        :param other: A segment to check for intersection with the current
                segment.
        :param thresh: Threshold to determine if the associated lines of the
                two segments intersect.

        Returns
        -------
        n:
            Number of points of intersection.
        intersection:
            :const:`None` if `n` is zero, :class:`Point` if `n` = 1,
            :class:`Segment` if `n` = :const:`numpy.inf`.
        """
        # pylint: disable=R0911
        # R0911: More return statements are used for clarify. Refactoring is
        #        possible but that would make the code less clear.
        n_int_line, intersection_line =\
                self.intersects_line(other.associated_line, thresh)

        if n_int_line == 0:
            return (0, None)

        if n_int_line == 1:
            assert isinstance(intersection_line, Point)
            if self.contains_point(intersection_line, thresh) and\
               other.contains_point(intersection_line, thresh):
                return (1, intersection_line)
            return (0, None)

        # n_int_line = inf. Both segments on the same line.
        # We define a list contained_endpts.
        # Several cases possible:
        #    Case                                         contained_endpts
        # a. No overlap: A-B  C-D                         []
        # b. Intersect at endpoints: A-BC-D               [B, C]
        # c. Partial overlap: A-C=B-D or variations       [B, C]
        # d. One segment lies within the other: A-C=D-B   [C, D]
        # e. One segment lies within the other: AC=D-B    [A, C, D]
        # f. Perfect coincidence: AC=DB                   [A, B, C, D]
        contained_endpts = []
        for point in other.endpoints:
            if self.contains_point(point, thresh):
                contained_endpts.append(point)
        for point in self.endpoints:
            if other.contains_point(point, thresh):
                contained_endpts.append(point)
        assert len(contained_endpts) in [0, 2, 3, 4]

        if len(contained_endpts) == 0:
            return (0, None)
        if len(contained_endpts) == 2:
            if contained_endpts[0].is_same_as(contained_endpts[1], thresh):
                return (1, contained_endpts[0])
            return (inf, Segment(contained_endpts))
        if len(contained_endpts) == 3:
            if not contained_endpts[0].is_same_as(contained_endpts[1], thresh):
                return (inf, Segment(contained_endpts[0:2]))
            seg = Segment([contained_endpts[0], contained_endpts[2]])
            return (inf, seg)
        return (inf, self)

    def intersects_hyperplane(self, hplane: Hyperplane,
                              thresh: float = COMMON_ZERO_TOLERANCE)\
                        -> Tuple[int, Union[None, Point, Segment]]:
        r"""Check if the current segment intersects `hplane`.

        In :math:`\mathbb{R}^n`, a hyperplane has codimension :math:`1` and a
        segment has codimension :math:`n-1`, as it is just a restricted line.
        The codimension of their intersection for :math:`n \ge 2` is therefore
        an integer in the range
        :math:`[\operatorname{max}(1, n-1), (n-1)+1] = [n-1, n]`, which
        corresponds to either a segment or a point.

        :param hplane: A hyperplane to check for intersection with the current
                segment.
        :param thresh: Threshold to determine if the segment and `plane` are
                parallel.

        Returns
        -------
        n:
            Number of points of intersection.
        intersection:
            :const:`None` if `n` is zero, :class:`Point` if `n` = 1,\
            :class:`Segment` if `n` = :const:`numpy.inf`.
        """
        n_int_hplane, intersection_hplane =\
                self.associated_line.intersects_hyperplane(hplane, thresh)

        if n_int_hplane == 0:
            return (0, None)

        if n_int_hplane == 1:
            assert isinstance(intersection_hplane, Point)
            if self.contains_point(intersection_hplane, thresh):
                return (1, intersection_hplane)
            return (0, None)

        return (inf, self)

    def intersects_contour(self, contour: Contour,
                           anchor: Optional[Point] = None,
                           thresh: float = COMMON_ZERO_TOLERANCE)\
            -> Tuple[int,
                     List[Tuple[Point, RealScalar]],
                     List[Tuple[Segment, RealScalar, RealScalar]],
                     List[Tuple[Segment, RealScalar, RealScalar]]]:
        r"""Check if the current segment intersects `contour` and find
        J-points and J-segments as defined in :cite:`article:Hsu1991`.

        :param contour: A contour to check for intersection with the current
                segment.
        :param anchor: A point corresponding to one of the two endpoints of
                the current segment. All J-points will be given a fraction
                relative to this point. If :const:`None`, the first endpoint
                if the current fragment will be used.
        :param thresh: Threshold to determine if the segment and `contour`
                are parallel.

        Returns
        -------
        n : int
            Number of intersection points.

            If ``0 <= n < inf``, `J_points` contains the points of
            intersection, `J_segments_inside` is empty while
            `J_segments_outside` contains sub-segments of the current segment
            demarcated by the points in `J_points`.

            If ``n == inf``, `J_points` contains the points of intersection
            between the current segment and the edges of `contour`,
            `J_segments_inside` and `J_segments_outside` contain the fractions
            of the current segment inside and outside of `contour`,
            respectively.
        J_points : List[Tuple[Point, RealScalar]]
            List of J-points and their associated fraction values.
        J_segments_inside : List[Tuple[Segment, RealScalar, RealScalar]]
            List of segments inside `contour` and the associated start and
            end fraction values.
        J_segments_outside : List[Tuple[Segment, RealScalar, RealScalar]]
            List of segments outside `contour` and the associated start and
            end fraction values.
        """
        # pylint: disable=C0103,R0912,R0914,R0915
        # C0103: J_segments are so named based on the cited paper.
        # R0912, R0914, R0915: This is a complex method. It is not
        #                      straightforward to break it down without losing
        #                      its coherence.
        if anchor is None:
            anchor = self.endpoints[0]
        assert anchor.is_same_as(self.endpoints[0]) or\
                anchor.is_same_as(self.endpoints[1])

        if not self.intersects_bounding_box(contour, thresh):
            return (0, [], [], [(self, 0, 1)])

        n_int_plane, intersection_plane =\
            self.associated_line.intersects(contour.associated_plane, thresh)
        # The line containing the segment is either coplanar with contour or
        # intersects the plane of contour at one point.
        assert n_int_plane != 0
        if n_int_plane == 1:
            # Segment and contour are not coplanar.
            assert isinstance(intersection_plane, Point)
            if not (self.contains_point(intersection_plane, thresh) and
                    contour.contains_point(intersection_plane, thresh)):
                return (0, [], [], [(self, 0, 1)])
            frac = self.find_fraction(anchor, intersection_plane)
            J_points = [(intersection_plane, frac)]

        else:
            # Segment and contour are coplanar.
            assert isinstance(intersection_plane, AffineSubspace)
            assert intersection_plane.affdim == 1
            intersection_plane = Line.from_affinesubspace(intersection_plane)
            edge_intersection_points = []
            for edge in contour.edges:
                n_int_seg, J_point = self.intersects_segment(edge, thresh)
                if n_int_seg == 1:
                    # J_point is the unique point of intersection.
                    assert isinstance(J_point, Point)
                    frac = self.find_fraction(anchor, J_point, thresh)
                    edge_intersection_points.append((J_point, frac))
                # We don't need to consider the case when n = inf:
                # this means the current segment contains an entire edge and
                # must intersect the two adjacent edges at one point each.

            eip = sorted(edge_intersection_points, key=lambda t: t[1])
            if len(eip) > 1:
                # Removing duplicate eips.
                J_points = [eip[0]]
                for i, point_tup in enumerate(eip[1:]):
                    if point_tup[0].is_same_as(J_points[-1][0], thresh):
                        continue
                    J_points.append(point_tup)
            else:
                J_points = eip

        endpoints = []
        for endpoint in self.endpoints:
            if endpoint.is_same_as(anchor, thresh):
                endpoints.append((endpoint, 0.0))
            else:
                endpoints.append((endpoint, 1.0))
        endpoints = sorted(endpoints, key=lambda t: t[1])

        finite_segments = []
        J_segments_inside = []
        J_segments_outside = []
        if len(J_points) > 0 and\
           not endpoints[0][0].is_same_as(J_points[0][0]):
            finite_segments.append((Segment([endpoints[0][0], J_points[0][0]]),
                                    endpoints[0][1], J_points[0][1]))

        if len(J_points) >= 2:
            for i, _ in enumerate(J_points[:-1]):
                finite_segments.append((Segment([J_points[i][0],
                                                 J_points[i+1][0]]),
                                        J_points[i][1], J_points[i+1][1]))

        if len(J_points) > 0 and\
           not endpoints[-1][0].is_same_as(J_points[-1][0]):
            finite_segments.append((Segment([endpoints[-1][0], J_points[-1][0]]),
                                    J_points[-1][1], endpoints[-1][1]))

        if len(finite_segments) == 0:
            finite_segments.append((Segment([endpoints[0][0],
                                             endpoints[-1][0]]),
                                    endpoints[0][1], endpoints[-1][1]))

        for current_segment in finite_segments:
            if contour.contains_point(current_segment[0].midpoint):
                J_segments_inside.append(current_segment)
            else:
                J_segments_outside.append(current_segment)

        if len(J_segments_inside) > 0:
            return (inf, J_points, J_segments_inside, J_segments_outside)
        return (len(J_points), J_points, J_segments_inside, J_segments_outside)

    def intersects_face(self, face: Face, anchor: Optional[Point] = None,
                        thresh: float = COMMON_ZERO_TOLERANCE)\
            -> Tuple[int,
                     List[Tuple[Point, RealScalar]],
                     List[Tuple[Segment, RealScalar, RealScalar]],
                     List[Tuple[Segment, RealScalar, RealScalar]]]:
        r"""Check if the current segment intersects `face` and find
        J-points and J-segments as defined in :cite:`article:Hsu1991`.

        :param face: A face to check for intersection with the current
                segment.
        :param anchor: A point corresponding to one of the two endpoints of
                the current segment. All J-points will be given a fraction
                relative to this point. If :const:`None`, the first endpoint
                if the current fragment will be used.
        :param thresh: Threshold to determine if the segment and `face`
                are parallel.

        Returns
        -------
        n : int
            Number of intersection points.

            If ``n == 0``, `J_points`, `J_segments_inside`, and
            `J_segments_inside` are all empty.

            If ``1 <= n < inf``, `J_points` contains the points of
            intersection, `J_segments_inside` is empty while
            `J_segments_outside` contains sub-segments of the current segment
            demarcated by the points in `J_points`.

            If ``n == inf``, `J_points` contains the points of intersection
            between the current segment and the edges of `face`,
            `J_segments_inside` and `J_segments_outside` contain the fractions
            of the current segment inside and outside of `face`,
            respectively.
        J_points : List[Tuple[Point, RealScalar]]
            List of J-points and their associated fraction values.
        J_segments_inside : List[Tuple[Segment, RealScalar, RealScalar]]
            List of segments inside `face` and the associated start and
            end fraction values.
        J_segments_outside : List[Tuple[Segment, RealScalar, RealScalar]]
            List of segments outside `face` and the associated start and
            end fraction values.
        """
        # pylint: disable=C0103,R0912,R0914
        # C0103: J_segments are so named based on the cited paper.
        # R0912, R0914: This is a complex method. It is not straightforward
        #               to break it down without losing its coherence.
        if anchor is None:
            anchor = self.endpoints[0]
        assert anchor.is_same_as(self.endpoints[0]) or\
                anchor.is_same_as(self.endpoints[1])

        if not self.intersects_bounding_box(face, thresh):
            return (0, [], [], [(self, 0, 1)])

        n_int_plane, intersection_plane =\
            self.associated_line.intersects(face.associated_plane, thresh)
        # The line containing the segment is either coplanar with face or
        # intersects the plane of face at one point.
        assert n_int_plane != 0
        if n_int_plane == 1:
            # Segment and face are not coplanar.
            assert isinstance(intersection_plane, Point)
            if not (self.contains_point(intersection_plane, thresh) and
                    face.contains_point(intersection_plane, thresh)):
                return (0, [], [], [(self, 0, 1)])
            frac = self.find_fraction(anchor, intersection_plane)
            J_points = [(intersection_plane, frac)]

        else:
            # Segment and contour are coplanar.
            # ceips: contour_edge_intersection_points
            ceips = []
            for contour in face.contours:
                int_contour = self.intersects_contour(contour, anchor, thresh)
                ceips.extend(int_contour[1])
            ceips.sort(key=lambda t: t[1])

            if len(ceips) > 1:
                # Removing duplicate ceips.
                J_points = [ceips[0]]
                for i, point_tup in enumerate(ceips[1:]):
                    if point_tup[0].is_same_as(J_points[-1][0], thresh):
                        continue
                    J_points.append(point_tup)
            else:
                J_points = ceips

        endpoints = []
        for endpoint in self.endpoints:
            if endpoint.is_same_as(anchor, thresh):
                endpoints.append((endpoint, 0.0))
            else:
                endpoints.append((endpoint, 1.0))
        endpoints.sort(key=lambda t: t[1])

        finite_segments = []
        J_segments_inside = []
        J_segments_outside = []
        if len(J_points) > 0 and\
           not endpoints[0][0].is_same_as(J_points[0][0]):
            finite_segments.append((Segment([endpoints[0][0], J_points[0][0]]),
                                    endpoints[0][1], J_points[0][1]))

        if len(J_points) >= 2:
            for i, _ in enumerate(J_points[:-1]):
                finite_segments.append((Segment([J_points[i][0],
                                                 J_points[i+1][0]]),
                                        J_points[i][1], J_points[i+1][1]))

        if len(J_points) > 0 and\
           not endpoints[-1][0].is_same_as(J_points[-1][0]):
            finite_segments.append((Segment([endpoints[-1][0], J_points[-1][0]]),
                                    endpoints[-1][1], J_points[-1][1]))

        if len(finite_segments) == 0:
            finite_segments.append((Segment([endpoints[0][0],
                                             endpoints[-1][0]]),
                                    endpoints[0][1], endpoints[-1][1]))

        for current_segment in finite_segments:
            if face.contains_point(current_segment[0].midpoint):
                J_segments_inside.append(current_segment)
            else:
                J_segments_outside.append(current_segment)

        if len(J_segments_inside) > 0:
            return (inf, J_points, J_segments_inside, J_segments_outside)
        return (len(J_points), J_points, J_segments_inside, J_segments_outside)

    def get_cabinet_projection(self, alpha: float = np.arctan(2)) -> Segment:
        r"""The cabinet projection of the current segment onto the
        :math:`xy`-plane.

        This is only defined if the current segment is in :math:`\mathbb{R}^3`.

        For a description of the cabinet projection, see
        :meth:`~.geometrical_space.Point.get_cabinet_projection`.

        :param alpha: Angle :math:`\alpha` of projection (radians).
                :math:`0 \leq \alpha < 2\pi`.

        :returns: The projected segment as a :class:`Segment`.
        """
        return Segment([point.get_cabinet_projection(alpha)
                        for point in self.endpoints])


class Contour(BoundedAffineSubspace[Segment]):
    r"""A class describing a contour in :math:`\mathbb{R}^n`, *i.e.*, an
    :math:`n`-dimensional coordinate space over :math:`\mathbb{R}`.

    "A contour is a closed planar polygon that may be one of ordered
    orientation." (:cite:`article:Hsu1991`)

    A contour is therefore a closed two-dimensional affine subspace whose
    boundary is a collection of vertex-sharing segments.

    Two contours are equal if and only if their edges are equal.
    """

    def __init__(self, edges: Sequence[Segment]) -> None:
        r"""
        :param edges: A sequence of endpoint-sharing segments defining the edges
                of the contour. Consecutive edges must share an endpoint.
                Any ordered orientation is implied by the order of the sequence.
        """
        for i, _ in enumerate(edges):
            assert edges[i-1].shares_connecting_endpoint(edges[i])[0],\
                    "Consecutive edges must share a connecting vertex."
        super().__init__(edges)
        assert self.affdim == 2,\
            f"Affine dimension of {self.affdim} detected. "\
            + "A contour can only exist in a two-dimensional affine subspace."
        self._find_bounding_box()

    @property
    def edges(self) -> List[Segment]:
        """A list of segments defining the edges of the contour.
        """
        return self.boundaries

    @property
    def vertices(self) -> List[Point]:
        """A list of all unique vertices in this contour.
        """
        edges = self.edges
        unique_vertices = [edges[i-1].shares_connecting_endpoint(\
                                edges[i], COMMON_ZERO_TOLERANCE)[1]\
                           for i in range(len(edges))]
        assert all(vertex is not None for vertex in unique_vertices)
        return unique_vertices  # type: ignore

    @property
    def associated_plane(self) -> Union[AffineSubspace, Hyperplane]:
        """The plane (two-dimensional hyperplane) containing this contour.

        This property has no setter and can only be set when :attr:`edges` is
        set.
        """
        assoc_aff = self.associated_affinesubspace
        if self.dim == 3 and not isinstance(assoc_aff, Hyperplane):
            return Hyperplane.from_affinesubspace(assoc_aff)
        return assoc_aff

    @classmethod
    def from_vertices(cls, vertices: Sequence[Point]) -> Contour:
        r"""Create a contour whose edges are constructed from consecutive
        vertices.

        :param vertices: Sequence of points that form the vertices of the
                contour.

        :returns: The contour where each consecutive pair of vertices in
                `vertices` forms an edge.
        """
        edges = [Segment([vertices[i%len(vertices)],
                          vertices[(i+1)%len(vertices)]])
                 for i in range(len(vertices))]
        return cls(edges)

    @classmethod
    def convex_hull_from_vertices(cls, vertices: Sequence[Point]) -> Contour:
        r"""Construct a two-dimensional contour as a convex hull from a
        sequence of vertices in :math:`\mathbb{R}^2`.

        :param vertices: A list of all non-repeating vertices in
                :math:`\mathbb{R}^2`.

        :returns: A contour as the convex hull of `vertices`.
        """
        assert all(vertex.dim == 2 for vertex in vertices)
        assert len(vertices) >= 3,\
                "At least three vertices are needed to form a " +\
                "two-dimensional convex contour."
        # qconvex requires each row to contain the coordinates of a vertex.
        coords = np.hstack([np.reshape(vertex.coordinates, (-1, 1))
                            for vertex in vertices]).T
        hull = qconvex("Fx", coords)
        hull_vertices = [vertices[int(vertex_str)] for vertex_str in hull[1:]]
        return Contour.from_vertices(hull_vertices)


    def get_front_normal(self, viewing_vector: Vector,
                         thresh: float = COMMON_ZERO_TOLERANCE)\
                                -> Optional[Vector]:
        r"""The "front-face" normal relative to `viewing_vector`. This is only
        well defined if the associated plane is also a hyperplane in its
        Euclidean space.

        The definition of "front-face" normal is given in
        :meth:`Hyperplane.get_front_normal`.

        :param viewing_vector: The viewing vector corresponding to a certain
                parallel projection.
        :param thresh: Threshold to determine if the contour is parallel to the
                viewing vector.

        :returns: The "front-face" normal relative to `viewing_vector` if the
                associated plane is a hyperplane, :const:`None` otherwise.
        """
        if not self.affdim == self.dim-1:
            return None
        # dim must be 3, and plane must be a Hyperplane in three dimensions.
        plane = self.associated_plane
        assert isinstance(plane, Hyperplane)
        return plane.get_front_normal(viewing_vector, thresh)

    def __str__(self) -> str:
        vertex_str = "\n    ".join([ver.__str__() for ver in self.vertices])
        return f"Contour[\n    {vertex_str}\n]"

    def __repr__(self) -> str:
        return "\n" + self.__str__()

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Contour):
            return NotImplemented
        return self.is_same_as(other, COMMON_ZERO_TOLERANCE)

    def __ne__(self, other: object) -> bool:
        return not self.__eq__(other)

    def __lt__(self, other: Contour) -> bool:
        self_edges = tuple(sorted(self.edges))
        other_edges = tuple(sorted(other.edges))
        if len(self_edges) != len(other_edges):
            return len(self_edges) < len(other_edges)
        return self_edges < other_edges

    def __gt__(self, other: Contour) -> bool:
        self_edges = tuple(sorted(self.edges))
        other_edges = tuple(sorted(other.edges))
        if len(self_edges) != len(other_edges):
            return len(self_edges) > len(other_edges)
        return self_edges > other_edges

    def __le__(self, other: Contour) -> bool:
        return self < other or self == other

    def __ge__(self, other: Contour) -> bool:
        return self > other or self == other

    def is_same_as(self, other: Contour,
                   thresh: float = COMMON_ZERO_TOLERANCE) -> bool:
        r"""Check if the current contour is the same contour as `other`.

        :param other: A contour to check for identicality with the current
                contour.
        :param thresh: Threshold to determine if two edges are identical.

        :returns: :const:`True` if `other` is identical to the current contour,
                :const:`False` if not.
        """
        if len(self.edges) != len(other.edges):
            return False
        self_edges_sorted = sorted(self.edges)
        other_edges_sorted = sorted(other.edges)
        return all(
            self_edges_sorted[i].is_same_as(other_edges_sorted[i], thresh)
            for i in range(len(self_edges_sorted))
        )

    def get_cabinet_projection(self, alpha: float = np.arctan(2)) -> Contour:
        r"""The cabinet projection of the current contour onto the
        :math:`xy`-plane.

        This is only defined if the current contour is in :math:`\mathbb{R}^3`.

        For a description of the cabinet projection, see
        :meth:`~.geometrical_space.Point.get_cabinet_projection`.

        :param alpha: Angle :math:`\alpha` of projection (radians).
                :math:`0 \leq \alpha < 2\pi`.

        :returns: The projected contour as a :class:`Contour`.
        """
        return Contour([edge.get_cabinet_projection(alpha)
                        for edge in self.edges])

    def is_coplanar(self, other: Contour,
                    thresh: float = COMMON_ZERO_TOLERANCE) -> bool:
        r"""Check if the current contour is coplanar with `other`.

        :param other: A contour to check for coplanarity with the current
                contour.
        :param thresh: Threshold for checking parallelism and coincidence
                between the two associated planes.

        :returns: :const:`True` if `other` is coplanar with the current contour,
                  :const:`False` if not.
        """
        assert self.dim == other.dim,\
                "Contours in different Euclidean spaces cannot be compared."
        self_plane = self.associated_plane
        other_plane = other.associated_plane
        if not self_plane.is_parallel(other_plane, thresh):
            return False
        if not self_plane.contains_point(other_plane.anchor, thresh):
            return False
        return True

    def contains_point(self, point: Point,
                       thresh: float = COMMON_ZERO_TOLERANCE) -> bool:
        """Check if `point` lies inside the current contour.

        This implementation is based on the winding number algorithm described
        `here <http://geomalgorithms.com/a03-_inclusion.html>`_.

        :param point: A point in the same Euclidean space as the current
                contour.
        :param thresh: Threshold for various comparisons.

        :returns: :const:`True` if `point` lies inside the current contour,
                  :const:`False` if not.
        """
        if not self.associated_plane.contains_point(point, thresh):
            return False
        if not self.intersects_bounding_box(point, thresh):
            return False
        if any(edge.contains_point(point, thresh) for edge in self.edges):
            return True

        # `point` and self are now coplanar (so live entirely in a
        # two-dimensional affine subspace, a.k.a. plane). `point` does not lie
        # on any edges either. In the basis of the orthogonal direction vectors
        # spanning this affine subspace, the components of all coplanar vectors
        # form R^2. We can therefore make use of plain old cross products.

        # Construct an orthonormal basis spanning this plane.
        basis = orth(self.associated_plane.directionmatrix)
        origin = self.associated_plane.anchor
        xvec = Vector(basis[:, 0].tolist()).normalise()
        yvec = Vector(basis[:, 1].tolist()).normalise()
        assert abs(xvec.dot(yvec)) < COMMON_ZERO_TOLERANCE

        winding_number = 0
        for edge in self.edges:
            wdn = _winding_number(edge, point, origin, (xvec, yvec), thresh)
            winding_number += wdn

        return winding_number != 0


class Face(BoundedAffineSubspace[Segment]):
    r"""A class describing a face in :math:`\mathbb{R}^n`, *i.e.*, an
    :math:`n`-dimensional coordinate space over :math:`\mathbb{R}`.

    "A face is specified by one or more contours that are coplanar."
    (:cite:`article:Hsu1991`)

    A face is therefore a disconnected two-dimensional affine subspace whose
    boundaries are formed from those of the composite contours. The boundaries
    of a face are therefore edges, but they are grouped together into contours.
    """
    def __init__(self, contours: Sequence[Contour]) -> None:
        """
        :param countours: A sequence of contours defining the face.
        """
        assert all(isinstance(contour, Contour) for contour in contours)
        self.contours = contours  # type: ignore
        edges = list(chain.from_iterable([contour.edges
                                          for contour in contours]))
        super().__init__(edges)
        self._find_bounding_box()

    @property
    def contours(self) -> List[Contour]:
        """A list of contours defining the face.
        """
        return self._contours

    @contours.setter
    def contours(self, contours: Sequence[Contour]) -> None:
        if len(contours) > 1:
            for contour in contours[1:]:
                assert contours[0].is_coplanar(contour),\
                    "All contours must be coplanar."
        self._contours = list(contours)

    @property
    def associated_plane(self) -> Union[AffineSubspace, Hyperplane]:
        """The plane (two-dimensional hyperplane) containing this face.

        This property has no setter and can only be set when :attr:`contours` is
        set.
        """
        return self.contours[0].associated_plane

    def __str__(self) -> str:
        face_str = "\n    ".join([contour.__str__().replace("\n", "\n    ")
                                  for contour in self.contours])
        return f"Face[\n    {face_str}\n]"

    def __repr__(self) -> str:
        return "\n" + self.__str__()

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Face):
            return NotImplemented
        return self.is_same_as(other, COMMON_ZERO_TOLERANCE)

    def __ne__(self, other: object) -> bool:
        return not self.__eq__(other)

    def __lt__(self, other: Face) -> bool:
        self_contours = tuple(sorted(self.contours))
        other_contours = tuple(sorted(other.contours))
        if len(self_contours) != len(other_contours):
            return len(self_contours) < len(other_contours)
        return self_contours < other_contours

    def __gt__(self, other: Face) -> bool:
        self_contours = tuple(sorted(self.contours))
        other_contours = tuple(sorted(other.contours))
        if len(self_contours) != len(other_contours):
            return len(self_contours) > len(other_contours)
        return self_contours > other_contours

    def __le__(self, other: Face) -> bool:
        return self < other or self == other

    def __ge__(self, other: Face) -> bool:
        return self > other or self == other

    def is_same_as(self, other: Face,
                   thresh: float = COMMON_ZERO_TOLERANCE) -> bool:
        r"""Check if the current face is the same face as `other`.

        As a face is a collection of contours, two faces are compared via
        the comparison of their composite contours that have been sorted.

        :param other: A face to check for identicality with the current
                face.
        :param thresh: Threshold to determine if two contours are identical.

        :returns: :const:`True` if `other` is identical to the current face,
                :const:`False` if not.
        """
        if len(self.contours) != len(other.contours):
            return False
        self_contours_sorted = sorted(self.contours)
        other_contours_sorted = sorted(other.contours)
        return all(
            self_contours_sorted[i].is_same_as(other_contours_sorted[i], thresh)
            for i in range(len(self_contours_sorted))
        )

    def contains_point(self, point: Point,
                       thresh: float = COMMON_ZERO_TOLERANCE) -> bool:
        """Check if the current face contains `point`.

        If the current face consists of multiple contours, then its inside
        is defined by the XOR operation (:cite:`article:Hsu1991`).

        :param point: A point in the same Euclidean space as the current
                face.
        :param thresh: Threshold for various comparisons.

        :returns: :const:`True` if `point` lies inside the current face,
                :const:`False` if not.
        """
        n_inside_contours = 0
        for contour in self.contours:
            if contour.contains_point(point, thresh):
                n_inside_contours += 1
        if n_inside_contours % 2 == 0:
            return False
        return True

    def get_front_normal(self, viewing_vector: Vector,
                         thresh: float = COMMON_ZERO_TOLERANCE)\
                                -> Optional[Vector]:
        r"""The "front-face" normal relative to `viewing_vector`. This is only
        well defined if the associated plane is also a hyperplane in its
        Euclidean space.

        The definition of "front-face" normal is given in
        :meth:`Hyperplane.get_front_normal`.

        :param viewing_vector: The viewing vector corresponding to a certain
                parallel projection.
        :param thresh: Threshold to determine if the face is parallel to the
                viewing vector.

        :returns: The "front-face" normal relative to `viewing_vector` if the
                associated plane is a hyperplane, :const:`None` otherwise.
        """
        return self.contours[0].get_front_normal(viewing_vector, thresh)

    def get_cabinet_projection(self, alpha: float = np.arctan(2)) -> Face:
        r"""The cabinet projection of the current face onto the
        :math:`xy`-plane.

        This is only defined if the current face is in :math:`\mathbb{R}^3`.

        For a description of the cabinet projection, see
        :meth:`~.geometrical_space.Point.get_cabinet_projection`.

        :param alpha: Angle :math:`\alpha` of projection (radians).
                :math:`0 \leq \alpha < 2\pi`.

        :returns: The projected face as a :class:`Face`.
        """
        return Face([contour.get_cabinet_projection(alpha)
                     for contour in self.contours])

    # override
    def translate_ip(self, vec: Vector,
                     translated: Optional[List[int]] = None) -> None:
        r"""Translate all points in the current bounded affine subspace by a
        displacement `vec`.

        The translation occurs in-place.

        :param vec: The displacement vector of the translation.
        :param translated: A list of the identifiers of objects that have
                already been translated in the current, potentially recursive,
                call to :meth:`translate_ip`.
        """
        # Contours are not boundaries of faces. Therefore they need to be reset
        # separately.
        super().translate_ip(vec, translated)
        for contour in self.contours:
            # Reset all contours
            contour.boundaries = contour.boundaries

    # override
    def rotate_ip(self, angle: float, axis: Vector,
                  rotated: Optional[List[int]] = None) -> None:
        r"""Rotate all points in the current bounded affine subspace through
        `angle` about `axis` about the origin.

        The rotation occurs **in-place**.
        At the moment, only rotations in :math:`\mathbb{R}^3` are implemented.

        :param angle: The angle of rotation (radians).
        :param axis: A vector indicating the rotation axis. This vector will be
                normalised.
        :param rotated: A list of the identifiers of objects that have already
                been rotated in the current, potentially recursive, call to
                :meth:`rotated_ip`.
        """
        # Contours are not boundaries of faces. Therefore they need to be reset
        # separately.
        super().rotate_ip(angle, axis, rotated)
        for contour in self.contours:
            # Reset all contours
            contour.boundaries = contour.boundaries


class Polyhedron(BoundedAffineSubspace[Face]):
    r"""A class describing a polyhedron in :math:`\mathbb{R}^n`, *i.e.*, an
    :math:`n`-dimensional coordinate space over :math:`\mathbb{R}`.

    A polyhedron is a bounded three-dimensional affine subspace whose boundaries
    are :class:`Face` objects.
    """

    def __init__(self, faces: Sequence[Face]) -> None:
        """
        :param faces: A sequence of faces forming the boundary of the
                polyhedron to be constructed.
        """
        assert all(isinstance(face, Face) for face in faces)
        super().__init__(faces)
        assert self.affdim == 3,\
            f"Affine dimension of {self.affdim} detected. "\
            + "A polyhedron can only exist in a three-dimensional "\
            + "affine subspace."
        self._find_bounding_box()

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Polyhedron):
            return NotImplemented
        return self.is_same_as(other, COMMON_ZERO_TOLERANCE)

    def __ne__(self, other: object) -> bool:
        return not self.__eq__(other)

    def __lt__(self, other: Polyhedron) -> bool:
        self_faces = tuple(sorted(self.faces))
        other_faces = tuple(sorted(other.faces))
        if len(self_faces) != len(other_faces):
            return len(self_faces) < len(other_faces)
        return self_faces < other_faces

    def __gt__(self, other: Polyhedron) -> bool:
        self_faces = tuple(sorted(self.faces))
        other_faces = tuple(sorted(other.faces))
        if len(self_faces) != len(other_faces):
            return len(self_faces) > len(other_faces)
        return self_faces > other_faces

    def __le__(self, other: Polyhedron) -> bool:
        return self < other or self == other

    def __ge__(self, other: Polyhedron) -> bool:
        return self > other or self == other

    def is_same_as(self, other: Polyhedron,
                   thresh: float = COMMON_ZERO_TOLERANCE) -> bool:
        r"""Check if the current polyhedron is the same polyhedron as `other`.

        As a polyhedron is a collection of faces, two polyhedrons are
        compared via the comparison of their composite faces that have been
        sorted.

        :param other: A polyhedron to check for identicality with the current
                polyhedron.
        :param thresh: Threshold to determine if two faces are identical.

        :returns: :const:`True` if `other` is identical to the current
                polyhedron, :const:`False` if not.
        """
        if len(self.faces) != len(other.faces):
            return False
        self_faces_sorted = sorted(self.faces)
        other_faces_sorted = sorted(other.faces)
        return all(
            self_faces_sorted[i].is_same_as(other_faces_sorted[i], thresh)
            for i in range(len(self_faces_sorted))
        )

    @property
    def faces(self) -> List[Face]:
        """A list of the faces forming the boundary of the polyhedron.
        """
        return self.boundaries

    @property
    def edges(self) -> List[Segment]:
        """A list of unique edges in the polyhedron.
        """
        edges = list(chain.from_iterable([contour.edges
                                          for face in self.faces
                                          for contour in face.contours]))
        edges.sort()
        edges_unique = [edges[0]]
        for edge in edges[1:]:
            if edge == edges_unique[-1]:
                continue
            edges_unique.append(edge)
        return edges_unique

    def __str__(self) -> str:
        face_str = "\n    ".join([face.__str__().replace("\n", "\n    ")
                                  for face in self.faces])
        return f"Polyhedron[\n    {face_str}\n]"

    def __repr__(self) -> str:
        return "\n" + self.__str__()

    @classmethod
    def convex_hull_from_vertices(cls, vertices: Sequence[Point]) -> Polyhedron:
        r"""Construct a three-dimensional polyhedron as a convex hull from a
        sequence of vertices in :math:`\mathbb{R}^3`.

        :param vertices: A list of all non-repeating vertices in
                :math:`\mathbb{R}^3`.

        :returns: A polyhedron whose faces are the non-simplicial faces of the
                convex hull of `vertices`.
        """
        assert all(vertex.dim == 3 for vertex in vertices)
        assert len(vertices) >= 4,\
                "At least four vertices are needed to form a " +\
                "three-dimensional polyhedron."
        # qconvex requires each row to contain the coordinates of a vertex.
        coords = np.hstack([np.reshape(vertex.coordinates, (-1, 1))
                            for vertex in vertices]).T
        hull = qconvex("i", coords)
        faces = []
        for face_vertices_str in hull[1:]:
            face_vertices_idx = [int(x) for x in face_vertices_str.split(' ')]
            face_vertices = [vertices[i] for i in face_vertices_idx]
            face = Face([Contour.from_vertices(face_vertices)])
            faces.append(face)
        return cls(faces)

    def get_cabinet_projection(self, alpha: float = np.arctan(2)) -> Face:
        r"""The cabinet projection of the current polyhedron onto the
        :math:`xy`-plane.

        This is only defined if the current polyhedron is in
        :math:`\mathbb{R}^3`. The two-dimensional projection of a
        three-dimensional polyhedron is described by a :class:`Face`
        consisting of multiple contours.

        For a description of the cabinet projection, see
        :meth:`~.geometrical_space.Point.get_cabinet_projection`.

        :param alpha: Angle :math:`\alpha` of projection (radians).
                :math:`0 \leq \alpha < 2\pi`.

        :returns: The projected polyhedron as a :class:`Face`.
        """
        proj_contours = [contour.get_cabinet_projection(alpha)
                         for face in self.faces
                         for contour in face.contours]
        return Face(proj_contours)


class VertexCollection(BoundedAffineSubspace[Segment]):
    r"""A vertex collection is essentially a three-dimensonal polyhedron but
    only with its vertices defined as boundaries. They might not be convex, and
    so no facets or contours are constructed. Edges are segments joining all
    pairs of vertices and are taken to form the boundaries of the object.
    """

    def __init__(self, vertices: Sequence[Point], cutoff: float) -> None:
        """
        :param vertices: A sequence of vertices defining the vertex collection.
        :param cutoff: A cut-off for inter-vertex distances to be
                considered as edges.
        """
        assert all(isinstance(vertex, Point) for vertex in vertices)
        edges = []
        for i in range(len(vertices)-1):
            for j in range(i+1, len(vertices)):
                if vertices[i] == vertices[j]:
                    continue
                edge_ij = Segment([vertices[i], vertices[j]])
                if edge_ij.length <= cutoff:
                    edges.append(edge_ij)
        edges.sort()
        super().__init__(edges)
        assert self.affdim in [2, 3],\
            f"Affine dimension of {self.affdim} detected. "\
            + "A vertex collection, for the time being, can only exist in a "\
            + "two- or three-dimensional affine subspace."
        self._cutoff = cutoff
        self._find_bounding_box()

    @property
    def vertices(self) -> List[Point]:
        """A sorted list of the unique vertices defining this vertex collection.

        .. note::
            This is identical to :attr:`non_adj_vertices`.
        """
        return self.non_adj_vertices

    @property
    def edges(self) -> List[Segment]:
        """A sorted list of segments joining all pairs of vertices.
        """
        return self.boundaries

    @property
    def cutoff(self) -> float:
        """The cutoff threshold for edge formation.

        This property has no setter.
        """
        return self._cutoff

    def __str__(self) -> str:
        vertex_str = "\n    ".join([vertex.__str__().replace("\n", "\n    ")
                                    for vertex in self.vertices])
        return f"VertexCollection[\n    {vertex_str}\n]"

    def __repr__(self) -> str:
        return "\n" + self.__str__()

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, VertexCollection):
            return NotImplemented
        return self.is_same_as(other, COMMON_ZERO_TOLERANCE)

    def __ne__(self, other: object) -> bool:
        return not self.__eq__(other)

    def __lt__(self, other: VertexCollection) -> bool:
        self_vertices = tuple(sorted(self.non_adj_vertices))
        other_vertices = tuple(sorted(other.non_adj_vertices))
        if len(self_vertices) != len(other_vertices):
            return len(self_vertices) < len(other_vertices)
        return self_vertices < other_vertices

    def __gt__(self, other: VertexCollection) -> bool:
        self_vertices = tuple(sorted(self.non_adj_vertices))
        other_vertices = tuple(sorted(other.non_adj_vertices))
        if len(self_vertices) != len(other_vertices):
            return len(self_vertices) > len(other_vertices)
        return self_vertices > other_vertices

    def __le__(self, other: VertexCollection) -> bool:
        return self < other or self == other

    def __ge__(self, other: VertexCollection) -> bool:
        return self > other or self == other

    def is_same_as(self, other: VertexCollection,
                   thresh: float = COMMON_ZERO_TOLERANCE) -> bool:
        r"""Check if the current vertex collection is the same as `other`.

        Two vertex collections are identical if the sorted lists of unique
        vertices are identical.

        :param other: A vertex collection to check for identicality with the
                current vertex collection.
        :param thresh: Threshold to determine if two vertices are identical.

        :returns: :const:`True` if `other` is identical to the current
                vertex collection, :const:`False` if not.
        """
        if len(self.non_adj_vertices) != len(other.non_adj_vertices):
            return False
        self_vertices = self.non_adj_vertices
        other_vertices = other.non_adj_vertices
        return all(
            self_vertices[i].is_same_as(other_vertices[i], thresh)
            for i in range(len(self_vertices))
        )

    def get_cabinet_projection(self, alpha: float = np.arctan(2))\
            -> VertexCollection:
        r"""The cabinet projection of the current segment onto the
        :math:`xy`-plane.

        This is only defined if the current vertex collection is in
        :math:`\mathbb{R}^3`.

        For a description of the cabinet projection, see
        :meth:`~.geometrical_space.Point.get_cabinet_projection`.

        :param alpha: Angle :math:`\alpha` of projection (radians).
                :math:`0 \leq \alpha < 2\pi`.

        :returns: The projected vertex collection as a
                :class:`VertexCollection`.
        """
        return VertexCollection([vertex.get_cabinet_projection(alpha)
                                 for vertex in self.vertices], self._cutoff)
