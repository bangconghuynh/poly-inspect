""":mod:`.analyticaltools_types` contains declarations for standard types used
throughout ``analytical-tools``.
"""

from typing import Union, List, cast
import numpy as np  # type: ignore


# ---------------
# Numerical types
# ---------------
RealScalar = Union[int, float]
Scalar = Union[RealScalar, complex]
inf = cast(int, np.inf)


# --------------------
# Coefficient matrices
# --------------------
# Each element is a coefficient matrix for one spin space.
Coefficients = List[np.ndarray]

# Each element is a list of coefficient matrices for one determinant.
MultiStates = List[Coefficients]
