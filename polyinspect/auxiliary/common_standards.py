""":mod:`.analyticaltools_standards` contains declarations for standard constants
and thresholds used throughout ``analytical-tools``.
"""

import numpy as np  # type: ignore
from .common_types import RealScalar

# Thresholds
LOOSE_ZERO_TOLERANCE = 1e-7
COMMON_ZERO_TOLERANCE = 1e-14
FLOAT_EPS = np.finfo(float).eps  # 2.22e-16
STRICT_ZERO_TOLERANCE = 1e-45


# Difference functions
def diff_rel(a: RealScalar, b: RealScalar) -> RealScalar:
    r"""Evalulate the relative difference between `a` and `b`.

    The relative difference between `a` and `b` is defined as

    .. math::
        \frac{\lvert a - b \rvert}{\lvert a + b \rvert}.
    """
    # pylint: disable=C0103
    return abs(a - b)/abs(a + b)
