""":mod:`.ao_basis_space` contains classes, functions, and types to handle
necessary AO basis set information in relation to symmetry transformation and
analysis.

We define the following types for a compact ``BasisAngularOrder`` data
structure:

    - ``BasisAngularOrder = Sequence[Tuple[str, BasisAtom]]``,
    - ``BasisAtom = Sequence[BasisShell]``,
    - ``BasisShell = Tuple[str, bool, Optional[Union[bool, Sequence[CartTuple]]]]``, and
    - ``CartTuple = Tuple[int, int, int]``,

where

    - ``BasisAngularOrder`` is an ordered sequence of ``BasisAtom`` in the
      order the atoms are defined in the molecule;
    - ``BasisAtom`` is an ordered sequence of ``BasisShell`` for each atom
      in the molecule;
    - ``BasisShell`` has the form ``(angmom, cart, order)``, where `order`
      is a boolean for ``increasingm`` if `cart` is :const:`False`, or
      ``Sequence[CartTuple]`` if `cart` is :const:`True`; and
    - ``CartTuple`` has the form :math:`(l_x, l_y, l_z)` containing the
      exponents of the :math:`x`, :math:`y`, and :math:`z` components
      in the Cartesian angular function.
"""

from __future__ import annotations

from typing import List, Tuple, Iterator, Optional, Union, Sequence
from itertools import chain


# The following types allow for a compact BasisAngularOrder sequence to be
# defined using only Python primitive data types.
# CartTuple: (lx, ly, lz)
CartTuple = Tuple[int, int, int]

# BasisShell: (angmom, cart, order),
#       where order is bool for increasingm if cart is False,
#       or    order is Sequence[CartTuple] if cart is True.
BasisShell = Tuple[str, bool, Optional[Union[bool, Sequence[CartTuple]]]]

# BasisAtom: an ordered sequence of BasisShell for each atom in the molecule.
BasisAtom = Sequence[BasisShell]

# BasisAngularOrder: an ordered sequence of BasisAtom in the order the atoms
#       are defined in the molecule.
BasisAngularOrder = Sequence[Tuple[str, BasisAtom]]


l_symbols = ["S", "P", "D", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O"]

def get_number_angular_basis(angmom: str, cart: bool) -> int:
    r"""Return the number of angular functions that a shell with angular
    momentum `angmom` has in spherical or Cartesian form.

    :param angmom: A single character giving the angular momentum of the shell.
            Composite angular momentum shells such as :math:`\mathrm{SP}` should
            be split into singular angular momentum shells first.
    :cart: :const:`True` for Cartesian angular functions, :const:`False` for
            spherical ones.

    :returns: The number of angular functions in this shell.
    """
    # pylint: disable=C0103
    # C0103: l is so named to match its mathematical symbol.
    assert angmom in l_symbols
    l = l_symbols.index(angmom)
    if cart:
        return int((l + 1) * (l + 2)/2)
    return 2*l + 1


def get_shell_indices(bao: BasisAngularOrder)\
        -> List[Tuple[int, int, int, bool, Union[bool, Sequence[Tuple[int, int, int]], None]]]:
    r"""Return a list of tuples, each of which contains the starting index,
    the ending index, the angular momentum :math:`l`, a flag indicating if
    spherical or Cartesian angular functions are being used for each shell in
    the molecule, and information about the ordering of the basis functions in
    the shell.

    :param bao: A basis angular order data structure containing all information
            about the basis set of a certain system.

    :returns: A list of shell indices and angular momenta as described above.
    """
    # pylint: disable=C0103
    # C0103: l is so named to match its mathematical symbol.
    all_shells: Iterator[BasisShell] = chain.from_iterable(list(zip(*bao))[1])
    last_shell_end = -1
    shell_indices: List[
        Tuple[int, int, int, bool, Union[bool, Sequence[Tuple[int, int, int]], None]]
    ] = []
    for shell in all_shells:
        (angmom, cart, order) = shell
        l = l_symbols.index(angmom)
        cur_shell_start = last_shell_end + 1
        cur_shell_end = last_shell_end + get_number_angular_basis(angmom, cart)
        last_shell_end = cur_shell_end
        shell_indices.append((cur_shell_start, cur_shell_end, l, cart, order))
    return shell_indices


def get_atom_basis_indices(bao: BasisAngularOrder)\
        -> List[Tuple[int, int, str]]:
    r"""Return a list of tuples, each of which contains the starting AO basis
    index, the ending AO basis index, and the chemical symbol for each atom in
    the molecule.

    :param bao: A basis angular order data structure containing all information
            about the angular functions in the basis set of a certain system.

    :returns: A list of shell indices and angular momenta as described above.
    """
    last_atom_end = -1
    atom_basis_indices: List[Tuple[int, int, str]] = []
    for symbol, basis_atom in bao:
        cur_atom_start = last_atom_end + 1
        count_shells = sum(get_number_angular_basis(angmom, cart)
                           for angmom, cart, _ in basis_atom)
        cur_atom_end = last_atom_end + count_shells
        last_atom_end = cur_atom_end
        atom_basis_indices.append((cur_atom_start, cur_atom_end, symbol))
    return atom_basis_indices
