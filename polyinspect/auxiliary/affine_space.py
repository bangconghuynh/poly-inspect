""":mod:`.affinespace` contains classes for general real affine subspaces of a
Euclidean space.
"""

from __future__ import annotations

from typing import Optional, Sequence, List, Tuple, Union
import numpy as np  # type: ignore
from scipy.linalg import null_space  # type: ignore

from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE
from polyinspect.auxiliary.common_types import RealScalar, inf
from polyinspect.auxiliary.geometrical_space import Point, Vector


class AffineSubspace():
    r"""A class describing an affine subspace of dimension :math:`K` in
    :math:`\mathbb{R}^n`.

    Mathematically, an affine subspace :math:`A` is a collection of all points
    whose position vectors satisfy the vector equation

    .. math:: \boldsymbol{r} =
            \boldsymbol{r}_0 + \sum_{i=1}^{K} \lambda_i \boldsymbol{d}_i,

    where :math:`\boldsymbol{d}_i \in L \subset \mathbb{R}^n` are linearly
    independent vectors in a linear subspace :math:`L` of :math:`\mathbb{R}^n`,
    and :math:`\lambda_i \in \mathbb{R}` are the linear combination
    coefficients. :math:`L` is called the *direction* of :math:`A`, and
    :math:`\dim A = \dim L \equiv K`.

    For :math:`K = n-2`, the affine subspace describes the general solution of
    the intersection of two non-parallel hyperplanes embedded in
    :math:`\mathbb{R}^n`. In particular, when :math:`n=3`, :math:`K=1` and the
    affine subspace becomes a line.
    """

    def __init__(self, anchor: Point, direction: Sequence[Vector]) -> None:
        r"""
        :param anchor: A point in the affine subspace corresponding to the
                position vector :math:`\boldsymbol{r}_0`.
        :param direction: A sequence of *linearly independent* vectors
                :math:`\boldsymbol{d}_i \in L` spanning the direction linear
                subspace associated with the affine subspace.

        :raises AssertionError: if `anchor` and `direction` have different
                dimensions.
        :raises ValueError: if the vectors in `direction` exhibit a linear
                dependence w.r.t. the threshold of
                :const:`~.common_standards.COMMON_ZERO_TOLERANCE`.
        """
        dirdim = {dirvec.dim for dirvec in direction}
        dirdim.add(anchor.dim)
        assert len(dirdim) == 1
        self.anchor = anchor
        self.direction = list(direction)

    @property
    def anchor(self) -> Point:
        """A point in the affine subspace.
        """
        return self._anchor

    @anchor.setter
    def anchor(self, point: Point) -> None:
        self._anchor = point

    @property
    def direction(self) -> List[Vector]:
        """The direction of the affine subspace.
        """
        return self._direction

    @direction.setter
    def direction(self, vectors: Sequence[Vector]) -> None:
        dim = vectors[0].dim
        affdim = len(vectors)
        assert affdim <= dim, "Affine dimension cannot exceed dimension."
        dirmat = np.zeros((dim, affdim))
        for i, dirvec in enumerate(vectors):
            dirmat[:, i] = dirvec.components
        assert np.linalg.matrix_rank(dirmat, tol=COMMON_ZERO_TOLERANCE)\
                == affdim, "Direction vectors are not all linearly independent."
        self._direction = []
        for dirvec in vectors:
            self._direction.append(dirvec.normalise().positise())

    @property
    def directionmatrix(self) -> np.ndarray:
        r"""The :math:`n \times K` matrix containing the column vectors
        in :attr:`direction`.

        This property has no setter.
        """
        dirmat = np.zeros((self.dim, self.affdim))
        for i, dirvec in enumerate(self.direction):
            dirmat[:, i] = dirvec.components
        return dirmat

    @property
    def dim(self) -> int:
        r"""The dimensionality :math:`n` of the coordinate space
        :math:`\mathbb{R}^n` in which this affine subspace lives.

        This property has no setter.
        """
        return self.anchor.dim

    @property
    def affdim(self) -> int:
        r"""The affine dimension :math:`K` of this affine subspace.

        This property has no setter.
        """
        return len(self.direction)

    def get_a_point(self) -> Point:
        """Return a point in the affine subspace. The easiest point to return
        is the anchor.

        This method is useful when a point in the affine subspace is required,
        but it does not matter which one.

        :returns: A point in the affine subspace, chosen to be the anchor.
        """
        return self.anchor

    def __str__(self) -> str:
        anchor_str = f"Anchor: {self.anchor}"
        dirs = "\n    ".join([vec.__str__() for vec in self.direction])
        dir_str = f"Direction:\n    {dirs}"
        return f"AffineSubspace[\n  {anchor_str}\n  {dir_str}\n]"

    __repr__ = __str__

    def get_point(self, params: Sequence[RealScalar]) -> Point:
        r"""Obtain a point on the line with position vector
        :math:`\boldsymbol{r}` satisfying

        .. math:: \boldsymbol{r} =
            \boldsymbol{r}_0 + \sum_{i=1}^{K} \lambda_i \boldsymbol{d}_i,

        for a particular set of values of `param` :math:`\lambda_i`.

        :param params: A sequence of scalar parameters :math:`\lambda_i`.

        :returns: The point in the affine subspace satisfying the above
                condition.
        """
        assert len(params) == self.affdim
        lincom = sum(params[i]*self.direction[i] for i in range(self.affdim))
        assert isinstance(lincom, Vector)
        vec = Vector.from_point(self.anchor) + lincom
        return Point.from_vector(vec)

    def is_parallel(self, other: AffineSubspace,
                    thresh: float = COMMON_ZERO_TOLERANCE) -> bool:
        r"""Check if the current affine subspace is parallel to `other`.

        The current affine subspace is parallel to `other` if and only if they
        both have the same associated linear subspace. To check this, we must
        check if the basis vectors for :math:`L` in the current affine subspace
        are fully linearly dependent on those in `other`. If
        :math:`\boldsymbol{M}` is the direction matrix of the current affine
        subspace, :math:`\boldsymbol{M}'` that of `other`, and
        :math:`(\boldsymbol{M} \mid \boldsymbol{M}')` the augmented matrix

        .. math::
            (\boldsymbol{M} \mid \boldsymbol{M}') =
            \begin{bmatrix} \boldsymbol{M} & \boldsymbol{M}' \end{bmatrix},

        the required condition is satisfied when

        .. math::
            \operatorname{rank}\ (\boldsymbol{M} \mid \boldsymbol{M}')
            \leq
            \max(\operatorname{rank}\boldsymbol{M},
                 \operatorname{rank}\boldsymbol{M}').

        :param other: An affine subspace to check for parallelism with the
                current affine subspace.
        :param thresh: Threshold to determine the nullity of a matrix.

        :returns: :const:`True` if the two affine subsspaces are parallel,
            :const:`False` if not.
        """
        mmat = self.directionmatrix
        mdmat = other.directionmatrix
        augmat = np.hstack([mmat, mdmat])
        rankvalues = [np.linalg.matrix_rank(mat, tol=thresh)
                      for mat in [mmat, mdmat, augmat]]
        return rankvalues[2] <= max(rankvalues[0:2])

    def contains_point(self, point: Point,
                       thresh: float = COMMON_ZERO_TOLERANCE) -> bool:
        r"""Check if `point` lies in the current affine subspace.

        Let :math:`boldsymbol{r}_1` be the position vector of `point`. If
        `point` lies in the current affine subspace, then the following
        equation admits a unique set of :math:`\lambda_i` as a solution.

        .. math:: \boldsymbol{r}_1 =
            \boldsymbol{r}_0 + \sum_{i=1}^{K} \lambda_i \boldsymbol{d}_i.

        By Rouché–Capelli theorem, this is fulfilled when

        .. math::
            \operatorname{rank}
                \begin{bmatrix}
                    \boldsymbol{M} & \boldsymbol{r}_1 - \boldsymbol{r}_0
                \end{bmatrix}
            = \operatorname{rank} \boldsymbol{M} = K,

        where :math:`\boldsymbol{M}` is the direction matrix of the current
        affine subspace.

        :param point: A point to check for membership in the current affine
                subspace.
        :param thresh: Threshold to determine the nullity of a matrix.

        :returns: :const:`True` if `point` lies in the current affine subspace,
                :const:`False` if not.
        """
        assert self.dim == point.dim,\
                "The current affine subspace and the given point do not live "\
                + "in the same Euclidean space."
        rmat = np.reshape(point.coordinates-self.anchor.coordinates, (-1, 1))
        mmat = self.directionmatrix
        augmat = np.hstack([mmat, rmat])
        rankvalues = {np.linalg.matrix_rank(mat, tol=thresh)
                      for mat in [mmat, augmat]}
        rankvalues.add(self.affdim)
        return len(rankvalues) == 1

    def find_parameters(self, point: Point,
                        thresh: float = COMMON_ZERO_TOLERANCE)\
                               -> Optional[List[RealScalar]]:
        r"""Determine the parameter of the line equation corresponding
        to `point`.

        Let :math:`\boldsymbol{r}_1` be the position vector of `point`. If
        `point` lies in the current affine subspace, then the following
        equation admits a unique set of :math:`\lambda_i` as a solution.

        .. math:: \boldsymbol{r}_1 =
            \boldsymbol{r}_0 + \sum_{i=1}^{K} \lambda_i \boldsymbol{d}_i.

        By Rouché–Capelli theorem, this is fulfilled when

        .. math::
            \operatorname{rank}
                \begin{bmatrix}
                    \boldsymbol{M} & \boldsymbol{r}_1 - \boldsymbol{r}_0
                \end{bmatrix}
            = \operatorname{rank} \boldsymbol{M} = K,

        where :math:`\boldsymbol{M}` is the direction matrix of the current
        affine subspace. Then, the column vector :math:`\boldsymbol{\Lambda}`
        containing the solution is given by

        .. math::
            \boldsymbol{\Lambda} =
            (\boldsymbol{M}^{\mathsf{T}} \boldsymbol{M})^{-1}
            \boldsymbol{M}^{\mathsf{T}}
            (\boldsymbol{r}_1 - \boldsymbol{r}_0).

        We note that, as :math:`\operatorname{rank} \boldsymbol{M} = K`,
        :math:`\boldsymbol{M}` has linearly independent columns, and so
        :math:`\boldsymbol{M}^{\mathsf{T}} \boldsymbol{M}` is invertible.

        :param point: A point in space.
        :param thresh: Threshold to determine the nullity of a matrix.

        :returns: List of parameters :math:`\lambda_i` corresponding to `point.`
                If `point` does not lie in the current affine subspace,
                :const:`None` will be returned.
        """
        rmat = np.reshape(point.coordinates-self.anchor.coordinates, (-1, 1))
        mmat = self.directionmatrix
        augmat = np.hstack([mmat, rmat])
        rankvalues = {np.linalg.matrix_rank(mat, tol=thresh)
                      for mat in [mmat, augmat]}
        rankvalues.add(self.affdim)
        if len(rankvalues) != 1:
            return None
        tempmat = np.dot(mmat.T, mmat)
        tempmatinv = np.linalg.inv(tempmat)
        lambdas = np.linalg.multi_dot([tempmatinv, mmat.T, rmat])
        return list(lambdas.flat)

    def intersects(self, other: AffineSubspace,
                   thresh: float = COMMON_ZERO_TOLERANCE)\
                           -> Tuple[int, Union[None, Point, AffineSubspace]]:
        r"""Check if the current affine subspace intersects another affine
        subspace `other`.

        At intersection, the following equation must be satisfied:

        .. math::
            \boldsymbol{r}_0
                + \sum_{i=1}^{K} \lambda_i \boldsymbol{d}_i =
            \boldsymbol{r}'_0
                + \sum_{i'=1}^{K'} \lambda'_{i'} \boldsymbol{d}'_{i'},

        which can be cast into a more recognisable form:

        .. math::
            \begin{bmatrix}
                \boldsymbol{d}_1 & \cdots & \boldsymbol{d}_K &
                -\boldsymbol{d}'_1 & \cdots & -\boldsymbol{d}'_{K'}
            \end{bmatrix}
            \begin{bmatrix}
                \lambda_1 \\
                \vdots \\
                \lambda_K \\
                \lambda'_1 \\
                \vdots \\
                \lambda'_{K'}
            \end{bmatrix}
            =
            \begin{bmatrix} \boldsymbol{r}'_0 - \boldsymbol{r}_0 \end{bmatrix}.

        We then identify the coefficient matrix :math:`\boldsymbol{M}` and the
        augmented matrix as

        .. math::
            \begin{align}
                \boldsymbol{M} &=
                    \begin{bmatrix}
                        \boldsymbol{d}_1 & \cdots & \boldsymbol{d}_K &
                        \boldsymbol{d}'_1 & \cdots & \boldsymbol{d}'_{K'}
                    \end{bmatrix},\\
                (\boldsymbol{M} \mid \Delta \boldsymbol{r}_0) &=
                    \begin{bmatrix}
                        \boldsymbol{d}_1 & \cdots & \boldsymbol{d}_K &
                        \boldsymbol{d}'_1 & \cdots & \boldsymbol{d}'_{K'} &
                        \boldsymbol{r}'_0 - \boldsymbol{r}_0
                    \end{bmatrix}.
            \end{align}

        By Rouché–Capelli theorem, there exists a non-null intersection when
        :math:`\operatorname{rank}\boldsymbol{M} =
        \operatorname{rank}\ (\boldsymbol{M} \mid \Delta \boldsymbol{r}_0)`.
        Furthermore, if :math:`\operatorname{rank}\boldsymbol{M} = K + K'`,
        the intersection is a unique point. Otherwise, it is an affine
        subspace of dimension :math:`K+K'-\operatorname{rank}\boldsymbol{M}`.


        :param other: A line to check for intersection with the current line.
        :param thresh: Threshold to determine the nullity of a matrix.

        Returns
        -------
        n:
            number of points of intersection
        intersection:
            :const:`None` if `n` is zero, :class:`Point` if `n` = 1,
            :class:`AffineSubspace` if `n` = :const:`numpy.inf`.
        """
        assert isinstance(other, AffineSubspace)
        # pylint: disable=C0103
        # C0103: K and Kd are okay as variable names to correspond to their
        #        mathematical symbols.
        if self.is_parallel(other, thresh):
            if self.contains_point(other.anchor, thresh) or\
               other.contains_point(self.anchor, thresh):
                if self.affdim <= other.affdim:
                    return (inf, self)
                return (inf, other)
            return (0, None)

        mmat = np.vstack([vec.components for vec in self.direction]
                         + [-vec.components for vec in other.direction]).T
        rmat = np.reshape(other.anchor.coordinates-self.anchor.coordinates,
                          (-1, 1))
        augmat = np.hstack([mmat, rmat])

        if np.linalg.matrix_rank(mmat) != np.linalg.matrix_rank(augmat):
            # No solutions.
            return (0, None)

        # Some solutions must exist.
        # We first find a particular solution as the least-norm solution.
        K = self.affdim
        Kd = other.affdim
        lambdas = np.linalg.lstsq(mmat, rmat, rcond=thresh)[0].tolist()
        assert len(lambdas) == K+Kd
        point_on_self = self.get_point(lambdas[0:K])
        point_on_other = other.get_point(lambdas[K:K+Kd])
        assert point_on_self.is_same_as(point_on_other, thresh)

        if np.linalg.matrix_rank(mmat) == K+Kd:
            # Unique solution: intersection at a single point
            # The least-norm solution is the only solution.
            return (1, point_on_self)

        # np.linalg.matrix_rank(mmat) != K+Kd, infinitely many solutions.
        # The least-norm solution gives a particular solution.
        # The general solution is obtained from the null space of mmat,
        # which gives the lambda coefficients for the direction vectors to
        # combine to give the direction of the affine subspace of intersection.
        lambdas_null = null_space(mmat)
        self_params_null = lambdas_null[0:K, :]
        nullspace = np.dot(self.directionmatrix, self_params_null)
        # pylint: disable=E1133
        # E1133: nullspace.T is iterable: looping through its rows.
        direction = [Vector(col) for col in nullspace.T]
        return (inf, AffineSubspace(point_on_self, direction))

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, AffineSubspace):
            return NotImplemented
        return self.is_same_as(other, COMMON_ZERO_TOLERANCE)

    def __ne__(self, other: object) -> bool:
        return not self.__eq__(other)

    def is_same_as(self, other: AffineSubspace,
                   thresh: float = COMMON_ZERO_TOLERANCE) -> bool:
        """Check if the current affine subspace is the same as `other`.

        :param other: An affine subspace to check for identicality with the
                current affine subspace.
        :param thresh: Threshold to determine the nullity of a matrix.

        :returns: `True` if `other` is the same as the current affine subspace,
                  `False` if not.
        """
        if self.dim != other.dim or self.affdim != other.affdim:
            return False
        if self.is_parallel(other, thresh):
            if self.contains_point(other.anchor, thresh):
                return True
        return False


class Line(AffineSubspace):
    r"""A class describing a line in :math:`\mathbb{R}^n`, *i.e.*, a
    one-dimensional affine subspace embedded in :math:`\mathbb{R}^n`.

    Mathematically, a line is a collection of all points satisfying
    the vector equation

    .. math:: \boldsymbol{r} = \boldsymbol{r}_0 + \lambda \boldsymbol{d}

    where :math:`\lambda \in \mathbb{R}` is a scalar parameter.
    The codimension of a line is thus :math:`n-1`.
    """

    def __init__(self, anchor: Point, direction: Vector) -> None:
        r"""
        :param anchor: A point on the line corresponding to the position vector
                :math:`\boldsymbol{r}_0`.
        :param direction: A vector :math:`\boldsymbol{d}` parallel to the line.

        :raises AssertionError: if `anchor` and `direction` have different
                dimensions.
        :raises ValueError: if `direction` is a null vector w.r.t. a threshold
                of :const:`~.common_standards.COMMON_ZERO_TOLERANCE`.
        """
        super().__init__(anchor, [direction])

    def __str__(self) -> str:
        return f"Line[{self.anchor} + lambda*{self.direction[0]}]"

    __repr__ = __str__

    @classmethod
    def from_affinesubspace(cls, aff: AffineSubspace) -> Line:
        r"""Create a line from an affine subspace of dimension one.

        :param aff: One-dimensional affine subspace from which a line is to be
                constructed.

        :raises AssertionError: if `aff` is not one-dimensional.

        :returns: A :class:`Line` corresponding to `aff`.
        """
        assert aff.affdim == 1,\
                "The provided affine subspace is not one-dimensional."
        return cls(aff.anchor, aff.direction[0])

    def intersects_line(self, other: Line,
                        thresh: float = COMMON_ZERO_TOLERANCE)\
                                -> Tuple[int, Union[None, Point, Line]]:
        """Check if the current line intersects `other`.

        :param other: A line to check for intersection with the current line.
        :param thresh: Threshold to determine the nullity of a matrix.

        Returns
        -------
        n:
            number of points of intersection
        intersection:
            :const:`None` if `n` is zero, :class:`Point` if `n` = 1,
            :class:`Line` if `n` = :const:`numpy.inf`.
        """
        assert isinstance(other, Line)
        (nsols, intersection) = super().intersects(other, thresh)
        if isinstance(intersection, AffineSubspace):
            # intersection is actually a line.
            # Line.from_affinesubspace does verify this.
            intersection = Line.from_affinesubspace(intersection)
        return (nsols, intersection)

    def intersects_hyperplane(self, hplane: Hyperplane,
                              thresh: float = COMMON_ZERO_TOLERANCE)\
                              -> Tuple[int, Union[None, Point, Line]]:
        r"""Check if the current line intersects `hplane`.

        In :math:`\mathbb{R}^n`, a hyperplane has codimension :math:`1` and a
        line has codimension :math:`n-1`. The codimension of their intersection
        for :math:`n \ge 2` is therefore an integer in the range
        :math:`[\operatorname{max}(1, n-1), (n-1)+1] = [n-1, n]`, which
        corresponds to either a line or a point.

        :param hplane: A hyperplane to check for intersection with the current
                line.
        :param thresh: Threshold to determine the nullity of a matrix.

        Returns
        -------
        n:
            number of points of intersection
        intersection:
            :const:`None` if `n` is zero, :class:`Point` if `n` = 1,
            :class:`Line` if `n` = :const:`numpy.inf`.
        """
        assert isinstance(hplane, Hyperplane)
        (nsols, intersection) = super().intersects(hplane, thresh)
        if isinstance(intersection, AffineSubspace):
            # intersection is actually a line.
            # Line.from_affinesubspace does verify this.
            intersection = Line.from_affinesubspace(intersection)
        return (nsols, intersection)

    def get_cabinet_projection(self, alpha: float = np.arctan(2)) -> Line:
        r"""The cabinet projection of the current line onto the
        :math:`xy`-plane.

        This is only defined if the current line is in :math:`\mathbb{R}^3`.

        For a description of the cabinet projection, see
        :meth:`~.geometrical_space.Point.get_cabinet_projection`.

        :param alpha: Angle :math:`\alpha` of projection (radians).
                :math:`0 \leq \alpha < 2\pi`.

        :returns: The projected line as a :class:`Line`.
        """
        # pylint: disable=C0103
        # C0103: x, y, z are okay as variable names since they denote
        #        coordinates.
        return Line(self.anchor.get_cabinet_projection(alpha),
                    self.direction[0].get_cabinet_projection(alpha))


class Hyperplane(AffineSubspace):
    r"""A class describing a hyperplane in :math:`\mathbb{R}^n`, *i.e.*, an
    affine subspace of dimension :math:`n-1` embedded in math:`\mathbb{R}^n`.

    Mathematically, a hyperplane is a collection of all points satisfying
    the vector equation

    .. math::
        \boldsymbol{r} \cdot \boldsymbol{n} =
            \boldsymbol{r}_0 \cdot \boldsymbol{n} = c

    where :math:`\boldsymbol{n}` is a unit vector normal to the hyperplane,
    :math:`\boldsymbol{r}_0` the position vector of a point on the hyperplane,
    and :math:`c` a scalar constant. By definition, the direction of the
    affine subspace corresponding to this hyperplane is the null space of the
    :math:`1 \times n` matrix :math:`\boldsymbol{n}^{\mathsf{T}}`.
    """

    def __init__(self, anchor: Point, normal: Vector) -> None:
        r"""
        :param normal: A vector (not necessarily normalised) normal to the
                plane.
        :param anchor: A point on the plane.
        """
        directionmatrix = null_space(np.reshape(normal.components, (1, -1)))
        direction = [Vector(col) for col in  directionmatrix.T]
        super().__init__(anchor, direction)
        self.normal = normal
        self._constant = Vector.from_point(anchor).dot(normal)

    @classmethod
    def from_affinesubspace(cls, aff: AffineSubspace) -> Hyperplane:
        r"""Create a hyperplane from an affine subspace of dimension :math:`n-1`
        in :math:`\mathbb{R}^n`.

        Let :math:`\boldsymbol{M}` be the direction matrix of `aff`. The normal
        vector to `aff` is given by the single :math:`n \times 1` column vector
        spanning the null space of :math:`\boldsymbol{M}^{\mathsf{T}}`.

        :param aff: :math:`(n-1)`-dimensional affine subspace from which a
                hyperplane is to be constructed.

        :raises AssertionError: if `aff` is not :math:`(n-1)`-dimensional.

        :returns: A :class:`Hyperplane` corresponding to `aff`.
        """
        assert aff.affdim == aff.dim-1,\
                "The provided affine subspace is not (n-1)-dimensional."
        normal_comps = list(null_space(aff.directionmatrix.T).flat)
        normal = Vector(normal_comps, n=aff.dim)
        return cls(aff.anchor, normal)

    @property
    def normal(self) -> Vector:
        """The unit vector normal to the hyperplane with the largest number
        of positive coefficients.

        Any non-unit vector will be normalised automatically.

        When a vector with fewer positive components than negative components
        is supplied, its negative will be taken. A positive component is one
        with a positive real part.
        """
        return self._normal

    @normal.setter
    def normal(self, vector: Vector) -> None:
        assert vector.dim == self.dim
        pos_count = len([x for x in vector if x > 0])
        neg_count = len([x for x in vector if x < 0])
        if pos_count >= neg_count:
            self._normal = vector.normalise()
        else:
            self._normal = -(vector.normalise())

    @property
    def constant(self) -> RealScalar:
        """The scalar constant in the vector equation corresponding to the
        unit vector normal to the plane with the largest number of positive
        components.

        This property has no setter.
        """
        return self._constant

    def __str__(self) -> str:
        anchor_vec = Vector.from_point(self.anchor)
        return f"Hyperplane[r·{self.normal}\n" +\
               f"           = {anchor_vec}·{self.normal}\n" +\
               f"           = {self.constant:.3f}]"

    __repr__ = __str__

    def get_front_normal(self, viewing_vector: Vector,
                         thresh: float = COMMON_ZERO_TOLERANCE) -> Vector:
        r"""The "front-face" normal relative to `viewing_vector`.

        The "front-face" normal has a positive dot product with
        `viewing_vector`.  If this dot product is zero, the hyperplane is
        parallel to `viewing_vector` and either normal can serve as the
        "front-face" normal.  The normal vector with more positive components
        is returned in this case.

        :param viewing_vector: The viewing vector corresponding to a certain
                parallel projection.
        :param thresh: Threshold to determine if the hyperplane is parallel to
                the viewing vector.

        :returns: The "front-face" normal relative to `viewing_vector`.
        """
        assert viewing_vector.dim == self.dim,\
                "Mismatched dimensions between the viewing vector and "\
                + "the current hyperplane."
        if self.normal.dot(viewing_vector.normalise()) <= -thresh:
            return -self.normal
        return self.normal

    def intersects_line(self, line: Line,
                        thresh: float = COMMON_ZERO_TOLERANCE)\
                                -> Tuple[int, Union[None, Point, Line]]:
        r"""Check if the current hyperplane intersects `line`.

        This calls :meth:`Line.intersects_hyperplane` since intersectivity is
        reflexive.

        :param line: A line to check for intersection with the current
                hyperplane.
        :param thresh: Threshold to determine the nullity of a matrix.

        Returns
        -------
        n:
            number of points of intersection
        intersection:
            :const:`None` if `n` is zero, :class:`Point` if `n` = 1,
            :class:`Line` if `n` = :const:`numpy.inf`.
        """
        assert isinstance(line, Line)
        return line.intersects_hyperplane(self, thresh)

    def intersects_hyperplane(self, other: Hyperplane,
                              thresh: float = COMMON_ZERO_TOLERANCE)\
                -> Tuple[int, Union[None, Line, Hyperplane, AffineSubspace]]:
        r"""Check if the current hyperplane intersects `other`.

        In :math:`\mathbb{R}^n`, a hyperplane has codimension :math:`1`. The
        codimension of the intersection of two hyperplanes is therefore an
        integer in the range
        :math:`[\operatorname{max}(1, 1), 1+1] = [1, 2]`, which corresponds
        to either a hyperplane of dimension :math:`n-1` or an affine subspace of
        dimension :math:`n-2`.
        In particular, if :math:`n=3`, the latter case turns out to be a line.

        :param other: A hyperplane to check for intersection with the current
                hyperplane.
        :param thresh: Threshold to determine the nullity of a matrix.

        Returns
        -------
        n:
            Number of points of intersection.
        intersection:
            :const:`None` if `n` is zero, :class:`Line`,
            :class:`AffineSubspace`, or :class:`Hyperplane` if
            `n` = :const:`numpy.inf`.
        """
        assert isinstance(other, Hyperplane)
        (nsols, intersection) = super().intersects(other, thresh)
        assert not isinstance(intersection, Point)
        if isinstance(intersection, AffineSubspace):
            if intersection.affdim == 1:
                # intersection is actually a line.
                # Line.from_affinesubspace does verify this.
                intersection = Line.from_affinesubspace(intersection)
            elif intersection.affdim == self.dim - 1:
                # intersection is actually a hyperplane.
                # Hyperplane.from_affinesubspace does verify this.
                intersection = Hyperplane.from_affinesubspace(intersection)
        return (nsols, intersection)
