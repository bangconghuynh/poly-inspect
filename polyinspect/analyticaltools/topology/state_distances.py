""":mod:`.state_distances` contains functions to calculate distances in the
space of one-particle density matrices.
"""

from __future__ import annotations

from typing import Sequence, Tuple, List
from math import ceil
import numpy as np  # type:ignore
import sympy  # type: ignore
from sympy import Abs, re, im, symbols, sympify, S, N,\
        solve_poly_system, FiniteSet  # type:ignore
from sympy.parsing.sympy_parser import parse_expr  # type: ignore
from sympy.solvers.solveset import nonlinsolve  # type: ignore
from scipy.spatial.distance import cdist  # type: ignore

from polyinspect.auxiliary.geometrical_space import Point
from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE
from polyinspect.auxiliary.common_types import RealScalar
from polyinspect.analyticaltools.wavefunctions.density1p import density_binary_map,\
        density_diff_self_map


def _expand(points: Sequence[Point]) -> None:
    r"""Endow each point in the sequence `points` with an additional zero
    coordinate.

    :param points: A sequence of points to be endowed with an additional
            zero coordinates.
    """
    for point in points:
        point.coordinates = np.append(point.coordinates, 0.0)


def _is_pos_def(mat: np.ndarray, thresh: float = COMMON_ZERO_TOLERANCE) -> bool:
    r"""Check if a matrix :math:`\boldsymbol{M}` is a positive definite matrix.

    A necessary condition for :math:`\boldsymbol{M}` to be positive definite is
    that :math:`\boldsymbol{M}` must be Hermitian, *i.e.*,
    :math:`\boldsymbol{M} = \boldsymbol{M}^{\dagger}`.

    If :math:`\boldsymbol{M}` is positive definite then
    :math:`\boldsymbol{v}^{\dagger}\boldsymbol{M}\boldsymbol{v} > 0 \quad
    \forall \boldsymbol{v} \in \mathbb{Z}^n \setminus \boldsymbol{0}`.

    :param mat: The matrix :math:`\boldsymbol{M}` to be checked for positive
            definiteness.

    :returns: :const:`True` if :math:`\boldsymbol{M}` is positive definite,
            :const:`False` if not.
    """
    # We use Cholesky decomposition to check if mat is positive-definite.
    # https://stackoverflow.com/a/44287862
    if np.allclose(mat, mat.conj().T, rtol=0, atol=thresh):
        try:
            np.linalg.cholesky(mat)
            return True
        except np.linalg.LinAlgError:
            return False
    return False


def intersect_spheres(spheres: Sequence[Tuple[Point, RealScalar]],
                      thresh: float = COMMON_ZERO_TOLERANCE,
                      precision: int = 16) -> List[Point]:
    r""" Return a list of solutions of the intersections of :math:`K` spheres
    in :math:`\mathbb{R}^K`. This is part of the implementation of Algorithm 1
    in :cite:`article:Alencar2018`.

    In their most general positions, the centres of the spheres span the
    :math:`\mathbb{R}^{K_{\mathrm{c}}}` affine space where
    :math:`K_{\mathrm{c}} = K - 1`.
    There can then be zero, one, or two real solutions for the interesection of
    all :math:`K` spheres.
    If only one real solution exists, it must reside in
    :math:`\mathbb{R}^{K_{\mathrm{c}}}`.
    If two real solutions exist, they reside in :math:`\mathbb{R}^{K}` and are
    mirror images of each other in the hyperplane defined by
    :math:`\mathbb{R}^{K_{\mathrm{c}}}`.

    If :math:`K_{\mathrm{c}} < K - 1`, there can be more than two real
    solutions.

    :param spheres: A sequence of tuples, each of which contains the  centre of
            a sphere and its radius.
    :param thresh: The threshold within which a new component is considered
            zero (and thus does not increase the embedding dimension), and
            within which any imaginary part is considered zero.
    :param precision: The number of decimal places for floating-point values of
            coordinates and radii when constructing sphere intersection
            equations, and of the components of the returned
            :class:`~auxiliary.geometrical_space.Point`.

    :returns: A list of intersection points, sorted in reversed lexicographic
            order of their coordinates such that, for each dimension, the
            positive coordinates come first.
    """
    # pylint: disable=C0103,R0914
    # C0103: The variables are named according to their mathematical symbols.
    # R0914: All local variables are needed for readability.

    # dimension of the affine span of the centres, i.e., K-1
    dims = {sphere[0].dim for sphere in spheres}
    assert len(dims) == 1,\
            "Inconsistent dimensionality detected in the specified centres."
    K = next(iter(dims)) + 1
    assert K <= len(spheres)

    # Generate a tuple of symbolic coordinates for the unknown intersection
    # point which is assumed to live in at most K dimensions, i.e.,
    # r = (r0, r1, ..., r(K-1)).
    r: Tuple[sympy.core.symbol.Symbol, ...] = symbols(f"r0:{K:d}")

    # Each sphere gives one equation for the unknown intersection point.
    eqn_system: List[sympy.core.expr.Expr] = []
    for sphere in spheres:
        centre = sphere[0]
        radius = sphere[1]
        str_expr = ""
        for icoordinate, coordinate in enumerate(centre):
            str_expr += f"({r[icoordinate]}-{coordinate:.{precision}f})**2 + "
        str_expr += f"({r[K-1]})**2 - {radius:.{precision}f}**2"

        expr = parse_expr(str_expr, evaluate=False)
        eqn_system.append(expr)

    # We store raw solutions in a FiniteSet so that we can use sympy's set
    # arithmetics to filter out unwanted complex solutions.
    # We also try a variety of solvers in the event no solutions can be found.
    raw_sols_list = solve_poly_system(eqn_system, *r)
    if raw_sols_list is not None:
        raw_sols = FiniteSet(*raw_sols_list)
    else:
        raw_sols = nonlinsolve(eqn_system, list(r))

    real_sols: Tuple[Tuple[sympy.core.expr.Expr, ...], ...]\
            = raw_sols.intersect(S.Reals**K).args

    if len(real_sols) > 2:
        # More than two real solutions found. We try to clean up any numerical
        # duplicates first by rounding all coordinate values to the number of
        # digits dictated by thresh such that the rounded values are less
        # precise than thresh, so that any artificial non-identicality due to
        # numerical errors can be removed.
        digits = -int(ceil(np.log10(thresh)))
        _real_sols = [tuple(coordinate.round(digits) for coordinate in sol)
                      for sol in real_sols]
        real_sols = tuple(set(_real_sols))

    assert len(real_sols) <= 2, "At most two real solutions are permissible."
    if len(real_sols) == 2:
        # Two real solutions found. We keep both of them for now.
        kept_sols = list(real_sols)

    elif len(real_sols) == 1:
        # Only one real solution found (or actually, two repreated solutions).
        # We eliminate the last coordinate which must be zero, since the solution
        # only lies in the first (K-1)th dimensions.
        assert real_sols[0][-1] == 0
        kept_sols = [real_sols[0][0:-1]]

    else:
        # There are no real solutions, but two complex solutions.
        # We need to check if the imaginary parts are negligible.
        complex_sols = raw_sols.args
        kept_sols = [tuple(re(coord) for coord in complex_sol)
                     for complex_sol in complex_sols
                     if all(Abs(im(coord)) < sympify(thresh)
                            for coord in complex_sol)]

    if len(kept_sols) == 2 and\
            Abs(kept_sols[0][-1]) < thresh and\
            Abs(kept_sols[1][-1]) < thresh:
        # Both solutions have very small last coordinates.
        # We keep the first one only, and eliminate the last coordinate,
        # so that we keep the solution in the first (K-1)th dimensions
        # and prevent and augmentation of the embedding dimensions.
        kept_sols = [kept_sols[0][0:-1]]

    # Sort the solutions such that the positive coordinate in each dimension
    # comes first.
    kept_sols = sorted(kept_sols, reverse=True)
    point_sols = [Point([float(N(comp, precision)) for comp in sol])
                  for sol in kept_sols]
    return point_sols


def edmsph(edm: np.ndarray, tol: float = 1e-14, thresh: float = 1e-7,
           precision: int = 16) -> Tuple[List[Point], int]:
    r"""Return the points realising a Euclidean distance matrix and the least
    possible embedding dimension.
    This implementation is based on Algorithm 1 in :cite:`article:Alencar2018`.

    :param edm: A Euclidean distance matrix :math:`\boldsymbol{D}` which must be
            a square hollow symmetric matrix whose non-negative elements
            :math:`D_{ij}` are pairwise Euclidean distances between points
            :math:`i` and :math:`j` in some Euclidean space.
    :param tol: The tolerance within which the Euclidean distance matrix
            constructed from the obtained position vectors is considered
            identical to the supplied Euclidean distance matrix `edm`. `tol`
            should be small so as to ensure that the points do indeed
            realise `edm` tightly.
    :param thresh: The threshold within which a new coordinate is considered
            zero (and thus does not increase the embedding dimension), and
            within which any imaginary part is considered zero. `thresh` can be
            fairly large (*e.g.*, around :math:`1\times10^{-5}`), as long as
            `tol` is sufficiently small to ensure that the obtained points
            realise `edm` tightly.
    :param precision: The number of decimal places for floating-point values of
            coordinates and radii when constructing sphere intersection
            equations, and of the coordinates of the returned
            :class:`~auxiliary.geometrical_space.Point`.

    :returns: A list of :class:`~auxiliary.geometrical_space.Point`
            realising :math:`\boldsymbol{D}`, and the associated embedding
            dimension.
    """
    # pylint: disable=C0103,R0914
    # C0103: The variables are named according to their mathematical symbols.
    # R0914: All local variables are needed for readability.

    n = edm.shape[0]  # Number of points
    assert edm.shape[1] == n,\
            "The Euclidean distance matrix is not a square matrix."
    assert np.all(np.abs(np.diag(edm)) < thresh),\
            "The Euclidean distance matrix is not hollow."
    assert np.allclose(edm, edm.T, rtol=0, atol=thresh),\
            "The Euclidean distance matrix is not symmetric."

    K = 1  # Current embedding dimension
    I = [0, 1]  # Indices of affinely independent vertices
    x = [Point([0]), Point([edm[1, 0]])]  # Known vertices
    for i in range(2, n):
        spheresconsidered = [(x[j], edm[i, j]) for j in I]
        gamma = intersect_spheres(spheresconsidered, thresh, precision)

        assert len(gamma) > 0,\
                f"No real solutions found for point i = {i}. "\
              + "Consider increasing thresh."
        assert len(gamma) <= 2,\
                f"Invalid number of solutions ({len(gamma)}) found for point "\
              + f"i = {i}."

        if len(gamma) == 1:
            # No change in embedding dimension.
            x.append(gamma[0])
        else:
            # Embedding dimension has increased by one.
            _expand(x)
            K += 1
            I.append(i)
            x.append(gamma[0])

    coordmat = np.array([xi.coordinates for xi in x])
    newedm = cdist(coordmat, coordmat, "euclidean")
    max_res = np.amax(np.abs(newedm - edm))
    assert max_res <= tol,\
            f"Maximum residual = {max_res:.3e} > tolerance = {tol:.3e}."

    print(f"Lowest embedding dimensions found: {K}")
    print(f"Tolerance: {tol:.3e}")

    return (x, K)


def density_char_mat(denmatss: Sequence[Sequence[np.ndarray]],
                     sao: np.ndarray,
                     sesquilinear: bool) -> np.ndarray:
    r"""Calculate the characteristic matrix :math:`\boldsymbol{\Gamma}` for a
    space spanned by set of basis density matrices.

    The matrix elements of :math:`\boldsymbol{\Gamma}` are given by

    .. math::
        \Gamma_{ab} =
            \sum_{\sigma}
                \braket{^{a}\boldsymbol{P}_{\sigma},
                      \ ^{b}\boldsymbol{P}_{\sigma}}_{\diamond},

    where
    :math:`\braket{^{a}\boldsymbol{P}_{\sigma},
    \ ^{b}\boldsymbol{P}_{\sigma}}_{\diamond}` is defined in
    :func:`~src.analyticaltools.wavefunctions.density1p.density_binary_map`.

    :math:`\boldsymbol{\Gamma}` must be positive-definite for the map
    :math:`\braket{\cdot, \cdot}_{\diamond}` to be a true inner product on this
    space.


    :param denmatss: Sequence of sequences of density matrices. Each element in
            the inner sequence is a density matrix for one spin space of a
            particular state.
    :param sao: The overlap matrix of the underlying atomic basis functions.
    :param sesquilinear: If :const:`True`, a symmetric sesquilinear form will
            be used for the map. If :const:`False`, a symmetric bilinear form
            will be used instead. See :func:`~src.analyticaltools.wavefunctions\
            .density1p.density_binary_map` for further information.

    :returns: The characteristic matrix :math:`\boldsymbol{\Gamma}` for the
            input density matrices.
    """
    charmat = np.array([[sum(density_binary_map(denmatsa, denmatsb,
                                                sao, sesquilinear))
                         for denmatsb in denmatss]
                        for denmatsa in denmatss])
    return charmat


def density_sqdist_mat(denmatss: Sequence[Sequence[np.ndarray]],
                       sao: np.ndarray,
                       frobenius: bool) -> np.ndarray:
    r"""Calculate the squared distance matrix for a set of density matrices.

    The pairwise squared distance between :math:`^{a}\boldsymbol{P}` and
    :math:`^{b}\boldsymbol{P}` is given by
    :math:`\sum_{\sigma}d_{ab, \sigma}^2`, where :math:`d_{ab, \sigma}^2` is
    defined in
    :func:`~src.analyticaltools.wavefunctions.density1p.density_diff_self_map`.


    :param denmatss: Sequence of sequences of density matrices. Each element in
            the inner sequence is a density matrix for one spin space of a
            particular state.
    :param sao: The overlap matrix of the underlying atomic basis functions.
    :param frobenius: If :const:`True`, :math:`\diamond = \dagger`.
            If :const:`False`, :math:`\diamond = \hat{e}`.
    :param frobenius: If :const:`True`, a Frobenius inner product will
            be used for the map. If :const:`False`, a non-Frobenius inner product
            will be used instead. See :func:`~src.analyticaltools.wavefunctions\
            .density1p.density_diff_self_map` for further information.
    :param thresh: Threshold to determine if the characteristic matrix of the
            supplied density matrices is positive definite.

    :returns: The squared distance matrix for the input density matrices.
    """
    # char_mat = density_char_mat(denmatss, sao, sesquilinear)
    # if not _is_pos_def(char_mat, thresh):
    #     warn("The characteristic matrix for the supplied density matrices "
    #          + "is not positive-definite. Be careful when interpreting "
    #          + "the resulted 'squared distances'.", RuntimeWarning)
    sqdistmat = np.array([[sum(density_diff_self_map(denmatsa, denmatsb,
                                                     sao, frobenius))
                           for denmatsb in denmatss]
                          for denmatsa in denmatss])
    return sqdistmat
