#!/usr/bin/env python3
""":mod:`.density1p` contains public functions to construct and manipulate
one-particle densities and density matrices.
"""

# pylint: disable=C0103

from __future__ import annotations

from typing import List, Sequence
import numpy as np  # type:ignore

from polyinspect.auxiliary.common_types import Scalar, Coefficients, MultiStates
from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE
from polyinspect.analyticaltools.wavefunctions.nonorthowavefunctions import\
        lowdin_pairing


def unweighted_codensity(cwi: np.ndarray, cxi: np.ndarray,
                         complexsymmetric: bool) -> np.ndarray:
    r"""Return the unweighted codensity matrix between spin-orbitals :math:`i`
    in determinants :math:`^{w}\Psi` and :math:`^{x}\Psi` defined by

    .. math::
        ^{wx}\boldsymbol{P}_i
          = \ ^{w}\boldsymbol{G}_i \otimes
            (^{x}\boldsymbol{G}_i^{\star})^{\diamond}
          = \ ^{w}\boldsymbol{G}_i
            \ ^{x}\boldsymbol{G}_i^{\blacklozenge},

    where :math:`\boldsymbol{G}_i` is the coefficient column vector for
    spin-orbital :math:`\chi_i` such that

    .. math::
        \chi_i(\boldsymbol{x}) = \omega_{\cdot\delta}(s)
                                 \psi_{\cdot\mu}(\boldsymbol{r})
                                 G_i^{\delta\mu, \cdot}.

    This is based on Equation 3 in :cite:`article:Thom2009b`.


    :param cwi: Coefficient column vector :math:`^{w}\!\boldsymbol{G}_i`.
    :param cxi: Coefficient column vector :math:`^{x}\!\boldsymbol{G}_i`.
    :param complexsymmetric: If :const:`True`, :math:`\diamond = \star`.
            If :const:`False`, :math:`\diamond = \hat{e}`.
            In both cases, :math:`\blacklozenge = \dagger\diamond`.

    :returns: The unweighted codensity matrix :math:`^{wx}\boldsymbol{P}_i`.
    """
    # pylint: disable=C0103
    # The variables are named according to their mathematical symbols.

    assert len(cwi.shape) == 1
    assert cwi.shape == cxi.shape
    if complexsymmetric:
        P = np.outer(cwi, cxi)
    else:
        P = np.outer(cwi, cxi.conj())
    return P


def weighted_codensity(cwt: np.ndarray, cxt: np.ndarray,
                       lowdin_overlaps: Sequence[Scalar],
                       zero_indices: Sequence[int],
                       complexsymmetric: bool) -> np.ndarray:
    r"""Return the weighted codensity matrix between a set of Löwdin-paired
    spin-orbitals in determinants :math:`^{w}\Psi` and :math:`^{x}\Psi`
    in a particular spin space :math:`\sigma` defined by

    .. math::
        ^{wx}\boldsymbol{W}_{\sigma} =
            \sum_{\substack{i = 1\\ ^{wx}\lambda_i \neq 0}}^{N_{\sigma}}
                \frac{^{wx}\boldsymbol{P}_i}{^{wx}\lambda_i}

    where :math:`^{wx}\boldsymbol{P}_i` is the unweighted codensity matrix
    between Löwdin-paired spin-orbitals :math:`i` in determinants
    :math:`^{w}\Psi` and :math:`^{x}\Psi`, and the sum runs over up to
    :math:`N_{\sigma}` spin-orbitals in the spin space :math:`\sigma` of
    consideration.

    This is based on Equation 4 in :cite:`article:Thom2009b`.


    :param cwt: Löwdin-paired coefficient matrix for spin-orbitals in
            spin space :math:`\sigma` of :math:`^{w}\Psi`.
    :param cxt: Löwdin-paired coefficient matrix for spin-orbitals in
            spin space :math:`\sigma` of :math:`^{x}\Psi`.
    :param lowdin_overlaps: Sequence of corresponding Löwdin overlaps.
    :param zero_indices: Sequence of indices of zero Löwdin overlaps.
    :param complexsymmetric: If :const:`True`, :math:`\diamond = \star`.
            If :const:`False`, :math:`\diamond = \hat{e}`.
            In both cases, :math:`\blacklozenge = \dagger\diamond`.
            This is needed for :func:`~.nonorthowavefunctions.lowdin_pairing`
            and :func:`.unweighted_codensity`.

    :returns: The weighted codensity matrix
            :math:`^{wx}\boldsymbol{W}_{\sigma}`.
    """
    # pylint: disable=C0103
    # The variables are named according to their mathematical symbols.

    assert cwt.shape == cxt.shape
    nbasis, nsigma = cwt.shape
    assert len(lowdin_overlaps) == nsigma
    maxdtype = max((ct.dtype for ct in [cwt, cxt]),
                   key=lambda dtype: dtype.num, default=np.dtype(np.float64))
    W = np.zeros((nbasis, nbasis), dtype=maxdtype)
    i_nonzero = [i for i in range(nsigma) if i not in zero_indices]
    for i in i_nonzero:
        W = W + unweighted_codensity(cwt[:, i], cxt[:, i],
                                     complexsymmetric)/lowdin_overlaps[i]
    return W


def noci_density(scf_css: MultiStates, noci_cs: Sequence[Scalar],
                 sao: np.ndarray, complexsymmetric: bool,
                 thresh_offdiag: float = COMMON_ZERO_TOLERANCE,
                 thresh_zeroov: float = 1e-20) -> List[np.ndarray]:
    r"""Calculate the contravariant density matrix for a NOCI wavefunction.

    Let :math:`^{m}\Phi = \sum_{w}^{N_{\mathrm{det}}}\ ^{w}\Psi A_{wm}` be a
    multi-determinantal wavefunction. The density matrix for :math:`^{m}\Phi`
    in a particular spin space :math:`\sigma` in the contravariant
    representation is given by

    .. math::
        ^{m}P_{\sigma}^{\delta\mu,\epsilon\nu} =
        \begin{cases}
            \sum_{wx}^{N_{\mathrm{det}}}
                A_{xm}A^{\star}_{wm}
                \ ^{wx}S
                \ ^{xw}{W_{\sigma}^{\delta\mu,\epsilon\nu}}
            & \textrm{no Löwdin zeros} \\
            \sum_{wx}^{N_{\mathrm{det}}}
                A_{xm}A^{\star}_{wm}
                \ ^{wx}\tilde{S}_i
                \ ^{xw}{P_i^{\delta\mu,\epsilon\nu}}
            & ^{wx}\lambda_i = 0, i \in \sigma \\
            0
                & \textrm{two Löwdin zeros or }
                  ^{wx}\lambda_i = 0 \textrm{ but } i \notin \sigma
        \end{cases}

    where :math:`^{xw}\boldsymbol{W}_{\sigma}` is the weighted codensity matrix
    in spin space :math:`\sigma` between Löwdin-paired determinants :math:`x`
    and :math:`w`, :math:`^{xw}\boldsymbol{P}_i` the unweighted codensity
    matrix for Löwdin-paired spin-orbitals :math:`i` between determinants
    :math:`x` and :math:`w`, and
    :math:`^{wx}\tilde{S}_i = \prod_{j \ne i} {^{wx}\lambda_j}`.


    :param scf_css: List of lists of coefficient matrices of basis SCF
            determinants. Each element in the inner list corresponds to a
            coefficient matrix for one spin space.
    :param noci_cs: Sequence of :math:`A_{wm}` coefficients of the basis SCF
            determinants in the NOCI linear combination.
    :param sao: The overlap matrix :math:`\boldsymbol{S}_{\mathrm{AO}}` of the
            underlying atomic basis functions. This is needed for
            :func:`~.nonorthowavefunctions.lowdin_pairing`.
    :param complexsymmetric: If :const:`True`, :math:`\diamond = \star`.
            If :const:`False`, :math:`\diamond = \hat{e}`.
            In both cases, :math:`\blacklozenge = \dagger\diamond`.
            This is needed for :func:`~.nonorthowavefunctions.lowdin_pairing`,
            :func:`.unweighted_codensity`, and :func:`weighted_codensity`.
    :param thresh_offdiag: Threshold to check if the off-diagonal elements in
            the original orbital overlap matrix and in the Löwdin-paired
            orbital overlap matrix :math:`^{wx}\boldsymbol{\Lambda}` are zero.
            This is needed for
            :func:`~.nonorthowavefunctions.lowdin_pairing`.
    :param thresh_zeroov: Threshold to identify which Löwdin overlaps
            :math:`^{wx}\lambda_i` are zero. This is needed for
            :func:`~.nonorthowavefunctions.lowdin_pairing`.

    :returns: List of density matrices. Each element in the list corresponds to
            a density matrix for one spin space.
    """
    # pylint: disable=R0913,R0914
    # R0913: All six arguments are needed.
    # R0914: Having more local variables helps make the code more
    #        understandable.

    nspins_set = {len(cs) for cs in scf_css}
    assert len(nspins_set) == 1,\
            "Inconsistent number of spin spaces across the SCF coefficients."
    nspins = next(iter(nspins_set))

    assert len(scf_css) == len(noci_cs),\
            "Inconsistent numbers of SCF determinants and NOCI coefficients."

    # List of density matrices for the spin spaces
    denmats: List[np.ndarray] = []
    maxdtype = max((c.dtype for cs in scf_css for c in cs),
                   key=lambda dtype: dtype.num, default=np.dtype(np.float64))
    for ispin in range(nspins):
        nbasis_set = {cs[ispin].shape[0] for cs in scf_css}
        assert len(nbasis_set) == 1,\
                "Inconsistent number of basis functions across the SCF "\
                + f"coefficients in spin space {ispin}."
        nbasis = next(iter(nbasis_set))
        denmats.append(np.zeros((nbasis, nbasis), dtype=maxdtype))

    for w, cws in enumerate(scf_css):
        for x, cxs in enumerate(scf_css):
            # Calculating the contribution of the density matrix due to the
            # transition from state w to state x

            # Perform Löwdin pairing for all spin spaces
            # cwts is a tuple of cwt's from all spin spaces.
            # cxts is a tuple of cxt's from all spin spaces.
            # lowdin_overlapss is a tuple of lowdin_overlaps's from all spin
            #   spaces.
            # zero_indicess is a tuple of zero_indices's from all spin spaces.
            cwts, cxts, lowdin_overlapss, zero_indicess =\
                    zip(*[lowdin_pairing(cws[ispin], cxs[ispin],
                                         sao, complexsymmetric,
                                         thresh_offdiag, thresh_zeroov)
                          for ispin in range(nspins)])

            nzeross = [len(zero_indices) for zero_indices in zero_indicess]
            if sum(nzeross) >= 2:
                # More than one zero. This transition does not contribute to
                # any elements in denmats.
                continue

            reduced_ovs = []
            for ispin in range(nspins):
                reduced_ov_spin =\
                        np.prod([lowdin_overlapss[ispin][i]
                                 for i in range(len(lowdin_overlapss[ispin]))
                                 if i not in zero_indicess[ispin]])
                reduced_ovs.append(reduced_ov_spin)
            reduced_ov = np.prod(reduced_ovs)

            for ispin in range(nspins):
                lowdin_overlaps = lowdin_overlapss[ispin]
                zero_indices = zero_indicess[ispin]
                if nzeross[ispin] == 0:
                    denmat_spin =\
                            weighted_codensity(cxts[ispin], cwts[ispin],
                                               lowdin_overlaps, zero_indices,
                                               complexsymmetric)
                else:  # nzeross[ispin] == 1
                    denmat_spin =\
                        unweighted_codensity(cxts[ispin][:, zero_indices[0]],
                                             cwts[ispin][:, zero_indices[0]],
                                             complexsymmetric)
                if complexsymmetric:
                    denmat_spin =\
                            denmat_spin*noci_cs[x]*noci_cs[w]*reduced_ov
                else:
                    denmat_spin =\
                            denmat_spin*noci_cs[x]*np.conj(noci_cs[w])*reduced_ov
                denmats[ispin] = denmats[ispin] + denmat_spin

    return denmats


def scf_density(scf_cs: Coefficients, complexsymmetric: bool)\
        -> List[np.ndarray]:
    r"""Calculate the contravariant density matrix for a single determinant.

    Let :math:`^{w}\Psi` a single-determinantal wavefunction. Its density matrix
    in spin space :math:`\sigma` in the contravariant representation is given by

    .. math::
        ^{w}P^{\delta\mu,\epsilon\nu} =\ ^{ww}{W^{\delta\mu,\epsilon\nu}}

    where :math:`^{ww}\boldsymbol{W}` is the weighted codensity matrix between
    the determinant :math:`^{w}\Psi` and itself, in which all Löwdin overlaps
    are set to unity.


    :param scf_cs: List of coefficient matrices of basis SCF
            determinants. Each element corresponds to a coefficient matrix for
            one spin space.
    :param complexsymmetric: If :const:`True`, :math:`\diamond = \star`.
            If :const:`False`, :math:`\diamond = \hat{e}`.
            In both cases, :math:`\blacklozenge = \dagger\diamond`.
            This is needed for :func:`weighted_codensity`.

    :returns: List of density matrices. Each element in the list corresponds to
            a density matrix for one spin space.
    """
    denmats = []
    for cw in scf_cs:
        nsigma = cw.shape[1]
        lowdin_overlaps = [1]*nsigma
        denmats.append(weighted_codensity(cw, cw,
                                          lowdin_overlaps, [],
                                          complexsymmetric))
    return denmats


def density_binary_map(denmatsa: Sequence[np.ndarray],
                       denmatsb: Sequence[np.ndarray],
                       sao: np.ndarray, frobenius: bool) -> List[Scalar]:
    r"""Calculate the map
    :math:`\braket{\cdot, \cdot}_{\diamond}: \mathbb{F}^{M^2}_{N_{\mathrm{e}}}
    \times \mathbb{F}^{M^2}_{N_{\mathrm{e}}} \rightarrow \mathbb{F}`
    which takes a pair of :math:`M \times M` density matrices describing
    :math:`N_{\mathrm{e}}` electrons and returns a scalar in the field
    :math:`\mathbb{F}`.

    This map is defined for a pair of density matrices
    :math:`^{a}\boldsymbol{P}_{\sigma}` and :math:`^{b}\boldsymbol{P}_{\sigma}`
    in a given spin space by

    .. math::
        \braket{^{a}\boldsymbol{P}_{\sigma},
                \ ^{b}\boldsymbol{P}_{\sigma}}_{\diamond}
            = \mathrm{tr}
              [(^{a}\boldsymbol{P}_{\sigma}
               \ \boldsymbol{S}_{\mathrm{AO}})^{\diamond}
               \ ^{b}\boldsymbol{P}_{\sigma}
               \ \boldsymbol{S}_{\mathrm{AO}}
              ],

    and is not necessarily an inner product.


    :param denmatsa: Sequence of density matrices for wavefunction :math:`a`.
            Each element corresponds to a density matrix for one spin space.
    :param denmatsb: Sequence of density matrices for wavefunction :math:`b`.
            Each element corresponds to a density matrix for one spin space.
    :param sao: The overlap matrix :math:`\boldsymbol{S}_{\mathrm{AO}}` of the
            underlying atomic basis functions.
    :param frobenius: If :const:`True`, :math:`\diamond = \dagger`.
            If :const:`False`, :math:`\diamond = \hat{e}`.

    :returns: List of scalars, each of which is the value of
            :math:`\braket{\cdot, \cdot}_{\diamond}` for one spin space.
    """
    maps = []
    assert len(denmatsa) == len(denmatsb),\
            "denmatsa and denmatsb must contain the same number of elements."
    denmatss = zip(denmatsa, denmatsb)
    for denmata, denmatb in denmatss:
        if frobenius:
            maps.append(np.einsum("ji,kj,kl,li->",
                                  sao.conj(), denmata.conj(),
                                  denmatb, sao, optimize="optimal"))
        else:
            maps.append(np.einsum("ji,kj,kl,li->",
                                  denmata, sao,
                                  denmatb, sao, optimize="optimal"))
    return maps


def density_diff_self_map(denmatsa: Sequence[np.ndarray],
                          denmatsb: Sequence[np.ndarray],
                          sao: np.ndarray, frobenius: bool) -> List[Scalar]:
    r"""Calculate the difference self map between a pair of density matrices
    :math:`^{a}\boldsymbol{P}_{\sigma}` and :math:`^{b}\boldsymbol{P}_{\sigma}`
    in a given spin space :math:`\sigma` by

    .. math::
        d_{ab, \sigma}^2
            = \braket{{^{a}\boldsymbol{P}_{\sigma}}
                       - {^{b}\boldsymbol{P}_{\sigma}},
                    \ {^{a}\boldsymbol{P}_{\sigma}}
                       - {^{b}\boldsymbol{P}_{\sigma}}}_{\diamond}

    where the binary map
    :math:`\braket{\cdot, \cdot}_{\diamond}: \mathbb{F}^{M^2}_{N_{\mathrm{e}}}
    \times \mathbb{F}^{M^2}_{N_{\mathrm{e}}} \rightarrow \mathbb{F}` is given
    in :func:`.density_binary_map`.


    :param denmatsa: Sequence of density matrices for wavefunction :math:`a`.
            Each element corresponds to a density matrix for one spin space.
    :param denmatsb: Sequence of density matrices for wavefunction :math:`b`.
            Each element corresponds to a density matrix for one spin space.
    :param sao: The overlap matrix :math:`\boldsymbol{S}_{\mathrm{AO}}` of the
            underlying atomic basis functions.
    :param frobenius: If :const:`True`, :math:`\diamond = \dagger`.
            If :const:`False`, :math:`\diamond = \hat{e}`.

    :returns: List of scalars, each of which is the difference self map in one
            spin space.
    """
    assert len(denmatsa) == len(denmatsb),\
            "denmatsa and denmatsb must contain the same number of elements."
    denmatss = zip(denmatsa, denmatsb)
    denmatsdiff = [denmata - denmatb for (denmata, denmatb) in denmatss]
    return density_binary_map(denmatsdiff, denmatsdiff, sao, frobenius)
