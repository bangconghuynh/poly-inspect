#!/usr/bin/env python3
""":mod:`.nonorthowavefunctions` contains public functions to handle
non-orthogonal wavefunctions.
"""

from __future__ import annotations

from typing import List, Tuple
import numpy as np  # type:ignore

from polyinspect.auxiliary.common_types import Scalar, Coefficients, MultiStates
from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE, FLOAT_EPS


def det_ov(cws: Coefficients, cxs: Coefficients,
           sao: np.ndarray, complexsymmetric: bool) -> Scalar:
    r"""Calculate the overlap between two determinants, defined by

    .. math::

        S_{wx}=\braket{^{w}\Psi^{\diamond}|^{x}\Psi}.


    :param cws: Coefficient matrices of :math:`^{w}\Psi`.
    :param cxs: Coefficient matrices of :math:`^{x}\Psi`.
    :param sao: The overlap matrix of the underlying atomic basis functions.
    :param complexsymmetric: If :const:`True`, :math:`\diamond = \star`.
            If :const:`False`, :math:`\diamond = \hat{e}`.
            Note that the bra already contains an implicit :math:`\star`
            operation.

    :returns: The value of :math:`S_{wx}`.
    """
    # pylint: disable=C0103
    # The variables are named according to their mathematical symbols.

    assert len(cws) == len(cxs)
    Swx = 1.0
    for i, cw in enumerate(cws):
        cx = cxs[i]
        if not complexsymmetric:
            Swx *= np.linalg.det(np.linalg.multi_dot([np.conj(np.transpose(cw)),
                                                      sao,
                                                      cx]))
        else:
            Swx *= np.linalg.det(np.linalg.multi_dot([np.transpose(cw),
                                                      sao,
                                                      cx]))
    return Swx


def get_Smat(cwss: MultiStates, cxss: MultiStates, sao: np.ndarray,
             complexsymmetric: bool) -> np.ndarray:
    r"""Calculate the overlap matrix between two set of determinants.
    Each element is given by

    .. math::

        S_{wx}=\braket{^{w}\Psi^{\diamond}|^{x}\Psi}.


    :param cwss: List of lists of coefficient matrices of the first set of
            determinants. Each element in the inner list is a coefficient
            matrix for one spin space.
    :param cxss: List of lists of coefficient matrices of the second set of
            determinants. Each element in the inner list is a coefficient
            matrix for one spin space.
    :param sao: The overlap matrix of the underlying atomic basis functions.
    :param complexsymmetric: If :const:`True`, :math:`\diamond = \star`.
            If :const:`False`, :math:`\diamond = \hat{e}`.
            Note that the bra already contains an implicit :math:`\star`
            operation.

    :returns: The overlap matrix.
    """
    # pylint: disable=C0103
    # The variables are named according to their mathematical symbols.

    maxdtype = max((c.dtype for cs in cwss + cxss for c in cs),
                   key=lambda dtype: dtype.num, default=np.dtype(np.float64))
    Smat = np.zeros((len(cwss), len(cxss)), dtype=maxdtype)
    for w, cws in enumerate(cwss):
        for x, cxs in enumerate(cxss):
            Smat[w, x] = det_ov(cws, cxs, sao, complexsymmetric)
    return Smat


def get_Xmat(
    Smat: np.ndarray, thresh: float = np.sqrt(COMMON_ZERO_TOLERANCE)
) -> np.ndarray:
    r"""Calculate the transformation matrix :math:`\boldsymbol{X}` that brings
    a **square** overlap matrix :math:`\boldsymbol{S}` to full rank, *i.e.*,
    :math:`\tilde{\boldsymbol{S}}` defined by

    .. math::

        \tilde{\boldsymbol{S}} = \boldsymbol{X}^{\blacklozenge}
                                   \boldsymbol{S}
                                   \boldsymbol{X}

    is a full-rank matrix.

    If :math:`\boldsymbol{S}` is already of full rank, then
    :math:`\boldsymbol{X}` is set to the identity matrix so as to avoid mixing
    the original determinants.
    If :math:`\boldsymbol{S}` is complex-symmetric, then
    :math:`\blacklozenge = \mathsf{T}`.
    If :math:`\boldsymbol{S}` is complex-Hermitian, then
    :math:`\blacklozenge = \dagger`.


    :param Smat: A square overlap matrix :math:`\boldsymbol{S}`.
    :param thresh: A threshold to consider eigenvalues to be zero.

    :returns: The transformation matrix :math:`\boldsymbol{X}`.
    """
    # pylint: disable=C0103
    # The variables are named according to their mathematical symbols.

    assert thresh > 0
    assert Smat.shape[0] == Smat.shape[1]

    # If Smat is complex-Hermitian, then eigvals have to be real and eigvecs
    # have to be unitary.
    # If Smat is complex-symmetric but not complex-Hermitian, then eigvals
    # can be complex and eigvecs are only complex-orthogonal.
    # This dictates how X should be used outside of this function. Either way,
    # X can be constructed from np.linalg.eig.
    eigvals, eigvecs = np.linalg.eig(Smat)
    eigvals_sortindex = sorted(range(len(eigvals)),
                               key=lambda k: np.abs(eigvals[k]))
    eigvals_sortindex = list(np.array(eigvals_sortindex[::-1]))
    eigvals = eigvals[eigvals_sortindex]
    print("Eigenvalues of the overlap matrix in descending order of magnitudes:")
    for eigval in eigvals:
        print(f"{eigval:+.7e}")
    eigvecs = eigvecs[:, eigvals_sortindex]
    nullity = 0
    for v in eigvals[::-1]:
        if np.abs(v) < thresh:
            nullity += 1
        else:
            break
    if nullity == 0:
        return np.identity(Smat.shape[0])
    rank = Smat.shape[0] - nullity
    invsqrteigvals = np.diag([1/np.sqrt(v) for v in eigvals[0:rank]])
    Xmat = np.dot(eigvecs[:, 0:rank], invsqrteigvals)
    return Xmat


def lowdin_pairing(cw: np.ndarray, cx: np.ndarray,
                   sao: np.ndarray, complexsymmetric: bool,
                   thresh_offdiag: float = COMMON_ZERO_TOLERANCE,
                   thresh_zeroov: float = FLOAT_EPS)\
        -> Tuple[np.ndarray, np.ndarray, List[Scalar], List[int]]:
    r"""Perform Löwdin pairing on two coefficient matrices
    :math:`^{w}\boldsymbol{G}` and :math:`^{x}\boldsymbol{G}` such that

    .. math::
        ^{wx}\boldsymbol{\Lambda}
            = \mathrm{diag}(^{wx}\lambda_i)
            = ^{w}\!\tilde{\boldsymbol{G}}^{\blacklozenge}
              \ \boldsymbol{S}_{\mathrm{AO}}
              \ ^{x}\tilde{\boldsymbol{G}},

    where the Löwdin-paired coefficient matrices are given by

    .. math::
        \begin{align}
            ^{w}\!\tilde{\boldsymbol{G}}
                &=\ ^{w}\boldsymbol{G}\ ^{wx}\boldsymbol{U}^{\diamond} \\
            ^{x}\!\tilde{\boldsymbol{G}}
                &=\ ^{x}\boldsymbol{G}\ ^{wx}\boldsymbol{V}
        \end{align}

    with :math:`^{wx}\boldsymbol{U}` and :math:`^{wx}\boldsymbol{V}` being
    SVD factorisation matrices:

    .. math::
        ^{w}\boldsymbol{G}^{\blacklozenge}
        \ \boldsymbol{S}_{\mathrm{AO}}
        \ ^{x}\boldsymbol{G}
        =
        \ ^{wx}\boldsymbol{U}
        \ ^{wx}\boldsymbol{\Lambda}
        \ ^{wx}\boldsymbol{V}^{\dagger}.



    :param cw: Coefficient matrix :math:`^{w}\boldsymbol{G}`.
    :param cx: Coefficient matrix :math:`^{x}\boldsymbol{G}`.
    :param sao: The overlap matrix :math:`\boldsymbol{S}_{\mathrm{AO}}` of the
            underlying atomic basis functions.
    :param complexsymmetric: If :const:`True`, :math:`\diamond = \star`.
            If :const:`False`, :math:`\diamond = \hat{e}`.
            In both cases, :math:`\blacklozenge = \dagger\diamond`.
    :param thresh_offdiag: Threshold to check if the off-diagonal elements in
            the original orbital overlap matrix and in the Löwdin-paired
            orbital overlap matrix :math:`^{wx}\boldsymbol{\Lambda}` are zero.
    :param thresh_zeroov: Threshold to identify which Löwdin overlaps
            :math:`^{wx}\lambda_i` are zero.

    :returns: The value of :math:`S_{wx}`.
    """
    # pylint: disable=C0103,R0913,R0914
    # C0103: The variables are named according to their mathematical symbols.
    # R0913: All arguments are needed to define Löwdin pairing well.
    # R0914: Having more local variables helps keep the code more
    #        understandable.

    assert (cw.shape == cx.shape),\
            f"Coefficient dimensions mismatched: cw{cw.shape} !~ cx{cx.shape}."

    if complexsymmetric:
        init_orb_Smat = np.einsum("ji,jk,kl->il", cw, sao, cx,
                                  optimize='optimal')
    else:
        init_orb_Smat = np.einsum("ji,jk,kl->il", cw.conj(), sao, cx,
                                  optimize='optimal')

    max_offdiag = np.amax(np.abs(init_orb_Smat
                                 - np.diag(np.diag(init_orb_Smat))))

    if max_offdiag <= thresh_offdiag:
        lowdin_overlaps = np.diag(init_orb_Smat).tolist()
        zero_indices = [i for i, ov in enumerate(lowdin_overlaps)
                        if abs(ov) < thresh_zeroov]
        return (cw, cx, lowdin_overlaps, zero_indices)

    U, _, Vh = np.linalg.svd(init_orb_Smat)
    V = Vh.conj().T
    detV = np.linalg.det(V)

    if complexsymmetric:
        cwt = np.dot(cw, U.conj())
        detUc = np.linalg.det(U.conj())
        cwt[:, 0] *= detUc.conj()
    else:
        cwt = np.dot(cw, U)
        detU = np.linalg.det(U)
        cwt[:, 0] *= detU.conj()
    cxt = np.dot(cx, V)
    cxt[:, 0] *= detV.conj()

    if complexsymmetric:
        lowdin_orb_Smat = np.linalg.multi_dot([cwt.T, sao, cxt])
    else:
        lowdin_orb_Smat = np.linalg.multi_dot([cwt.conj().T, sao, cxt])

    # Checking for diagonality after Löwdin pairing
    max_offdiag = np.amax(np.abs(lowdin_orb_Smat
                                 - np.diag(np.diag(lowdin_orb_Smat))))

    assert max_offdiag <= thresh_offdiag,\
            "Löwdin overlap matrix deviates from diagonality. "\
            + f"Maximum off-diagonal overlap has magnitude {max_offdiag:.3e} "\
            + f"> threshold of {thresh_offdiag:.3e}. Löwdin pairing has failed."
    lowdin_overlaps = np.diag(lowdin_orb_Smat).tolist()
    zero_indices = [i for i, ov in enumerate(lowdin_overlaps)
                    if abs(ov) < thresh_zeroov]
    return (cwt, cxt, lowdin_overlaps, zero_indices)
