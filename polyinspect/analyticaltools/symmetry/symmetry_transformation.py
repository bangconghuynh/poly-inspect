""":mod:`.symmetry_transformation` contains public functions to perform symmetry
transformation of SCF determinants.

We define the following types for a compact ``CompositeSymmetryOperation`` data
structure:

    - ``Config = Tuple[Union[RealScalar, Vector], ...]``,
    - ``MOIndices = Sequence[Sequence[int]]``,
    - ``SymmetryOperation = Tuple[str, Optional[Config], Optional[MOIndices]]``,
    - ``CompositeSymmetryOperation = List[SymmetryOperation]``.
"""

# pylint: disable=C0103

from __future__ import annotations

from typing import Tuple, Sequence, Optional, List, Union, cast
from itertools import chain
from scipy.spatial.transform import Rotation  # type: ignore
import numpy as np  # type: ignore
import scipy.linalg  # type: ignore

from polyinspect.auxiliary.ao_basis_space import BasisAngularOrder, get_shell_indices,\
        get_atom_basis_indices
from polyinspect.auxiliary.common_types import Coefficients, MultiStates, RealScalar
from polyinspect.auxiliary.geometrical_space import Vector, proper_rotation_matrix,\
        improper_rotation_matrix

from .angmom3d import Rmat, Rlmat, Dmat_angleaxis_gen, Dmat_Euler_gen
from .shconversion import sh_cart2r, sh_r2cart
from .symmetry_core import Symmetry


# -----
# Types
# -----
# The following type aliases allow for a compact SymmetryOperation and
# CompositeSymmetryOperation sequence to be defined using only Python primitive
# data types.
# SymmetryOperation: ("Code", (config), (mo_indices))
Config = Tuple[Union[RealScalar, Vector], ...]
MOIndices = Sequence[Sequence[int]]
SymmetryOperation = Tuple[str, Optional[Config], Optional[MOIndices]]
CompositeSymmetryOperation = List[SymmetryOperation]

transformation_signatures = ["C", "S", "Q", "-", "E", "K", "T", "SF"]


def _find_n_spin(cs: Coefficients, bao: BasisAngularOrder) -> Tuple[int, int]:
    r"""Determine the number of spin components of the spin-spatial direct
    product basis in which the coefficient matrices are expressed.

    :param cs: Coefficient matrices of a determinant.
            Distinct elements in this list are coefficient matrices for
            distinct spin spaces.

    :returns: A tuple of two integers, the first being the number of spin
            components per spin space, and the second being the total number
            of spin components in the entire basis set, taking into account
            spin constraints.
    """
    shell_indices = get_shell_indices(bao)
    n_spin_spaces = len(cs)
    n_spatial = shell_indices[-1][1] + 1
    if all(c.shape[0] % n_spatial == 0 for c in cs):
        assert len(set(c.shape[0]//n_spatial for c in cs)) == 1,\
            "Inconsistent spin components for different coefficient matrices."
        n_spin_per_matrix = cs[0].shape[0]//n_spatial
    else:
        raise ValueError("Inconsistent AO basis functions and coefficients.")
    return n_spin_per_matrix, n_spin_per_matrix*n_spin_spaces


def _permute_coeffs(cs: Coefficients, perm: Sequence[int],
                    bao: BasisAngularOrder,
                    mo_indices: MOIndices = None)\
                            -> Tuple[Coefficients, BasisAngularOrder]:
    r"""Permute the rows of the coefficient matrices in accordance with the
    permutation of basis functions brought about by a symmetry transformation.
    A corresponding permutation of the basis angular order data structure will
    also be returned.

    :param cs: Coefficient matrices of a determinant.
            Distinct elements in this list are coefficient matrices for
            distinct spin spaces.
    :param perm: A list showing the new indices to which the atoms are moved.
    :param bao: A basis angular order data structure containing all information
            about the angular functions in the basis set of a certain system.
            See :mod:`~src.auxiliary.ao_basis_space` for more details.
    :param mo_indices: A sequence of sequences of indices of the MOs to be
            transformed. If this is :const:`None`, all MOs in all spin spaces
            will be transformed. Each inner sequence corresponds to a distinct
            spin space. If an element in the outer sequence is :const:`None`,
            all MOs in the corresponding spind space will be transformed.

    :returns: Permuted coefficient matrices and basis angular order data
            structure.
    """
    # pylint: disable=R0914
    # R0914: Some variables are defined to make the code more explicit.
    n_spin_per_matrix, _ = _find_n_spin(cs, bao)
    atom_basis_indices = get_atom_basis_indices(bao)
    n_spatial = atom_basis_indices[-1][1] + 1
    assert len(atom_basis_indices) == len(perm),\
        "The number of atoms does not match the number of permutation indices."
    if mo_indices is not None:
        assert len(mo_indices) == len(cs)

    pre_image_atom_indices = [perm.index(i) for i in range(len(perm))]
    permuted_bao = [bao[i] for i in pre_image_atom_indices]
    pre_image_basis_indices_spatial =\
            list(chain.from_iterable([range(atom_basis_indices[i][0],
                                            atom_basis_indices[i][1] + 1)
                                      for i in pre_image_atom_indices]))
    pre_image_basis_indices = [ind + i_spin*n_spatial
                               for i_spin in range(n_spin_per_matrix)
                               for ind in pre_image_basis_indices_spatial]

    if mo_indices is not None:
        permuted_cs = []
        for (c, spin_mo_indices) in zip(cs, mo_indices):
            if spin_mo_indices is not None:
                c_copied = np.copy(c)
                c_copied[:, list(spin_mo_indices)] =\
                    c_copied[:, list(spin_mo_indices)][pre_image_basis_indices]
                permuted_cs.append(c_copied)
            else:
                permuted_cs.append(c[pre_image_basis_indices])
    else:
        permuted_cs = [c[pre_image_basis_indices] for c in cs]

    return permuted_cs, permuted_bao


def _transform_coeffs(cs: Coefficients, perm: Sequence[int],
                      R: np.ndarray, bao: BasisAngularOrder,
                      mo_indices: MOIndices = None,
                      Dmat: Optional[np.ndarray] = None) -> Coefficients:
    r"""Transform coefficient matrices in accordance with the
    transformation of basis functions brought about by a symmetry transformation
    in three dimensions.

    :param cs: Coefficient matrices of a determinant.
            Distinct elements in this list are coefficient matrices for
            distinct spin spaces.
    :param perm: A sequence showing the new indices to which the atoms are
            moved.
    :param R: The representation matrix of the transformation of interest in \
            the basis of the coordinate *functions* :math:`(y, z, x)`, which \
            are proportional to the real spherical harmonics \
            :math:`(Y_{1,-1}, Y_{1,0}, Y_{1,1})`.
    :param bao: A basis angular order data structure containing all information
            about the angular functions in the basis set of a certain system.
            See :mod:`~src.auxiliary.ao_basis_space` for more details.
    :param mo_indices: A sequence of sequences of indices of the MOs to be
            transformed. If this is :const:`None`, all MOs in all spin spaces
            will be transformed. Each inner sequence corresponds to a distinct
            spin space. If an element in the outer sequence is :const:`None`,
            all MOs in the corresponding spind space will be transformed.
    :param Dmat: A Wigner rotation matrix for :math:`j = 1/2`. If this is
            provided and valid, a general spin-spatial rotation will be
            performed, and the coefficient matrices must be expressed in the
            general spin-spatial direct product basis. If this is :const:`None`,
            only a spatial rotation will be performed.

    :returns: Transformed coefficient matrices of this determinant.
    """
    # pylint: disable=R0912,R0913,R0914
    # R0912: This is a low-level function that does not seem to make sense
    #        to be broken down further.
    # R0913: The arguments are needed to fully describe a transformation.
    # R0914: Some variables are defined to make the code more explicit.
    n_spin_per_matrix, _ = _find_n_spin(cs, bao)
    atom_basis_indices = get_atom_basis_indices(bao)
    assert len(atom_basis_indices) == len(perm),\
        "The number of atoms does not match the number of permutation indices."
    if mo_indices is not None:
        assert len(mo_indices) == len(cs)

    cs_permuted, bao_permuted = _permute_coeffs(cs, perm, bao, mo_indices)
    shell_indices_permuted = get_shell_indices(bao_permuted)

    l_max = max(shell[2] for shell in shell_indices_permuted)
    Rls = [np.array([[1]]), R]
    for l in range(2, l_max + 1):
        Rls.append(Rlmat(l, R, Rls[-1]))
    cart2rss_lex = [sh_cart2r(lcart) for lcart in range(l_max + 1)]
    r2cartss_lex = [sh_r2cart(lcart) for lcart in range(l_max + 1)]

    tmat_blocks = []
    for (_, _, l, cart, order) in shell_indices_permuted:
        if cart:
            # Cartesian functions. Convert them to real solid harmonics first
            # before applying the transformation.
            tmat = np.zeros((((l+1)*(l+2))//2, ((l+1)*(l+2))//2))
            order = cast(Optional[Sequence[Tuple[int, int, int]]], order)
            if order is None:
                Xmats = cart2rss_lex[l]
                Wmats = r2cartss_lex[l]
            else:
                Xmats = sh_cart2r(l, cartorder=order, increasingm=True)
                Wmats = sh_r2cart(l, cartorder=order, increasingm=True)
            for i, (Xmat, Wmat) in enumerate(zip(Xmats, Wmats)):
                l_pure = l - 2*i
                tmat += np.linalg.multi_dot([Wmat, Rls[l_pure], Xmat])
        else:
            # Spherical functions.
            tmat = Rls[l]
        tmat_blocks.append(tmat)
    block_diagonal_tmat = scipy.linalg.block_diag(*tmat_blocks)
    if Dmat is not None:
        assert Dmat.shape == (n_spin_per_matrix, n_spin_per_matrix)
        spin_tmat = np.kron(Dmat, block_diagonal_tmat)
    else:
        spin_tmat = block_diagonal_tmat

    if mo_indices is not None:
        transformed_cs = []
        for (c, spin_mo_indices) in zip(cs_permuted, mo_indices):
            if spin_mo_indices is not None:
                maxdtype = max((c.dtype, spin_tmat.dtype),
                               key=lambda dtype: dtype.num,
                               default=np.dtype(np.float64))
                c_copied = c.astype(maxdtype)
                c_copied[:, list(spin_mo_indices)] =\
                    np.dot(spin_tmat, c_copied[:, list(spin_mo_indices)])
                transformed_cs.append(c_copied)
            else:
                transformed_cs.append(np.dot(spin_tmat, c))
    else:
        transformed_cs = [np.dot(spin_tmat, c) for c in cs_permuted]

    return transformed_cs


def get_symequiv(cs: Coefficients,
                 comp_sym_ops: Sequence[CompositeSymmetryOperation],
                 bao: BasisAngularOrder, sym: Symmetry) -> MultiStates:
    r"""Generate symmetry-equivalent determinants from a starting determinant
    and a list of composite symmetry operations.

    For each composite symmetry operation, the constituent elementary
    symmetry operations are applied from right to left.

    :param cs: Coefficient matrices of the starting determinant.
            Distinct elements in this list are coefficient matrices for
            distinct spin spaces.
    :param comp_sym_ops: A sequence of composite symmetry operations.
    :param bao: A basis angular order data structure containing all information
            about the angular functions in the basis set of a certain system.
            See :mod:`~src.auxiliary.ao_basis_space` for more details.
    :param sym: A :class:`~.symmetry_core.Symmetry` object containing the
            symmetry information of the associated molecule.

    :returns: A list of lists of coefficient matrices of symmetry-equivalent
            determinants.
    """
    css: MultiStates = []
    for comp_sym_op in comp_sym_ops:
        tcs = cs
        for sym_op in reversed(comp_sym_op):
            tcs = unified_transform(tcs, sym_op, bao, sym)
        css.append(tcs)
    return css


def unified_transform(cs: Coefficients, sym_op: SymmetryOperation,
                      bao: BasisAngularOrder, sym: Symmetry) -> Coefficients:
    r"""A unified interface function that calls the corresponding
    transformation function based on the instruction given in `sym_op`.

    :param cs: Coefficient matrices of a determinant.
            Distinct elements in this list are coefficient matrices for
            distinct spin spaces.
    :param sym_op: A :attr:`SymmetryOperation` data structure having the
            form
            ``("type_source_spinrot", (config), (mo_indices))``,
            where

            - ``"signature_source_spinrot"`` is a string specifying the
              transformation type, where, in particular,

                - ``signature`` specifies the transformation signature and can be
                  one of

                    - ``C``: proper spatial rotation,
                    - ``S``: improper spatial rotation,
                    - ``Q``: proper spatial rotation of :math:`2\pi`,
                    - ``-``: no spatial rotation,
                    - ``E``: identity,
                    - ``K``: complex conjugation,
                    - ``T``: time reversal, or
                    - ``SF``: spin flip;

                - ``source`` specifies how the symmetry operation is generated
                  and can be one of

                    - ``e``: using a symmetry element in the point group of the
                      molecule as stored in `sym`,
                    - ``g``: using a symmetry generator in the point group of the
                      molecule as stored in `sym`, or
                    - ``-``: using a manually defined angle-axis parameter,

                  noting that this is ignored for signatures ``-``, ``K`` and
                  ``T``.

                - ``spinrot`` specifies if spin rotation is also carried out
                  and can be one of

                    - ``sr``: perform spin rotation, or
                    - ``-``: do not perform spin rotation;

            - ``(config)`` is a tuple of integers specifying the configuration
              of the transformation and whose structure depends on the
              transformation signature, *i.e.*,

                - for signatures ``C`` and ``S``, this takes the form
                  ``(order, index, power)`` where

                    - ``order``: the order of the proper/improper rotation
                      element,
                    - ``index``: the index of the proper/improper rotation
                      element as stored in `sym`,
                    - ``power``: the exponent to which this operation is raised;

                - for signature ``Q``, this takes the form ``(order, index)`` where

                    - ``order``: the order of the proper rotation element,
                    - ``index``: the index of the proper rotation element as
                      stored in `sym`,

                  noting that ``order`` and ``index`` simply serve to identify
                  the axis about which the :math:`2\pi`-rotation takes place;

                - for signature ``-``, spin rotation must be activated in order
                  for the entire transformation to be non-trivial, and therefore
                  ``(config)`` can be

                  - ``(order, index, power)`` for ``source == "g"`` or
                    ``source == "e"`` so that the spin rotation utilises one of
                    the already existing proper rotation axes, or
                  - ``(angle: float, axis: Vector)`` for ``source == "-"`` so
                    that the spin rotation can be performed through an arbitrary
                    angle about an arbitrary axis;

                - for signatures ``E``, ``K`` and ``T``, ``(config)`` is
                  ignored.

            - ``(mo_indices)`` is a tuple of tuples of indices of the MOs to be
              transformed. If this is :const:`None`, all MOs will be
              transformed. Each inner sequence corresponds to a distinct spin
              space. If an element in the outer sequence is :const:`None`,
              all MOs in the corresponding spind space will be transformed.

    :param bao: A basis angular order data structure containing all information
            about the angular functions in the basis set of a certain system.
            See :mod:`~src.auxiliary.ao_basis_space` for more details.
    :param sym: A :class:`~.symmetry_core.Symmetry` object containing the
            symmetry information of the associated molecule.

    :returns: Transformed coefficient matrices.
    """
    (ttype_str, config, mo_indices) = sym_op
    ttype_list = ttype_str.split("_")
    assert len(ttype_list) == 3, "Invalid transformation type syntax."
    [signature, source, spinrot] = ttype_list
    assert signature in transformation_signatures,\
            f"Invalid transformation signature {signature}."
    assert source in ["g", "e", "-"], f"Invalid source choice {source}."
    assert spinrot in ["sr", "-"], f"Invalid spin rotation choice {spinrot}."

    if signature in ["C", "S"]:
        assert config is not None
        tcs = _rotation(signature == "C", cs, source, spinrot == "sr",
                        config, bao, sym, mo_indices)

    elif signature == "Q":
        assert config is not None and len(config) == 2
        (order, index) = config
        assert isinstance(order, int)
        assert isinstance(index, int)
        tcs = _rotation(True, cs, source, spinrot == "sr",
                        (order, index, order), bao, sym, mo_indices)

    elif signature == "-":
        assert spinrot != "-", "This is a trivial transformation."
        assert config is not None
        tcs = _spin_rotation(cs, source, config, bao, sym, mo_indices)

    elif signature == "E":
        tcs = cs

    elif signature == "K":
        tcs = _complex_conjugation(cs, mo_indices)

    elif signature == "T":
        tcs = _time_reversal(cs, bao, mo_indices)

    elif signature == "SF":
        tcs = _spin_flip(cs, bao)

    return tcs


def _rotation(proper: bool, cs: Coefficients, source: str, spinrot: bool,
              config: Config, bao: BasisAngularOrder, sym: Symmetry,
              mo_indices: Optional[MOIndices] = None) -> Coefficients:
    r"""Perform a proper rotation on the supplied coefficients.

    :param proper: A flag to indicate if the rotation is proper or improper.
    :param cs: Coefficient matrices of a determinant.
            Distinct elements in this list are coefficient matrices for
            distinct spin spaces.
    :param source: The source of the symmetry element to be used to generate
            the proper rotation. This can be either ``"e"`` for symmetry element
            or ``"g"`` for symmetry generator of the group.
    :param spinrot: If :const:`True`, the proper rotation will be accompanied
            by an associated spin rotation.
    :param order: The order of the proper rotation element.
    :param index: The index of the proper rotation element as stored in `sym`.
    :param power: The exponent to which this operation is raised which must be
            an integer. However, if ``order == -1``, then this is an operation
            of infinite order, and `power` is taken as the angle of rotation
            in :math:`\pi` radians instead.
    :param bao: A basis angular order data structure containing all information
            about the angular functions in the basis set of a certain system.
            See :mod:`~src.auxiliary.ao_basis_space` for more details.
    :param sym: A :class:`~.symmetry_core.Symmetry` object containing the
            symmetry information of the associated molecule.
    :param mo_indices: A sequence of sequences of indices of the MOs to be
            transformed. If this is :const:`None`, all MOs in all spin spaces
            will be transformed. Each inner sequence corresponds to a distinct
            spin space. If an element in the outer sequence is :const:`None`,
            all MOs in the corresponding spind space will be transformed.

    :returns: Rotated coefficient matrices.
    """
    # pylint: disable=R0912,R0913,R0914
    # R0913: The arguments are needed to fully describe a transformation.
    assert len(config) == 3
    (order, index, power) = config
    assert isinstance(order, int)
    assert isinstance(index, int)
    assert isinstance(power, (int, float))
    if source == "e":
        if proper:
            sym_element_source = sym.proper_elements
        else:
            sym_element_source = sym.improper_elements
    elif source == "g":
        if proper:
            sym_element_source = sym.proper_generators
        else:
            sym_element_source = sym.improper_generators
    else:
        raise ValueError(f'source {source} is neither "g" nor "e".')
    assert order in sym_element_source, f"Order {order} not found."

    if order != -1:
        assert isinstance(power, int)
        angle = 2*np.pi*power/order
    else:
        angle = power*np.pi
    axis = sym_element_source[order][index]

    # From here onwards, angle already takes into account power.
    if proper:
        perm = sym.proper_permutation(angle=angle, axis=axis)
        R = Rmat(angle, axis)
    else:
        perm = sym.improper_permutation(angle=angle, axis=axis)
        R = improper_rotation_matrix(angle, axis)[np.ix_([1, 2, 0], [1, 2, 0])]
    assert perm is not None

    n_spin_per_matrix, n_spin_total = _find_n_spin(cs, bao)
    j = (n_spin_total - 1)/2
    if spinrot:
        assert n_spin_total == n_spin_per_matrix,\
                "Spin rotation can only be performed when the overall spin"\
                + " space is not sub-divided into restricted spin subspaces."
        if proper:
            Dmat: Optional[np.ndarray] = Dmat_angleaxis_gen(j, angle, axis)
        else:
            # We need the spin rotation matrix corresponding to R' where
            # S = iR' with i being the inversion.
            # At the moment, the angle and axis are for R where S = σR.
            # We have: R' = C2*R
            # We then find the Euler angles associated with R' which then
            # allow Dmat to be constructed.
            Rp = np.dot(proper_rotation_matrix(np.pi, axis),
                        proper_rotation_matrix(angle, axis))
            rp = Rotation.from_matrix(Rp)
            euler = rp.as_euler("zyz").tolist()
            Dmat = Dmat_Euler_gen(j, euler)
    else:
        if n_spin_per_matrix == 1:
            Dmat = None
        else:
            Dmat = np.identity(n_spin_per_matrix)

    return _transform_coeffs(cs, perm, R, bao, mo_indices, Dmat)


def _spin_rotation(cs: Coefficients, source: str, config: Config,
                   bao: BasisAngularOrder,
                   sym: Optional[Symmetry] = None,
                   mo_indices: Optional[MOIndices] = None) -> Coefficients:
    r"""Perform a pure spin rotation on the supplied coefficients.

    .. note::
        Spin rotation can only be performed when the overall spin space is not
        sub-divided into restricted spin subspaces.

    :param cs: Coefficient matrix of a determinant. See **Note** above.
    :param source: The source of the *spatial* symmetry element to be used to
            generate the spin rotation. This can be either

            - ``"e"`` for *proper* symmetry element,
            - ``"g"`` for *proper* symmetry generator, or
            - ``"-"`` for a manual specification of the angle and axis of spin
              rotation.

    :param config: The configuration of the spin rotation. This can be either

            - ``(order, index, power)`` for ``source == "g"`` or
              ``source == "e"`` so that the spin rotation utilises one of the
              already existing proper rotation axes, or
            - ``(angle: float, axis: Vector)`` for ``source == "-"`` so that
              the spin rotation can be performed through an arbitrary
              angle about an arbitrary axis.

    :param bao: A basis angular order data structure containing all information
            about the angular functions in the basis set of a certain system.
            See :mod:`~src.auxiliary.ao_basis_space` for more details.
    :param sym: A :class:`~.symmetry_core.Symmetry` object containing the
            symmetry information of the associated molecule. This is only
            needed if ``source == "g"`` or ``source == "e"``.
    :param mo_indices: A sequence of sequences of indices of the MOs to be
            transformed. If this is :const:`None`, all MOs in all spin spaces
            will be transformed. Each inner sequence corresponds to a distinct
            spin space. If an element in the outer sequence is :const:`None`,
            all MOs in the corresponding spind space will be transformed.

    :returns: Rotated coefficient matrices.
    """
    # pylint: disable=R0913,R0914
    # R0913: The arguments are needed to fully describe a spin rotation.
    # R0914: Some variables are defined to make the code more explicit.
    n_spin_per_matrix, n_spin_total = _find_n_spin(cs, bao)
    assert n_spin_total == n_spin_per_matrix and len(cs) == 1,\
            "Spin rotation can only be performed when the overall spin"\
            + " space is not sub-divided into restricted spin subspaces."
    j = (n_spin_total - 1)/2

    if source in ["e", "g"]:
        assert len(config) == 3
        assert sym is not None
        (order, index, power) = config
        assert isinstance(order, int)
        assert isinstance(index, int)
        if source == "e":
            sym_element_source = sym.proper_elements
        else:
            sym_element_source = sym.proper_generators
        assert order in sym_element_source, f"Order {order} not found."

        if order != -1:
            assert isinstance(power, int)
            angle = 2*np.pi*power/order
        else:
            assert isinstance(power, (int, float))
            angle = power*np.pi
        axis = sym_element_source[order][index]

    else:
        assert source == "-"
        assert len(config) == 2
        assert isinstance(config[0], (int, float))
        assert isinstance(config[1], Vector)
        angle = config[0]
        axis = config[1]

    Dmat = Dmat_angleaxis_gen(j, angle, axis)
    assert Dmat.shape == (n_spin_per_matrix, n_spin_per_matrix)

    shell_indices = get_shell_indices(bao)
    n_spatial = shell_indices[-1][1] + 1
    spin_tmat = np.kron(Dmat, np.identity(n_spatial))

    if mo_indices is not None:
        transformed_cs = []
        for (c, spin_mo_indices) in zip(cs, mo_indices):
            if spin_mo_indices is not None:
                maxdtype = max((c.dtype, spin_tmat.dtype),
                               key=lambda dtype: dtype.num,
                               default=np.dtype(np.float64))
                c_copied = c.astype(maxdtype)
                c_copied[:, list(spin_mo_indices)] =\
                    np.dot(spin_tmat, c_copied[:, list(spin_mo_indices)])
                transformed_cs.append(c_copied)
            else:
                transformed_cs.append(np.dot(spin_tmat, c))
    else:
        transformed_cs = [np.dot(spin_tmat, c) for c in cs]

    return transformed_cs


def _complex_conjugation(cs: Coefficients,
                         mo_indices: Optional[MOIndices] = None)\
        -> Coefficients:
    r"""Take the complex conjugation of the supplied coefficients.

    :param cs: Coefficient matrices of a determinant.
            Distinct elements in this list are coefficient matrices for
            distinct spin spaces.
    :param mo_indices: A sequence of sequences of indices of the MOs to be
            transformed. If this is :const:`None`, all MOs in all spin spaces
            will be transformed. Each inner sequence corresponds to a distinct
            spin space. If an element in the outer sequence is :const:`None`,
            all MOs in the corresponding spind space will be transformed.

    :returns: Complex-conjugated coefficient matrices.
    """
    if mo_indices is not None:
        transformed_cs = []
        for (c, spin_mo_indices) in zip(cs, mo_indices):
            if spin_mo_indices is not None:
                c_copied = np.copy(c)
                c_copied[:, list(spin_mo_indices)] =\
                    c_copied[:, list(spin_mo_indices)].conj()
                transformed_cs.append(c_copied)
            else:
                transformed_cs.append(c.conj())
    else:
        transformed_cs = [c.conj() for c in cs]

    return transformed_cs


def _time_reversal(cs: Coefficients, bao: BasisAngularOrder,
                   mo_indices: Optional[MOIndices] = None) -> Coefficients:
    r"""Take the time reversal of the supplied coefficients.

    The time reversal operator is given for an :math:`N_{\mathrm{e}}`-electron
    wavefunction by

    .. math::
        \hat{\Theta} = \hat{R}\left(\pi \hat{\boldsymbol{y}} \right) \hat{K},

    where :math:`\hat{K}` is the complex conjugation operator, and
    :math:`\hat{R}\left(\pi \hat{\boldsymbol{y}} \right)` a spin rotation by
    :math:`\pi` about the space-fixed :math:`y`-axis.

    .. note::
        Since time reversal involves a spin rotation, it can only be performed
        when the overall spin space is not sub-divided into restricted spin
        subspaces.


    :param cs: Coefficient matrix of a determinant. See **Note** above.
    :param bao: A basis angular order data structure containing all information
            about the angular functions in the basis set of a certain system.
            See :mod:`~src.auxiliary.ao_basis_space` for more details.
    :param mo_indices: A sequence of sequences of indices of the MOs to be
            transformed. If this is :const:`None`, all MOs in all spin spaces
            will be transformed. Each inner sequence corresponds to a distinct
            spin space. If an element in the outer sequence is :const:`None`,
            all MOs in the corresponding spind space will be transformed.

    :returns: Complex-conjugated coefficient matrices.
    """
    conj_cs = _complex_conjugation(cs, mo_indices)
    timerev_cs = _spin_rotation(conj_cs, "-", (np.pi, Vector([0, 1, 0])),
                                bao, mo_indices=mo_indices)
    return timerev_cs


def _spin_flip(cs: Coefficients, bao: BasisAngularOrder) -> Coefficients:
    r"""Perform spin flips on the supplied coefficients.

    The spin projection quantum number :math:`m_s` of each MO becomes
    :math:`-m_s` after spin flip.


    :param cs: Coefficient matrices of a determinant.
            Distinct elements in this list are coefficient matrices for
            distinct spin spaces.
            For spin flip to make sence, each spin space must contain only one
            spin component, and the spin spaces must be ordered according to
            their :math:`m_s` values.
    :param bao: A basis angular order data structure containing all information
            about the angular functions in the basis set of a certain system.
            See :mod:`~src.auxiliary.ao_basis_space` for more details.
    :param mo_indices: A sequence of sequences of indices of the MOs to be
            transformed. If this is :const:`None`, all MOs in all spin spaces
            will be transformed. Each inner sequence corresponds to a distinct
            spin space. If an element in the outer sequence is :const:`None`,
            all MOs in the corresponding spind space will be transformed.

    :returns: Complex-conjugated coefficient matrices.
    """
    n_spin_per_matrix, _ = _find_n_spin(cs, bao)
    assert n_spin_per_matrix == 1
    return list(reversed(cs))
