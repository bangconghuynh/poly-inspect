""":mod:`.angmom3d` contains functions to  to perform
three-dimensional transformations of spherical harmonics and spinors.
"""

from __future__ import annotations

from typing import Sequence, Tuple
from math import sqrt
import numpy as np  # type:ignore
from scipy.spatial.transform import Rotation  # type: ignore

from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE
from polyinspect.auxiliary.geometrical_space import Vector
from .shconversion import _fact


# Three-dimensional rotations of spherical harmonics and spinors.
# For the rotation of real spherical harmonics, a recursive implementation
# based on :cite:`article:Ivanic1996` and :cite:`article:Ivanic1998` is adopted.

def _kdelta(i: int, j: int) -> int:
    r"""Return the Kronecker delta :math:`\delta_{ij}`.

    :param i: Index :math:`i`.
    :param j: Index :math:`j`.

    :returns: 1 if :math:`i = j`, 0 if :math:`i \neq j`.
    """
    return int(i == j)


def _func_P(i: int, l: int, mu: int, mdash: int,
            R: np.ndarray, Rlm1: np.ndarray) -> float:
    r"""Return the function :math:`_iP^l_{\mu m'}` as defined in Table 2 of
    :cite:`article:Ivanic1996` and :cite:`article:Ivanic1998`.

    :param i: Index :math:`i` satisfying :math:`-1 \le i \le 1`.
    :param l: Spherical harmonic order :math:`l`.
    :param mu: Index :math:`\mu` satisfying :math:`-l+1 \le \mu \le l-1`.
    :param mdash: Index :math:`m'` satisfying :math:`-l \le m' \le l`.
    :param R: The representation matrix of the transformation of interest in \
            the basis of the coordinate *functions* :math:`(y, z, x)`, which \
            are proportional to the real spherical harmonics \
            :math:`(Y_{1,-1}, Y_{1,0}, Y_{1,1})`.
    :param Rlm1: The representation matrix of the transformation of interest \
            in the basis of the real spherical harmonics :math:`Y_{l-1,m}` \
            ordered by increasing :math:`m`.

    :returns: The value of :math:`_iP^l_{\mu m'}`.
    """
    # pylint: disable=C0103,R0913
    # C0103: The _func_P is named in accordance with its mathematical symbols
    #        in the paper.
    # R0913: The number of arguments is necessary to specify this function.

    assert abs(i) <= 1
    assert l >= 0
    assert abs(mu) <= l-1, f"Index mu = {mu} lies outside [{-l+1}, {l-1}]."
    assert abs(mdash) <= l, f"Index mdash = {mdash} lies outside [{-l}, {l}]."
    assert R.shape == (3, 3), "R must be a 3*3 matrix."
    assert Rlm1.shape == (2*l-1, 2*l-1),\
            f"Rlm1 must be a {2*l-1}*{2*l-1} matrix."

    if mdash == l:
        return (R[i+1, 1+1] * Rlm1[mu+(l-1), l-1+(l-1)]
                - R[i+1, -1+1] * Rlm1[mu+(l-1), -l+1+(l-1)])
    if mdash == -l:
        return (R[i+1, 1+1] * Rlm1[mu+(l-1), -l+1+(l-1)]
                + R[i+1, -1+1] * Rlm1[mu+(l-1), l-1+(l-1)])
    return R[i+1, 0+1] * Rlm1[mu+(l-1), mdash+(l-1)]


def _func_U(l: int, m: int, mdash: int,
            R: np.ndarray, Rlm1: np.ndarray) -> float:
    r"""Return the function :math:`U^l_{mm'}` as defined in Table 2 of
    :cite:`article:Ivanic1996` and :cite:`article:Ivanic1998`.

    :param l: Spherical harmonic order :math:`l`.
    :param m: Index :math:`m` satisfying :math:`-l+1 \le m \le l-1`.
    :param mdash: Index :math:`m'` satisfying :math:`-l \le m' \le l`.
    :param R: The representation matrix of the transformation of interest in \
            the basis of the coordinate *functions* :math:`(y, z, x)`, which \
            are proportional to the real spherical harmonics \
            :math:`(Y_{1,-1}, Y_{1,0}, Y_{1,1})`.
    :param Rlm1: The representation matrix of the transformation of interest \
            in the basis of the real spherical harmonics :math:`Y_{l-1,m}` \
            ordered by increasing :math:`m`.

    :returns: The value of :math:`U^l_{mm'}`.
    """
    # pylint: disable=C0103
    # C0103: The _func_U is named in accordance with its mathematical symbols
    #        in the paper.
    assert l >= 0
    assert abs(m) <= l-1
    return _func_P(0, l, m, mdash, R, Rlm1)


def _func_V(l: int, m: int, mdash: int,
            R: np.ndarray, Rlm1: np.ndarray) -> float:
    r"""Return the function :math:`V^l_{mm'}` as defined in Table 2 of
    :cite:`article:Ivanic1996` and :cite:`article:Ivanic1998`.

    :param l: Spherical harmonic order :math:`l`.
    :param m: Index :math:`m` satisfying :math:`-l \le m \le l`.
    :param mdash: Index :math:`m'` satisfying :math:`-l \le m' \le l`.
    :param R: The representation matrix of the transformation of interest in \
            the basis of the coordinate *functions* :math:`(y, z, x)`, which \
            are proportional to the real spherical harmonics \
            :math:`(Y_{1,-1}, Y_{1,0}, Y_{1,1})`.
    :param Rlm1: The representation matrix of the transformation of interest \
            in the basis of the real spherical harmonics :math:`Y_{l-1,m}` \
            ordered by increasing :math:`m`.

    :returns: The value of :math:`V^l_{mm'}`.
    """
    # pylint: disable=C0103
    # C0103: The _func_V is named in accordance with its mathematical symbols
    #        in the paper.
    assert l >= 0
    assert abs(m) <= l
    if m > 0:
        return (_func_P(1, l, m-1, mdash, R, Rlm1)*sqrt(1 + _kdelta(m, 1))
                - _func_P(-1, l, -m+1, mdash, R, Rlm1)*(1 - _kdelta(m, 1)))
    if m < 0:
        return (_func_P(1, l, m+1, mdash, R, Rlm1)*(1 - _kdelta(m, -1))
                + _func_P(-1, l, -m-1, mdash, R, Rlm1)\
                        *sqrt(1 + _kdelta(m, -1)))
    return (_func_P(1, l, 1, mdash, R, Rlm1)
            + _func_P(-1, l, -1, mdash, R, Rlm1))


def _func_W(l: int, m: int, mdash: int,
            R: np.ndarray, Rlm1: np.ndarray) -> float:
    r"""Return the function :math:`W^l_{mm'}` as defined in Table 2 of
    :cite:`article:Ivanic1996` and :cite:`article:Ivanic1998`.

    :param l: Spherical harmonic order :math:`l`.
    :param m: Index :math:`m` satisfying :math:`-l+2 \le m \le l-2` and \
            :math:`m \neq 0`.
    :param mdash: Index :math:`m'` satisfying :math:`-l \le m' \le l`.
    :param R: The representation matrix of the transformation of interest in \
            the basis of the coordinate *functions* :math:`(y, z, x)`, which \
            are proportional to the real spherical harmonics \
            :math:`(Y_{1,-1}, Y_{1,0}, Y_{1,1})`.
    :param Rlm1: The representation matrix of the transformation of interest \
            in the basis of the real spherical harmonics :math:`Y_{l-1,m}` \
            ordered by increasing :math:`m`.

    :returns: The value of :math:`W^l_{mm'}`.
    """
    # pylint: disable=C0103
    # C0103: The _func_W is named in accordance with its mathematical symbols
    #        in the paper.
    assert l >= 0
    assert m != 0 and abs(m) <= l-2
    if m > 0:
        return (_func_P(1, l, m+1, mdash, R, Rlm1)
                + _func_P(-1, l, -m-1, mdash, R, Rlm1))
    return (_func_P(1, l, m-1, mdash, R, Rlm1)
            - _func_P(-1, l, -m+1, mdash, R, Rlm1))


def _coeff_u(l: int, m: int, mdash: int) -> float:
    r"""Return the coefficient :math:`u^l_{mm'}` as defined in Table 1 of
    :cite:`article:Ivanic1996` and :cite:`article:Ivanic1998`.

    :param l: Spherical harmonic order :math:`l`.
    :param m: Index :math:`m` satisfying :math:`-l \le m \le l`.
    :param mdash: Index :math:`m'` satisfying :math:`-l \le m' \le l`.

    :returns: The value of :math:`u^l_{mm'}`.
    """
    # pylint: disable=C0103
    # C0103: The _coeff_u is named in accordance with its mathematical symbols
    #        in the paper.
    assert l >= 0
    assert abs(m) <= l and abs(mdash) <= l
    num = (l+m)*(l-m)
    if abs(mdash) < l:
        den = (l+mdash)*(l-mdash)
    else:
        den = (2*l)*(2*l-1)
    return sqrt(num/den)


def _coeff_v(l: int, m: int, mdash: int) -> float:
    r"""Return the coefficient :math:`v^l_{mm'}` as defined in Table 1 of
    :cite:`article:Ivanic1996` and :cite:`article:Ivanic1998`.

    :param l: Spherical harmonic order :math:`l`.
    :param m: Index :math:`m` satisfying :math:`-l \le m \le l`.
    :param mdash: Index :math:`m'` satisfying :math:`-l \le m' \le l`.

    :returns: The value of :math:`v^l_{mm'}`.
    """
    # pylint: disable=C0103
    # C0103: The _coeff_v is named in accordance with its mathematical symbols
    #        in the paper.
    assert l >= 0
    assert abs(m) <= l and abs(mdash) <= l

    num = (1 + _kdelta(m, 0)) * (l + abs(m) - 1) * (l + abs(m))
    if abs(mdash) < l:
        den = (l + mdash) * (l - mdash)
    else:
        den = (2*l) * (2*l - 1)
    return 0.5 * sqrt(num/den) * (1 - 2*_kdelta(m, 0))


def _coeff_w(l: int, m: int, mdash: int) -> float:
    r"""Return the coefficient :math:`w^l_{mm'}` as defined in Table 1 of
    :cite:`article:Ivanic1996` and :cite:`article:Ivanic1998`.

    :param l: Spherical harmonic order :math:`l`.
    :param m: Index :math:`m` satisfying :math:`-l \le m \le l`.
    :param mdash: Index :math:`m'` satisfying :math:`-l \le m' \le l`.

    :returns: The value of :math:`w^l_{mm'}`.
    """
    # pylint: disable=C0103
    # C0103: The _coeff_w is named in accordance with its mathematical symbols
    #        in the paper.
    assert l >= 0
    assert abs(m) <= l and abs(mdash) <= l

    num = (l - abs(m) - 1) * (l - abs(m))
    if abs(mdash) < l:
        den = (l + mdash) * (l - mdash)
    else:
        den = (2*l) * (2*l - 1)
    return -0.5 * sqrt(num/den) * (1 - _kdelta(m, 0))


def Rmat(angle: float, axis: Vector) -> np.ndarray:
    r"""Return the representation matrix :math:`\boldsymbol{R}` for a rotation
    in the basis of the coordinate *functions* :math:`(y, z, x)`.

    Let :math:`\hat{R}(\phi, \hat{\boldsymbol{n}})` be a rotation parametrised
    by the angle :math:`\phi` and axis :math:`\hat{\boldsymbol{n}}`.
    The corresponding representation matrix
    :math:`\boldsymbol{R}(\phi, \hat{\boldsymbol{n}})` is defined as

    .. math::
        \hat{R}(\phi, \hat{\boldsymbol{n}})\ (y, z, x)
        = (y, z, x) \boldsymbol{R}(\phi, \hat{\boldsymbol{n}})

    See Section **2**-4 of :cite:`book:Altmann1986` for a detailed discussion on
    how :math:`(y, z, x)` should be considered as coordinate *functions*.

    :param angle: The angle :math:`\phi` of the rotation in radians. A positive
            rotation is an anticlockwise rotation when looking down `axis`.
    :param axis: A space-fixed vector defining the axis of rotation. The
            supplied vector will be normalised.

    :returns: The representation matrix
            :math:`\boldsymbol{R}(\phi, \hat{\boldsymbol{n}})`.
    """
    # pylint: disable=C0103
    # C0103: Rmat is named in accordance with its mathematical symbols
    #        in the paper.
    assert axis.dim == 3
    axis = axis.normalise()
    rot = Rotation.from_rotvec(angle*axis.components)
    return rot.as_matrix()[np.ix_([1, 2, 0], [1, 2, 0])]


def Rlmat(l: int, R: np.ndarray, Rlm1: np.ndarray) -> np.ndarray:
    r"""Return the representation matrix :math:`\boldsymbol{R}^l` for a
    transformation of interest in the basis of real spherical harmonics
    :math:`Y_{lm}` ordered by increasing :math:`m`, as defined in Equation 5.8
    and given in Equation 8.1 of :cite:`article:Ivanic1996`.


    :param l: Spherical harmonic order :math:`l \ge 2`.
    :param R: The representation matrix of the transformation of interest in \
            the basis of the coordinate *functions* :math:`(y, z, x)`, which \
            are proportional to the real spherical harmonics \
            :math:`(Y_{1,-1}, Y_{1,0}, Y_{1,1})`.
    :param Rlm1: The representation matrix of the transformation of interest \
            in the basis of the real spherical harmonics :math:`Y_{l-1,m}` \
            ordered by increasing :math:`m`.

    :returns: The matrix :math:`\boldsymbol{R}^l`.
    """
    # pylint: disable=C0103
    # C0103: Rlmat is named in accordance with its mathematical symbols
    #        in the paper.
    assert l >= 2
    Rl = np.zeros((2*l+1, 2*l+1))
    for mi in range(2*l+1):
        m = mi - l
        for mdashi in range(2*l+1):
            mdash = mdashi - l

            cu = _coeff_u(l, m, mdash)
            fU = 0.0
            if abs(cu) > COMMON_ZERO_TOLERANCE:
                fU = _func_U(l, m, mdash, R, Rlm1)

            cv = _coeff_v(l, m, mdash)
            fV = 0.0
            if abs(cv) > COMMON_ZERO_TOLERANCE:
                fV = _func_V(l, m, mdash, R, Rlm1)

            cw = _coeff_w(l, m, mdash)
            fW = 0.0
            if abs(cw) > COMMON_ZERO_TOLERANCE:
                fW = _func_W(l, m, mdash, R, Rlm1)

            Rl[mi, mdashi] = cu*fU + cv*fV + cw*fW
    return Rl


def _Dmat_Euler_elem(mdashi: int, mi: int,
                     Euler_angles: Sequence[float]) -> float:
    r"""Return an element in the Wigner rotation matrix for :math:`j = 1/2`,
    defined by

    .. math::
        \hat{R}(\alpha, \beta, \gamma) \ket{\tfrac{1}{2}m}
        = \sum_{m'} \ket{\tfrac{1}{2}m'} D^{(1/2)}_{m'm}(\alpha, \beta, \gamma).

    :param mdashi: Index for :math:`m'` given by :math:`m'+\tfrac{1}{2}`.
    :param mi: Index for :math:`m` given by :math:`m+\tfrac{1}{2}`.
    :param Euler_angles: The sequence containing the Euler angles
            :math:`(\alpha, \beta, \gamma)` in radians, following the Whitaker
            convention.

    :returns: The element :math:`D^{(1/2)}_{m'm}(\alpha, \beta, \gamma)`.
    """
    # pylint: disable=C0103
    # C0103: The variables are named according to their mathematical symbols.
    assert 0 <= mdashi <= 1 and 0 <= mi <= 1
    assert len(Euler_angles) == 3
    (alpha, beta, gamma) = tuple(Euler_angles)
    d = 0.0
    if mi == 1:  # m = 1/2
        if mdashi == 1:  # mdash == 1/2
            d = np.cos(beta/2)
        else:  # mdash == -1/2
            d = np.sin(beta/2)
    else:  # m = -1/2
        if mdashi == 1:  # mdash == 1/2
            d = -np.sin(beta/2)
        else:  # mdash == -1/2
            d = np.cos(beta/2)
    return np.exp(-1j*alpha*(mdashi-0.5) - 1j*gamma*(mi-0.5))*d


def Dmat_Euler(Euler_angles: Sequence[float],
               increasingm: bool = False) -> np.ndarray:
    r"""Return the Wigner rotation matrix for :math:`j = 1/2` whose elements are
    defined by

    .. math::
        \hat{R}(\alpha, \beta, \gamma) \ket{\tfrac{1}{2}m}
        = \sum_{m'} \ket{\tfrac{1}{2}m'} D^{(1/2)}_{m'm}(\alpha, \beta, \gamma).

    :param Euler_angles: The sequence containing the Euler angles
            :math:`(\alpha, \beta, \gamma)` in radians, following the Whitaker
            convention, *i.e.*, :math:`z_2-y-z_1` (extrinsic rotations).
    :param increasingm: If :const:`True`, the rows and columns of
            :math:`\boldsymbol{D}^{(1/2)}` are arranged in increasing
            order of :math:`m`. If :const:`False`, the order is reversed.
            The default is :const:`False`, in accordance with the conventional
            order of :math:`\left( \ket{\tfrac{1}{2},\tfrac{1}{2}},
            \ \ket{\tfrac{1}{2},-\tfrac{1}{2}} \right)`.

    :returns: The matrix :math:`\boldsymbol{D}^{(1/2)}(\alpha, \beta, \gamma)`.
    """
    # pylint: disable=C0103
    # C0103: The variables are named according to their mathematical symbols.
    D = np.zeros(shape=(2, 2), dtype=np.complex128)
    for mdashi in range(2):
        for mi in range(2):
            D[mdashi, mi] = _Dmat_Euler_elem(mdashi, mi, Euler_angles)

    if not increasingm:
        D = np.flip(np.flip(D, axis=1), axis=0)
    return D


def Dmat_angleaxis(angle: float, axis: Vector,
                   increasingm: bool = False) -> np.ndarray:
    r"""Return the Wigner rotation matrix for :math:`j = 1/2` whose elements are
    defined by

    .. math::
        \hat{R}(\phi\hat{\boldsymbol{n}}) \ket{\tfrac{1}{2}m}
        = \sum_{m'} \ket{\tfrac{1}{2}m'}
                    D^{(1/2)}_{m'm}(\phi\hat{\boldsymbol{n}}).

    The parametrisation of :math:`\boldsymbol{D}^{(1/2)}` by
    :math:`\phi` and :math:`\hat{\boldsymbol{n}}` is given in (**4**-9.12) of
    :cite:`book:Altmann1986`.

    :param angle: The angle :math:`\phi` of the rotation in radians. A positive
            rotation is an anticlockwise rotation when looking down `axis`.
    :param axis: A space-fixed vector defining the axis of rotation. The
            supplied vector will be normalised.
    :param increasingm: If :const:`True`, the rows and columns of
            :math:`\boldsymbol{D}^{(1/2)}` are arranged in increasing
            order of :math:`m`. If :const:`False`, the order is reversed.
            The default is :const:`False`, in accordance with the conventional
            order of :math:`\left( \ket{\tfrac{1}{2},\tfrac{1}{2}},
            \ \ket{\tfrac{1}{2},-\tfrac{1}{2}} \right)`.

    :returns: The Wigner rotation matrix
            :math:`\boldsymbol{D}^{(1/2)}(\phi, \hat{\boldsymbol{n}})`.
    """
    # pylint: disable=C0103
    # C0103: Rmat is named in accordance with its mathematical symbols
    #        in the paper.
    assert axis.dim == 3
    axis = axis.normalise()
    (nx, ny, nz) = (axis[0], axis[1], axis[2])

    D = np.zeros(shape=(2, 2), dtype=np.complex128)
    # Calculate the elements explicitly
    # For consistency with the rest of the code, we use increasing m ordering
    # in these assignments. We will then flip the matrix later, if need be, so
    # as to be consistent with the conventional (alpha, beta) ordering.
    D[0, 0] = np.cos(angle/2) + 1j*nz*np.sin(angle/2)
    D[0, 1] = (ny - 1j*nx)*np.sin(angle/2)
    D[1, 0] = -(ny + 1j*nx)*np.sin(angle/2)
    D[1, 1] = np.cos(angle/2) - 1j*nz*np.sin(angle/2)

    if not increasingm:
        D = np.flip(np.flip(D, axis=1), axis=0)
    return D


def _Dmat_Euler_gen_elem(j: float, mdashi: int, mi: int,
                         Euler_angles: Sequence[float]) -> float:
    r"""Return an element in the Wigner rotation matrix for an integral or
    half-integral :math:`j`, defined by

    .. math::
        \hat{R}(\alpha, \beta, \gamma) \ket{jm}
        = \sum_{m'} \ket{jm'} D^{(j)}_{m'm}(\alpha, \beta, \gamma).

    The explicit expression for the elements of
    :math:`\boldsymbol{D}^{(1/2)}(\alpha, \beta, \gamma)` is given in
    :cite:`notes:Stone2006`.

    :param j: The angular momentum :math:`j`, which can be an integer or a
            half-integer.
    :param mdashi: Index for :math:`m'` given by :math:`m'+j`.
    :param mi: Index for :math:`m` given by :math:`m+j`.
    :param Euler_angles: The sequence containing the Euler angles
            :math:`(\alpha, \beta, \gamma)` in radians, following the Whitaker
            convention.

    :returns: The element :math:`D^{(j)}_{m'm}(\alpha, \beta, \gamma)`.
    """
    # pylint: disable=C0103,R0914
    # C0103: The variables are named according to their mathematical symbols.
    # R0914: The extra local variables are to facilitate clarity in code.
    assert 0 <= mdashi <= 2*j and 0 <= mi <= 2*j
    d = 0.0
    assert len(Euler_angles) == 3
    (alpha, beta, gamma) = tuple(Euler_angles)
    mdash = mdashi - j
    m = mi - j

    prefactor = np.exp(-1j*alpha*mdash - 1j*gamma*m)
    tmax = min(int(j+mdash), int(j-m))
    tmin = max(0, int(mdash-m))
    d = 0.0
    for t in range(tmin, tmax+1):
        num = sqrt(_fact(int(j+mdash))
                   *_fact(int(j-mdash))
                   *_fact(int(j+m))
                   *_fact(int(j-m)))
        den = _fact(int(j+mdash-t))\
              *_fact(int(j-m-t))\
              *_fact(t)\
              *_fact(int(t+m-mdash))
        trigfactor = np.power(np.cos(beta/2), int(2*j+mdash-m-2*t))\
                     *np.power(np.sin(beta/2), int(2*t-mdash+m))

        d += ((-1)**t)*(num/den)*trigfactor

    return prefactor*d


def Dmat_Euler_gen(j: float, Euler_angles: Sequence[float],
                   increasingm: bool = False) -> np.ndarray:
    r"""Return the Wigner rotation matrix for an integral or half-integral
    :math:`j` whose elements are defined by

    .. math::
        \hat{R}(\alpha, \beta, \gamma) \ket{jm}
        = \sum_{m'} \ket{jm'} D^{(j)}_{m'm}(\alpha, \beta, \gamma).

    The explicit expression for the elements of
    :math:`\boldsymbol{D}^{(1/2)}(\alpha, \beta, \gamma)` is given in
    :cite:`notes:Stone2006`.

    :param j: The angular momentum :math:`j`, which can be an integer or a\
            half-integer.
    :param Euler_angles: The sequence containing the Euler angles\
            :math:`(\alpha, \beta, \gamma)` in radians, following the Whitaker
            convention.
    :param increasingm: If :const:`True`, the rows and columns of \
            :math:`\boldsymbol{D}^{(1/2)}` are arranged in increasing \
            order of :math:`m`. If :const:`False`, the order is reversed. \
            The default is :const:`False`.

    :returns: The matrix :math:`\boldsymbol{D}^{(j)}(\alpha, \beta, \gamma)`.
    """
    # pylint: disable=C0103
    # C0103: The variables are named according to their mathematical symbols.
    assert abs(2*j - int(2*j)) < COMMON_ZERO_TOLERANCE
    D = np.zeros(shape=(int(2*j+1), int(2*j+1)), dtype=np.complex128)
    for mdashi in range(int(2*j+1)):
        for mi in range(int(2*j+1)):
            D[mdashi, mi] = _Dmat_Euler_gen_elem(j, mdashi, mi, Euler_angles)

    if not increasingm:
        D = np.flip(np.flip(D, axis=1), axis=0)
    return D


def Dmat_angleaxis_gen(j: float, angle: float, axis: Vector,
                       increasingm: bool = False) -> np.ndarray:
    r"""Return the Wigner rotation matrix for an integral or half-integral
    :math:`j` whose elements are defined by

    .. math::
        \hat{R}(\phi\hat{\boldsymbol{n}}) \ket{jm}
        = \sum_{m'} \ket{jm'}
                    D^{(j)}_{m'm}(\phi\hat{\boldsymbol{n}}).

    :param j: The angular momentum :math:`j`, which can be an integer or a
            half-integer.
    :param angle: The angle :math:`\phi` of the rotation in radians. A positive
            rotation is an anticlockwise rotation when looking down `axis`.
    :param axis: A space-fixed vector defining the axis of rotation. The
            supplied vector will be normalised.
    :param increasingm: If :const:`True`, the rows and columns of
            :math:`\boldsymbol{D}^{(1/2)}` are arranged in increasing
            order of :math:`m`. If :const:`False`, the order is reversed.
            The default is :const:`False`, in accordance with the conventional
            order of :math:`\left( \ket{\tfrac{1}{2},\tfrac{1}{2}},
            \ \ket{\tfrac{1}{2},-\tfrac{1}{2}} \right)`.

    :returns: The Wigner rotation matrix
            :math:`\boldsymbol{D}^{(j)}(\phi, \hat{\boldsymbol{n}})`.
    """
    # pylint: disable=C0103
    # C0103: Rmat is named in accordance with its mathematical symbols
    #        in the paper.
    assert axis.dim == 3
    axis = axis.normalise()
    Euler_angles = _angleaxis_to_Euler(angle, axis)
    return Dmat_Euler_gen(j, Euler_angles, increasingm)


def _angleaxis_to_Euler(angle: float, axis: Vector)\
        -> Tuple[float, float, float]:
    r"""Convert an angle and axis of rotation to Euler angles using Equations
    (**3**-5.4) to (**3**-5.10) in :cite:`book:Altmann1986`, but with an
    extended range,

    .. math::
        0 \le \alpha \le 2\pi, \quad
        0 \le \beta \le \pi, \quad
        0 \le \gamma \le 4\pi,

    such that all angle-axis parametrisations of
    :math:`\phi\hat{\boldsymbol{n}}` for :math:`0 \le \phi \le 4\pi` are mapped
    to unique triplets of :math:`(\alpha, \beta, \gamma)`, as explained in
    :cite:`article:Fan1999`.

    When :math:`\beta = 0`, only the sum :math:`\alpha+\gamma` is determined.
    Likewise, when :math:`\beta = \pi`, only the difference
    :math:`\alpha-\gamma` is determined.
    We thus set :math:`\alpha = 0` in these cases and solve for :math:`\gamma`
    without changing the nature of the results.

    :param angle: The angle :math:`\phi` of the rotation in radians. A positive
            rotation is an anticlockwise rotation when looking down `axis`.
    :param axis: A space-fixed vector defining the axis of rotation
            :math:`\hat{\boldsymbol{n}}`. The supplied vector will be
            normalised.

    :returns: The tuple containing the Euler angles
            :math:`(\alpha, \beta, \gamma)` in radians, following the Whitaker
            convention.
    """
    # pylint: disable=C0103
    # C0103: Euler is capitalised since it is a proper noun.
    assert axis.dim == 3
    axis = axis.normalise()
    (nx, ny, nz) = (axis[0], axis[1], axis[2])

    cosbeta = 1 - 2*(nx**2 + ny**2)*(np.sin(angle/2))**2
    if abs(cosbeta) > 1:
        # Numerical errors can cause cosbeta to be outside [-1, 1].
        assert abs(cosbeta) - 1 < COMMON_ZERO_TOLERANCE
        cosbeta = np.around(cosbeta)

    # np.arccos gives 0 <= beta <= pi.
    beta = np.arccos(cosbeta)

    if abs(1 - abs(cosbeta)) >= COMMON_ZERO_TOLERANCE:
        # cosbeta != 1 or -1, beta != 0 or pi
        # alpha and gamma are given by Equations (**3**-5.4) to (**3**-5.10)
        # in :cite:`book:Altmann1986`.
        # These equations yield the same alpha and gamma for phi and phi+2pi.
        # We therefore account for double-group behaviours separately.
        numalpha = -nx*np.sin(angle) + 2*ny*nz*(np.sin(angle/2))**2
        denalpha = ny*np.sin(angle) + 2*nx*nz*(np.sin(angle/2))**2
        alpha = np.arctan2(numalpha, denalpha)
        alpha = alpha % (2*np.pi)

        numgamma = nx*np.sin(angle) + 2*ny*nz*(np.sin(angle/2.0))**2
        dengamma = ny*np.sin(angle) - 2*nx*nz*(np.sin(angle/2.0))**2
        gamma = np.arctan2(numgamma, dengamma)
        doublemask = (angle//(2*np.pi)) % 2
        gamma = gamma + doublemask*2*np.pi
        gamma = gamma % (4*np.pi)

    elif abs(1 - cosbeta) < COMMON_ZERO_TOLERANCE:
        # cosbeta == 1, beta == 0
        # cos(0.5(alpha+gamma)) = cos(0.5phi)
        # We set alpha == 0 by convention.
        # We then set gamma = phi mod (4*pi).
        alpha = 0
        gamma = angle % (4*np.pi)

    else:
        # cosbeta == -1, beta == pi
        # sin(0.5phi) must be non-zero, otherwise cosbeta == 1, a
        # contradiction.
        # sin(0.5(alpha-gamma)) = -nx*sin(0.5phi)
        # cos(0.5(alpha-gamma)) = +ny*sin(0.5phi)
        # We set alpha == 0 by convention.
        # gamma then lies in [-2pi, 2pi].
        # We obtain the same gamma for phi and phi+2pi.
        # We therefore account for double-group behaviours separately.
        alpha = 0
        gamma = 2*np.arctan2(nx, ny)
        doublemask = (angle//(2*np.pi)) % 2
        gamma = gamma + doublemask*2*np.pi
        gamma = gamma % (4*np.pi)
        gamma = gamma % (4*np.pi)

    return (alpha, beta, gamma)
