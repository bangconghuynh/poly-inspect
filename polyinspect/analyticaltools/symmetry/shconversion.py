""":mod:`.shconversion` contains functions to transform between solid
harmonic Gaussian functions and Cartesian Gaussian functions.
"""

from __future__ import annotations

from typing import Tuple, Optional, Sequence, List
import numpy as np  # type:ignore
from scipy.special import factorial, comb  # type:ignore

from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE
from polyinspect.auxiliary.ao_basis_space import CartTuple


def _fact(n: int) -> int:
    r"""Calculate :math:`n!\ (n \in \mathbb{N})` exactly.

    :param n: A non-negative integer.

    :returns: :math:`n!` as an exact long integer.
    """
    # pylint: disable=C0103
    # C0103: The variables are named in accordance with their
    #        mathematical symbols.

    assert n >= 0
    return factorial(n, exact=True)

def norm_spherical_gaussian(n: int, alpha: float) -> float:
    r"""Obtain the normalisation constant for a solid harmonic Gaussian, as
    given in Equation 8 of :cite:`article:Schlegel1995`.

    The complex solid harmonic Gaussian is defined in Equation 1 of
    :cite:`article:Schlegel1995` as

    .. math::
        \tilde{g}(\alpha, l, m, n, \boldsymbol{r})
        = \tilde{N}(n, \alpha) Y_l^m r^n e^{-\alpha r^2},

    where :math:`Y_l^m` is a complex spherical harmonic of degree :math:`l`
    and order :math:`m`.

    :param n: The exponent of the radial part of the solid harmonic Gaussian.
    :param alpha: The coefficient on the exponent of the Gaussian term.

    :returns: The normalisation constant :math:`\tilde{N}(n, \alpha)`.
    """
    # pylint: disable=C0103
    # C0103: The variables are named in accordance with their
    #        mathematical symbols.

    assert n >= 0
    num = 2**(2*n+3)*_fact(n+1)*alpha**(n+1.5)
    den = _fact(2*n+2)*np.sqrt(np.pi)
    norm = np.sqrt(num/den)
    return norm


def norm_cartesian_gaussian(lcartqns: Tuple[int, int, int], alpha: float)\
        -> float:
    r"""Obtain the normalisation constant for a Cartesian Gaussian, as given in
    Equation 9 of :cite:`article:Schlegel1995`.

    The Cartesian Gaussian is defined in Equation 2 of
    :cite:`article:Schlegel1995` as

    .. math::
        g(\alpha, l_x, l_y, l_z, \boldsymbol{r})
        = N(l_x, l_y, l_z, \alpha) x^{l_x} y^{l_y} z^{l_z} e^{-\alpha r^2}.

    :param lcartqns: A tuple of :math:`(l_x, l_y, l_z)` specifying the \
            exponents of the Cartesian components of the Cartesian Gaussian.
    :param alpha: The coefficient on the exponent of the Gaussian term.

    :returns: The normalisation constant :math:`N(l_x, l_y, l_z, \alpha)`.
    """
    # pylint: disable=C0103
    # C0103: The variables are named in accordance with their
    #        mathematical symbols.

    assert all(x >= 0 for x in lcartqns)
    (lx, ly, lz) = lcartqns
    lcart = lx + ly + lz
    num = 2**(2*lcart)*_fact(lx)*_fact(ly)*_fact(lz)\
            *alpha**(lcart+1.5)
    den = _fact(2*lx)*_fact(2*ly)*_fact(2*lz)*np.sqrt(np.pi**3)
    norm = np.sqrt(num/den)
    return norm


def complexc(lpureqns: Tuple[int, int], lcartqns: Tuple[int, int, int],
             csphase: bool = True) -> complex:
    r"""Obtain the complex coefficients :math:`c(l, m_l, n, l_x, l_y, l_z)`
    based on Equation 15 of :cite:`article:Schlegel1995`, but more generalised
    for :math:`l \leq l_{\mathrm{cart}} = l_x + l_y + l_z`.

    Let :math:`\tilde{g}(\alpha, l, m_l, l_{\mathrm{cart}}, \boldsymbol{r})` be
    a complex solid harmonic Gaussian as defined in Equation 1 of
    :cite:`article:Schlegel1995` with :math:`n = l_{\mathrm{cart}}`,
    and let :math:`g(\alpha, l_x, l_y, l_z, \boldsymbol{r})` be a Cartesian
    Gaussian as defined in Equation 2 of :cite:`article:Schlegel1995`.
    The complex coefficients :math:`c(l, m_l, n, l_x, l_y, l_z)` effect the
    transformation

    .. math::
        \tilde{g}(\alpha, l, m_l, l_{\mathrm{cart}}, \boldsymbol{r})
        = \sum_{l_x+l_y+l_z=l_{\mathrm{cart}}}
            c(l, m_l, l_{\mathrm{cart}}, l_x, l_y, l_z)
            g(\alpha, l_x, l_y, l_z, \boldsymbol{r})


    and are given by

    .. math::
        c(l, m_l, l_{\mathrm{cart}}, l_x, l_y, l_z)
        = \frac{\tilde{N}(l_{\mathrm{cart}}, \alpha)}{N(l_x, l_y, l_z, \alpha)}
        \tilde{c}(l, m_l, l_{\mathrm{cart}}, l_x, l_y, l_z).

    The normalisation constants :math:`\tilde{N}(l_{\mathrm{cart}}, \alpha)`
    and :math:`N(l_x, l_y, l_z, \alpha)` are given in Equations 8 and 9 of
    :cite:`article:Schlegel1995`, and for :math:`n = l_{\mathrm{cart}}`, this
    ratio turns out to be independent of :math:`\alpha`.
    The more general form of
    :math:`\tilde{c}(l, m_l, l_{\mathrm{cart}}, l_x, l_y, l_z)` has been derived
    to be

    .. math::
        \tilde{c}(l, m_l, l_{\mathrm{cart}}, l_x, l_y, l_z)
        = \frac{\lambda_{\mathrm{cs}}}{2^l l!}
            \sqrt{\frac{(2l+1)(l-\lvert m_l \rvert)!}{4\pi(l+\lvert m_l \rvert)!}}
            \sum_{i=0}^{(l-\lvert m_l \rvert)/2}
                {l\choose i} \frac{(-1)^i(2l-2i)!}{(l-\lvert m_l \rvert -2i)!}\\
            \sum_{p=0}^{\lvert m_l \rvert} {{\lvert m_l \rvert} \choose p}
                (\pm \mathbb{i})^{\lvert m_l \rvert-p}
            \sum_{q=0}^{\Delta l/2} {{\Delta l/2} \choose q} {i \choose j_q}
            \sum_{k=0}^{j_q} {q \choose t_{pk}} {j_q \choose k}

    where :math:`+\mathbb{i}` applies for :math:`m_l > 0`, :math:`-\mathbb{i}`
    for :math:`m_l \le 0`, :math:`\lambda_{\mathrm{cs}}` is the Condon--Shortley
    phase given by

    .. math::
        \lambda_{\mathrm{cs}} =
            \begin{cases}
                (-1)^{m_l} & m_l > 0 \\
                1          & m_l \leq 0
            \end{cases}

    and

    .. math::
        t_{pk} = \frac{l_x-p-2k}{2} \quad \textrm{and} \quad
        j_q = \frac{l_x+l_y-\lvert m_l \rvert-2q}{2}.

    If :math:`\Delta l` is odd,
    :math:`\tilde{c}(l, m_l, l_{\mathrm{cart}}, l_x, l_y, l_z)` must vanish.
    When  :math:`t_{pk}` or :math:`j_q` is a half-integer, the inner sum in
    which it is involved evaluates to zero.

    :param lpureqns: A tuple of :math:`(l, m_l)` specifying the quantum \
            numbers for the spherical harmonic component of the solid harmonic \
            Gaussian.
    :param lcartqns: A tuple of :math:`(l_x, l_y, l_z)` specifying the \
            exponents of the Cartesian components of the Cartesian Gaussian.
    :param csphase: If :const:`True`, the Condon--Shortley phase will be used \
            as defined above. If :const:`False`, this phase will be set to \
            unity.

    :returns: :math:`c(l, m_l, l_{\mathrm{cart}}, l_x, l_y, l_z)`.
    """
    # pylint: disable=C0103,R0914,R1702
    # C0103: The variables are named in accordance with their
    #        mathematical symbols.
    # R0914: More local variables are defined for clarity. Some of these could
    #        be substituted directly into expressions, but that would make the
    #        maths very difficult to follow.
    # R1702: The number of nested blocks is needed to implement the quadruple
    #        summation. It might however be possible for a faster, less nested
    #        method to be implemented in the future.

    (l, m) = lpureqns
    assert l >= 0
    assert all(x >= 0 for x in lcartqns)
    (lx, ly, lz) = lcartqns
    lcart = lx + ly + lz
    dl = lcart - l
    if dl % 2 != 0:
        return 0+0j
    num = (2*l+1)*_fact(l-abs(m))
    den = 4*np.pi*_fact(l+abs(m))
    prefactor = 1.0/(2**l*_fact(l))*np.sqrt(num/den)
    if csphase and m > 0:
        prefactor *= (-1)**m
    Ntilde = norm_spherical_gaussian(lcart, 1)
    N = norm_cartesian_gaussian(lcartqns, 1)
    si = 0+0j
    for i in range(int((l-abs(m))/2)+1):
        ifactor = comb(l, i)\
                *((-1)**i*_fact(2*l-2*i))/(_fact(l-abs(m)-2*i))
        sp = 0+0j
        for p in range(abs(m)+1):
            if m > 0:
                pfactor = comb(abs(m), p)*(1j)**(abs(m)-p)
            else:
                pfactor = comb(abs(m), p)*(-1j)**(abs(m)-p)
            sq = 0+0j
            for q in range(int(dl/2)+1):
                jq = lx + ly - abs(m) - 2*q
                if jq % 2 == 1:
                    qfactor = 0
                else:
                    jq = int(jq/2)
                    qfactor = comb(int(dl/2), q)*comb(i, jq)
                    sk = 0+0j
                    for k in range(0, jq+1):
                        tpk = lx - p - 2*k
                        if tpk % 2 == 1:
                            kfactor = 0
                        else:
                            tpk = int(tpk/2)
                            kfactor = comb(q, tpk)*comb(jq, k)
                            sk += kfactor
                    sq += qfactor*sk
            sp += pfactor*sq
        si += ifactor*sp
    return (Ntilde/N)*prefactor*si


def cartov(lcartqns1: Tuple[int, int, int],
           lcartqns2: Tuple[int, int, int]) -> float:
    r"""Obtain the overlap between two normalised Cartesian Gaussians of the
    same order and radial width, as given in Equation 19 of
    :cite:`article:Schlegel1995`.

    :param lcartqns1: A tuple of :math:`(l_x, l_y, l_z)` specifying the \
            exponents of the Cartesian components of the first Cartesian \
            Gaussian.
    :param lcartqns2: A tuple of :math:`(l_x, l_y, l_z)` specifying the \
            exponents of the Cartesian components of the second Cartesian \
            Gaussian.

    :returns: The overlap between the two specified normalised Cartesian \
            Gaussian.
    """

    assert all(x >= 0 for x in lcartqns1+lcartqns2)
    assert sum(lcartqns1) == sum(lcartqns2),\
            "This function only calculates the overlap between Cartesian "\
          + "Gaussians of the same order."

    (lx1, ly1, lz1) = lcartqns1
    (lx2, ly2, lz2) = lcartqns2

    if (lx1+lx2) % 2 == (ly1+ly2) % 2 == (lz1+lz2) % 2 == 0:
        num1 = _fact(lx1+lx2)*_fact(ly1+ly2)*_fact(lz1+lz2)
        den1 = _fact(int((lx1+lx2)/2))\
               * _fact(int((ly1+ly2)/2))\
               * _fact(int((lz1+lz2)/2))
        num2 = _fact(lx1)*_fact(ly1)*_fact(lz1)\
               * _fact(lx2)*_fact(ly2)*_fact(lz2)
        den2 = _fact(2*lx1)*_fact(2*ly1)*_fact(2*lz1)\
               * _fact(2*lx2)*_fact(2*ly2)*_fact(2*lz2)
        return num1/den1*np.sqrt(num2/den2)
    return 0


def complexcinv(lcartqns: Tuple[int, int, int], lpureqns: Tuple[int, int],
                csphase: bool = True) -> complex:
    r"""Obtain the inverse complex coefficients
    :math:`c^{-1}(l_x, l_y, l_z, l, m_l, l_{\mathrm{cart}})`
    based on Equation 18 of :cite:`article:Schlegel1995`, but more generalised
    for :math:`l \leq l_{\mathrm{cart}} = l_x + l_y + l_z`.

    Let :math:`\tilde{g}(\alpha, l, m_l, l_{\mathrm{cart}}, \boldsymbol{r})` be
    a complex solid harmonic Gaussian as defined in Equation 1 of
    :cite:`article:Schlegel1995` with :math:`n = l_{\mathrm{cart}}`,
    and let :math:`g(\alpha, l_x, l_y, l_z, \boldsymbol{r})` be a Cartesian
    Gaussian as defined in Equation 2 of :cite:`article:Schlegel1995`.
    The inverse complex coefficients
    :math:`c^{-1}(l_x, l_y, l_z, l, m_l, l_{\mathrm{cart}})` effect the inverse
    transformation

    .. math::
        g(\alpha, l_x, l_y, l_z, \boldsymbol{r})
        = \sum_{l \le l_{\mathrm{cart}} = l_x+l_y+l_z} \sum_{m_l = -l}^{l}
            c^{-1}(l_x, l_y, l_z, l, m_l, l_{\mathrm{cart}})
            \tilde{g}(\alpha, l, m_l, l_{\mathrm{cart}}, \boldsymbol{r}).


    :param lcartqns: A tuple of :math:`(l_x, l_y, l_z)` specifying the \
            exponents of the Cartesian components of the Cartesian Gaussian.
    :param lpureqns: A tuple of :math:`(l, m_l)` specifying the quantum \
            numbers for the spherical harmonic component of the solid harmonic \
            Gaussian.
    :param csphase: If :const:`True`, the Condon--Shortley phase will be used \
            as defined in :func:`~.complexc`. If :const:`False`, this \
            phase will be set to unity.

    :returns: :math:`c^{-1}(l_x, l_y, l_z, l, m_l, l_{\mathrm{cart}})`.
    """
    cinv = 0+0j
    assert all(x >= 0 for x in lcartqns)
    lcart = sum(lcartqns)
    for lx2 in range(0, lcart+1):
        for ly2 in range(0, lcart - lx2 + 1):
            lz2 = lcart - lx2 - ly2
            cinv += cartov(lcartqns, (lx2, ly2, lz2)) \
                    * np.conj(complexc(lpureqns, (lx2, ly2, lz2), csphase))
    return cinv


def sh_c2r_mat(l: int, csphase: bool = True, increasingm: bool = True)\
        -> np.ndarray:
    r"""Obtain the transformation matrix :math:`\boldsymbol{\Upsilon}^{(l)}`
    allowing complex spherical harmonics to be expressed as linear combinations
    of real spherical harmonics.

    Let :math:`Y_{lm}` be a real spherical harmonic of degree :math:`l`.
    Then, a complex spherical harmonic of degree :math:`l` and order :math:`m`
    is given by

    .. math::

        Y_l^m =
            \begin{cases}
                \frac{\lambda_{\mathrm{cs}}}{\sqrt{2}}
                \left(Y_{l\lvert m \rvert}
                      - \mathbb{i} Y_{l,-\lvert m \rvert}\right)
                & \mathrm{if}\ m < 0 \\
                Y_{l0} & \mathrm{if}\ m = 0 \\
                \frac{\lambda_{\mathrm{cs}}}{\sqrt{2}}
                \left(Y_{l\lvert m \rvert}
                      + \mathbb{i} Y_{l,-\lvert m \rvert}\right)
                & \mathrm{if}\ m > 0 \\
            \end{cases}

    where :math:`\lambda_{\mathrm{cs}}` is the Condon--Shortley phase as defined
    in :func:`~.complexc`.
    The linear combination coefficients can then be gathered into a square
    matrix :math:`\boldsymbol{\Upsilon}^{(l)}` of dimensions
    :math:`(2l+1)\times(2l+1)` such that

    .. math::
        Y_l^m = \sum_{m'} Y_{lm'} \Upsilon^{(l)}_{m'm}.

    :param l: The spherical harmonic degree.
    :param csphase: If :const:`True`, \
            :math:`\lambda_{\mathrm{cs}}` is as defined in \
            :func:`~.complexc`. If :const:`False`, \
            :math:`\lambda_{\mathrm{cs}} = 1`.
    :param increasingm: If :const:`True`, the rows and columns of \
            :math:`\boldsymbol{\Upsilon}^{(l)}` are arranged in increasing \
            order of  :math:`m_l = -l, \ldots, l`. If :const:`False`, the \
            order is reversed: :math:`m_l = l, \ldots, -l`.

    :returns: The :math:`\boldsymbol{\Upsilon}^{(l)}` matrix.
    """
    # pylint: disable=C0103
    # l is fine as a variable name to denote the ang. mom. quantum number.
    assert l >= 0
    Upmat = np.zeros((2*l+1, 2*l+1), dtype=np.complex128)
    for mcomplex in range(-l, l+1):
        absmreal = abs(mcomplex)
        if mcomplex < 0:
            Upmat[-absmreal+l, mcomplex+l] = -1.0j/np.sqrt(2)
            Upmat[+absmreal+l, mcomplex+l] = 1.0/np.sqrt(2)
        elif mcomplex == 0:
            Upmat[l, l] = 1
        else:
            if csphase:
                lcs = (-1)**mcomplex
            else:
                lcs = 1
            Upmat[-absmreal+l, mcomplex+l] = lcs*1.0j/np.sqrt(2)
            Upmat[+absmreal+l, mcomplex+l] = lcs*1.0/np.sqrt(2)
    if not increasingm:
        Upmat = np.flip(np.flip(Upmat, axis=1), axis=0)
    return Upmat


def sh_r2c_mat(l: int, csphase: bool = True, increasingm: bool = True)\
        -> np.ndarray:
    r"""Obtain the matrix :math:`\boldsymbol{\Upsilon}^{(l)\dagger}` allowing
    real spherical harmonics to be expressed as linear combinations of complex
    spherical harmonics.

    Let :math:`Y_l^m` be a complex spherical harmonic of degree :math:`l` and
    order :math:`m`.
    Then, a real degree-:math:`l` spherical harmonic :math:`Y_{lm}` can be
    defined as

    .. math::

        Y_{lm} =
            \begin{cases}
                \frac{\mathbb{i}}{\sqrt{2}}
                \left(Y_l^{-\lvert m \rvert}
                      - \lambda'_{\mathrm{cs}} Y_l^{\lvert m \rvert}\right)
                & \mathrm{if}\ m < 0 \\
                Y_l^0 & \mathrm{if}\ m = 0 \\
                \frac{1}{\sqrt{2}}
                \left(Y_l^{-\lvert m \rvert}
                      + \lambda'_{\mathrm{cs}} Y_l^{\lvert m \rvert}\right)
                & \mathrm{if}\ m > 0 \\
            \end{cases}

    where :math:`\lambda'_{\mathrm{cs}} = (-1)^{\lvert m \rvert}` if the
    Condon--Shortley phase as defined in :func:`~.complexc` is employed
    for the complex spherical harmonics, and :math:`\lambda'_{\mathrm{cs}} = 1`
    otherwise.
    The linear combination coefficients turn out to be given by the elements of
    matrix :math:`\boldsymbol{\Upsilon}^{(l)\dagger}` of dimensions
    :math:`(2l+1)\times(2l+1)` such that

    .. math::
        Y_{lm} = \sum_{m'} Y_l^{m'} [\Upsilon^{(l)\dagger}]_{m'm}.

    It is obvious from the orthonormality of :math:`Y_{lm}` and :math:`Y_l^m`
    that :math:`\boldsymbol{\Upsilon}^{(l)\dagger}
    = [\boldsymbol{\Upsilon}^{(l)}]^{-1}` where
    :math:`\boldsymbol{\Upsilon}^{(l)}` is defined in :func:`.sh_c2r_mat`.

    :param l: The spherical harmonic degree.
    :param csphase: If :const:`True`, \
            :math:`\lambda'_{\mathrm{cs}} = (-1)^{\lvert m \rvert}`. If \
            :const:`False`, :math:`\lambda_{\mathrm{cs}} = 1`.
    :param increasingm: If :const:`True`, the rows and columns of \
            :math:`[\boldsymbol{\Upsilon}^{(l)}]^{-1}` are arranged in \
            increasing order of  :math:`m_l = -l, \ldots, l`. If \
            :const:`False`, the order is reversed: :math:`m_l = l, \ldots, -l`.

    :returns: The :math:`\boldsymbol{\Upsilon}^{(l)\dagger}` matrix.
    """
    # pylint: disable=C0103
    # l is fine as a variable name to denote the ang. mom. quantum number.
    assert l >= 0
    mat = sh_c2r_mat(l, csphase, increasingm)
    return mat.T.conj()


def sh_cl2cart_mat(lcart: int, l: int,
                   cartorder: Optional[Sequence[CartTuple]] = None,
                   csphase: bool = True, increasingm: bool = True)\
                        -> np.ndarray:
    r"""Obtain the matrix :math:`\boldsymbol{U}^{(l_{\mathrm{cart}}, l)}`
    containing linear combination coefficients of Cartesian Gaussians in the
    expansion of a complex solid harmonic Gaussian, *i.e.*, briefly,

    .. math::
        \tilde{\boldsymbol{g}}^{\mathsf{T}}(l)
            = \boldsymbol{g}^{\mathsf{T}}(l_{\mathrm{cart}})
            \ \boldsymbol{U}^{(l_{\mathrm{cart}}, l)}.

    Let :math:`\tilde{g}(\alpha, \lambda, l_{\mathrm{cart}}, \boldsymbol{r})` be
    a complex solid harmonic Gaussian as defined in Equation 1 of
    :cite:`article:Schlegel1995` with :math:`n = l_{\mathrm{cart}}`,
    and let :math:`g(\alpha, \lambda_{\mathrm{cart}}, \boldsymbol{r})` be a
    Cartesian Gaussian as defined in Equation 2 of :cite:`article:Schlegel1995`.
    Here, :math:`\lambda` is a single index labelling a complex solid harmonic
    Gaussian of spherical harmonic degree :math:`l` and order :math:`m_l`, and
    :math:`\lambda_{\mathrm{cart}}` a single index labelling a Cartesian Gaussian
    of degrees :math:`(l_x, l_y, l_z)` such that
    :math:`l_x + l_y + l_z = l_{\mathrm{cart}}`.
    We can then write

    .. math::
        \tilde{g}(\alpha, \lambda, l_{\mathrm{cart}}, \boldsymbol{r})
        = \sum_{\lambda_{\mathrm{cart}}}
            g(\alpha, \lambda_{\mathrm{cart}}, \boldsymbol{r})
            U^{(l_{\mathrm{cart}}, l)}_{\lambda_{\mathrm{cart}}\lambda}

    where :math:`U^{(l_{\mathrm{cart}}, l)}_{\lambda_{\mathrm{cart}}\lambda}`
    is given by the complex coefficients

    .. math::
        U^{(l_{\mathrm{cart}}, l)}_{\lambda_{\mathrm{cart}}\lambda} =
            c(l, m_l, l_{\mathrm{cart}}, l_x, l_y, l_z)

    defined in :func:`.complexc`.

    :math:`\boldsymbol{U}^{(l_{\mathrm{cart}}, l)}` has dimensions
    :math:`\frac{1}{2}(l_{\mathrm{cart}}+1)(l_{\mathrm{cart}}+2) \times (2l+1)`
    and contains only zero elements if :math:`l` and :math:`l_{\mathrm{cart}}`
    have different parities.
    It can be verified that
    :math:`\boldsymbol{V}^{(l,l_{\mathrm{cart}})}
    \ \boldsymbol{U}^{(l_{\mathrm{cart}}, l)} = \boldsymbol{I}_{2l+1}`, where
    :math:`\boldsymbol{V}^{(l,l_{\mathrm{cart}})}` is given in
    :func:`sh_cart2cl_mat`.

    :param lcart: The total Cartesian degree for the Cartesian Gaussians and
            also for the radial part of the solid harmonic Gaussian.
    :param l: The degree of the complex spherical harmonic factor in the solid
            harmonic Gaussian.
    :param cartorder: A sequence of :math:`(l_x, l_y, l_z)` tuples giving the
            ordering of the Cartesian Gaussians. If :const:`None`, the
            lexicographic descending order will be used. For example, if
            :math:`l_{\mathrm{cart}} = 2`, `cartorder` will be set to
            ``[(2, 0, 0), (1, 1, 0), (1, 0, 1), (0, 2, 0), (0, 1, 1), (0, 0, 2)]``.
    :param csphase: Set to :const:`True` to use the Condon--Shortley phase in
            the calculations of  the :math:`c` coefficients. See
            :func:`~.complexc` for more details.
    :param increasingm: If :const:`True`, the columns of
            :math:`\boldsymbol{U}^{(l_{\mathrm{cart}}, l)}` are arranged in
            increasing order of  :math:`m_l = -l, \ldots, l`. If
            :const:`False`, the order is reversed: :math:`m_l = l, \ldots, -l`.

    :returns: The :math:`\boldsymbol{U}^{(l_{\mathrm{cart}}, l)}` matrix.
    """
    # pylint: disable=C0103
    # l is fine as a variable name to denote the ang. mom. quantum number.
    assert l >= 0 and lcart >= 0
    if cartorder is None:
        cartorder = [(lx, ly, lcart-lx-ly)
                     for lx in range(lcart, -1, -1)
                     for ly in range(lcart-lx, -1, -1)]
        cartorder.sort(reverse=True)
    else:
        assert all(sum(tup) == lcart and all(x >= 0 for x in tup)
                   for tup in cartorder)
    Umat = np.zeros(((lcart+1)*(lcart+2)//2, 2*l+1), dtype=np.complex128)
    for m in range(-l, l+1):
        for icart in range((lcart+1)*(lcart+2)//2):
            Umat[icart, m+l] = complexc((l, m), cartorder[icart], csphase)
    if not increasingm:
        Umat = np.flip(Umat, axis=1)
    return Umat


def sh_cart2cl_mat(l: int, lcart: int,
                   cartorder: Optional[Sequence[CartTuple]] = None,
                   csphase: bool = True, increasingm: bool = True)\
                        -> np.ndarray:
    r"""Obtain the matrix :math:`\boldsymbol{V}^{(l, l_{\mathrm{cart}})}`
    containing linear combination coefficients of complex solid harmonic
    Gaussians of a specific degree in the expansion of Cartesian Gaussians,
    *i.e.*, briefly,

    .. math::
        \boldsymbol{g}^{\mathsf{T}}(l_{\mathrm{cart}})
            = \tilde{\boldsymbol{g}}^{\mathsf{T}}(l)
            \ \boldsymbol{V}^{(l, l_{\mathrm{cart}})}.

    Let :math:`\tilde{g}(\alpha, \lambda, l_{\mathrm{cart}}, \boldsymbol{r})` be
    a complex solid harmonic Gaussian as defined in Equation 1 of
    :cite:`article:Schlegel1995` with :math:`n = l_{\mathrm{cart}}`,
    and let :math:`g(\alpha, \lambda_{\mathrm{cart}}, \boldsymbol{r})` be a
    Cartesian Gaussian as defined in Equation 2 of :cite:`article:Schlegel1995`.
    Here, :math:`\lambda` is a single index labelling a complex solid harmonic
    Gaussian of spherical harmonic degree :math:`l` and order :math:`m_l`, and
    :math:`\lambda_{\mathrm{cart}}` a single index labelling a Cartesian Gaussian
    of degrees :math:`(l_x, l_y, l_z)` such that
    :math:`l_x + l_y + l_z = l_{\mathrm{cart}}`.
    We can then write

    .. math::
        g(\alpha, \lambda_{\mathrm{cart}}, \boldsymbol{r})
        = \sum_{\substack{\lambda\\ l \leq l_{\mathrm{cart}}}}
            \tilde{g}(\alpha, \lambda, l_{\mathrm{cart}}, \boldsymbol{r})
            V^{(l_{\mathrm{cart}})}_{\lambda\lambda_{\mathrm{cart}}}

    where :math:`V^{(l_{\mathrm{cart}})}_{\lambda\lambda_{\mathrm{cart}}}` is
    given by the inverse complex coefficients

    .. math::
        V^{(l_{\mathrm{cart}})}_{\lambda\lambda_{\mathrm{cart}}} =
            c^{-1}(l_x, l_y, l_z, l, m_l, l_{\mathrm{cart}})

    defined in :func:`.complexcinv`.

    We can order the rows :math:`\lambda` of
    :math:`\boldsymbol{V}^{(l_{\mathrm{cart}})}` that have the same :math:`l`
    into rectangular blocks of dimensions
    :math:`(2l+1) \times \frac{1}{2}(l_{\mathrm{cart}}+1)(l_{\mathrm{cart}}+2)`
    which give contributions from complex solid harmonic Gaussians of a
    particular degree :math:`l`.
    We denote these blocks :math:`\boldsymbol{V}^{(l, l_{\mathrm{cart}})}`.
    They contain only zero elements if :math:`l` and :math:`l_{\mathrm{cart}}`
    have different parities.

    :param l: The degree of the complex spherical harmonic factor in the solid
            harmonic Gaussians.
    :param lcart: The total Cartesian degree for the Cartesian Gaussian and
            also for the radial part of the solid harmonic Gaussians.
    :param cartorder: A sequence of :math:`(l_x, l_y, l_z)` tuples giving the
            ordering of the Cartesian Gaussians. If :const:`None`, the
            lexicographic descending order will be used. For example, if
            :math:`l_{\mathrm{cart}} = 2`, `cartorder` will be set to
            ``[(2, 0, 0), (1, 1, 0), (1, 0, 1), (0, 2, 0), (0, 1, 1), (0, 0, 2)]``.
    :param csphase: Set to :const:`True` to use the Condon--Shortley phase in
            the calculations of  the :math:`c^{-1}` coefficients. See
            :func:`~.complexc` and :func:`~complexcinv` for more
            details.
    :param increasingm: If :const:`True`, the rows of
            :math:`\boldsymbol{V}^{(l, l_{\mathrm{cart}})}` are arranged in
            increasing order of  :math:`m_l = -l, \ldots, l`. If
            :const:`False`, the order is reversed: :math:`m_l = l, \ldots, -l`.

    :returns: The :math:`\boldsymbol{V}^{(l, l_{\mathrm{cart}})}` block.
    """
    # pylint: disable=C0103
    # l is fine as a variable name to denote the ang. mom. quantum number.
    assert l >= 0 and lcart >= 0
    if cartorder is None:
        cartorder = [(lx, ly, lcart-lx-ly)\
                     for lx in range(lcart, -1, -1)\
                     for ly in range(lcart-lx, -1, -1)]
        cartorder.sort(reverse=True)
    else:
        assert all(sum(tup) == lcart and all(x >= 0 for x in tup)
                   for tup in cartorder)
    Vmat = np.zeros((2*l+1, ((lcart+1)*(lcart+2))//2), dtype=np.complex128)
    for icart in range((lcart+1)*(lcart+2)//2):
        for m in range(-l, l+1):
            Vmat[m+l, icart] = complexcinv(cartorder[icart], (l, m), csphase)
    if not increasingm:
        Vmat = np.flip(Vmat, axis=0)
    return Vmat


def sh_rl2cart_mat(lcart: int, l: int,
                   cartorder: Optional[Sequence[CartTuple]] = None,
                   csphase: bool = True, increasingm: bool = True)\
                        -> np.ndarray:
    r"""Obtain the matrix :math:`\boldsymbol{W}^{(l_{\mathrm{cart}}, l)}`
    containing linear combination coefficients of Cartesian Gaussians in the
    expansion of a real solid harmonic Gaussian, *i.e.*, briefly,

    .. math::
        \bar{\boldsymbol{g}}^{\mathsf{T}}(l)
            = \boldsymbol{g}^{\mathsf{T}}(l_{\mathrm{cart}})
            \ \boldsymbol{W}^{(l_{\mathrm{cart}}, l)}.

    Let :math:`\bar{g}(\alpha, \lambda, l_{\mathrm{cart}}, \boldsymbol{r})` be
    a real solid harmonic Gaussian defined in a similar manner to Equation 1 of
    :cite:`article:Schlegel1995` with :math:`n = l_{\mathrm{cart}}` but with,
    real rather than complex spherical harmonic factors,
    and let :math:`g(\alpha, \lambda_{\mathrm{cart}}, \boldsymbol{r})` be a
    Cartesian Gaussian as defined in Equation 2 of :cite:`article:Schlegel1995`.
    Here, :math:`\lambda` is a single index labelling a complex solid harmonic
    Gaussian of spherical harmonic degree :math:`l` and order :math:`m_l`, and
    :math:`\lambda_{\mathrm{cart}}` a single index labelling a Cartesian Gaussian
    of degrees :math:`(l_x, l_y, l_z)` such that
    :math:`l_x + l_y + l_z = l_{\mathrm{cart}}`.
    We can then write

    .. math::
        \bar{g}(\alpha, \lambda, l_{\mathrm{cart}}, \boldsymbol{r})
        = \sum_{\lambda_{\mathrm{cart}}}
            g(\alpha, \lambda_{\mathrm{cart}}, \boldsymbol{r})
            W^{(l_{\mathrm{cart}}, l)}_{\lambda_{\mathrm{cart}}\lambda}.

    :math:`\boldsymbol{W}^{(l_{\mathrm{cart}}, l)}` is given by

    .. math::
        \boldsymbol{W}^{(l_{\mathrm{cart}}, l)}
        = \boldsymbol{U}^{(l_{\mathrm{cart}}, l)}
          \boldsymbol{\Upsilon}^{(l)\dagger},

    where :math:`\boldsymbol{\Upsilon}^{(l)\dagger}` is defined in
    :func:`.sh_r2c_mat` and :math:`\boldsymbol{U}^{(l_{\mathrm{cart}}, l)}`
    in :func:`.sh_cl2cart_mat`.
    :math:`\boldsymbol{W}^{(l_{\mathrm{cart}}, l)}` must be real.
    :math:`\boldsymbol{W}^{(l_{\mathrm{cart}}, l)}` has dimensions
    :math:`\frac{1}{2}(l_{\mathrm{cart}}+1)(l_{\mathrm{cart}}+2) \times (2l+1)`
    and contains only zero elements if :math:`l` and :math:`l_{\mathrm{cart}}`
    have different parities.
    It can be verified that
    :math:`\boldsymbol{X}^{(l,l_{\mathrm{cart}})}
    \ \boldsymbol{W}^{(l_{\mathrm{cart}}, l)} = \boldsymbol{I}_{2l+1}`, where
    :math:`\boldsymbol{X}^{(l,l_{\mathrm{cart}})}` is given in
    :func:`sh_cart2rl_mat`.


    :param lcart: The total Cartesian degree for the Cartesian Gaussians and \
            also for the radial part of the solid harmonic Gaussian.
    :param l: The degree of the complex spherical harmonic factor in the solid \
            harmonic Gaussian.
    :param cartorder: A sequence of :math:`(l_x, l_y, l_z)` tuples giving the \
            ordering of the Cartesian Gaussians. If :const:`None`, the \
            lexicographic descending order will be used. For example, if \
            :math:`l_{\mathrm{cart}} = 2`, `cartorder` will be set to \
            ``[(2, 0, 0), (1, 1, 0), (1, 0, 1), (0, 2, 0), (0, 1, 1), (0, 0, 2)]``.
    :param csphase: Set to :const:`True` to use the Condon--Shortley phase in \
            the calculations of  the :math:`c` coefficients. See \
            :func:`~.complexc` for more details.
    :param increasingm: If :const:`True`, the columns of \
            :math:`\boldsymbol{U}^{(l_{\mathrm{cart}}, l)}` are arranged in \
            increasing order of  :math:`m_l = -l, \ldots, l`. If \
            :const:`False`, the order is reversed: :math:`m_l = l, \ldots, -l`.

    :returns: The :math:`\boldsymbol{W}^{(l_{\mathrm{cart}}, l)}` matrix.
    """
    # pylint: disable=C0103
    # l is fine as a variable name to denote the ang. mom. quantum number.
    Upmatdagger = sh_r2c_mat(l, csphase, increasingm)
    Umat = sh_cl2cart_mat(lcart, l, cartorder, csphase, increasingm)
    Wmat = np.dot(Umat, Upmatdagger)
    assert np.all(np.abs(Wmat.imag) < COMMON_ZERO_TOLERANCE),\
            "Wmat is not entirely real."
    return Wmat.real


def sh_cart2rl_mat(l: int, lcart: int,
                   cartorder: Optional[Sequence[CartTuple]] = None,
                   csphase: bool = True, increasingm: bool = True)\
                        -> np.ndarray:
    r"""Obtain the real matrix :math:`\boldsymbol{X}^{(l, l_{\mathrm{cart}})}`
    containing linear combination coefficients of real solid harmonic
    Gaussians of a specific degree in the expansion of Cartesian Gaussians,
    *i.e.*, briefly,

    .. math::
        \boldsymbol{g}^{\mathsf{T}}(l_{\mathrm{cart}})
            = \bar{\boldsymbol{g}}^{\mathsf{T}}(l)
            \ \boldsymbol{X}^{(l, l_{\mathrm{cart}})}.

    Let :math:`\bar{g}(\alpha, \lambda, l_{\mathrm{cart}}, \boldsymbol{r})` be
    a real solid harmonic Gaussian defined in a similar manner to Equation 1 of
    :cite:`article:Schlegel1995` with :math:`n = l_{\mathrm{cart}}`, but with
    real rather than complex spherical harmonic factors,
    and let :math:`g(\alpha, \lambda_{\mathrm{cart}}, \boldsymbol{r})` be a
    Cartesian Gaussian as defined in Equation 2 of :cite:`article:Schlegel1995`.
    Here, :math:`\lambda` is a single index labelling a real solid harmonic
    Gaussian of spherical harmonic degree :math:`l` and real order :math:`m_l`,
    and :math:`\lambda_{\mathrm{cart}}` a single index labelling a Cartesian
    Gaussian of degrees :math:`(l_x, l_y, l_z)` such that
    :math:`l_x + l_y + l_z = l_{\mathrm{cart}}`.
    We can then write

    .. math::
        g(\alpha, \lambda_{\mathrm{cart}}, \boldsymbol{r})
        = \sum_{\substack{\lambda\\ l \leq l_{\mathrm{cart}}}}
            \bar{g}(\alpha, \lambda, l_{\mathrm{cart}}, \boldsymbol{r})
            X^{(l_{\mathrm{cart}})}_{\lambda\lambda_{\mathrm{cart}}}.

    We can order the rows :math:`\lambda` of
    :math:`\boldsymbol{X}^{(l_{\mathrm{cart}})}` that have the same :math:`l`
    into rectangular blocks of dimensions
    :math:`(2l+1) \times \frac{1}{2}(l_{\mathrm{cart}}+1)(l_{\mathrm{cart}}+2)`.
    We denote these blocks :math:`\boldsymbol{X}^{(l, l_{\mathrm{cart}})}` which
    are given by

    .. math::
        \boldsymbol{X}^{(l, l_{\mathrm{cart}})}
        = \boldsymbol{\Upsilon}^{(l)} \boldsymbol{V}^{(l, l_{\mathrm{cart}})},

    where :math:`\boldsymbol{\Upsilon}^{(l)}` is defined in
    :func:`.sh_c2r_mat` and :math:`\boldsymbol{V}^{(l, l_{\mathrm{cart}})}`
    in :func:`.sh_cart2cl_mat`.
    :math:`\boldsymbol{X}^{(l, l_{\mathrm{cart}})}` must be real.

    :param l: The degree of the real spherical harmonic factor in the solid \
            harmonic Gaussians.
    :param lcart: The total Cartesian degree for the Cartesian Gaussian and \
            also for the radial part of the solid harmonic Gaussians.
    :param cartorder: A sequence of :math:`(l_x, l_y, l_z)` tuples giving the \
            ordering of the Cartesian Gaussians. If :const:`None`, the \
            lexicographic descending order will be used. For example, if \
            :math:`l_{\mathrm{cart}} = 2`, `cartorder` will be set to \
            ``[(2, 0, 0), (1, 1, 0), (1, 0, 1), (0, 2, 0), (0, 1, 1), (0, 0, 2)]``.
    :param csphase: Set to :const:`True` to use the Condon--Shortley phase in \
            the calculations of  the :math:`c^{-1}` coefficients. See \
            :func:`~.complexc` and :func:`~complexcinv` for more \
            details.
    :param increasingm: If :const:`True`, the rows of \
            :math:`\boldsymbol{V}^{(l, l_{\mathrm{cart}})}` are arranged in \
            increasing order of  :math:`m_l = -l, \ldots, l`. If \
            :const:`False`, the order is reversed: :math:`m_l = l, \ldots, -l`.

    :returns: The :math:`\boldsymbol{X}^{(l, l_{\mathrm{cart}})}` block.
    """
    # pylint: disable=C0103
    # l is fine as a variable name to denote the ang. mom. quantum number.
    Upmat = sh_c2r_mat(l, csphase, increasingm)
    Vmat = sh_cart2cl_mat(l, lcart, cartorder, csphase, increasingm)
    Xmat = np.dot(Upmat, Vmat)
    assert np.all(np.abs(Xmat.imag) < COMMON_ZERO_TOLERANCE),\
            "Xmat is not entirely real."
    return Xmat.real


def sh_r2cart(lcart: int,
              cartorder: Optional[Sequence[CartTuple]] = None,
              csphase: bool = True, increasingm: bool = True)\
                    -> List[np.ndarray]:
    r"""Return a list of :math:`\boldsymbol{W}^{(l_{\mathrm{cart}}, l)}` for
    :math:`l_{\mathrm{cart}} \ge l \ge 0` and
    :math:`l \equiv l_{\mathrm{cart}} \pmod 2`.

    :math:`\boldsymbol{W}^{(l_{\mathrm{cart}}, l)}` is defined in
    :func:`sh_rl2cart_mat`.


    :param lcart: The total Cartesian degree for the Cartesian Gaussians and \
            also for the radial part of the solid harmonic Gaussian.
    :param cartorder: A sequence of :math:`(l_x, l_y, l_z)` tuples giving the \
            ordering of the Cartesian Gaussians. If :const:`None`, the \
            lexicographic descending order will be used. For example, if \
            :math:`l_{\mathrm{cart}} = 2`, `cartorder` will be set to \
            ``[(2, 0, 0), (1, 1, 0), (1, 0, 1), (0, 2, 0), (0, 1, 1), (0, 0, 2)]``.
    :param csphase: Set to :const:`True` to use the Condon--Shortley phase in \
            the calculations of  the :math:`c` coefficients. See \
            :func:`~.complexc` for more details.
    :param increasingm: If :const:`True`, the columns of \
            :math:`\boldsymbol{U}^{(l_{\mathrm{cart}}, l)}` are arranged in \
            increasing order of  :math:`m_l = -l, \ldots, l`. If \
            :const:`False`, the order is reversed: :math:`m_l = l, \ldots, -l`.

    :returns: The list of :math:`\boldsymbol{W}^{(l_{\mathrm{cart}}, l)}` \
            matrices with :math:`l_{\mathrm{cart}} \ge l \ge 0` and \
            :math:`l \equiv l_{\mathrm{cart}} \pmod 2` in decreasing :math:`l` \
            order.
    """
    # pylint: disable=C0103
    # C0103: The variables are named in accordance with their
    #        mathematical symbols.

    Wmat_all = []
    for l in range(lcart, -1, -2):
        Wmat_all.append(sh_rl2cart_mat(lcart, l, cartorder, csphase, increasingm))
    return Wmat_all


def sh_cart2r(lcart: int,
              cartorder: Optional[Sequence[CartTuple]] = None,
              csphase: bool = True, increasingm: bool = True)\
                   -> List[np.ndarray]:
    r"""Return a list of :math:`\boldsymbol{X}^{(l, l_{\mathrm{cart}})}` for
    :math:`l_{\mathrm{cart}} \ge l \ge 0` and
    :math:`l \equiv l_{\mathrm{cart}} \pmod 2`.

    :math:`\boldsymbol{X}^{(l, l_{\mathrm{cart}})}` is defined in
    :func:`sh_cart2r_mat`.


    :param lcart: The total Cartesian degree for the Cartesian Gaussians and \
            also for the radial part of the solid harmonic Gaussian.
    :param cartorder: A sequence of :math:`(l_x, l_y, l_z)` tuples giving the \
            ordering of the Cartesian Gaussians. If :const:`None`, the \
            lexicographic descending order will be used. For example, if \
            :math:`l_{\mathrm{cart}} = 2`, `cartorder` will be set to \
            ``[(2, 0, 0), (1, 1, 0), (1, 0, 1), (0, 2, 0), (0, 1, 1), (0, 0, 2)]``.
    :param csphase: Set to :const:`True` to use the Condon--Shortley phase in \
            the calculations of  the :math:`c` coefficients. See \
            :func:`~.complexc` for more details.
    :param increasingm: If :const:`True`, the columns of \
            :math:`\boldsymbol{U}^{(l_{\mathrm{cart}}, l)}` are arranged in \
            increasing order of  :math:`m_l = -l, \ldots, l`. If \
            :const:`False`, the order is reversed: :math:`m_l = l, \ldots, -l`.

    :returns: The list of :math:`\boldsymbol{X}^{(l, l_{\mathrm{cart}})}` \
            matrices with :math:`l_{\mathrm{cart}} \ge l \ge 0` and \
            :math:`l \equiv l_{\mathrm{cart}} \pmod 2` in decreasing :math:`l` \
            order.
    """
    # pylint: disable=C0103
    # C0103: The variables are named in accordance with their
    #        mathematical symbols.

    Xmat_all = []
    for l in range(lcart, -1, -2):
        Xmat_all.append(sh_cart2rl_mat(l, lcart, cartorder, csphase, increasingm))
    return Xmat_all
