""":mod:`.groups` contains public functions to construct group-related
mathematical objects, such as character tables.
"""

# pylint: disable=C0103

from __future__ import annotations

from typing import List, Dict, Tuple, Optional, Sequence, Union, cast
from collections import OrderedDict
import re
import itertools
import numpy as np  # type:ignore

from polyinspect.auxiliary.common_types import Scalar


SymDict = Dict[str, List[int]]
# CharTab = Dict[Union[SymmetrySymbol, str], Union[List]]


def ss(symstr: str) -> SymmetrySymbol:
    r"""A wrapper function to construct a symmetry symbol from its
    representation string.

    :param symstr: A string of the format
            ``^(presuper)_(presub)|main|^(postsuper)_(postsub)`` to be
            parsed. The ``presuper``, ``presub``, ``postsuper``, and
            ``postsub`` strings **cannot** contain parentheses.

    :returns: A :class:`.SymmetrySymbol` object.
    """
    return SymmetrySymbol.from_str(symstr)


class SymmetrySymbol():
    r"""A class managing symmetry symbols, such as irreducible representations
    with spin multiplicities.

    Each symbol has the format

    .. math::
        ^{\textrm{presuper}}_{\textrm{presub}}
        \textrm{main}
        ^{\textrm{postsuper}}_{\textrm{postsub}}.
    """
    def __init__(self,
                 main: str,
                 presuper: Optional[str] = None,
                 presub: Optional[str] = None,
                 postsuper: Optional[str] = None,
                 postsub: Optional[str] = None) -> None:
        r"""
        :param main: The main part of the symmetry symbol.
        :param presuper: The pre-superscript, usually used for spin
                multiplicity. If :const:`None`, an empty string will be used.
        :param presub: The pre-subscript, not usually used. If :const:`None`,
                an empty string will be used.
        :param postsuper: The post-superscript, not usually used. If
                :const:`None`, an empty string will be used.
        :param postsub: The post-subscript, usually used for distinguishing
                between different symmetries. If :const:`None`, an empty
                string will be used.
        """
        # pylint: disable=R0913
        # R0913: The arguments are all needed.
        assert len(main) > 0
        self._main = main
        if presuper is None:
            self._presuper = ""
        else:
            assert "(" not in presuper
            assert ")" not in presuper
            self._presuper = presuper
        if presub is None:
            self._presub = ""
        else:
            assert "(" not in presub
            assert ")" not in presub
            self._presub = presub
        if postsuper is None:
            self._postsuper = ""
        else:
            assert "(" not in postsuper
            assert ")" not in postsuper
            self._postsuper = postsuper
        if postsub is None:
            self._postsub = ""
        else:
            assert "(" not in postsub
            assert ")" not in postsub
            self._postsub = postsub

    @classmethod
    def from_str(cls, symstr: str) -> SymmetrySymbol:
        r"""Parse a symmetry string into a :class:`.SymmetrySymbol`.

        :param symstr: A string of the format ``main`` or
                ``^(presuper)_(presub)|main|^(postsuper)_(postsub)`` to be
                parsed. The ``presuper``, ``presub``, ``postsuper``, and
                ``postsub`` strings **cannot** contain parentheses.

        :returns: A :class:`.SymmetrySymbol` object constructed from the
                specified string.
        """
        strs = symstr.split("|")
        if len(strs) == 1:
            return cls(strs[0])
        assert len(strs) == 3, "Invalid symmetry symbol format."
        prestr = strs[0]
        mainstr = strs[1]
        poststr = strs[2]
        presuperstr_re = re.search(r"\^\((.*?)\)", prestr)
        presuperstr = ""
        if presuperstr_re is not None:
            presuperstr = presuperstr_re.group(1)
            assert "("  not in presuperstr
            assert ")"  not in presuperstr
        presubstr_re = re.search(r"_\((.*?)\)", prestr)
        presubstr = ""
        if presubstr_re is not None:
            presubstr = presubstr_re.group(1)
            assert "("  not in presubstr
            assert ")"  not in presubstr
        postsuperstr_re = re.search(r"\^\((.*?)\)", poststr)
        postsuperstr = ""
        if postsuperstr_re is not None:
            postsuperstr = postsuperstr_re.group(1)
            assert "("  not in postsuperstr
            assert ")"  not in postsuperstr
        postsubstr_re = re.search(r"_\((.*?)\)", poststr)
        postsubstr = ""
        if postsubstr_re is not None:
            postsubstr = postsubstr_re.group(1)
            assert "("  not in postsubstr
            assert ")"  not in postsubstr
        return cls(mainstr, presuperstr, presubstr, postsuperstr, postsubstr)

    @property
    def main(self) -> str:
        r"""The main part of the symmetry symbol.
        """
        return self._main

    @main.setter
    def main(self, main_str: str) -> None:
        self._main = main_str

    @property
    def presuper(self) -> str:
        r"""The pre-superscript part of the symmetry symbol.
        """
        return self._presuper

    @presuper.setter
    def presuper(self, presuper_str: str) -> None:
        self._presuper = presuper_str

    @property
    def presub(self) -> str:
        r"""The pre-subscript part of the symmetry symbol.
        """
        return self._presub

    @presub.setter
    def presub(self, presub_str: str) -> None:
        self._presub = presub_str

    @property
    def postsuper(self) -> str:
        r"""The post-superscript part of the symmetry symbol.
        """
        return self._postsuper

    @postsuper.setter
    def postsuper(self, postsuper_str: str) -> None:
        self._postsuper = postsuper_str

    @property
    def postsub(self) -> str:
        r"""The post-subscript part of the symmetry symbol.
        """
        return self._postsub

    @postsub.setter
    def postsub(self, postsub_str: str) -> None:
        self._postsub = postsub_str

    @property
    def latex_str(self) -> str:
        r"""The LaTeX string for this symmetry symbol.
        """
        if len(self.presuper) + len(self.presub) > 0:
            pre_ltx = f"\\prescript{{{self.presuper}}}{{{self.presub}}}"
            main_ltx = f"{{{self.main}}}"
        else:
            pre_ltx = ""
            main_ltx = f"{self.main}"
        post_ltx = ""
        if len(self.postsuper) > 0:
            post_ltx += f"^{{{self.postsuper}}}"
        if len(self.postsub) > 0:
            post_ltx += f"_{{{self.postsub}}}"
        return pre_ltx + main_ltx + post_ltx

    @property
    def flattened_str(self) -> str:
        r"""The flattened string for this symmetry symbol.
        """
        string =\
                self.presuper + self.presub\
                + self.main\
                + self.postsuper + self.postsub
        return string

    def __repr__(self):
        if len(self.presuper) > 0:
            presuper_str = f"^({self.presuper})"
        else:
            presuper_str = ""
        if len(self.presub) > 0:
            presub_str = f"_({self.presub})"
        else:
            presub_str = ""
        main_str = f"|{self.main}|"
        if len(self.postsuper) > 0:
            postsuper_str = f"^({self.postsuper})"
        else:
            postsuper_str = ""
        if len(self.postsub) > 0:
            postsub_str = f"_({self.postsub})"
        else:
            postsub_str = ""
        return presuper_str + presub_str + main_str + postsuper_str + postsub_str

    __str__ = __repr__

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, SymmetrySymbol):
            return NotImplemented
        equal = (self.main == other.main and
                 self.presuper == other.presuper and
                 self.presub == other.presub and
                 self.postsuper == other.postsuper and
                 self.postsub == other.postsub)
        return equal

    def __hash__(self) -> int:
        return hash(self.__repr__())


class CharacterTable():
    r"""A class managing character tables.

    Each character table contains a :attr:`.conjugacy_classes` which stores
    information of the conjugacy classes in the group, and a
    :attr:`.chartab` which contains the characters of the irreducible
    (co)representations of the group.
    """
    def __init__(self,
                 name: str,
                 ccsg: Sequence[Tuple[str, int]],
                 chartab: Optional[Dict[SymmetrySymbol,
                                        Sequence[Scalar]]] = None) -> None:
        r"""
        :param name: The name of the character table.
        :param ccsg: A sequence of conjugacy class name and its order.
        :param chartab: A dictionary of the irreducible (co)representations of
                the group and their characters.
        """
        self._name = name
        self._ccsg = tuple(ccsg)
        self._char_arr: Optional[np.ndarray] = None
        if chartab is not None and len(chartab) > 0:
            n_columns_set = {len(columns) for columns in chartab.values()}
            assert len(n_columns_set) == 1,\
                    "Inconsistent numbers of columns between different rows."
            n_columns = next(iter(n_columns_set))
            assert n_columns == len(ccsg),\
                "Inconsistent column number and number of conjugacy classes."
            assert all(isinstance(sym, SymmetrySymbol)
                       for sym in chartab.keys())
            self._chartab = OrderedDict([(sym, tuple(chars))
                                         for sym, chars in chartab.items()])
            self._char_arr = np.array([list(chars)
                                       for chars in chartab.values()])
        else:
            self._chartab = OrderedDict([])

    @classmethod
    def from_ndarray(cls, name: str, ccsg: Sequence[Tuple[str, int]],
                     irreps: Sequence[SymmetrySymbol],
                     char_arr: np.ndarray) -> CharacterTable:
        r"""Construct a character table from sequences of classes and
        irreducible (co)representations, and a matching :class:`numpy.char_arr`
        array.

        :param name: The name of the character table.
        :param ccsg: A sequence of conjugacy class name and its order.
        :param irreps: A sequence of irreducible corepresentation symbols.
        :param char_arr: A :class:`numpy.char_arr` array containing the
                characters.

        :returns: The corresponding character table.
        """
        assert char_arr.shape == (len(irreps), len(ccsg)),\
                "Mismatched array dimensions."
        chartab = OrderedDict([(irreps[i],
                                cast(Sequence[Scalar],
                                     char_arr[i, :].tolist()))
                               for i in range(len(irreps))])
        return cls(name, ccsg, chartab)


    @property
    def name(self) -> str:
        r"""The name of the character table.
        """
        return self._name

    @name.setter
    def name(self, name_str: str) -> None:
        self._name = name_str

    @property
    def ccsg(self) -> Tuple[Tuple[str, int], ...]:
        r"""The conjugacy classes and their orders in this group.

        This property has no setter.
        """
        return self._ccsg

    @property
    def ccs(self) -> Tuple[str, ...]:
        r"""The conjugacy classes.

        This property has no setter.
        """
        return tuple(conj[0] for conj in self.ccsg)

    @property
    def conj_g(self) -> Tuple[int, ...]:
        r"""The orders of the conjugacy classes.

        This property has no setter.
        """
        return tuple(conj[1] for conj in self._ccsg)

    @property
    def chartab(self) -> Dict[SymmetrySymbol, Tuple[Scalar, ...]]:
        r"""The characters of the irreducible (co)representations in this group.

        This property has no setter.
        """
        return self._chartab

    @property
    def group_order(self) -> int:
        r"""The order of the group.

        This property has no setter.
        """
        return sum(self.conj_g)

    @property
    def n_classes(self) -> int:
        r"""The number of conjugacy classes of the group.

        This property has no setter.
        """
        return len(self.ccs)

    @property
    def n_irreps(self) -> int:
        r"""The number of irreducible (co)representations of the group.

        This property has no setter.
        """
        return len(self.irreps)

    @property
    def irreps(self) -> List[SymmetrySymbol]:
        r"""A list of the irreducible (co)representations in this group.
        """
        return list(self.chartab.keys())

    @property
    def char_arr(self) -> Optional[np.ndarray]:
        r"""The array corresponding to the characters in this table.

        This property has no setter.
        """
        return self._char_arr

    def add_irrep(self, irrep_symbol: SymmetrySymbol,
                  chars: Sequence[Scalar]) -> None:
        r"""Add an irreducible (co)representation to the character table.

        :param irrep_symbol: The symmetry symbol for the (co)representation to
                be added.
        :param chars: The characters of this (co)representation.
        """
        assert len(chars) == self.n_classes,\
                "Inconsistent number of conjugacy classes supplied."
        assert irrep_symbol not in self.chartab,\
                f"Irreducible (co)representation {irrep_symbol} already exists!"
        self._chartab[irrep_symbol] = tuple(chars)
        if self.char_arr is not None:
            self._char_arr =\
                    np.vstack((self.char_arr, np.array(chars, ndmin=2)))
        else:
            self._char_arr = np.array(chars, ndmin=2)

    def __getitem__(self,
                    irrep_cc: Union[SymmetrySymbol,
                                    Tuple[SymmetrySymbol, str]])\
                                        -> Union[Tuple[Scalar, ...], Scalar]:
        r"""Query the character table to return a character value for an irrep
        and a conjugacy class.

        :param irrep_cc: A symmetry symbol or a tuple of symmetry symbol and
                conjugacy class.

        :return: The corresponding character value.
        """
        if isinstance(irrep_cc, SymmetrySymbol):
            irrep = irrep_cc
            return self.chartab[irrep]
        irrep, cc = irrep_cc
        cc_idx = self.ccs.index(cc)
        return self.chartab[irrep][cc_idx]

    def __str__(self) -> str:
        first_width = max(len(str(irrep)) for irrep in self.irreps) + 2
        dps = 3
        assert self.char_arr is not None
        char_width = max(len(f"{char:>+.{dps}f}")
                         for char in self.char_arr.flatten())
        cc_width = max(len(f"{str(g)} {cc}") for cc, g in self.ccsg)
        digit_width = max(char_width, cc_width) + 2
        stg = f"{self.name:^{first_width}} │"
        for cc, g in self.ccsg:
            stg += f"{f'{str(g)} {cc}':>{digit_width}} │"
        stg = stg[:-2]
        tab_width = len(stg)
        stg = "━"*tab_width + "\n" + stg
        stg += "\n"
        stg += "┈"*tab_width
        stg += "\n"
        for irrep in self.irreps:
            stg += f"{str(irrep):^{first_width}} │"
            for cc in self.ccs:
                char = self[irrep, cc]
                stg += f"{char:>+{digit_width}.{dps}f} │"
            stg = stg[:-2] + "\n"
        stg += "━"*tab_width + "\n"
        return stg


def chartab_cyclic(n: int, double: bool = False) -> CharacterTable:
    r"""Construct the character table for a cyclic group of any order.

    The cyclic group :math:`\mathcal{C}_n` has order :math:`n` and presentation
    :math:`\braket{r \mid r^n = e}`.
    In the context of molecular symmetry, the group generator :math:`r` is
    identified with the :math:`\hat{C}_n` rotation operation.

    The double cyclic group :math:`\mathcal{C}^*_n` is isomorphic to
    :math:`\mathcal{C}_{2n}` and therefore has order :math:`2n` and presentation
    :math:`\braket{r \mid r^{2n} = e}`.

    All cyclic groups are abelian.

    :param n: The integer :math:`n` characterising the cyclic group.
    :param double: If :const:`False`, the group is :math:`\mathcal{C}_n`.
            If :const:`True`, the group is :math:`\mathcal{C}^*_n`.

    :returns: The character table of the specified group.
    """
    cc: List[Tuple[str, int]] = []
    chartab_list: List[Tuple[SymmetrySymbol, Sequence[Scalar]]] = []

    # Determine the order of the group
    if double:
        order = 2*n
        group_name = f"C*{n}"
    else:
        order = n
        group_name = f"C{n}"

    # Add conjugacy classes: abelian, so each element is in its own class
    cc.append(("E", 1))
    for k in range(1, order):
        if k != n:
            class_name = f"C_{n}^{k}"
        else:
            class_name = "Q"
        cc.append((class_name, 1))

    # Add totally symmetric irrep
    chartab_list.append((SymmetrySymbol("A"), [1]*order))

    # If the order is even, there is always one B irrep.
    if order % 2 == 0:
        chartab_list.append((SymmetrySymbol("B"),
                             [(-1)**k for k in range(order)]))

    # The remaining irreps come in complex-conjugate pairs
    for irep in range(1, np.ceil(order/2).astype(int)):
        chartab_list.append((SymmetrySymbol("Gamma", postsub=f"{irep}"),
                             [np.exp(2j*np.pi*irep*k/(order))
                              for k in range(order)]))
        chartab_list.append((SymmetrySymbol("Gamma", postsub=f"{-irep}"),
                             [np.exp(-2j*np.pi*irep*k/(order))
                              for k in range(order)]))

    chartab = OrderedDict(chartab_list)
    return CharacterTable(group_name, cc, chartab)


def direct_product(name: str, ct1: CharacterTable, ct2: CharacterTable)\
        -> CharacterTable:
    r"""Construct the character table for the direct-product group of the two
    groups whose character tables are `ct1` and `ct2`.

    .. note::
        The direct-product operation is non-commutative.
        The direct product can only be taken if the two groups have no elements
        in common except the identity, and if the (co)representation of each
        group lies in the centralizer of the (co)representation of the other.

    :param name: Name of the new character table.
    :param ct1: The first character table.
    :param ct2: The second character table. The symmetry symbols for the
            irreducible (co)representations in this table must not have
            any prescripts.

    :returns: The direct-product character table.
    """
    assert all(len(irrep.presuper) + len(irrep.presub) == 0
               for irrep in ct2.irreps)
    irreps = tuple(ss(f"^({irrep1.flattened_str})"
                      + f"|{irrep2.main}|"
                      + f"^({irrep2.postsuper})_({irrep2.postsub})")
                   for irrep1, irrep2 in\
                           itertools.product(ct1.irreps, ct2.irreps))
    ccs = tuple(f"{cc1}.{cc2}"
                for cc1, cc2 in itertools.product(ct1.ccs, ct2.ccs))
    conj_g = tuple(g1*g2
                   for g1, g2 in itertools.product(ct1.conj_g, ct2.conj_g))
    ccsg = tuple(zip(ccs, conj_g))
    char_arr = np.kron(ct1.char_arr, ct2.char_arr)
    return CharacterTable.from_ndarray(name, ccsg, irreps, char_arr)
