""":mod:`.symmetry_core` contains the core classes and functions to handle
spatial symmetry and point groups of molecules.
"""

# pylint: disable=C0302
# C0302: Moving the auxiliary functions to another module will likely create
#        circular imports.

from __future__ import annotations

from typing import List, Dict, Optional, Tuple, Sequence
from math import ceil, gcd
from itertools import combinations
from sympy import divisors  # type: ignore
from scipy.spatial.distance import cdist  # type: ignore
import numpy as np  # type: ignore

from polyinspect.auxiliary.common_types import RealScalar
from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE, diff_rel
from polyinspect.auxiliary.geometrical_space import Vector, improper_rotation_matrix,\
        check_regular_polygon, proper_rotation_matrix
from polyinspect.auxiliary.chemical_structures import Atom, Molecule


# Defining rotational symmetry codes
ROTSYM_SPHTOP = 1
ROTSYM_SYMTOP_OBLATE = 2
ROTSYM_SYMTOP_OBLATE_PLANAR = 3
ROTSYM_SYMTOP_PROLATE = 4
ROTSYM_SYMTOP_PROLATE_LINEAR = 5
ROTSYM_ASYMTOP = 6
ROTSYM_ASYMTOP_PLANAR = 7


# Defining parameters for checking count_c2 convergence
# The number of pairs to check before turning on guard
START_GUARD = 30
# Threshold for count_c2_stable/n_pairs to exceed beyond stability
STABLE_C2_RATIO = 0.5


class Symmetry():
    r"""A class containing and managing symmetry information of a molecule.
    """
    # pylint: disable=R0902
    # R0902: The high number of instance attributes is needed because this is
    #        a fairly complicated class that cannot be broken down.

    def __init__(self, molecule: Molecule,
                 moi_thresh: float = COMMON_ZERO_TOLERANCE,
                 dist_thresh: float = COMMON_ZERO_TOLERANCE) -> None:
        r"""
        :param molecule: A :class:`~src.auxiliary.chemical_structures.Molecule`
                object containing information of the molecule for which symmetry
                information is to be obtained and stored in the current
                :class:`.Symmetry` object.
        :param moi_thresh: Threshold for moment of inertia comparisons.
        :param dist_thresh: Threshold for distance comparisons.
        """
        assert moi_thresh > 0
        assert dist_thresh > 0
        self._moi_thresh = moi_thresh
        self._dist_thresh = dist_thresh
        self._rotational_symmetry = np.nan
        self._seass: List[List[Atom]] = []
        self._point_group = ""
        self._proper_generators: Dict[int, List[Vector]] = {}
        self._improper_generators: Dict[int, List[Vector]] = {}
        self._sigma_generators: Dict[str, List[Vector]] = {}
        self._proper_elements: Dict[int, List[Vector]] = {1:
                                                          [Vector([0, 0, 1])]}
        self._improper_elements: Dict[int, List[Vector]] = {}
        self._sigma_elements: Dict[str, List[Vector]] = {}
        self.molecule = molecule

    @property
    def molecule(self) -> Molecule:
        r"""The molecule with which the current :class:`.Symmetry` object is
        associated.

        Changing the molecule associated with the current :class:`.Symmetry`
        object will cause all internal attributes to be reset.

        Point group information and symmetry elements will need to be
        re-evaluated via :meth:`.analyse`.
        """
        return self._molecule

    @molecule.setter
    def molecule(self, mol: Molecule) -> None:
        self._rotational_symmetry = np.nan
        self._seass = []
        self._point_group = ""
        self._proper_generators = {}
        self._improper_generators = {}
        self._sigma_generators = {}
        self._proper_elements = {1: [Vector([0, 0, 1])]}
        self._improper_elements = {}
        self._sigma_elements = {}
        self._molecule = mol

    @property
    def rotational_symmetry(self) -> int:
        r"""An integer code denoting the rotational symmetry of the associated
        :attr:`.molecule` based on its moments of inertia.

        This code follows that defined in the original implementation of the
        algorithm described in :cite:`article:Beruski2014` by the same authors.
        The possible values are:

        - {} = spherical top
        - {} = symmetric top: oblate
        - {} = symmetric top: oblate, planar
        - {} = symmetric top: prolate
        - {} = linear
        - {} = asymmetric top
        - {} = asymmetric top: planar

        This property has no setter and can only be set during symmetry
        analysis at initialisation, when :attr:`.molecule` is changed or when
        :meth:`.analyse()` is called.
        """
        return int(self._rotational_symmetry)

    if rotational_symmetry.__doc__ is not None:
        rotational_symmetry.__doc__ =\
                rotational_symmetry.__doc__.format(ROTSYM_SPHTOP,
                                                   ROTSYM_SYMTOP_OBLATE,
                                                   ROTSYM_SYMTOP_OBLATE_PLANAR,
                                                   ROTSYM_SYMTOP_PROLATE,
                                                   ROTSYM_SYMTOP_PROLATE_LINEAR,
                                                   ROTSYM_ASYMTOP,
                                                   ROTSYM_ASYMTOP_PLANAR)

    @property
    def point_group(self) -> str:
        r"""The point group of :attr:`.molecule` in Schönflies notation.

        This property has no setter and can only be set during symmetry
        analysis at initialisation, when :attr:`.molecule` is changed or when
        :meth:`.analyse()` is called.
        """
        return self._point_group

    @property
    def sea_groups(self) -> List[List[Atom]]:
        r"""A list of lists containing the symmetrically equivalent atoms
        (SEAs) in :attr:`.molecule`.

        Each group of symmetrically equivalent atoms is contained in a sublist.

        This property has no setter and can only be set during symmetry
        analysis at initialisation, when :attr:`.molecule` is changed, or when
        :meth:`.analyse` is invoked.
        """
        return self._seass

    @property
    def proper_generators(self) -> Dict[int, List[Vector]]:
        r"""A dictionary containing the proper rotation generators possessed by
        the :attr:`.molecule` associated with this :class:`.Symmetry` object.

        Each `key` in the dictionary is an integer giving the order of the
        rotation, and the associated `value` gives a list of rotation axes of
        that order.
        If `key` is positive, it is equal to the order :math:`n` in :math:`C_n`.
        If `key` is ``-1``, :math:`n = \infty`.

        This property has no setter.
        """
        return self._proper_generators

    @property
    def improper_generators(self) -> Dict[int, List[Vector]]:
        r"""A dictionary containing the improper rotation generators possessed by
        the :attr:`.molecule` associated with this :class:`.Symmetry` object.

        Each `key` in the dictionary is an integer giving the order of the
        rotation, and the associated `value` gives a list of improper rotation
        axes of that order.
        If `key` is positive, it is equal to the order :math:`n` in :math:`S_n`.
        If `key` is ``-1``, :math:`n = \infty`.

        This property has no setter.
        """
        return self._improper_generators

    @property
    def proper_elements(self) -> Dict[int, List[Vector]]:
        r"""A dictionary containing the proper rotation elements possessed by
        the :attr:`.molecule` associated with this :class:`.Symmetry` object.

        Each `key` in the dictionary is a finite and positive integer giving
        the order :math:`n` of the rotation :math:`C_n`, and the associated
        `value` gives a list of rotation axes of that order.

        This property has no setter.
        """
        return self._proper_elements

    @property
    def improper_elements(self) -> Dict[int, List[Vector]]:
        r"""A dictionary containing the improper rotation elements possessed by
        the :attr:`.molecule` associated with this :class:`.Symmetry` object.

        Each `key` in the dictionary is a finite and positive integer giving
        the order :math:`n` of the improper rotation :math:`S_n`, and the
        associated `value` gives a list of improper rotation axes of that order.

        This property has no setter.
        """
        return self._improper_elements

    @property
    def sigma_generators(self) -> Dict[str, List[Vector]]:
        r"""A dictionary containing the mirror plane generators possessed by the
        :attr:`.molecule` associated with this :class:`.Symmetry` object.

        This property has no setter.
        """
        return self._sigma_generators

    @property
    def sigma_elements(self) -> Dict[str, List[Vector]]:
        r"""A dictionary containing the mirror planes possessed by the
        :attr:`.molecule` associated with this :class:`.Symmetry` object.

        This property has no setter.
        """
        return self._sigma_elements

    @property
    def max_proper_order(self) -> int:
        r"""The highest proper rotation order of the molecule.

        If ``-1`` is obtained, the highest order is :math:`\infty`.
        """
        if -1 in self.proper_generators.keys():
            return -1
        return max(self.proper_elements.keys())

    @property
    def moi_thresh(self) -> float:
        r"""The threshold for moment of inertia comparisons.

        This property has no setter.
        """
        return self._moi_thresh

    @property
    def dist_thresh(self) -> float:
        r"""The threshold for distance comparisons.

        This property has no setter.
        """
        return self._dist_thresh

    def analyse(self,
                moi_thresh: Optional[float] = None,
                dist_thresh: Optional[float] = None) -> None:
        r"""Analyse the symmetry of :attr:`.molecule` and populate the
        attributes of this :class:`.Symmetry` objects with relevant values.

        :param moi_thresh: Threshold for moment of inertia comparisons. If
                :const:`None`, :attr:`.moi_thresh` will be used.
        :param dist_thresh: Threshold for distance comparisons.
        """
        if moi_thresh is not None:
            self._moi_thresh = moi_thresh

        if dist_thresh is not None:
            self._dist_thresh = dist_thresh

        self._rotational_symmetry =\
                self._determine_rotational_symmetry()
        self._seass = self._find_sea_groups()
        if self.rotational_symmetry == ROTSYM_SPHTOP:
            self._analyse_spherical()
        elif self.rotational_symmetry in [ROTSYM_SYMTOP_OBLATE,
                                          ROTSYM_SYMTOP_OBLATE_PLANAR,
                                          ROTSYM_SYMTOP_PROLATE]:
            self._analyse_symmetric()
        elif self.rotational_symmetry == ROTSYM_SYMTOP_PROLATE_LINEAR:
            self._analyse_linear()
        else:
            self._analyse_asymmetric()

    def proper_permutation(self, n: Optional[int] = None, power: int = 1,
                           index: Optional[int] = None,
                           angle: Optional[RealScalar] = None,
                           axis: Optional[Vector] = None,
                           dist_thresh: Optional[float] = None)\
                                   -> Optional[List[int]]:
        r"""Apply a proper operation :math:`C_n^p` on :attr:`.molecule` and,
        if :math:`C_n^p` is a symmetry operation of attr:`.molecule`, give
        the permutation of the atoms brought about by this operation.

        :param n: Order :math:`n` of the proper rotation. A positive rotation
                angle is an anticlockwise rotation when looking down `axis`.
                If this is provided, `angle` will be ignored.
        :param power: The exponent :math:`p` to which this operation is raised.
        :param index: The index of the required proper rotation axis of order
                `n` in :attr:`proper_elements`. If this is provided, `axis`
                will be ignored.
        :param angle: The angle of rotation. A positive rotation angle is an
                anticlockwise rotation when looking down `axis`. This will only
                be used if `n` is not provided.
        :param axis: A vector determining the direction of the rotation axis.
                This vector will be normalised and positised. This will only
                be used if `index` is not provided.
        :param dist_thresh: Threshold for comparing distances between the atoms
                before and after the rotation.

        :returns: A list showing the new indices to which the atoms are moved
                if the specified :math:`C_n^p` is indeed a symmetry operation,
                :const:`None` otherwise.
        """
        # pylint: disable=C0103,R0913
        # C0103: Cn and n are so named to match their mathematical symbols.
        if dist_thresh is None:
            dist_thresh = self.dist_thresh

        if n is not None:
            angle = 2*np.pi/n
        assert angle is not None

        if index is not None:
            assert n is not None
            axis = self.proper_elements[n][index]
        else:
            assert axis is not None
            axis = axis.normalise().positise()
        tmat = proper_rotation_matrix(angle, axis, power)
        original_positions = np.array([atom.position.coordinates/atom.mass
                                       for atom in self.molecule])
        transformed_positions = np.dot(original_positions, tmat)

        comparison_distmat = cdist(original_positions, transformed_positions,
                                   "euclidean")
        maxdist = np.max(comparison_distmat)
        if maxdist > dist_thresh:
            comparison_distmat /= maxdist
        comparison_distmat_zeroidx = np.where(comparison_distmat < dist_thresh)
        if comparison_distmat_zeroidx[1].shape[0]\
                != original_positions.shape[0]:
            return None
        return comparison_distmat_zeroidx[1].tolist()

    def improper_permutation(self, n: Optional[int] = None, power: int = 1,
                             index: Optional[int] = None,
                             angle: Optional[RealScalar] = None,
                             axis: Optional[Vector] = None,
                             dist_thresh: Optional[float] = None)\
                                     -> Optional[List[int]]:
        r"""Apply an improper operation :math:`S_n^p` on :attr:`.molecule` and,
        if :math:`S_n^p` is a symmetry operation of attr:`.molecule`, give
        the permutation of the atoms brought about by this operation.

        :param n: Order :math:`n` of the improper rotation. A positive rotation
                angle is an anticlockwise rotation when looking down `axis`.
                If this is provided, `angle` will be ignored.
        :param power: The exponent :math:`p` to which this operation is raised.
        :param index: The index of the required proper rotation axis of order
                `n` in :attr:`improper_elements`. If this is provided, `axis`
                will be ignored.
        :param angle: The angle of improper rotation. A positive rotation angle
                is an anticlockwise rotation when looking down `axis`. This will
                only be used if `n` is not provided.
        :param axis: A vector determining the direction of the rotation axis.
                This vector will be normalised and positised. This will only
                be used if `index` is not provided.
        :param dist_thresh: Threshold for comparing distances between the atoms
                before and after the rotation.

        :returns: A list showing the new indices to which the atoms are moved
                if the specified :math:`S_n^p` is indeed a symmetry operation,
                :const:`None` otherwise.
        """
        # pylint: disable=C0103,R0913
        # C0103: Cn and n are so named to match their mathematical symbols.
        if dist_thresh is None:
            dist_thresh = self.dist_thresh

        if n is not None:
            angle = 2*np.pi/n
        assert angle is not None

        if index is not None:
            assert n is not None
            axis = self.improper_elements[n][index]
        else:
            assert axis is not None
            axis = axis.normalise().positise()
        tmat = improper_rotation_matrix(angle, axis, power)
        original_positions = np.array([atom.position.coordinates/atom.mass
                                       for atom in self.molecule])
        transformed_positions = np.dot(original_positions, tmat)

        comparison_distmat = cdist(original_positions, transformed_positions,
                                   "euclidean")
        maxdist = np.max(comparison_distmat)
        if maxdist > dist_thresh:
            comparison_distmat /= maxdist
        comparison_distmat_zeroidx = np.where(comparison_distmat < dist_thresh)
        if comparison_distmat_zeroidx[1].shape[0]\
                != original_positions.shape[0]:
            return None
        return comparison_distmat_zeroidx[1].tolist()


    def _determine_rotational_symmetry(self,
                                       moi_thresh: Optional[float] = None)\
                                               -> int:
        r"""Determine the rotational symmetry of :attr:`.molecule` and set the
        property :attr:`.rotational_symmetry`.

        :param moi_thresh: Threshold for moment of inertia comparisons.
        """
        # pylint: disable=R0911
        # R0911: There are seven return statements for seven types of rotational
        #        symmetry.
        if moi_thresh is None:
            moi_thresh = self.moi_thresh
        moi, _ = self.molecule.moi
        if sum(abs(moi_x) < moi_thresh for moi_x in moi) >= 2:
            # full rotation
            return ROTSYM_SPHTOP

        if diff_rel(moi[0], moi[1]) < moi_thresh:
            # spherical or oblate
            if diff_rel(moi[1], moi[2]) < moi_thresh:
                # spherical
                return ROTSYM_SPHTOP
            # abs(moi[0] - moi[2]) > 2*thresh => oblate
            if diff_rel(moi[2], (moi[0] + moi[1])) < moi_thresh:
                # oblate, planar
                return ROTSYM_SYMTOP_OBLATE_PLANAR
            return ROTSYM_SYMTOP_OBLATE

        if diff_rel(moi[1], moi[2]) < moi_thresh:
            # prolate
            if abs(moi[0]) < moi_thresh:
                # linear
                return ROTSYM_SYMTOP_PROLATE_LINEAR
            return ROTSYM_SYMTOP_PROLATE

        # asymmetric top
        if diff_rel(moi[2], (moi[0] + moi[1])) < moi_thresh:
            # asymmetric, planar
            return ROTSYM_ASYMTOP_PLANAR
        return ROTSYM_ASYMTOP

    def _find_sea_groups(self, dist_thresh: Optional[float] = None)\
            -> List[List[Atom]]:
        r"""Construct groups of Symmetrically Equivalent Atoms (SEAs) in
        :attr:`.molecule`.

        :param dist_thresh: The threshold for interatomic distance comparisons.
                Two distances :math:`d_1` and :math:`d_2` are approximately
                equal if :math:`\lvert d_1 - d_2 \rvert <` `dist_thresh`.
                If :const:`None`, :attr:`.dist_thresh` will be used.

        :returns: A list of lists where each inner list contains references
                to symmetrically equivalent atoms in :attr:`.molecule`.
        """
        if dist_thresh is None:
            dist_thresh = self.dist_thresh
        assert dist_thresh > 0
        # Constructing the non-symmetric distance matrix:
        # Dij = dist(i, j)/mi
        all_coords = np.array([atom.position.coordinates
                               for atom in self.molecule])
        all_masses = np.array([atom.mass for atom in self.molecule])
        distmat = cdist(all_coords, all_coords, "euclidean")
        distmat /= all_masses[:, None]

        digits = -int(ceil(np.log10(dist_thresh)))
        distmat = np.sort(distmat, axis=0)  # sort each column of distmat
        max_dist = np.max(distmat)
        if max_dist > dist_thresh:
            distmat /= max_dist
        distmat = np.around(distmat, decimals=digits)
        unique, unique_indices = np.unique(distmat, axis=1, return_inverse=True)
        # unique contains the unique columns of distmat
        # unique_indices shows how the columns in distmat map to the
        #   unique columns

        seass: List[List[Atom]] = [[] for _ in range(unique.shape[1])]
        for i, atom in enumerate(self.molecule):
            seass[unique_indices[i]].append(atom)
        return seass

    def _add_proper(self, n: int, axis: Vector, generator: bool = False,
                    thresh: Optional[float] = None) -> bool:
        r"""Add a proper rotation operation of order `n` (:math:`C_n`) along
        `axis` after checking if this axis is already present.

        :param n: Order of the proper rotation operation. If `n` is positive,
                then the rotation order :math:`n` is given by `n`. If `n` is
                ``-1``, then :math:`n = \infty` for an infinitesimal generator.
        :param axis: A vector determining the axis of the proper rotation
                operation. This vector will be normalised and positised.
        :param generator: If :const:`True`, this operation will be added as a
                generator. If :const:`False`, it will be added as an element
                instead.
        :param thresh: Threshold for comparing `axis` and existing axes of the
                same `n` in the appropriate generator or element set.

        :returns: :const:`True` if the specified :math:`C_n` rotation is not
                present and has just been added to the current element or
                generator set of the :class:`.Symmetry` object,
                :const:`False` otherwise.
        """
        # pylint: disable=C0103
        # C0103: n is so named to match its mathematical symbol.
        assert axis.dim == 3, "Only axes in three dimensions are allowed."
        assert n > 0 or n == -1
        if thresh is None:
            thresh = self.dist_thresh
        axis = axis.normalise().positise()
        if generator:
            destination = self._proper_generators
            dest = "generator"
        else:
            assert n > 0, "C∞ is not a valid element."
            destination = self._proper_elements
            dest = "element"
        if n in destination.keys():
            if any(cur_axis.is_same_as(axis, thresh) or
                   cur_axis.is_same_as(-axis, thresh)
                   for cur_axis in destination[n]):
                return False
            destination[n].append(axis)
        else:
            destination[n] = [axis]
        if n > 0:
            print(f"Proper rotation {dest}: C_{n} along {axis} added.")
        else:
            print(f"Proper rotation {dest}: C∞ along {axis} added.")
        return True

    def _add_improper(self, n: int, axis: Vector, generator: bool = False,
                      thresh: Optional[float] = None,
                      sigma: str = '') -> bool:
        r"""Add an improper rotation element or generator of order `n`
        (:math:`S_n`) along `axis` after checking if this axis is already
        present.
        If `sigma` is specified for `n` = 1, the :math:`S_n` axis is also
        classified into the appropriate mirror plane type.

        :param n: Order of the improper rotation operation. `n` must be
                positive, then the rotation order :math:`n` is given by `n`.
                `n` cannot be ``-1`` as there cannot be infinitesimal
                improper generators.
        :param axis: A vector determining the axis of the improper rotation
                operation. This vector will be normalised and positised.
        :param generator: If :const:`True`, this operation will be added as a
                generator. If :const:`False`, it will be added as an element
                instead.
        :param thresh: Threshold for comparing `axis` and existing axes of the
                same `n` in the appropriate generator or element set.

        :returns: :const:`True` if the specified :math:`S_n` rotation is not
                present and has just been added to the current element or
                generator set of the :class:`.Symmetry` object,
                :const:`False` otherwise.
        """
        # pylint: disable=C0103,R0913
        # C0103: Sn and n are so named to match their mathematical symbols.
        # R0913: The number of arguments is needed for a full specification
        #        of the improper element.
        assert axis.dim == 3, "Only axes in three dimensions are allowed."
        assert n > 0, "S∞ is not a valid element or generator."
        if thresh is None:
            thresh = self.dist_thresh
        axis = axis.normalise().positise()
        if generator:
            destination = self._improper_generators
            sigma_destination = self._sigma_generators
            dest = "generator"
        else:
            destination = self._improper_elements
            sigma_destination = self._sigma_elements
            dest = "element"
        if n in destination.keys():
            if n == 2 and len(destination[2]) > 0:
                return False
            if any(cur_axis.is_same_as(axis, thresh) or
                   cur_axis.is_same_as(-axis, thresh)
                   for cur_axis in destination[n]):
                return False
            destination[n].append(axis)
        else:
            destination[n] = [axis]
        if n == 1:
            assert sigma in ['', 'd', 'v', 'h'], "Invalid mirror plane type."
            if sigma in sigma_destination.keys():
                sigma_destination[sigma].append(axis)
            else:
                sigma_destination[sigma] = [axis]
            print(f"Mirror plane {dest} (σ{sigma}): S_{n} along {axis} added.")
        elif n == 2:
            print(f"Inversion centre {dest} (i): S_{n} along {axis} added.")
        else:
            print(f"Improper rotation {dest}: S_{n} along {axis} added.")
        return True

    def _check_proper(self, n: int, axis: Vector,
                      dist_thresh: Optional[float] = None) -> bool:
        r"""Check for the existence of a :math:`C_n` axis along `axis` in
        :attr:`.molecule`.

        :param n: Order of the proper rotation.
        :param axis: A vector determining the direction of the rotation axis.
                This vector will be normalised and positised.
        :param dist_thresh: Threshold for comparing distances between the atoms
                before and after the rotation.

        :returns: :const:`True` if the specified :math:`C_n` axis is present,
                :const:`False` otherwise.
        """
        # pylint: disable=C0103
        # C0103: Cn and n are so named to match their mathematical symbols.
        if dist_thresh is None:
            dist_thresh = self.dist_thresh

        angle = 2*np.pi/n
        axis = axis.normalise().positise()
        tmat = proper_rotation_matrix(angle, axis)
        original_positions = np.array([atom.position.coordinates/atom.mass
                                       for atom in self.molecule])
        transformed_positions = np.dot(original_positions, tmat.T)

        comparison_distmat = cdist(original_positions, transformed_positions,
                                   "euclidean")
        maxdist = np.max(comparison_distmat)
        if maxdist > dist_thresh:
            comparison_distmat /= maxdist
        coincidence_count = np.sum(np.any(comparison_distmat < dist_thresh,
                                          axis=0))
        return coincidence_count == len(self.molecule.atoms)

    def _check_improper(self, n: int, axis: Vector,
                        dist_thresh: Optional[float] = None) -> bool:
        r"""Check for the existence of an :math:`S_n` axis along `axis` in
        :attr:`.molecule`.

        :param n: Order of the improper rotation.
        :param axis: A vector determining the direction of the rotation axis
                and the normal of plane of the associated reflection. This
                vector will be normalised and positised.
        :param dist_thresh: Threshold for comparing distances between the atoms
                before and after the rotation.

        :returns: :const:`True` if the specified :math:`S_n` axis is present,
                :const:`False` otherwise.
        """
        # pylint: disable=C0103
        # C0103: Sn and n are so named to match their mathematical symbols.
        if dist_thresh is None:
            dist_thresh = self.dist_thresh

        angle = 2*np.pi/n
        axis = axis.normalise().positise()
        tmat = improper_rotation_matrix(angle, axis)
        original_positions = np.array([atom.position.coordinates/atom.mass
                                       for atom in self.molecule])
        transformed_positions = np.dot(original_positions, tmat.T)

        comparison_distmat = cdist(original_positions, transformed_positions,
                                   "euclidean")
        maxdist = np.max(comparison_distmat)
        if maxdist > dist_thresh:
            comparison_distmat /= maxdist
        coincidence_count = np.sum(np.any(comparison_distmat < dist_thresh,
                                          axis=0))
        return coincidence_count == len(self.molecule.atoms)

    def _analyse_linear(self, dist_thresh: Optional[float] = None) -> None:
        r"""Internal method to analyse the symmetry of a linear system and
        populate the relevant attributes in this :class:`.Symmetry` object.

        The possible linear point groups are :math:`\mathcal{C}_{\infty v}` and
        :math:`\mathcal{D}_{\infty h}`.

        :param dist_thresh: The threshold for distance comparisons. If
                :const:`None`, :attr:`.dist_thresh` will be used.
        """
        # pylint: disable=W0212
        # W0212: Accessing _check_improper is okay here since we are still in the
        #        implementation of Symmetry.
        if dist_thresh is None:
            dist_thresh = self.dist_thresh

        assert self.rotational_symmetry == ROTSYM_SYMTOP_PROLATE_LINEAR
        assert all(len(sea_group) <= 2 for sea_group in self.sea_groups)
        sea_inversion_count = sum(Symmetry(Molecule(sea_group),
                                           self.moi_thresh,
                                           self.dist_thresh)\
                                    ._check_improper(2, Vector([0, 0, 1]))
                                  for sea_group in self.sea_groups)

        if sea_inversion_count == len(self.sea_groups):
            self._point_group = "D∞h"
            _, principal_axes = self.molecule.moi
            self._add_proper(-1, principal_axes[0], generator=True)  # C∞
            self._add_proper(2, principal_axes[1], generator=True)  # C2
            self._add_improper(1, principal_axes[0],
                               generator=True, sigma='h')  # σh
            self._add_improper(2, Vector([0, 0, 1]), generator=False)  # i
        else:
            self._point_group = 'C∞v'
            _, principal_axes = self.molecule.moi
            self._add_proper(-1, principal_axes[0], generator=True)  # C∞
            self._add_improper(1, principal_axes[1],
                               generator=True, sigma='v')  # σv

    def _analyse_spherical(self, moi_thresh: Optional[float] = None,
                           dist_thresh: Optional[float] = None) -> None:
        r"""Internal method to analyse the symmetry of a spherical top system
        and populate the relevant attributes in this :class:`.Symmetry` object.

        The possible spherical top point groups are :math:`\mathcal{T}`,
        :math:`\mathcal{T}_d`, :math:`\mathcal{T}_h`, :math:`\mathcal{O}`,
        :math:`\mathcal{O}_h`, :math:`\mathcal{I}`, :math:`\mathcal{I}_h`, and
        :math:`\mathsf{O}(3)`.

        :param moi_thresh: Threshold for moment of inertia comparisons. If
                :const:`None`, :attr:`.moi_thresh` will be used.
        :param dist_thresh: The threshold for distance comparisons. If
                :const:`None`, :attr:`.dist_thresh` will be used.
        """
        # pylint: disable=R0912,R0914,R0915
        # R0912, R0914, R0915: It is possible to pull out the implementations
        #                      of the various checks, but that would make the
        #                      code lose its coherence and become fragmented.
        if moi_thresh is None:
            moi_thresh = self.moi_thresh
        if dist_thresh is None:
            dist_thresh = self.dist_thresh

        assert self.rotational_symmetry == ROTSYM_SPHTOP
        if sum(abs(moi_x) < moi_thresh for moi_x in self.molecule.moi[0]) >= 2:
            assert len(self.molecule.atoms) == 1
            self._point_group = "O(3)"
            self._add_proper(-1, Vector([0, 0, 1]), generator=True)
            self._add_proper(-1, Vector([0, 1, 0]), generator=True)
            self._add_proper(-1, Vector([1, 0, 0]), generator=True)
            return

        # Locating all possible and distinct C2 axes
        count_c2 = _search_c2_spherical(self, c2_termination_counts=[3, 9, 15])
        print(f"Located {count_c2} C2 axes.")
        assert count_c2 in [3, 9, 15]

        # Locating improper elements
        if count_c2 == 3:
            # Tetrahedral, so either T, Td, or Th
            print("Tetrahedral family.")
            if self._check_improper(2, Vector([0, 0, 1])):
                # Inversion centre
                print("Located an inversion centre.")
                self._point_group = "Th"
                assert self._add_improper(2, Vector([0, 0, 1]))
                assert self._add_improper(2, Vector([0, 0, 1]), generator=True)

            elif self._check_improper(1, self.proper_elements[2][0]\
                                            + self.proper_elements[2][1]):
                # σd
                print("Located σd.")
                self._point_group = "Td"
                for (c2_axis_i, c2_axis_j) in\
                        combinations(self.proper_elements[2], r=2):
                    assert self._check_improper(1, c2_axis_i + c2_axis_j)
                    assert self._add_improper(1, c2_axis_i + c2_axis_j,
                                              sigma="d")
                    assert self._check_improper(1, c2_axis_i - c2_axis_j)
                    assert self._add_improper(1, c2_axis_i - c2_axis_j,
                                              sigma="d")

                assert self._add_improper(1, self.improper_elements[1][0],
                                          generator=True, sigma="d")

            else:
                # Chiral
                self._point_group = "T"

        elif count_c2 == 9:
            # 6 C2 and 3 C4^2; Octahedral, so either O or Oh
            print("Octahedral family.")
            if self._check_improper(2, Vector([0, 0, 1])):
                # Inversion centre
                print("Located an inversion centre.")
                self._point_group = "Oh"
                assert self._add_improper(2, Vector([0, 0, 1]))
                assert self._add_improper(2, Vector([0, 0, 1]), generator=True)

            else:
                # Chiral
                self._point_group = "O"

        elif count_c2 == 15:
            # Icosahedral, so either I or Ih
            print("Icosahedral family.")
            if self._check_improper(2, Vector([0, 0, 1])):
                # Inversion centre
                print("Located an inversion centre.")
                self._point_group = "Ih"
                assert self._add_improper(2, Vector([0, 0, 1]))
                assert self._add_improper(2, Vector([0, 0, 1]), generator=True)

            else:
                # Chiral
                self._point_group = "I"

        # Locating all possible and distinct C3 axes
        count_c3 = 0
        found_consistent_c3 = False
        for sea_group in self.sea_groups:
            if len(sea_group) < 3:
                continue
            if found_consistent_c3:
                break
            for atom3s in combinations(sea_group, r=3):
                if not check_regular_polygon([atom.position
                                              for atom in atom3s],
                                             dist_thresh):
                    continue
                vec_01 = atom3s[0].position.get_vector_to(atom3s[1].position)
                vec_02 = atom3s[0].position.get_vector_to(atom3s[2].position)
                vec_normal = vec_01.cross(vec_02)
                assert vec_normal.norm > dist_thresh
                if self._check_proper(3, vec_normal):
                    count_c3 += self._add_proper(3, vec_normal)
                if count_c2 == 3 and count_c3 == 4:
                    # Tetrahedral, 4 C3 axes
                    found_consistent_c3 = True
                    break
                if count_c2 == 9 and count_c3 == 4:
                    # Octahedral, 4 C3 axes
                    found_consistent_c3 = True
                    break
                if count_c2 == 15 and count_c3 == 10:
                    # Icosahedral, 10 C3 axes
                    found_consistent_c3 = True
                    break

        assert found_consistent_c3

        if count_c3 == 4:
            # Tetrahedral or octahedral
            for c3_axis in self.proper_elements[3]:
                self._add_proper(3, c3_axis, generator=True)

        # Locating all possible and distinct C4 axes for O and Oh point groups
        if count_c2 == 9:
            count_c4 = 0
            found_consistent_c4 = False
            for sea_group in self.sea_groups:
                if found_consistent_c4:
                    break
                if len(sea_group) < 4:
                    continue
                for atom4s in combinations(sea_group, r=4):
                    if not check_regular_polygon([atom.position
                                                  for atom in atom4s],
                                                 dist_thresh):
                        continue

                    vec_01 = atom4s[0].position.get_vector_to(atom4s[1].position)
                    vec_02 = atom4s[0].position.get_vector_to(atom4s[2].position)
                    vec_normal = vec_01.cross(vec_02)
                    assert vec_normal.norm >= dist_thresh
                    if self._check_proper(4, vec_normal):
                        count_c4 += self._add_proper(4, vec_normal)

                    if count_c4 == 3:
                        found_consistent_c4 = True
                        break

            assert found_consistent_c4
            self._add_proper(4, self.proper_elements[4][0], generator=True)

        # Locating all possible and distinct C5 axes for I and Ih point groups
        if count_c2 == 15:
            count_c5 = 0
            found_consistent_c5 = False
            for sea_group in self.sea_groups:
                if found_consistent_c5:
                    break
                if len(sea_group) < 5:
                    continue
                for atom5s in combinations(sea_group, r=5):
                    if not check_regular_polygon([atom.position
                                                  for atom in atom5s],
                                                 dist_thresh):
                        continue

                    vec_01 = atom5s[0].position.get_vector_to(atom5s[1].position)
                    vec_02 = atom5s[0].position.get_vector_to(atom5s[2].position)
                    vec_normal = vec_01.cross(vec_02)
                    assert vec_normal.norm > dist_thresh
                    if self._check_proper(5, vec_normal):
                        count_c5 += self._add_proper(5, vec_normal)
                        self._add_proper(5, vec_normal, generator=True)

                    if count_c5 == 6:
                        found_consistent_c5 = True
                        break

            assert found_consistent_c5

        # Locating any other improper rotation axes for the non-chinal groups
        if self.point_group == "Td":
            count_s4 = 0
            for c2_axis in self.proper_elements[2]:
                if self._check_improper(4, c2_axis):
                    count_s4 += self._add_improper(4, c2_axis)
            assert count_s4 == 3

        elif self.point_group == "Th":
            count_sigmah = 0
            for c2_axis in self.proper_elements[2]:
                if self._check_improper(1, c2_axis):
                    count_sigmah += self._add_improper(1, c2_axis, sigma="h")
            assert count_sigmah == 3
            count_s6 = 0
            for c3_axis in self.proper_elements[3]:
                if self._check_improper(6, c3_axis):
                    count_s6 += self._add_improper(6, c3_axis)
            assert count_s6 == 4

        elif self.point_group == "Oh":
            count_s4 = 0
            count_sigmah = 0
            count_sigmad = 0
            for c2_axis in self.proper_elements[2]:
                if self._check_improper(4, c2_axis):
                    count_s4 += self._add_improper(4, c2_axis)
                    if self._check_improper(1, c2_axis):
                        count_sigmah += self._add_improper(1, c2_axis,
                                                           sigma="h")
                elif self._check_improper(1, c2_axis):
                    count_sigmad += self._add_improper(1, c2_axis, sigma="d")
            assert count_s4 == count_sigmah == 3
            assert count_sigmad == 6
            count_s6 = 0
            for c3_axis in self.proper_elements[3]:
                if self._check_improper(6, c3_axis):
                    count_s6 += self._add_improper(6, c3_axis)
            assert count_s6 == 4

        elif self.point_group == "Ih":
            count_s10 = 0
            for c5_axis in self.proper_elements[5]:
                if self._check_improper(10, c5_axis):
                    count_s10 += self._add_improper(10, c5_axis)
            assert count_s10 == 6
            count_s6 = 0
            for c3_axis in self.proper_elements[3]:
                if self._check_improper(6, c3_axis):
                    count_s6 += self._add_improper(6, c3_axis)
            assert count_s6 == 10
            count_sigma = 0
            for c2_axis in self.proper_elements[2]:
                if self._check_improper(1, c2_axis):
                    count_sigma += self._add_improper(1, c2_axis)
            assert count_sigma == 15

    def _analyse_symmetric(self, moi_thresh: Optional[float] = None,
                           dist_thresh: Optional[float] = None) -> None:
        r"""Internal method to analyse the symmetry of a symmetric top system
        and populate the relevant attributes in this :class:`.Symmetry` object.

        The possible symmetric top point groups are :math:`\mathcal{C}_n`
        (except :math:`\mathcal{C}_1` and :math:`\mathcal{C}_2`),
        :math:`\mathcal{C}_{nh}` (except :math:`\mathcal{C}_{2h}`),
        :math:`\mathcal{C}_{nv}` (except :math:`\mathcal{C}_{2v}`),
        :math:`\mathcal{D}_{n}` (except :math:`\mathcal{D}_{2}`),
        :math:`\mathcal{D}_{nh}` (except :math:`\mathcal{D}_{2h}`),
        :math:`\mathcal{D}_{nd}`, and :math:`\mathcal{S}_{n}`.
        The exceptions are all Abelian groups.

        :param moi_thresh: Threshold for moment of inertia comparisons. If
                :const:`None`, :attr:`.moi_thresh` will be used.
        :param dist_thresh: The threshold for distance comparisons. If
                :const:`None`, :attr:`.dist_thresh` will be used.
        """
        # pylint: disable=R0912,R0914,R0915, R1702
        # R0912, R0914, R0915: It is possible to pull out the implementations
        #                      of the various checks, but that would make the
        #                      code lose its coherence and become fragmented.
        # R1702: There are quite a few branches to classify into point groups.
        if moi_thresh is None:
            moi_thresh = self.moi_thresh
        if dist_thresh is None:
            dist_thresh = self.dist_thresh

        assert self.rotational_symmetry in (ROTSYM_SYMTOP_OBLATE,
                                            ROTSYM_SYMTOP_OBLATE_PLANAR,
                                            ROTSYM_SYMTOP_PROLATE)

        _search_proper_rotations(self, asymmetric=False,
                                 moi_thresh=moi_thresh,
                                 dist_thresh=dist_thresh)

        # Classify into point groups
        dihedral = False
        max_ord = self.max_proper_order
        if 2 in self.proper_elements.keys():
            if max_ord > 2:
                assert len(self.proper_elements[max_ord]) == 1
                n_c2_perp = len([c2_axis
                                 for c2_axis in self.proper_elements[2]
                                 if abs(c2_axis.dot(\
                                         self.proper_elements[max_ord][0]\
                                        )\
                                       ) < dist_thresh])
                dihedral = (n_c2_perp == max_ord)
            else:
                dihedral = (max_ord == 2 and len(self.proper_elements[2]) == 3)

        if dihedral:
            print("Dihedral family.")
            self._add_proper(max_ord, self.proper_elements[max_ord][0],
                             generator=True)
            c2_axis = self.proper_elements[2][0]
            for c2_axis in self.proper_elements[2]:
                principal_dot_c2 = self.proper_generators[max_ord][0]\
                        .dot(c2_axis)
                if abs(principal_dot_c2) < dist_thresh:
                    break
            self._add_proper(2, c2_axis, generator=True)

            if self._check_improper(1, self.proper_elements[max_ord][0]):
                # Dnh
                print("Located σh.")
                self._point_group = f"D{max_ord}h"
                assert max_ord > 2
                # if max_ord == 2:
                #     for c2_axis in self.proper_elements[2]:
                #         assert self._check_improper(1, c2_axis)
                #         assert self._add_improper(1, c2_axis, sigma="h")
                # else:
                self._add_improper(1, self.proper_elements[max_ord][0],
                                   sigma="h")
                self._add_improper(1, self.sigma_elements["h"][0], sigma="h",
                                   generator=True)

                # Locate all other mirror planes and improper axes
                # We take all the other mirror planes to be σv.
                # It's really not worth trying to classify them into σv and σd,
                # as this classification is more conventional than fundamental.
                if max_ord % 2 == 0:
                    # Dnh, n even, an inversion centre is expected.
                    assert self._check_improper(2, Vector([0, 0, 1]))
                    self._add_improper(2, Vector([0, 0, 1]))
                    for n_proper, c_axes in self.proper_elements.items():
                        if n_proper == 1:
                            continue
                        for c_axis in c_axes:
                            (n_improper, s_axis, sigma) =\
                                    _get_s_from_icn(n_proper, c_axis,
                                                    self.sigma_elements["h"][0])
                            assert self._check_improper(n_improper, s_axis)
                            self._add_improper(n_improper, s_axis, sigma=sigma)

                else:
                    # Dnh, n odd, only σh is present.
                    for n_proper, c_axes in self.proper_elements.items():
                        if n_proper == 1:
                            continue
                        for c_axis in c_axes:
                            (n_improper, s_axis, sigma) =\
                                _get_s_from_sigmahcn(n_proper, c_axis,
                                                     self.sigma_elements["h"][0])
                            assert self._check_improper(n_improper, s_axis)
                            self._add_improper(n_improper, s_axis, sigma=sigma)

            else:
                # Dnd
                count_sigmad = 0
                for c2_axis_i, c2_axis_j in\
                        combinations(self.proper_elements[2], r=2):
                    if count_sigmad == max_ord:
                        break
                    if self._check_improper(1, c2_axis_i + c2_axis_j):
                        count_sigmad +=\
                                self._add_improper(1, c2_axis_i + c2_axis_j,
                                                   sigma="d")
                    if self._check_improper(1, c2_axis_i - c2_axis_j):
                        count_sigmad +=\
                                self._add_improper(1, c2_axis_i - c2_axis_j,
                                                   sigma="d")

                print(f"Located {count_sigmad} σd.")
                if count_sigmad == max_ord:
                    self._add_improper(1, self.sigma_elements["d"][0], sigma="d",
                                       generator=True)
                    self._point_group = f"D{max_ord}d"
                    if max_ord % 2 == 0:
                        # Dnd, n even, only σd planes are present.
                        for n_proper, c_axes in self.proper_elements.items():
                            if n_proper == 1:
                                continue
                            for c_axis in c_axes:
                                if self._check_improper(2*n_proper, c_axis):
                                    self._add_improper(2*n_proper, c_axis)

                    else:
                        # Dnd, n odd, an inversion centre is expected.
                        assert self._check_improper(2, Vector([0, 0, 1]))
                        self._add_improper(2, Vector([0, 0, 1]))
                        for n_proper, c_axes in self.proper_elements.items():
                            if n_proper == 1:
                                continue
                            for c_axis in c_axes:
                                (n_improper, s_axis, sigma) =\
                                        _get_s_from_icn(n_proper, c_axis,
                                                        self.proper_elements[max_ord][0])
                                assert self._check_improper(n_improper, s_axis)
                                if sigma == "v":
                                    sigma = "d"
                                self._add_improper(n_improper, s_axis,
                                                   sigma=sigma)

                else:
                    self._point_group = f"D{max_ord}"

        else:
            print("Non-dihedral family.")
            # Locate σv planes
            count_sigmav = 0
            for sea_group in self.sea_groups:
                if count_sigmav == max_ord:
                    break
                k_sea = len(sea_group)
                if k_sea < 2:
                    continue
                for atom_i, atom_j in combinations(sea_group, r=2):
                    if count_sigmav == max_ord:
                        break
                    normal = atom_i.position.get_vector_to(atom_j.position)
                    if self._check_improper(1, normal):
                        count_sigmav += self._add_improper(1, normal, sigma="v")
            print(f"Found {count_sigmav} σv planes.")

            if count_sigmav == max_ord:
                # Cnv
                self._point_group = f"C{max_ord}v"
                self._add_proper(max_ord, self.proper_elements[max_ord][0],
                                 generator=True)
                self._add_improper(1, self.sigma_elements["v"][0], sigma="v",
                                   generator=True)

            elif self._check_improper(1, self.proper_elements[max_ord][0]):
                # Cnh
                self._add_improper(1, self.proper_elements[max_ord][0], sigma="h")
                print("Found no σv planes but one σh plane.")
                self._point_group = f"C{max_ord}h"
                self._add_proper(max_ord, self.proper_elements[max_ord][0],
                                 generator=True)
                self._add_improper(1, self.sigma_elements["h"][0], sigma="h",
                                   generator=True)

                # Locate the remaining improper elements
                if max_ord % 2 == 0:
                    # Cnh, n even, an inversion centre is expected.
                    assert self._check_improper(2, Vector([0, 0, 1]))
                    self._add_improper(2, Vector([0, 0, 1]))
                    for n_proper, c_axes in self.proper_elements.items():
                        if n_proper == 1:
                            continue
                        for c_axis in c_axes:
                            (n_improper, s_axis, sigma) =\
                                    _get_s_from_icn(n_proper, c_axis,
                                                    self.proper_elements[max_ord][0])
                            assert self._check_improper(n_improper, s_axis)
                            self._add_improper(n_improper, s_axis, sigma=sigma)

                else:
                    # Cnh, n odd, only σh is present.
                    for n_proper, c_axes in self.proper_elements.items():
                        if n_proper == 1:
                            continue
                        for c_axis in c_axes:
                            (n_improper, s_axis, sigma) =\
                                _get_s_from_sigmahcn(n_proper, c_axis,
                                                     self.sigma_elements["h"][0])
                            assert self._check_improper(n_improper, s_axis)
                            self._add_improper(n_improper, s_axis, sigma=sigma)

            elif self._check_improper(2*max_ord,
                                      self.proper_elements[max_ord][0]):
                # S2n
                self._add_improper(2*max_ord, self.proper_elements[max_ord][0])
                self._add_improper(2*max_ord, self.proper_elements[max_ord][0],
                                   generator=True)
                self._point_group = f"S{2*max_ord}"
                # Locate the remaining improper symmetry elements
                if max_ord % 2 != 0:
                    # Odd rotation sub groups, an inversion centre is expected.
                    assert self._check_improper(2, Vector([0, 0, 1]))
                    self._add_improper(2, Vector([0, 0, 1]))

            else:
                # Cn
                self._add_proper(max_ord, self.proper_elements[max_ord][0],
                                 generator=True)
                self._point_group = f"C{max_ord}"

    def _analyse_asymmetric(self, moi_thresh: Optional[float] = None,
                            dist_thresh: Optional[float] = None) -> None:
        r"""Internal method to analyse the symmetry of an asymmetric system and
        populate the relevant attributes in this :class:`.Symmetry` object.

        The possible asymmetric point groups are :math:`\mathcal{C}_1`,
        :math:`\mathcal{C}_2`, :math:`\mathcal{C}_i`, :math:`\mathcal{C}_s`,
        :math:`\mathcal{C}_{2h}`, :math:`\mathcal{C}_{2v}`,
        :math:`\mathcal{D}_{2}`, and :math:`\mathcal{D}_{2h}`.
        These are all Abelian groups.

        :param moi_thresh: Threshold for moment of inertia comparisons. If
                :const:`None`, :attr:`.moi_thresh` will be used.
        :param dist_thresh: The threshold for distance comparisons. If
                :const:`None`, :attr:`.dist_thresh` will be used.
        """
        # pylint: disable=R0912,R0915
        # R0912, R0915: There are quite a lot of cases to check for.
        if moi_thresh is None:
            moi_thresh = self.moi_thresh
        if dist_thresh is None:
            dist_thresh = self.dist_thresh

        assert self.rotational_symmetry in (ROTSYM_ASYMTOP,
                                            ROTSYM_ASYMTOP_PLANAR)

        _search_proper_rotations(self, asymmetric=True,
                                 moi_thresh=moi_thresh,
                                 dist_thresh=dist_thresh)

        # Classify into point groups
        if 2 in self.proper_elements.keys():
            count_c2 = len(self.proper_elements[2])
        else:
            count_c2 = 0
        assert count_c2 in [0, 1, 3]
        max_ord = self.max_proper_order
        if count_c2 == 3:
            # Dihedral, either D2h or D2
            print("Dihedral family.")
            assert max_ord == 2
            self._add_proper(max_ord, self.proper_elements[max_ord][0],
                             generator=True)
            self._add_proper(2, self.proper_elements[2][1],
                             generator=True)
            if self._check_improper(2, Vector([0, 0, 1])):
                # Inversion centre, D2h
                print("Located an inversion centre.")
                self._point_group = "D2h"
                self._add_improper(2, Vector([0, 0, 1]))
                for c2_axis in self.proper_elements[2]:
                    assert self._check_improper(1, c2_axis)
                    assert self._add_improper(1, c2_axis)
                self._add_improper(1, self.sigma_elements[""][0],
                                   generator=True)

            else:
                # Chiral, D2
                self._point_group = "D2"

        elif count_c2 == 1:
            # Non-dihedral, either C2, C2v, or C2h
            print("Non-dihedral family.")
            assert max_ord == 2
            self._add_proper(max_ord, self.proper_elements[max_ord][0],
                             generator=True)

            if self._check_improper(2, Vector([0, 0, 1])):
                # C2h
                print("Located an inversion centre.")
                self._add_improper(2, Vector([0, 0, 1]))
                self._point_group = "C2h"
                assert self._check_improper(1, self.proper_elements[2][0])
                self._add_improper(1, self.proper_elements[2][0],
                                   sigma="h")
                self._add_improper(1, self.sigma_elements["h"][0],
                                   sigma="h", generator=True)
            else:
                # Locate σv planes
                count_sigmav = 0
                if self.rotational_symmetry == ROTSYM_ASYMTOP_PLANAR:
                    assert self._check_improper(1, self.molecule.moi[1][2])
                    count_sigmav += self._add_improper(1,
                                                       self.molecule.moi[1][2],
                                                       sigma="v")

                for sea_group in self.sea_groups:
                    if count_sigmav == 2:
                        break
                    k_sea = len(sea_group)
                    if k_sea < 2:
                        continue
                    for atom_i, atom_j in combinations(sea_group, r=2):
                        if count_sigmav == 2:
                            break
                        normal = atom_i.position.get_vector_to(atom_j.position)
                        if self._check_improper(1, normal):
                            count_sigmav += self._add_improper(1, normal,
                                                               sigma="v")

                print(f"Located {count_sigmav} σv.")
                if count_sigmav == 2:
                    self._point_group = "C2v"
                    self._add_improper(1, self.sigma_elements["v"][0],
                                       sigma="v", generator=True)


                else:
                    assert count_sigmav == 0
                    self._point_group = "C2"

        else:
            # No C2 axes, either C1, Ci, or Cs
            if self._check_improper(2, Vector([0, 0, 1])):
                print("Located an inversion centre.")
                self._add_improper(2, Vector([0, 0, 1]))
                self._add_improper(2, Vector([0, 0, 1]), generator=True)
                self._point_group = "Ci"
            else:
                # Locate mirror planes
                count_sigma = 0
                for sea_group in self.sea_groups:
                    if count_sigma > 0:
                        break
                    k_sea = len(sea_group)
                    if k_sea < 2:
                        continue
                    for atom_i, atom_j in combinations(sea_group, r=2):
                        normal = atom_i.position.get_vector_to(atom_j.position)
                        if self._check_improper(1, normal):
                            count_sigma += self._add_improper(1, normal)

                print(f"Located {count_sigma} σ.")
                if count_sigma > 0:
                    assert count_sigma == 1
                    self._add_improper(1, self.sigma_elements[""][0],
                                       generator=True)
                    self._point_group = "Cs"
                else:
                    self._add_proper(1, self.proper_elements[1][0],
                                     generator=True)
                    self._point_group = "C1"


# ============================
# Auxiliary internal functions
# ============================

def _get_s_from_icn(n_proper: int, c_axis: Vector,
                    principal_axis: Optional[Vector] = None,
                    thresh: float = COMMON_ZERO_TOLERANCE)\
                            -> Tuple[int, Vector, str]:
    r"""Return the order and axis of the improper element given by
    :math:`i \times C_n`.

    :param n_proper: Order of the proper rotation :math:`C_n`.
    :param c_axis: Axis of the proper rotation :math:`C_n`.
    :param principal_axis: The principal axis of the molecule. This is needed
            to deduce the nature of the resulted improper element
            (:math:`h` or :math:`v`) should it turn out to be a mirror plane.
    :param thresh: Threshold for considering if two vectors are parallel or
            orthogonal.

    Returns:
        - The order :math:`n'` of the improper element :math:`S_{n'}`.
        - The axis of the improper element :math:`S_{n'}`.
        - If :math:`n' = 1` and principal_axis is provided, the type of the
          mirror plane.
    """
    assert thresh > 0
    n_improper = int((2*n_proper)/gcd(2*n_proper, n_proper+2))
    s_axis = c_axis.normalise()
    if principal_axis is None or n_improper > 1:
        return (n_improper, s_axis, "")
    assert principal_axis is not None
    principal_axis = principal_axis.normalise()
    if abs(s_axis.dot(principal_axis)) < thresh:
        # Vertical plane containing principal axis
        return (n_improper, s_axis, "v")
    if s_axis.cross(principal_axis).norm/(s_axis.norm*principal_axis.norm)\
            < thresh:
        # Horizontal plane orthogonal to principal axis
        return (n_improper, s_axis, "v")
    return (n_improper, s_axis, "")


def _get_s_from_sigmahcn(n_proper: int, c_axis: Vector,
                         sigmah_normal: Vector,
                         thresh: float = COMMON_ZERO_TOLERANCE)\
                            -> Tuple[int, Vector, str]:
    r"""Return the order and axis of the improper element given by
    :math:`\simga_h \times C_n` in a dihedral group.

    In a dihedral group, :math:`C_n` is either orthogonal to
    :math:`\sigma_h`, in which case :math:`n \in \mathbb{N}`, or contained in
    :math:`\sigma_h`, in which case :math:`n = 2` only.

    :param n_proper: Order of the proper rotation :math:`C_n`.
    :param c_axis: Axis of the proper rotation :math:`C_n`.
    :param sigmah_normal: The normal vector of :math:`\sigma_h`.
    :param thresh: Threshold for considering if two vectors are parallel or
            orthogonal.

    Returns:
        - The order :math:`n'` of the improper element :math:`S_{n'}`.
        - The axis of the improper element :math:`S_{n'}`.
        - If :math:`n' = 1`, the type of the mirror plane.
    """
    assert thresh > 0
    c_axis = c_axis.normalise()
    sigmah_normal = sigmah_normal.normalise()
    if c_axis.cross(sigmah_normal).norm/(c_axis.norm*sigmah_normal.norm)\
            < thresh:
        # Cn is orthogonal to σh.
        n_improper = n_proper
        s_axis = c_axis
        if n_improper == 1:
            return (n_improper, s_axis, "h")
        return (n_improper, s_axis, "")

    assert abs(c_axis.dot(sigmah_normal)) < thresh,\
            "Cn must be contained in σh."
    assert n_proper == 2, "n must be equal to 2."
    # Cn is C2 and is contained in σh.
    # The resulted improper element is a σv plane.
    s_axis = c_axis.cross(sigmah_normal).normalise()
    return (1, s_axis, "v")


def _search_c2_spherical(sym: Symmetry,
                         start_guard: int = START_GUARD,
                         convergence_thresh: float = STABLE_C2_RATIO,
                         c2_termination_counts: Optional[Sequence[int]] = None)\
        -> int:
    r"""Locate and add all possible and distinct :math:`C_2` axes present in
    `sym`, provided that `sym` is a spherical top.

    :param sym: A :class:`.Symmetry` object for which the :math:`C_2` axes are
            to be located.
    :param start_guard: The number of atom pairs to be considered before the
            early termination guard is turned on to break the search loop
            before all possible atom pairs can be considered.
    :param convergence_thresh: Let `count_c2_stable` be the number of
            consecutive atom pairs for which no new :math:`C_2` axes are found,
            and `n_pairs` the number of atom pairs considered thus far. When
            the early termination guard is on, if `count_c2_stable/n_pairs`
            exceeds `convergence_thresh`, and if the number of :math:`C_2` axes
            located is in `c2_termination_counts`,  then the search is
            terminated.
    :param c2_termination_counts: The expected numbers of :math:`C_2` axes to
            be found. If :const:`None`, this will not be considered in the early
            termination guard at all.

    :returns: The number of distinct :math:`C_2` axes located and added to
            `sym`.
    """
    # pylint: disable=W0212
    # W0212: Accessing the private methods is okay here since we are still in
    # the same module as Symmetry.
    assert sym.rotational_symmetry == ROTSYM_SPHTOP
    count_c2 = 0
    count_c2_stable = 0
    n_pairs = 0
    for sea_group in sym.sea_groups:
        if len(sea_group) < 2:
            continue
        for atom_i, atom_j in combinations(sea_group, r=2):
            n_pairs += 1
            atom_i_pos = Vector.from_point(atom_i.position)
            atom_j_pos = Vector.from_point(atom_j.position)

            # Case B: C2 might cross through any two atoms
            if sym._check_proper(2, atom_i_pos):
                if sym._add_proper(2, atom_i_pos):
                    count_c2 += 1
                    count_c2_stable = 0

            # Case A: C2 might cross through the midpoint of two atoms
            midvec = 0.5*(atom_i_pos + atom_j_pos)
            if midvec.norm >= sym.dist_thresh and\
                    sym._check_proper(2, midvec):
                if sym._add_proper(2, midvec):
                    count_c2 += 1
                    count_c2_stable = 0

            # Check if count_c2 has reached stability.
            if count_c2_stable/n_pairs > convergence_thresh and\
                    n_pairs > start_guard:
                if c2_termination_counts is not None:
                    if count_c2 in c2_termination_counts:
                        break
                else:
                    break
            count_c2_stable += 1

        if c2_termination_counts is not None and\
                count_c2 in c2_termination_counts:
            break
    return count_c2


def _search_proper_rotations(sym: Symmetry, asymmetric: bool,
                             moi_thresh: Optional[float] = None,
                             dist_thresh: Optional[float] = None) -> None:
    r"""Internal method to locate and add all proper rotation elements present
    in the :attr:`~.Symmetry.molecule` associated with `sym`.

    :param sym: A :class:`.Symmetry` object.
    :param asymmetric: If :const:`True`, the search assumes that the group is
            one of the Abelian point groups for which the highest possible
            rotation order is :math:`2`, and there can be at most three
            :math:`C_2` axes.
    :param moi_thresh: Threshold for moment of inertia comparisons. If
            :const:`None`, :attr:`.moi_thresh` will be used.
    :param dist_thresh: The threshold for distance comparisons. If
            :const:`None`, :attr:`.dist_thresh` will be used.
    """
    # pylint: disable=W0212,R0912,R0914,R0915,R1702
    # W0212: Accessing the private methods is okay here since we are still in
    #        the same module as Symmetry.
    # R0914: Having more local variables makes the code more readable.
    # R0912,R0915, R1702: There are quite a lot of cases to check for.
    if moi_thresh is None:
        moi_thresh = sym.moi_thresh
    if dist_thresh is None:
        dist_thresh = sym.dist_thresh

    # Locating rotation axes from the SEA groups
    linear_sea_groups = []
    count_c2 = 0
    for sea_group in sym.sea_groups:
        if asymmetric and count_c2 == 3:
            break
        k_sea = len(sea_group)
        if k_sea == 1:
            continue
        if k_sea == 2:
            print("A linear SEA set detected.")
            linear_sea_groups.append(sea_group)
        else:
            sea_mol = Molecule(sea_group)
            sea_moi, sea_axes = sea_mol.moi
            # Searching for high-order rotation axes
            if diff_rel(sea_moi[0] + sea_moi[1], sea_moi[2]) < moi_thresh:
                # Planar SEA
                if diff_rel(sea_moi[0], sea_moi[1]) < moi_thresh:
                    # Regular k-sided polygon
                    print(f"A regular {k_sea}-sided polygon SEA set detected.")
                    k_fac_range = divisors(k_sea)[1:]
                else:
                    # Irregular k-sided polygon
                    print(f"An irregular {k_sea}-sided polygon"
                          + " SEA set detected.")
                    k_fac_range = divisors(k_sea)[1:-1]

                for k_fac in k_fac_range:
                    if sym._check_proper(k_fac, sea_axes[2]):
                        if k_fac == 2:
                            count_c2 += sym._add_proper(k_fac, sea_axes[2])
                        else:
                            sym._add_proper(k_fac, sea_axes[2])
            else:
                # Polyhedral SEA
                if diff_rel(sea_moi[1], sea_moi[2]) < moi_thresh:
                    # Prolate symmetric top
                    print("A prolate symmetric top SEA set detected.")
                    assert k_sea % 2 == 0, "The number of atoms in this"\
                            + " SEA group must be even."
                    for k_fac in divisors(k_sea//2)[1:]:
                        if sym._check_proper(k_fac, sea_axes[0]):
                            if k_fac == 2:
                                count_c2 += sym._add_proper(k_fac, sea_axes[0])
                            else:
                                sym._add_proper(k_fac, sea_axes[0])
                elif diff_rel(sea_moi[0], sea_moi[1]) < moi_thresh:
                    # Oblate symmetric top
                    print("An oblate symmetric top SEA set detected.")
                    assert k_sea % 2 == 0, "The number of atoms in this"\
                            + " SEA group must be even."
                    for k_fac in divisors(k_sea//2)[1:]:
                        if sym._check_proper(k_fac, sea_axes[2]):
                            if k_fac == 2:
                                count_c2 += sym._add_proper(k_fac, sea_axes[2])
                            else:
                                sym._add_proper(k_fac, sea_axes[2])
                else:
                    # Asymmetric top
                    print("An asymmetric rotor SEA set detected.")
                    for sea_axis in sea_axes:
                        if sym._check_proper(2, sea_axis):
                            count_c2 += sym._add_proper(2, sea_axis)

        # Searching for any remaining C2 axes
        for atom_i, atom_j in combinations(sea_group, r=2):
            if asymmetric and count_c2 == 3:
                break

            atom_i_pos = Vector.from_point(atom_i.position)
            atom_j_pos = Vector.from_point(atom_j.position)
            # Case B: C2 might cross through any two atoms
            if sym._check_proper(2, atom_i_pos):
                count_c2 += sym._add_proper(2, atom_i_pos)

            # Case A: C2 might cross through the midpoint of two atoms
            midvec = 0.5*(atom_i_pos + atom_j_pos)
            if midvec.norm >= dist_thresh and\
                    sym._check_proper(2, midvec):
                count_c2 += sym._add_proper(2, midvec)

    if asymmetric and count_c2 == 3:
        return

    # Search for any remaining C2 axes
    # Case C: Molecules with two or more sets of non-parallel linear diatomic
    #         SEA groups
    if len(linear_sea_groups) >= 2:
        vec_0 = linear_sea_groups[0][0].position\
                    .get_vector_to(linear_sea_groups[0][1].position)
        for next_sea_group in linear_sea_groups[1:]:
            vec_1 = next_sea_group[0].position\
                    .get_vector_to(next_sea_group[1].position)
            normal = vec_0.cross(vec_1)
            if normal.norm > dist_thresh:
                break
        if normal.norm > dist_thresh and sym._check_proper(2, normal):
            sym._add_proper(2, normal)
