#!/usr/bin/env python3
""":mod:`.symmetry_analysis` contains public functions to perform symmetry
analysis of SCF determinants.
"""

# pylint: disable=C0103

from __future__ import annotations

from typing import List, Tuple, Sequence
from math import ceil
import numpy as np  # type:ignore

from polyinspect.auxiliary.ao_basis_space import BasisAngularOrder
from polyinspect.auxiliary.common_types import MultiStates
from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE
from polyinspect.analyticaltools.wavefunctions.nonorthowavefunctions import det_ov,\
        get_Smat, get_Xmat

from .groups import SymmetrySymbol, CharacterTable
from .symmetry_core import Symmetry
from .symmetry_transformation import get_symequiv, CompositeSymmetryOperation


def get_Tmat(css: MultiStates, comp_sym_op: CompositeSymmetryOperation,
             bao: BasisAngularOrder, sym: Symmetry,
             sao: np.ndarray, complexsymmetric: bool) -> np.ndarray:
    r"""Calculate the transformation overlap matrix a set of determinants and its
    symmetry transformation equivalents.
    Each element is given by

    .. math::
        T_{wx}=\braket{^{w}\Psi^{\diamond}|\hat{R}\ ^{x}\Psi},

    where :math:`\hat{R}` is a (potentially composite) symmetry transformation
    defined by `comp_sym_op`.

    :param css: List of coefficient matrices of the set of determinants.
    :param comp_sym_op: A composite symmetry operation :math:`\hat{R}`. See
            :mod:`.symmetry_transformation` for more information.
    :param bao: A basis angular order data structure containing all information
            about the angular functions in the basis set of a certain system.
            See :mod:`~src.auxiliary.ao_basis_space` for more details.
    :param sym: A :class:`~.symmetry_core.Symmetry` object containing the
            symmetry information of the associated molecule.
    :param sao: The overlap matrix of the underlying atomic basis functions.
    :param complexsymmetric: If :const:`True`, :math:`\diamond = \star`.
            If :const:`False`, :math:`\diamond = \hat{e}`.
            Note that the bra already contains an implicit :math:`\star`
            operation.

    :returns: The transformation overlap matrix.
    """
    # pylint: disable=R0913
    # R0913: All arguments are required to enable Tmat to be constructed while
    #        not having to lump several arguments together.
    tcss = [get_symequiv(cs, [comp_sym_op], bao, sym)[0] for cs in css]
    Tmat = np.array([[det_ov(cs, tcs, sao, complexsymmetric)
                      for tcs in tcss]
                     for cs in css])
    return Tmat


def _calc_Dmat(Smat: np.ndarray, Xmat: np.ndarray, Tmat: np.ndarray,
               complexsymmetric: bool) -> np.ndarray:
    r"""Internal method to calculate the representation matrix
    :math:`\boldsymbol{D}(\hat{R})` from the ingredient matrices according to

    .. math::
        \boldsymbol{D}(\hat{R}) =
            \tilde{\boldsymbol{S}}^{-1} \boldsymbol{X}^{\blacklozenge}
            \boldsymbol{T}(\hat{R}) \boldsymbol{X}.


    :param Smat: The overlap matrix :math:`\boldsymbol{S}`.
    :param Xmat: The transformation matrix :math:`\boldsymbol{X}` to make
            :math:`\boldsymbol{S}` full rank.
    :param Tmat: The transformation overlap matrix :math:`\boldsymbol{T}`.
    :param complexsymmetric: If :const:`True`,
            :math:`\blacklozenge = \mathsf{T}`.  If :const:`False`,
            :math:`\blacklozenge = \dagger`.

    :returns: The representation matrix :math:`\boldsymbol{D}`.
    """
    if not complexsymmetric:
        Xmath = np.conj(np.transpose(Xmat))
    else:
        Xmath = np.transpose(Xmat)
    Smattilde = np.linalg.multi_dot([Xmath, Smat, Xmat])
    Smattildeinv = np.linalg.inv(Smattilde)
    Dmat = np.linalg.multi_dot([Smattildeinv, Xmath, Tmat, Xmat])
    return Dmat


def _calc_Dmat_noci(
    Amat: np.ndarray,
    Smat: np.ndarray,
    Tmat: np.ndarray,
    complexsymmetric: bool,
    lindep_thresh: float = np.sqrt(COMMON_ZERO_TOLERANCE)
) -> np.ndarray:
    r"""Internal method to calculate the NOCI representation matrix
    :math:`\boldsymbol{D}^{\textrm{NOCI}}(\hat{R})` from the ingredient matrices
    according to

    .. math::
        \boldsymbol{D}^{\textrm{NOCI}}(\hat{R}) =
            (\tilde{\boldsymbol{S}}^{\textrm{NOCI}})^{-1}
            \boldsymbol{X}^{\blacklozenge}
            \boldsymbol{A}^{\blacklozenge}
            \boldsymbol{T}(\hat{R})
            \boldsymbol{A}
            \boldsymbol{X},

    where

    .. math::
        \tilde{\boldsymbol{S}}^{\textrm{NOCI}} =
            \boldsymbol{X}^{\blacklozenge}
            \boldsymbol{S}^{\textrm{NOCI}}
            \boldsymbol{X}

    with

    .. math::
        \boldsymbol{S}}^{\textrm{NOCI}} =
            \boldsymbol{A}^{\blacklozenge}
            \boldsymbol{S}
            \boldsymbol{A}.

    :param Amat: The :math:`N_{\textrm{det}} \times n` coefficient matrix of
        :math:`n` NOCI wavefunctions in a particular subspace.
    :param Smat: The overlap matrix :math:`\boldsymbol{S}` of all
        :math:`N_{\textrm{det}}` determinants.
    :param Tmat: The transformation overlap matrix :math:`\boldsymbol{T}` of all
        :math:`N_{\textrm{det}}` determinants.
    :param complexsymmetric: If :const:`True`,
            :math:`\blacklozenge = \mathsf{T}`.  If :const:`False`,
            :math:`\blacklozenge = \dagger`.
    :param lindep_thresh: A threshold for determining linear dependence.

    :returns: The representation matrix :math:`\boldsymbol{D}^{\textrm{NOCI}}`.
    """
    if not complexsymmetric:
        Amath = np.conj(np.transpose(Amat))
    else:
        Amath = np.transpose(Amat)
    SmatNOCI = np.linalg.multi_dot([Amath, Smat, Amat])
    Xmat = get_Xmat(SmatNOCI, lindep_thresh)
    if not complexsymmetric:
        Xmath = np.conj(np.transpose(Xmat))
    else:
        Xmath = np.transpose(Xmat)
    SmatNOCItilde = np.linalg.multi_dot([Xmath, SmatNOCI, Xmat])
    SmatNOCItildeinv = np.linalg.inv(SmatNOCItilde)
    DmatNOCI = np.linalg.multi_dot([SmatNOCItildeinv, Xmath, Amath, Tmat, Amat, Xmat])
    return DmatNOCI


def get_Dmat(css: MultiStates, comp_sym_op: CompositeSymmetryOperation,
             bao: BasisAngularOrder, sym: Symmetry,
             sao: np.ndarray, complexsymmetric: bool,
             thresh: float = np.sqrt(COMMON_ZERO_TOLERANCE)) -> np.ndarray:
    r"""Calculate the representation matrix for a symmetry operation
    :math:`\hat{R}` in the space spanned by a set of single determinants.

    :param css: List of coefficient matrices of the set of determinants.
    :param comp_sym_op: A composite symmetry operation :math:`\hat{R}`. See
            :mod:`.symmetry_transformation` for more information.
    :param bao: A basis angular order data structure containing all information
            about the angular functions in the basis set of a certain system.
            See :mod:`~src.auxiliary.ao_basis_space` for more details.
    :param sym: A :class:`~.symmetry_core.Symmetry` object containing the
            symmetry information of the associated molecule.
    :param sao: The overlap matrix of the underlying atomic basis functions.
    :param complexsymmetric: If :const:`True`, in the inner product
            :math:`\braket{\Psi_i^{\diamond}|\Psi_j}`,
            :math:`\diamond = \star`.
            If :const:`False`, :math:`\diamond = \hat{e}`.
            Note that the bra already contains an implicit :math:`\star`
            operation.
    :param thresh: A threshold to determine linear dependence.

    :returns: The representation matrix.
    """
    # pylint: disable=R0913
    # R0913: All arguments are required to enable Dmat to be constructed while
    #        not having to lump several arguments together.
    Smat = get_Smat(css, css, sao, complexsymmetric)
    Xmat = get_Xmat(Smat, thresh)
    Tmat = get_Tmat(css, comp_sym_op, bao, sym, sao, complexsymmetric)
    Dmat = _calc_Dmat(Smat, Xmat, Tmat, complexsymmetric)
    return Dmat


def get_Dmat_noci(
    css: MultiStates,
    Amat: np.ndarray,
    subspace_groups: Sequence[Sequence[int]],
    comp_sym_op: CompositeSymmetryOperation,
    bao: BasisAngularOrder,
    sym: Symmetry,
    sao: np.ndarray,
    complexsymmetric: bool,
    lindep_thresh: float = np.sqrt(COMMON_ZERO_TOLERANCE)
) -> List[np.ndarray]:
    r"""Calculate the representation matrix for a symmetry operation
    :math:`\hat{R}` in the subspaces spanned by various linear combinations of a
    set of single determinants.

    :param css: List of coefficient matrices of the set of determinants.
    :param Amat: The :math:`N_{\textrm{det}} \times N_{\textrm{NOCI}}`
        coefficient matrix of NOCI wavefunctions.
    :param subspace_groups: A sequence of sequences of NOCI indices; each inner
        sequence contains the indices for one NOCI subspace in which the symmetry
        analysis is to be performed.
    :param comp_sym_op: A composite symmetry operation :math:`\hat{R}`. See
            :mod:`.symmetry_transformation` for more information.
    :param bao: A basis angular order data structure containing all information
            about the angular functions in the basis set of a certain system.
            See :mod:`~src.auxiliary.ao_basis_space` for more details.
    :param sym: A :class:`~.symmetry_core.Symmetry` object containing the
            symmetry information of the associated molecule.
    :param sao: The overlap matrix of the underlying atomic basis functions.
    :param complexsymmetric: If :const:`True`, in the inner product
            :math:`\braket{\Psi_i^{\diamond}|\Psi_j}`,
            :math:`\diamond = \star`.
            If :const:`False`, :math:`\diamond = \hat{e}`.
            Note that the bra already contains an implicit :math:`\star`
            operation.
    :param lindep_thresh: A threshold to determine linear dependence.

    :returns: The representation matrix.
    """
    # pylint: disable=R0913
    # R0913: All arguments are required to enable Dmat to be constructed while
    #        not having to lump several arguments together.
    Smat = get_Smat(css, css, sao, complexsymmetric)
    Tmat = get_Tmat(css, comp_sym_op, bao, sym, sao, complexsymmetric)
    DmatNOCIs = []
    for subspace_group in subspace_groups:
        Amatsubspace = Amat[:, subspace_group]
        DmatNOCIs.append(_calc_Dmat_noci(
            Amatsubspace,
            Smat,
            Tmat,
            complexsymmetric,
            lindep_thresh
        ))
    return DmatNOCIs


def get_chi(Dmat: np.ndarray) -> complex:
    r"""Calculate the character of a representation matrix.

    :param Dmat: A representation matrix.

    :returns: The character of the supplied representation matrix, given by
            its trace.
    """
    return np.trace(Dmat)


def _reduce_rep(rep: Sequence[complex], chartab: CharacterTable,
                thresh: float = COMMON_ZERO_TOLERANCE)\
        -> List[Tuple[int, SymmetrySymbol]]:
    r"""Reduce a sequence of characters of a representation to a direct sum of
    irreducible representations in a group.

    :param rep: A sequence of characters of a representation. These need to be
            ordered according to the conjugacy classes in `char_tab`.
    :param chartab: The character table of the point group under which the
            calculated representation is to be reduced.
    :param thresh: Threshold for various assertions.

    :returns: A list of tuples showing the direct sum of the irreducible
            representations in the provided character table.
    """
    rep_sym = []
    for irrep, chars in chartab.chartab.items():
        gchars = np.multiply(chartab.conj_g, chars)
        c = np.dot(gchars.conj(), rep)/chartab.group_order
        assert abs(c.imag) < thresh
        assert c.real >= -thresh
        if c.real > thresh:
            digits = -int(ceil(np.log10(thresh)))
            crealrounded = np.around(c.real, decimals=digits)
            assert crealrounded.is_integer(),\
                    f"Rounded coefficient {crealrounded} is not an integer."
            rep_sym.append((int(crealrounded), irrep))
    return rep_sym


def get_rep(css: MultiStates, comp_sym_ops: List[CompositeSymmetryOperation],
            bao: BasisAngularOrder, sym: Symmetry,
            sao: np.ndarray, chartab: CharacterTable,
            complexsymmetric: bool,
            thresh: float = COMMON_ZERO_TOLERANCE,
            lindep_thresh: float = np.sqrt(COMMON_ZERO_TOLERANCE))\
                    -> List[Tuple[int, SymmetrySymbol]]:
    r"""Calculate the characters of a representation spanned by a set of single
    determinants.

    :param css: List of coefficient matrices of the set of determinants.
    :param comp_sym_ops: A sequence of composite symmetry operations whose
            characters in the representation spanned by the determinants in
            `css` are to be found. The order of these operations must match
            that of those in `char_tab`.
    :param bao: A basis angular order data structure containing all information
            about the angular functions in the basis set of a certain system.
            See :mod:`~src.auxiliary.ao_basis_space` for more details.
    :param sym: A :class:`~.symmetry_core.Symmetry` object containing the
            symmetry information of the associated molecule.
    :param sao: The overlap matrix of the underlying atomic basis functions.
    :param char_tab: Ordered dictionary containing the character table of the
            point group under which the calculated representation is to be
            reduced.
    :param complexsymmetric: If :const:`True`, in the inner product
            :math:`\braket{\Psi_i^{\diamond}|\Psi_j}`,
            :math:`\diamond = \star`.
            If :const:`False`, :math:`\diamond = \hat{e}`.
            Note that the bra already contains an implicit :math:`\star`
            operation.
    :param thresh: A threshold to check integrality of the irrep multiplicities.
    :param lindep_thresh: A threshold to determine linear dependence in the
            symmetry-equivalent coefficients.

    :returns: The representation as a direct sum of the irreducible
            representations in the supplied character table.
    """
    # pylint: disable=R0913,R0914
    # R0913: All arguments are required to enable Tmat and Dmat to be
    #        constructed while not having to lump several arguments together.
    rep = []
    # rep_sym: List[Tuple[int, SymmetrySymbol]] = []
    Smat = get_Smat(css, css, sao, complexsymmetric)
    Xmat = get_Xmat(Smat, lindep_thresh)

    for comp_sym_op in comp_sym_ops:
        Tmat = get_Tmat(css, comp_sym_op, bao, sym, sao, complexsymmetric)
        Dmat = _calc_Dmat(Smat, Xmat, Tmat, complexsymmetric)
        rep.append(get_chi(Dmat))

    rep_sym = _reduce_rep(rep, chartab, thresh)
    return rep_sym


def get_rep_noci(
    css: MultiStates,
    Amat: np.ndarray,
    subspace_groups: Sequence[Sequence[int]],
    comp_sym_ops: List[CompositeSymmetryOperation],
    bao: BasisAngularOrder,
    sym: Symmetry,
    sao: np.ndarray,
    chartab: CharacterTable,
    complexsymmetric: bool,
    thresh: float = COMMON_ZERO_TOLERANCE,
    lindep_thresh: float = np.sqrt(COMMON_ZERO_TOLERANCE)
) -> List[List[Tuple[int, SymmetrySymbol]]]:
    r"""Calculate the characters of representations spanned by linear combinations
    of a set of single determinants.

    :param css: List of coefficient matrices of the set of determinants.
    :param Amat: The :math:`N_{\textrm{det}} \times N_{\textrm{NOCI}}`
        coefficient matrix of NOCI wavefunctions.
    :param subspace_groups: A sequence of sequences of NOCI indices; each inner
        sequence contains the indices for one NOCI subspace in which the symmetry
        analysis is to be performed.
    :param comp_sym_ops: A sequence of composite symmetry operations whose
            characters in the representation spanned by the determinants in
            `css` are to be found. The order of these operations must match
            that of those in `char_tab`.
    :param bao: A basis angular order data structure containing all information
            about the angular functions in the basis set of a certain system.
            See :mod:`~src.auxiliary.ao_basis_space` for more details.
    :param sym: A :class:`~.symmetry_core.Symmetry` object containing the
            symmetry information of the associated molecule.
    :param sao: The overlap matrix of the underlying atomic basis functions.
    :param char_tab: Ordered dictionary containing the character table of the
            point group under which the calculated representation is to be
            reduced.
    :param complexsymmetric: If :const:`True`, in the inner product
            :math:`\braket{\Psi_i^{\diamond}|\Psi_j}`,
            :math:`\diamond = \star`.
            If :const:`False`, :math:`\diamond = \hat{e}`.
            Note that the bra already contains an implicit :math:`\star`
            operation.
    :param thresh: A threshold to check integrality of the irrep multiplicities.
    :param lindep_thresh: A threshold to determine linear dependence in the
            symmetry-equivalent coefficients.

    :returns: The representation as a direct sum of the irreducible
            representations in the supplied character table.
    """
    # pylint: disable=R0913,R0914
    # R0913: All arguments are required to enable Tmat and Dmat to be
    #        constructed while not having to lump several arguments together.
    reps: List[List[complex]] = [[] for _ in subspace_groups]

    for comp_sym_op in comp_sym_ops:
        DmatNOCIs = get_Dmat_noci(
            css,
            Amat,
            subspace_groups,
            comp_sym_op,
            bao,
            sym,
            sao,
            complexsymmetric,
            lindep_thresh
        )
        for i, DmatNOCI in enumerate(DmatNOCIs):
            reps[i].append(get_chi(DmatNOCI))

    rep_syms = [_reduce_rep(rep, chartab, thresh) for rep in reps]
    return rep_syms


def print_rep(rep_sym: List[Tuple[int, SymmetrySymbol]], fmt: str = 'latex',
              comment: str = '') -> str:
    r"""Print the supplied representation as a readable format.

    :param rep_sym: The representation as a direct sum of the irreducible
            representations in a particular point group.
    :param fmt: Print format: `latex` or `plain`.
    :param comment: Additional comment string to be appended to the formatted
            representation string.

    :returns: The formatted representation.
    """
    outstr = ''
    for i, (coeff, irrep) in enumerate(rep_sym):
        if coeff != 1:
            coeff_str = str(coeff)
        else:
            coeff_str = ''
        if fmt == 'latex':
            outstr += f"{coeff_str}{irrep.latex_str}"
        else:
            outstr += f"{coeff_str}{irrep}"
        if i < len(rep_sym) - 1:
            if fmt == 'latex':
                outstr += r" \oplus "
            else:
                outstr += " + "
    if len(comment) > 0:
        outstr += f" {comment}"
    return outstr
