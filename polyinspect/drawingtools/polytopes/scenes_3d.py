""":mod:`.scenes_3d` contains classes and methods for managing and
drawing two-dimensional projections of three-dimensional polyhedra and vertex
collections. This implementation is based loosely on :cite:`article:Hsu1991`.
"""

from __future__ import annotations

from typing import Sequence, List, Tuple, Union, Optional, cast
import copy
import textwrap
import numpy as np  # type: ignore

from polyinspect.auxiliary.common_standards import COMMON_ZERO_TOLERANCE
from polyinspect.auxiliary.geometrical_space import Point, Vector
from polyinspect.auxiliary.affine_space import Hyperplane
from polyinspect.auxiliary.bounded_affine_space import Segment, Face, Polyhedron,\
        VertexCollection

default_color_list = ('black!20!orange', 'violet', 'green',
                      'DarkBlue', 'DarkGoldenrod', 'FireBrick',
                      'NavyBlue', 'Fuchsia')

def _analyse_edge(edge: Segment, faces: Sequence[Face],
                  viewing_vector: Vector, cabinet_angle: float, thresh: float)\
                    -> Tuple[List[Segment], List[Segment]]:
    r"""Determine the visible and hidden fractions of an edge given a sequence
    of faces.

    :param edge: An edge to be broken up into visible and hidden fractions.
    :param faces: A sequence of faces.

    :returns: A tuple containing a list of the visible fractions and another
            list of hidden fractions of the given edge.
    """
    # pylint: disable=R0912,R0914,R0915
    # R0912,R0914: This is a fairly complicated function that requires more local
    #              variables and branches to ensure readability.
    edge_hidden = []
    edge_visible = []
    previous_segments_visible = [copy.deepcopy(edge)]
    for test_face in faces:
        updated_segments_visible = []
        updated_segments_hidden = []
        front_normal = test_face.get_front_normal(viewing_vector)
        assert isinstance(front_normal, Vector)
        face_point = test_face.get_a_point()
        test_face_2d = test_face.get_cabinet_projection(cabinet_angle)

        if len(previous_segments_visible) == 0:
            # No more visible segments left. Skipping the rest of the faces.
            break

        while len(previous_segments_visible) > 0:
            test_segment = previous_segments_visible.pop()
            back_segment = False
            back_segment_parallel = False
            test_segment_2d = test_segment.get_cabinet_projection(cabinet_angle)
            if not test_segment_2d.intersects_bounding_box(test_face_2d):
                # test_segment not covered by test_face in
                # 2d projection
                updated_segments_visible.append(test_segment)
                continue  # next in previous_segments_visible

            assert isinstance(test_face.associated_plane, Hyperplane)
            n_int_plane, intersection_plane =\
                    test_segment.intersects_hyperplane\
                            (test_face.associated_plane, thresh)

            if n_int_plane == 0:
                # Segment parallel to plane of face
                edge_point = test_segment.get_a_point()
                fe_vector = face_point.get_vector_to(edge_point)
                if fe_vector.dot(front_normal) > thresh:
                    # Segment in front of face. We are done.
                    updated_segments_visible.append(test_segment)
                    continue  # next in previous_segments_visible

                # Segment on the back of face.
                # Needs further testing, as some parts might
                # be hidden.
                back_test_segment = test_segment
                back_segment = True
                back_segment_parallel = True

            if n_int_plane == np.inf:
                # Segment on plane. We are done.
                updated_segments_visible.append(test_segment)
                continue  # next in previous_segments_visible

            if n_int_plane == 1:
                # Segment intersects plane at one point.
                # Front fraction always visible.
                # Back fraction needs further testing.
                assert isinstance(intersection_plane, Point)
                for endpoint in test_segment.endpoints:
                    if face_point == endpoint:
                        # This endpoint coincides with face_point
                        # and hence lies on the plane.
                        continue
                    fe_vector = face_point.get_vector_to(endpoint).normalise()
                    frontback = fe_vector.dot(front_normal)
                    if frontback > thresh:
                        # This endpoint lies in front.
                        assert endpoint != intersection_plane
                        front_test_segment = Segment([endpoint,
                                                      intersection_plane])
                        updated_segments_visible.append(front_test_segment)
                        continue  # next endpoint

                    if -thresh <= frontback <= thresh:
                        # This endpoint lies on the plane.
                        continue  # next endpoint

                    # This endpoint lies on the back.
                    assert endpoint != intersection_plane
                    back_test_segment = Segment([endpoint, intersection_plane])
                    back_segment = True

            if not back_segment:
                continue  # next in previous_segments_visible

            # If we get to this point, back_test_segment exists.
            back_test_segment_2d = back_test_segment\
                    .get_cabinet_projection(cabinet_angle)
            if back_segment_parallel:
                anchor = back_test_segment.endpoints[0]
            else:
                assert isinstance(intersection_plane, Point)
                anchor = intersection_plane
            anchor_2d = anchor.get_cabinet_projection(cabinet_angle)
            (_, _, segments_2d_hidden, segments_2d_visible) =\
                    back_test_segment_2d.intersects_face\
                            (test_face_2d, anchor_2d, thresh)
            # We need to convert the 2d information back to 3D data.
            # We need 3D data for further testing with other faces.
            # For parallel projection, mu is the same in both 3D
            # and 2D.
            new_hidden_segments =\
                    [back_test_segment.get_subsegment(mu_start, mu_end, anchor,
                                                      thresh)
                     for (_, mu_start, mu_end) in segments_2d_hidden
                     if abs(mu_end - mu_start) >= thresh]
            updated_segments_hidden.extend(new_hidden_segments)
            new_visible_segments =\
                    [back_test_segment.get_subsegment(mu_start, mu_end, anchor,
                                                      thresh)
                     for (_, mu_start, mu_end) in segments_2d_visible
                     if abs(mu_end - mu_start) >= thresh]
            updated_segments_visible.extend(new_visible_segments)
        # End of while; no more segments left in
        # previous_segments_visible for this face

        # Preparing for the next face
        previous_segments_visible = updated_segments_visible

        # Saving all segments from this edge hidden by this face.
        edge_hidden.extend(updated_segments_hidden)

    # End of for; no more faces left for this edge.
    # Saving all visible segments from this edge.
    edge_visible.extend(previous_segments_visible)

    return (edge_visible, edge_hidden)


def _analyse_vertex(vertex: Point, faces: Sequence[Face],
                    viewing_vector: Vector, cabinet_angle: float,
                    thresh: float) -> bool:
    r"""Determine the given vertex is visible or hidden given a sequence of
    faces.

    :param vertex: An vertex to be analysed for visibility.
    :param faces: A sequence of faces.

    :returns: :const:`True` if the vertex is visible, :const:`False` if not.
    """
    visible = True
    for test_face in faces:
        front_normal = test_face.get_front_normal(viewing_vector)
        assert isinstance(front_normal, Vector)
        face_point = test_face.get_a_point()
        test_face_2d = test_face.get_cabinet_projection(cabinet_angle)
        vertex_2d = vertex.get_cabinet_projection(cabinet_angle)
        if not vertex_2d.intersects_bounding_box(test_face_2d):
            # test_vertex not covered by test_face in 2d projection
            continue  # next test_face

        # test_vertex covered by test_face in 2d projection
        if test_face.associated_plane.contains_point(vertex, thresh):
            # Vertex on face's plane and is visible w.r.t. this face
            continue  # next test_face

        # Vertex off-plane
        assert face_point != vertex
        fv_vector = face_point.get_vector_to(vertex).normalise()
        frontback = fv_vector.dot(front_normal)
        assert abs(frontback) >= thresh
        if frontback > thresh:
            # This vertex lies in front.
            continue  # next in test_face

        # This vertex lies on the back.
        # We test to see if it is covered by test_face.
        if test_face_2d.contains_point(vertex_2d):
            # It is indeed covered.
            visible = False
            break

    return visible


class Scene():
    r"""A class to contain, manage, and draw multiple polyhedra and vertex
    collections in a three-dimensional scene.
    """
    # pylint: disable=R0902,R0904
    # R0902: x, y, z are okay as variable names since they denote
    #        coordinates.
    # R0904: The number of public methods is required.

    def __init__(self,
                 objects: Sequence[Union[Polyhedron, VertexCollection]],
                 loose_points: Optional[Sequence[Point]] = None,
                 alpha: float = np.arctan(2),
                 thresh: float = COMMON_ZERO_TOLERANCE) -> None:
        r"""
        :param objects: An ordered list of polyhedra and vertex collections in
                the scene.
        :param alpha: Cabinet projection angle (radians).
        :param thresh: Threshold for various comparisons.
        """
        self._scene_axes = (Vector([1, 0, 0]),
                            Vector([0, 1, 0]),
                            Vector([0, 0, 1]))
        self.cabinet_angle = alpha
        self.thresh = thresh
        self._visible_segments: List[List[Segment]] = []
        self._hidden_segments: List[List[Segment]] = []
        self._visible_vertices: List[List[Point]] = []
        self._hidden_vertices: List[List[Point]] = []
        if loose_points is None:
            self._loose_points = []
        else:
            self._loose_points = list(loose_points)
        self.objects = objects  # type: ignore


    @property
    def thresh(self) -> float:
        r"""Threshold for various comparisons in the construction and
        manipulation of this scene.
        """
        return self._thresh

    @thresh.setter
    def thresh(self, thr: float) -> None:
        self._thresh = thr

    @property
    def objects(self) -> List[Union[Polyhedron, VertexCollection]]:
        r"""A list of all polyhedra and vertex collections in the scene.

        When :attr:`.objects` is updated, :attr:`.visible_segments`,
        :attr:`.hidden_segments`, :attr:`.visible_vertices`, and
        :attr:`.hidden_vertices` are also updated.
        """
        return self._objects

    @objects.setter
    def objects(self, objects: Sequence[Union[Polyhedron, VertexCollection]])\
            -> None:
        assert all(isinstance(obj, (Polyhedron, VertexCollection))
                   for obj in objects)
        self._objects = list(objects)
        self.refresh()

    @property
    def polyhedra(self) -> List[Polyhedron]:
        r"""A list of all polyhedra in the scene.

        This property has no setter.
        """
        return [obj for obj in self.objects
                if isinstance(obj, Polyhedron)]

    @property
    def vertexcollections(self) -> List[VertexCollection]:
        r"""A list of all non-polyhedron vertex collections in the scene.

        This property has no setter.
        """
        return [obj for obj in self.objects
                if isinstance(obj, VertexCollection)]

    @property
    def loose_points(self) -> List[Point]:
        r"""A list of all loose points in the scene.

        Loose points are points that do not belong to a polyhedron or a vertex
        collection.

        This property has no setter.
        """
        return self._loose_points


    @property
    def all_vertices(self) -> List[Point]:
        r"""A list of all unique vertices in the scene.
        """
        return [ver for obj in self.objects for ver in obj.non_adj_vertices]\
               + self.loose_points

    @property
    def all_edges(self) -> List[Segment]:
        r"""A list of all unique edges in the scene.
        """
        edges = [edge for obj in self.objects for edge in obj.edges]
        edges.sort()
        if len(edges) > 1:
            unique_edges = [edges[0]]
            for edge in edges[1:]:
                if edge == unique_edges[-1]:
                    continue
                unique_edges.append(edge)
        else:
            unique_edges = edges
        return unique_edges

    @property
    def all_faces(self) -> List[Face]:
        """A list of all unique faces in the scene.

        .. note::
            Faces are only defined for polyhedra and not vertex collections.
        """
        return [face for polyhedron in self.polyhedra
                for face in polyhedron.faces]

    @property
    def cabinet_angle(self) -> float:
        r"""The cabinet projection angle (radians) of the view of this scene.

        This angle lies within the :math:`[0, 2\pi)` interval.
        """
        return self._cabinet_angle

    @cabinet_angle.setter
    def cabinet_angle(self, alpha: float) -> None:
        angle = alpha
        while angle >= 2*np.pi:
            angle -= 2*np.pi
        while angle < 0:
            angle += 2*np.pi
        self._cabinet_angle = angle

    @property
    def viewing_vector(self) -> Vector:
        r"""The unit viewing vector of the view of this scene.

        This is a normalised vector proportional to
        :math:`(\frac{1}{2}\cos\alpha, \frac{1}{2}\sin\alpha, 1)`
        where :math:`\alpha` is the cabinet projection angle.
        """
        alpha = self.cabinet_angle
        return Vector([0.5*np.cos(alpha), 0.5*np.sin(alpha), 1]).normalise()

    @property
    def scene_axes(self) -> Tuple[Vector, ...]:
        r"""The axes attached to the scene.

        These axes are fixed in space and are not transformed with the objects
        in the scene.

        This property has no setter.
        """
        return self._scene_axes

    @property
    def visible_segments(self) -> List[List[Segment]]:
        r"""The visible segments of the edges w.r.t. the current viewing vector.

        Each element in the outer list is a list containing the visible segments
        of an element in :attr:`.objects`.

        This property has no setter and can only be updated by :meth:`.refresh`.
        """
        return self._visible_segments

    @property
    def visible_vertices(self) -> List[List[Point]]:
        r"""The visible vertices w.r.t. the current viewing vector.

        Each element in the outer list is a list containing the visible
        vertices of an element in :attr:`.objects`.

        This property has no setter and can only be updated by :meth:`.refresh`.
        """
        return self._visible_vertices

    @property
    def hidden_segments(self) -> List[List[Segment]]:
        r"""The hidden segments of the edges w.r.t. the current viewing vector.

        Each element in the outer list is a list containing the hidden segments
        of an element in :attr:`.objects`.

        This property has no setter and can only be updated by :meth:`.refresh`.
        """
        return self._hidden_segments

    @property
    def hidden_vertices(self) -> List[List[Point]]:
        r"""The hidden vertices w.r.t. the current viewing vector.

        Each element in the outer list is a list containing the hidden vertices
        of an element in :attr:`.objects`.

        This property has no setter and can only be updated by :meth:`.refresh`.
        """
        return self._hidden_vertices

    @property
    def visible_segments_2d(self) -> List[List[Segment]]:
        r"""The visible segments of the edges w.r.t. the current
        :attr:`.viewing_vector` in the cabinet projection.

        Each element in the outer list is a list containing the visible segments
        in the cabinet projection of an element in :attr:`.objects`.

        This property has no setter.
        """
        return list(map(lambda sublist:
                        [s.get_cabinet_projection(self.cabinet_angle)
                         for s in sublist], self.visible_segments))

    @property
    def visible_vertices_2d(self) -> List[List[Point]]:
        r"""The visible vertices w.r.t. the current
        :attr:`.viewing_vector` in the cabinet projection.

        Each element in the outer list is a list containing the visible vertices
        in the cabinet projection of an element in :attr:`.objects`.

        This property has no setter.
        """
        return list(map(lambda sublist:
                        [v.get_cabinet_projection(self.cabinet_angle)
                         for v in sublist], self.visible_vertices))

    @property
    def hidden_segments_2d(self) -> List[List[Segment]]:
        r"""The hidden segments of the edges w.r.t. the current
        :attr:`.viewing_vector` in the cabinet projection.

        Each element in the outer list is a list containing the hidden segments
        in the cabinet projection of an element in :attr:`.objects`.

        This property has no setter.
        """
        return list(map(lambda sublist:
                        [s.get_cabinet_projection(self.cabinet_angle)
                         for s in sublist], self.hidden_segments))

    @property
    def hidden_vertices_2d(self) -> List[List[Point]]:
        r"""The hidden vertices w.r.t. the current
        :attr:`.viewing_vector` in the cabinet projection.

        Each element in the outer list is a list containing the hidden vertices
        in the cabinet projection of an element in :attr:`.objects`.

        This property has no setter.
        """
        return list(map(lambda sublist:
                        [v.get_cabinet_projection(self.cabinet_angle)
                         for v in sublist], self.hidden_vertices))

    def refresh(self) -> None:
        r"""Update visible and hidden segments and vertices.
        """
        self._visible_segments, self._hidden_segments =\
                self._get_visible_hidden_segments(self.thresh)
        self._visible_vertices, self._hidden_vertices =\
                self._get_visible_hidden_vertices(self.thresh)

    def centre_objects(self) -> None:
        r"""Translate all objects in the scene such that their combined
        geometric centre is at the origin.

        The translation occurs in-place.
        """
        centre = cast(Point, sum(self.all_vertices))/len(self.all_vertices)
        tvec = Vector.from_point(centre)
        for obj in self.objects:
            obj.translate_ip(-tvec)
        for lsp in self.loose_points:
            lsp.translate_ip(-tvec, [])

    def rotate_objects(self, angle: float, axis: Vector) -> None:
        r"""Rotate the all objects in the scene through `angle` about the vector
        `axis` about the origin.

        After the rotation, visible and hidden segments and vertices will be
        updated.

        :param angle: Angle of rotation (radians).
        :param axis: Vector indicating the axis of rotation. This vector
                will be normalised.
        """
        for obj in self.objects:
            obj.rotate_ip(angle, axis)
        for lsp in self.loose_points:
            lsp.rotate_ip(angle, axis, [])

        self.refresh()

    def _get_visible_hidden_segments(self,
                                     thresh: float = COMMON_ZERO_TOLERANCE)\
                                    -> Tuple[List[List[Segment]],
                                             List[List[Segment]]]:
        # Visibility is only meaningful in the projected view, so we check for
        # visibility  of any back edges or segments in the projected view.
        # However, we store all segments in the full object space.
        visible: List[List[Segment]] = []
        hidden: List[List[Segment]] = []
        for obj in self.objects:
            visible.append([])
            hidden.append([])
            for edge in obj.edges:
                edge_visible, edge_hidden = _analyse_edge(edge, self.all_faces,
                                                          self.viewing_vector,
                                                          self.cabinet_angle,
                                                          thresh)
                visible[-1].extend(edge_visible)
                hidden[-1].extend(edge_hidden)

        visible.extend([[]]*len(self.loose_points))
        hidden.extend([[]]*len(self.loose_points))

        return ([sorted(vis) for vis in visible],
                [sorted(hid) for hid in hidden])


    def _get_visible_hidden_vertices(self, thresh: float = COMMON_ZERO_TOLERANCE)\
                                    -> Tuple[List[List[Point]],\
                                             List[List[Point]]]:
        # Visibility is only meaningful in the projected view, so we check for
        # visibility of any back vertices in the projected view.
        # However, we store all vertices in the full object space.
        visible: List[List[Point]] = []
        hidden: List[List[Point]] = []
        for obj in self.objects:
            visible.append([])
            hidden.append([])
            for vertex in obj.non_adj_vertices:
                vertex_visible = _analyse_vertex(vertex, self.all_faces,
                                                 self.viewing_vector,
                                                 self.cabinet_angle, thresh)

                if vertex_visible:
                    visible[-1].append(vertex)
                else:
                    hidden[-1].append(vertex)

        for loose_point in self.loose_points:
            visible.append([])
            hidden.append([])
            point_visible = _analyse_vertex(loose_point, self.all_faces,
                                            self.viewing_vector,
                                            self.cabinet_angle, thresh)

            if point_visible:
                visible[-1].append(loose_point)
            else:
                hidden[-1].append(loose_point)

        return ([sorted(vis) for vis in visible],
                [sorted(hid) for hid in hidden])


def write_scene_to_tikz(scene: Scene,
                        output: str,
                        bbox: Optional[List[Tuple[float, float]]] = None,
                        vertex_size: str = "1pt",
                        scale: float = 1.0,
                        color_list: Sequence[str] = default_color_list,
                        comment: str = "") -> None:
    r"""Write the given scene to a standalone TeX file.

    :param output: Output file name.
    :param bbox: The bounding box of the TikZ picture in the format
            [(xmin,xmax),(ymin,ymax)]. The default is [(-1, 1), (-1, 1)].
    :param vertex_size: Size of the circle nodes used to depict vertices.
    :param scale: Scale factor for the entire TikZ picture.
    :param color_list: The list of colours used for the different
            polyhedra or vertex collections.
    :param comment: Additional comments to be printed with the scene.
    """
    # pylint: disable=C0103,C0301,R0904,R0913,R0914,R0915
    # C0103: Variable names follow geometrical conventions.
    # C0301: One very long line is okay.
    # R0904, R0913, R0914, R0915: This complex class cannot be broken down
    #                             further.
    preamble_str =\
            r"""
            \PassOptionsToPackage{svgnames}{xcolor}
            \documentclass[tikz]{standalone}
            \usepackage{pgfplots}
            \pgfplotsset{compat=1.16}
            \usetikzlibrary{arrows.meta, calc, luamath, positioning}

            \begin{document}
                \begin{tikzpicture}
            """
    if bbox is None:
        bbox = [(-1, 1), (-1, 1)]
    (xmin, xmax) = bbox[0]
    (ymin, ymax) = bbox[1]
    preamble_str +=\
            fr"""
                    \useasboundingbox ({xmin*scale}, {ymin*scale})
                            rectangle ({xmax*scale}, {ymax*scale});
            """

    preamble_str +=\
            fr"""
                    \scalebox{{ {scale} }}{{
            """

    axis_size = 0.2
    axis_origin = Point([xmin + axis_size, ymin + axis_size, 0])
    axis_origin_2d = axis_origin.get_cabinet_projection(scene.cabinet_angle)
    xstart, ystart = axis_origin_2d[0], axis_origin_2d[1]
    for i, axis in enumerate(scene.scene_axes):
        axis_end = Point.from_vector(Vector.from_point(axis_origin)
                                     + axis_size*axis)
        axis_end_2d = axis_end.get_cabinet_projection(scene.cabinet_angle)
        xend, yend = axis_end_2d[0], axis_end_2d[1]
        if xend >= xstart:
            if yend >= ystart:
                anchor = 'south west'
            else:
                anchor = 'north west'
        else:
            if yend >= ystart:
                anchor = 'south east'
            else:
                anchor = 'north east'
        preamble_str +=\
            rf"""
                    \draw[-{{Diamond[scale=0.3]}}, very thin]
                            ({xstart+0:.13f}, {ystart+0:.13f}) -- ({xend+0:.13f}, {yend+0:.13f});
                    \node[inner sep={axis_size}, scale={axis_size*0.7}, anchor={anchor}]
                            at ({xend+0:.13f}, {yend+0:.13f}) (axis{i}) {{ ${i}$ }};
            """

    all_str = preamble_str
    hidden_str =\
            r"""
                    % ===============
                    % Hidden segments
                    % ===============
            """
    hidden_vertices_str =\
            r"""
                    % ===============
                    % Hidden vertices
                    % ===============
            """
    visible_str =\
            r"""
                    % ================
                    % Visible segments
                    % ================
            """
    visible_vertices_str =\
            r"""
                    % ================
                    % Visible vertices
                    % ================
            """

    for pi in range(len(scene.objects) + len(scene.loose_points)):
        color = color_list[pi % len(color_list)]

        poly_str =\
            fr"""
                    % ---------------
                    % Polyhedron {pi}
                    % ---------------
            """

        hidden_vertices_str += poly_str + "\n"
        hidden_str += poly_str + "\n"
        visible_vertices_str += poly_str + "\n"
        visible_str += poly_str + "\n"

        for vertex_2d in scene.hidden_vertices_2d[pi]:
            x, y = vertex_2d[0], vertex_2d[1]
            hidden_vertices_str +=\
            fr"""
                    \node at ({x}, {y})
                        [circle, fill = {color}!50!white, very thin,
                         draw = black!50!white, inner sep={vertex_size}]{{}};
            """
        hidden_vertices_str += "\n"

        for segment_2d in scene.hidden_segments_2d[pi]:
            xstart, ystart = segment_2d.endpoints[0][0], segment_2d.endpoints[0][1]
            xend, yend = segment_2d.endpoints[1][0], segment_2d.endpoints[1][1]
            hidden_str +=\
            fr"""
                    \draw[{color}, opacity=0.25] ({xstart+0:.13f}, {ystart+0:.13f}) -- ({xend+0:.13f}, {yend+0:.13f});
            """
        hidden_str += "\n"

        for vertex_2d in scene.visible_vertices_2d[pi]:
            x, y = vertex_2d[0], vertex_2d[1]
            visible_vertices_str +=\
            rf"""
                    \node at ({x}, {y})
                        [circle, fill = {color}, very thin,
                         draw = black, inner sep={vertex_size}]{{}};
            """
        visible_vertices_str += "\n"

        for segment_2d in scene.visible_segments_2d[pi]:
            xstart, ystart = segment_2d.endpoints[0][0], segment_2d.endpoints[0][1]
            xend, yend = segment_2d.endpoints[1][0], segment_2d.endpoints[1][1]
            visible_str +=\
            fr"""
                    \draw[{color}] ({xstart+0:.13f}, {ystart+0:.13f}) -- ({xend+0:.13f}, {yend+0:.13f});
            """
        visible_str += "\n"

    all_str +=\
            hidden_str +\
            hidden_vertices_str +\
            visible_str +\
            visible_vertices_str

    end_str =\
            fr"""
                    }}
                    \node[anchor=north, align=center] (comment) at (current bounding box.north) {{{comment}}};
                \end{{tikzpicture}}
            \end{{document}}
            """

    all_str += end_str

    with open(output, 'w') as f:
        f.write(textwrap.dedent(all_str))
