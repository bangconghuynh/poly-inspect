#!/usr/bin/env python3

from typing import Tuple

import numpy as np  # type: ignore
import mpmath  # type: ignore
from mpmath import mp, mpf, mpc  # type: ignore
from pyscf import gto, scf  # type: ignore

from polyinspect.auxiliary.common_standards import STRICT_ZERO_TOLERANCE


"""
We define common functions to analyse the solutions of the following system of
equations:
    B1m*sin(2ta) + B2mm*sin(2ta)*cos^2(tb) +  B2mp*sin(2ta)*sin^2(tb) + B3*cos*(2ta)*sin(2tb) = 0
    B1p*sin(2tb) + B2pm*sin(2tb)*cos^2(ta) +  B2pp*sin(2tb)*sin^2(ta) + B3*cos*(2tb)*sin(2ta) = 0
"""


def printf2(name, v, width, precision):
    print(f"{name:7}: {float(v.real):+20.13f}{float(v.imag):+20.13f}j")


def get_elec_energy(ca1: mpc, ca2: mpc, cb1: mpc, cb2: mpc,
                    A1m: mpf, A1p: mpf,
                    A2m: mpf, A2p: mpf,
                    A3: mpf,
                    A4m: mpf, A4p: mpf,
                    A5: mpf, A6: mpf) -> Tuple[mpc, mpc, mpc]:

    E1e =  (ca1**2+ca2**2)*A1m + 2*ca1*ca2*A2m\
         + (cb1**2+cb2**2)*A1p + 2*cb1*cb2*A2p

    E2e =  (ca1**2*cb1**2 + ca2**2*cb2**2)*A3\
         + 2*(ca1**2 + ca2**2)*cb1*cb2*A4p\
         + 2*(cb1**2 + cb2**2)*ca1*ca2*A4m\
         + (ca1**2*cb2**2 + ca2**2*cb1**2)*A5\
         + 4*ca1*ca2*cb1*cb2*A6

    return E1e, E2e, E1e + E2e


def get_coeffs(ta: mpc, tb: mpc, etam: mpf, etap: mpf, gammam: mpf, gammap: mpf)\
        -> Tuple[mpc, mpc, mpc, mpc]:
    ca1: mpc = mp.sqrt(etam/2)*(- (mp.cos(ta))/(mp.sqrt(1-gammam))
                                + (mp.sin(ta))/(mp.sqrt(1+gammam)))
    ca2: mpc = mp.sqrt(etam/2)*(+ (mp.cos(ta))/(mp.sqrt(1-gammam))
                                + (mp.sin(ta))/(mp.sqrt(1+gammam)))
    cb1: mpc = mp.sqrt(etap/2)*(- (mp.cos(tb))/(mp.sqrt(1-gammap))
                                + (mp.sin(tb))/(mp.sqrt(1+gammap)))
    cb2: mpc = mp.sqrt(etap/2)*(+ (mp.cos(tb))/(mp.sqrt(1-gammap))
                                + (mp.sin(tb))/(mp.sqrt(1+gammap)))
    return ca1, ca2, cb1, cb2


def get_gammaeta(S1m: mpf, S1p: mpf, S2m: mpf, S2p: mpf)\
        -> Tuple[mpf, mpf, mpf, mpf]:
    gammam = S2m/S1m
    gammap = S2p/S1p
    etam = 1/(2*S1m)
    etap = 1/(2*S1p)
    return gammam, gammap, etam, etap


def get_B(A1m: mpf, A1p: mpf, A2m: mpf, A2p: mpf,
          A3: mpf, A4m: mpf, A4p: mpf, A5: mpf, A6: mpf,
          gammam: mpf, gammap: mpf, etam: mpf, etap: mpf)\
                  -> Tuple[mpc, mpc, mpc, mpc, mpc, mpc, mpc]:

    B1m = (2*etam)/(1-gammam**2)*(-gammam*A1m+A2m)
    B1p = (2*etap)/(1-gammap**2)*(-gammap*A1p+A2p)

    B2mm = (etam*etap)/((1-gammam**2)*(1-gammap))\
            *(-gammam*(A3+A5) + 2*(gammam*A4p + A4m) - 2*A6)
    B2mp = (etam*etap)/((1-gammam**2)*(1+gammap))\
            *(-gammam*(A3+A5) + 2*(-gammam*A4p + A4m) + 2*A6)
    B2pm = (etam*etap)/((1-gammap**2)*(1-gammam))\
            *(-gammap*(A3+A5) + 2*(gammap*A4m + A4p) - 2*A6)
    B2pp = (etam*etap)/((1-gammam**2)*(1+gammam))\
            *(-gammap*(A3+A5) + 2*(-gammap*A4m + A4p) + 2*A6)

    B3 = (etam*etap)/(mp.sqrt(1-gammam**2)*mp.sqrt(1-gammap**2))*(A3-A5)

    return B1m, B1p, B2mm, B2mp, B2pm, B2pp, B3


def get_D(B1m: mpc, B1p: mpc,
          B2mm: mpc, B2mp: mpc,
          B2pm: mpc, B2pp: mpc,
          B3: mpc) -> Tuple[mpc, mpc, mpc, mpc]:

    Bmsum = 2*B1m + B2mm + B2mp
    Bpsum = 2*B1p + B2pm + B2pp

    Delta = 4*(B3**4) + 4*(B1m+B2mm)*(B1m+B2mp)*(B1p+B2pm)*(B1p+B2pp)\
            + (B3**2)*(Bmsum**2 + Bpsum**2 - 2*(B2mm-B2mp)*(B2pm-B2pp))

    D1m = 4*(B3**4) - 2*(B1m+B2mm)*(B1m+B2mp)*(B1p+B2pm)*(B2pm-B2pp)\
            + (B3**2)*(Bmsum**2 - (B2mm-B2mp)*(2*B1p+3*B2pm-B2pp))
    D1p = 4*(B3**4) + 2*(B1m+B2mm)*(B1m+B2mp)*(B1p+B2pp)*(B2pm-B2pp)\
            + (B3**2)*(Bmsum**2 + (B2mm-B2mp)*(2*B1p+3*B2pp-B2pm))

    D2 = B3*Bmsum

    return Delta, D1m, D1p, D2


def get_zr(Delta: mpc, D1m: mpc, D1p: mpc, D2: mpc)\
        -> Tuple[mpc, mpc, mpc, mpc, mpc, mpc]:

    z21 = -mp.sqrt((D1p - D2*mp.sqrt(Delta))/(D1m+D1p))
    z22 = +mp.sqrt((D1m + D2*mp.sqrt(Delta))/(D1m+D1p))

    z31 = -mp.sqrt((D1p + D2*mp.sqrt(Delta))/(D1m+D1p))
    z32 = +mp.sqrt((D1m - D2*mp.sqrt(Delta))/(D1m+D1p))

    r2 = mp.sqrt((z21.real - z22.imag)**2 + (z21.imag + z22.real)**2)
    r3 = mp.sqrt((z31.real - z32.imag)**2 + (z31.imag + z32.real)**2)

    return z21, z22, z31, z32, r2, r3


def get_tb(ta: mpc,
           B1m: mpc, B1p: mpc,
           B2mm: mpc, B2mp: mpc,
           B2pm: mpc, B2pp: mpc,
           B3: mpc) -> mpc:
    x = B1p + B2pm*mp.cos(ta)**2 + B2pp*mp.sin(ta)**2
    y = -B3*mp.sin(2*ta)
    tbsols = []
    if mpmath.almosteq(B3, 0.0, STRICT_ZERO_TOLERANCE):
        # The B3 term vanishes. It is now much easier to solve for tb.
        if not mpmath.almosteq(x, 0.0, STRICT_ZERO_TOLERANCE):
            # The second equation still constrains tb.
            twicethetab = 0.0
        else:
            # The second equation does not constrain tb.
            # The first equation does.
            twicethetab = 2*mp.asin(mp.sqrt((B1m+B2mm)/(B2mm-B2mp)))
    else:
        try:
            z = y/x
        except ZeroDivisionError:
            z = -mp.inf
        twicethetab = mp.atan(z)
    tb = 0.5*(twicethetab)
    if mpmath.almosteq(B1m*mp.sin(2*ta)
                       + B2mm*mp.sin(2*ta)*(mp.cos(tb)**2)
                       + B2mp*mp.sin(2*ta)*(mp.sin(tb)**2)
                       + B3*mp.cos(2*ta)*mp.sin(2*tb), 0, STRICT_ZERO_TOLERANCE):
        tbsols.append(tb)
    tb = 0.5*(twicethetab+mp.pi)
    if mpmath.almosteq(B1m*mp.sin(2*ta)
                       + B2mm*mp.sin(2*ta)*(mp.cos(tb)**2)
                       + B2mp*mp.sin(2*ta)*(mp.sin(tb)**2)
                       + B3*mp.cos(2*ta)*mp.sin(2*tb), 0, STRICT_ZERO_TOLERANCE):
        tbsols.append(tb)
    if len(tbsols) == 2:
        assert mpmath.almosteq(mp.cos(2*ta), 0, STRICT_ZERO_TOLERANCE)
        tbsols.pop(0)
    assert len(tbsols) == 1
    return  tbsols[0]
