""":mod:`.integralgenerator` contains functions for generating
symmetry-equivalent electron integrals and terms.
"""

import numpy as np  # type:ignore
from pyscf import gto  # type:ignore

from typing import Sequence, List, Tuple, Union, Dict, Optional

SymDict = Dict[str, List[int]]

Integral = Tuple[int, ...]


sym1e_generic = {"cc": [1, 0]}
sym2e_generic = {"cc1": [2, 1, 0, 3],
                 "cc2": [0, 3, 2, 1],
                 "e_relabel": [1, 0, 3, 2]}


def positional_permutation(perm: Sequence[int], integral: Integral) -> Integral:
    new_integral: List[int] = []
    for i in perm:
        assert 0 <= i < len(integral)
        new_integral.append(integral[i])
    return tuple(new_integral)


def label_replacement(new_labels: Sequence[int], integral: Integral) -> Integral:
    new_integral: List[int] = []
    for A in integral:
        assert 0 < A <= len(new_labels)
        assert 0 < new_labels[A-1] <= len(new_labels)
        new_integral.append(new_labels[A-1])
    return tuple(new_integral)


def get_perm_sym(sym_dict: SymDict, sym: str, integral: Integral) -> Integral:
    perm = sym_dict[sym]
    return positional_permutation(perm, integral)


def get_spatial_sym(sym_dict: SymDict, sym: str, integral: Integral) -> Integral:
    new_labels = sym_dict[sym]
    return label_replacement(new_labels, integral)


def get_1e_numerical(onee: np.ndarray, integral: Integral) -> float:
    assert len(integral) == 2
    i = integral[0] - 1
    j = integral[1] - 1
    return onee[i][j]


def get_2e_numerical(twoe: np.ndarray, integral: Integral) -> float:
    assert len(integral) == 4
    i = integral[0] - 1
    k = integral[1] - 1
    j = integral[2] - 1
    l = integral[3] - 1
    return twoe[i][j][k][l]


def get_1e_integrals(sym1e_generic: SymDict,
                     sym_spatial: SymDict,
                     numerical_integrals: np.ndarray,
                     keep_vanishing: bool = True)\
        -> List[List[Tuple[Integral, str]]]:
    all_1e_integrals: List[Integral] = []
    sorted_1e_integrals: List[List[Tuple[Integral, str]]] = []
    nbasis = numerical_integrals.shape[0]
    for A in range(1, nbasis+1):
        for B in range(1, nbasis+1):
            all_1e_integrals.append((A, B))

    while len(all_1e_integrals) > 0:
        lead_integral = all_1e_integrals[0]
        sorted_1e_integrals.append([])
        equiv_integrals: List[Tuple[Integral, str]] = [(lead_integral, "lead")]

        for sym in sym1e_generic.keys():
            func_integrals = []
            for integral, label in equiv_integrals:
                func_integrals.append((get_perm_sym(sym1e_generic,
                                                    sym,
                                                    integral),
                                      f"{sym} {label}"))
            equiv_integrals.extend(func_integrals)

        for sym in sym_spatial.keys():
            func_integrals = []
            for integral, label in equiv_integrals:
                func_integrals.append((get_spatial_sym(sym_spatial,
                                                       sym,
                                                       integral),
                                       f"{sym} {label}"))
            equiv_integrals.extend(func_integrals)

        for equiv_integral, label in equiv_integrals:
            try:
                all_1e_integrals.remove(equiv_integral)
                sorted_1e_integrals[-1].append((equiv_integral, label))
            except:
                continue

    count = 0
    retained_1e_integrals = []
    for _, equiv_integrals in enumerate(sorted_1e_integrals):
        if not keep_vanishing and\
                abs(get_1e_numerical(numerical_integrals,
                                     equiv_integrals[0][0])) < 1e-14:
            continue
        retained_1e_integrals.append(equiv_integrals)
        count += 1
        print(f"Equivalent group {count}:")
        for integral, label in equiv_integrals:
            i = integral[0]-1
            j = integral[1]-1
            print(f"{get_1e_numerical(numerical_integrals, integral):20.10f} \t"
                  + f"{print_integral(integral, 'h')}", label)

    return retained_1e_integrals


def get_2e_integrals(sym2e_generic: SymDict,
                     sym_spatial: SymDict,
                     numerical_integrals: np.ndarray,
                     keep_vanishing: bool = True)\
        -> List[List[Tuple[Integral, str]]]:
    all_2e_integrals: List[Integral] = []
    sorted_2e_integrals: List[List[Tuple[Integral, str]]] = []
    nbasis = numerical_integrals.shape[0]
    for A in range(1, nbasis+1):
        for B in range(1, nbasis+1):
            for C in range(1, nbasis+1):
                for D in range(1, nbasis+1):
                    all_2e_integrals.append((A, B, C, D))

    while len(all_2e_integrals) > 0:
        lead_integral = all_2e_integrals[0]
        sorted_2e_integrals.append([])
        equiv_integrals = [(lead_integral, "lead")]

        for sym in sym2e_generic.keys():
            func_integrals = []
            for integral, label in equiv_integrals:
                func_integrals.append((get_perm_sym(sym2e_generic,
                                                    sym,
                                                    integral),
                                      f"{sym} {label}"))
            equiv_integrals.extend(func_integrals)

        for sym in sym_spatial.keys():
            func_integrals = []
            for integral, label in equiv_integrals:
                func_integrals.append((get_spatial_sym(sym_spatial,
                                                       sym,
                                                       integral),
                                       f"{sym} {label}"))
            equiv_integrals.extend(func_integrals)

        for equiv_integral, label in equiv_integrals:
            try:
                all_2e_integrals.remove(equiv_integral)
                sorted_2e_integrals[-1].append((equiv_integral, label))
            except:
                continue

    count = 0
    retained_2e_integrals = []
    for _, equiv_integrals in enumerate(sorted_2e_integrals):
        if not keep_vanishing and\
                abs(get_2e_numerical(numerical_integrals,
                                     equiv_integrals[0][0])) < 1e-14:
            continue
        retained_2e_integrals.append(equiv_integrals)
        count += 1
        print(f"Equivalent group {count}:")
        for integral, label in equiv_integrals:
            print(f"{get_2e_numerical(numerical_integrals, integral):20.10f} \t"
                + f"{print_integral(integral)}", label)

    return retained_2e_integrals


def print_integral(integral: Integral, op: str = None):
    if len(integral) == 2:
        if op is not None:
            return f"⟨{integral[0]}|{op}|{integral[1]}⟩"
        else:
            return f"⟨{integral[0]}|{integral[1]}⟩"
    elif len(integral) == 4:
        if op is not None:
            return f"⟨{integral[0]}{integral[1]}|{op}|{integral[2]}{integral[3]}⟩"
        else:
            return f"⟨{integral[0]}{integral[1]}|{integral[2]}{integral[3]}⟩"


def get_1e_terms(sorted_1e_integrals: List[List[Tuple[Integral, str]]],
                 coeffs: List[List[Tuple[float, str, int]]],
                 op: Optional[str] = None) -> Dict[str, List[str]]:
    terms_1e: Dict[str, List[str]] = {}
    for i, equiv_integrals in enumerate(sorted_1e_integrals):
        lead_integral = equiv_integrals[0][0]
        facs: Dict[str, float] = {}
        for Cspin in coeffs:
            for integral, label in equiv_integrals:
                A = integral[0]
                B = integral[1]
                coeff = Cspin[A-1][0]*Cspin[B-1][0]
                if coeff != 0:
                    Cspinsorted = sorted([Cspin[A-1], Cspin[B-1]], key = lambda x: x[2])
                    fac = f"{Cspinsorted[0][1]}{Cspinsorted[0][2]}"\
                          + f"*{Cspinsorted[1][1]}{Cspinsorted[1][2]}"
                    if fac in facs:
                        facs[fac] += coeff
                    else:
                        facs[fac] = coeff
        for fac, coeff in sorted(facs.items()):
            if coeff != 0:
                if fac in terms_1e:
                    if isinstance(coeff, float) or isinstance(coeff, int):
                        terms_1e[fac].append(\
                            f"{coeff:+}*{print_integral(lead_integral, op)}")
                    else:
                        terms_1e[fac].append(\
                            f"({coeff})*{print_integral(lead_integral, op)}")
                else:
                    if isinstance(coeff, float) or isinstance(coeff, int):
                        terms_1e[fac] = [\
                            f"{coeff:+}*{print_integral(lead_integral, op)}"]
                    else:
                        terms_1e[fac] = [\
                            f"({coeff})*{print_integral(lead_integral, op)}"]
    for fac, terms in sorted(terms_1e.items()):
        print(fac, "(", *terms, ")")
    return terms_1e

# Coulomb terms only, since the exchange term is zero by spin.
def get_2e_terms(sorted_2e_integrals: List[List[Tuple[Integral, str]]],
                 coeffs: List[List[Tuple[float, str, int]]],
                 op: Optional[str] = None,
                 ex: bool = False) -> Dict[str, List[str]]:
    [Ca, Cb] = coeffs
    terms_2e: Dict[str, List[str]] = {}
    for i, equiv_integrals in enumerate(sorted_2e_integrals):
        lead_integral = equiv_integrals[0][0]
        facs: Dict[str, float] = {}
        for integral, label in equiv_integrals:
            A = integral[0]
            B = integral[1]
            C = integral[2]
            D = integral[3]
            coeff = Ca[A-1][0]*Cb[B-1][0]*Ca[C-1][0]*Cb[D-1][0]
            if isinstance(coeff, float):
                not_zero = (abs(coeff) >= 1e-14)
            else:
                not_zero = not (coeff == 0)
            if not_zero:
                Casorted = sorted([Ca[A-1], Ca[C-1]], key=lambda x: x[2])
                Cbsorted = sorted([Cb[B-1], Cb[D-1]], key=lambda x: x[2])
                fac = f"{Casorted[0][1]}{Casorted[0][2]}"\
                      + f"*{Casorted[1][1]}{Casorted[1][2]}"\
                      + f"*{Cbsorted[0][1]}{Cbsorted[0][2]}"\
                      + f"*{Cbsorted[1][1]}{Cbsorted[1][2]}"
                if fac in facs:
                    facs[fac] += coeff
                else:
                    facs[fac] = coeff
            if ex:
                coeff = -1*Ca[A-1][0]*Cb[B-1][0]*Ca[D-1][0]*Cb[C-1][0]
                if abs(coeff) >= 1e-14:
                    Casorted = sorted([Ca[A-1], Ca[D-1]], key=lambda x: x[2])
                    Cbsorted = sorted([Cb[B-1], Cb[C-1]], key=lambda x: x[2])
                    fac = f"{Casorted[0][1]}{Casorted[0][2]}"\
                        + f"*{Casorted[1][1]}{Casorted[1][2]}"\
                        + f"*{Cbsorted[0][1]}{Cbsorted[0][2]}"\
                        + f"*{Cbsorted[1][1]}{Cbsorted[1][2]}"
                    if fac in facs:
                        facs[fac] += coeff
                    else:
                        facs[fac] = coeff
        for fac, coeff in sorted(facs.items()):
            if coeff != 0:
                if fac in terms_2e:
                    if isinstance(coeff, float) or isinstance(coeff, int):
                        terms_2e[fac].append(f"{coeff:+3}*{print_integral(lead_integral, op)}")
                    else:
                        terms_2e[fac].append(f"({coeff})*{print_integral(lead_integral, op)}")
                else:
                    if isinstance(coeff, float) or isinstance(coeff, int):
                        terms_2e[fac] = [f"{coeff:+3}*{print_integral(lead_integral, op)}"]
                    else:
                        terms_2e[fac] = [f"({coeff:3})*{print_integral(lead_integral, op)}"]
    for fac, terms in sorted(terms_2e.items()):
        print(fac, "(", *terms, ")")
    return terms_2e
