from typing import Tuple

import numpy as np  # type: ignore
import mpmath  # type: ignore
from mpmath import mp, mpf, mpc  # type: ignore
from pyscf import gto, scf  # type: ignore


def preamble(bb = None, scale = 1.0):
    preamble_str =\
            "\\PassOptionsToPackage{svgnames, luatex, rgb}{xcolor}\n" +\
            "\\documentclass[tikz]{standalone}\n" +\
            "\\usepackage{pgfplots, siunitx, graphicx, mathtools}\n" +\
            "\\pgfplotsset{compat=1.16}\n" +\
            "\\usetikzlibrary{arrows.meta, calc, luamath, positioning}\n" +\
            "% Font & languages\n" +\
            "\\usepackage[no-math]{fontspec}\n" +\
            "\\defaultfontfeatures{Ligatures=TeX, Numbers=Lining}\n" +\
            "\\usepackage{polyglossia}\n" +\
            "\\setmainlanguage[variant=uk]{english}\n" +\
            "\\setmainfont{Alegreya}[Ligatures=TeX]\n" +\
            "\\setsansfont{Alegreya-Sans}\n" +\
            "\\newcommand\\bmmax{2}\n" +\
            "\\usepackage{bm}\n" +\
            "\\usepackage[math-style=TeX]{unicode-math}\n" +\
            "\\setmathfont{STIX2Math.otf}\n" +\
            "\\setmathfont{Alegreya-Italic.otf}[Scale=MatchLowercase, ScaleAgain=0.99999, range = it/{latin, Latin, greek, Greek}]\n" +\
            "\\setmathfont{Alegreya-Regular.otf}[Scale=MatchLowercase, ScaleAgain=0.99999, range = up/{latin, Latin, greek, Greek, num}]\n" +\
            "\\setmathfont{Alegreya-BoldItalic.otf}[Scale=MatchLowercase,ScaleAgain=0.99999,range=bfit/{latin, Latin, greek, Greek}]\n" +\
            "\\setmathfont{Alegreya-Bold.otf}[Scale=MatchLowercase,ScaleAgain=0.99999,range=bfup/{latin, Latin, greek, Greek, num}]\n" +\
            "\\begin{document}\n" +\
            "\\begin{tikzpicture}\n"

    if bb is None:
        bb = [(-10, 10), (-10, 10)]
    (xmin, xmax) = bb[0]
    (ymin, ymax) = bb[1]
    preamble_str +=\
            f"\\useasboundingbox ({xmin*scale},{ymin*scale}) rectangle ({xmax*scale},{ymax*scale});\n" +\
            f"\\scalebox{{ {scale} }}{{\n"

    return preamble_str

def auxiliary(bb, a, k, ext_cons, mol_scale, text_scale, colorwheel, k_label = "k"):
    if bb is None:
        bb = [(-10, 10), (-10, 10)]
    (xmin, xmax) = bb[0]
    (ymin, ymax) = bb[1]
    aux_str =\
            f"\\node[inner sep=1pt, scale={text_scale}] (label) at ({(xmin+xmax)/2:.5f},{ymax-0.05*(ymax-ymin):.5f}) {{ $a = \\SI{{{a:.5f}}}{{\\angstrom}}, {k_label} = {k:.3f}$ }};\n" +\
            f"\\node[inner sep=1pt, scale={text_scale}] (ext_cons) at ({(xmin+xmax)/2:.5f},{ymax-0.10*(ymax-ymin):.5f}) {{ {ext_cons} }};\n" +\
            f"\\coordinate (colorwheel) at ({(xmin+xmax)/2:.5f},{ymin+0.05*(ymax-ymin):.5f});\n" +\
            f"\\node[anchor=south] (wheel) at (colorwheel) {{\includegraphics[scale={mol_scale}]{{{colorwheel}}}}};\n"
    return aux_str

def ending():
    end_str =\
            "}\n" +\
            "\\end{tikzpicture}\n" +\
            "\\end{document}\n"

    return end_str


def orb_circle(coord, cmag: float, carg: float, orb_scale: float, opacity: float = 1):
    # Obtain the phase colour
    orb_str =\
        f"\\xdefinecolor{{orbcolor}}{{hsb}}{{ {carg:.5f}, 1, 1 }}\n"

    # Plot the main orbital circle
    if abs(carg) < 1e-14 or abs(carg-0.5) < 1e-14 or abs(carg-1.0) < 1e-14:
        orb_str = orb_str +\
            f"\\draw[fill=orbcolor, thick, opacity={opacity}] ({coord}) circle [radius={cmag*orb_scale:.5f}];\n"
    elif abs(carg-0.25) < 1e-14 or abs(carg-0.75) < 1e-14:
        orb_str = orb_str +\
            f"\\draw[fill=orbcolor, dotted, opacity={opacity}] ({coord}) circle [radius={cmag*orb_scale:.5f}];\n"
    else:
        orb_str = orb_str +\
            f"\\draw[fill=orbcolor, opacity={opacity}] ({coord}) circle [radius={cmag*orb_scale:.5f}];\n"

    # Plot the phase dot
    orb_str = orb_str +\
        f"\\draw[fill=black, opacity={opacity}] ($({coord}) + ({carg*360:.5f}:{cmag*orb_scale:.5f})$) circle [radius=1pt];\n"
    return orb_str

def plot_orbital(x: float, y: float, a: float, k: float,
                 mol_scale: float, text_scale: float, orb_scale: float,
                 cs: Tuple[mpc, mpc, mpc, mpc],
                 label: str, side_label: bool = True):
    [c1, c2, c3, c4] = cs
    c1mag = float(mpmath.sqrt(mpmath.fabs(c1)))
    c1arg = float(mpmath.arg(c1)/(2*mp.pi))
    if c1arg < 0:
        c1arg += 1
    c2mag = float(mpmath.sqrt(mpmath.fabs(c2)))
    c2arg = float(mpmath.arg(c2)/(2*mp.pi))
    if c2arg < 0:
        c2arg += 1
    c3mag = float(mpmath.sqrt(mpmath.fabs(c3)))
    c3arg = float(mpmath.arg(c3)/(2*mp.pi))
    if c3arg < 0:
        c3arg += 1
    c4mag = float(mpmath.sqrt(mpmath.fabs(c4)))
    c4arg = float(mpmath.arg(c4)/(2*mp.pi))
    if c4arg < 0:
        c4arg += 1
    string =\
        f"% new orbital\n" +\
        f"\coordinate (orb) at ({x},{y});\n" +\
        f"\\coordinate (H1) at ($(orb) + ({-a*k/2*mol_scale:.5f},{a/2*mol_scale:.5f})$);\n" +\
        f"\\coordinate (H2) at ($(orb) + ({a*k/2*mol_scale:.5f},{a/2*mol_scale:.5f})$);\n" +\
        f"\\coordinate (H3) at ($(orb) + ({a*k/2*mol_scale:.5f},{-a/2*mol_scale:.5f})$);\n" +\
        f"\\coordinate (H4) at ($(orb) + ({-a*k/2*mol_scale:.5f},{-a/2*mol_scale:.5f})$);\n" +\
        f"\draw (H1) -- (H2) -- (H3) -- (H4) -- (H1) -- cycle;\n" +\
        f"\\node[inner sep=1pt, scale=0.5*{text_scale}] (orb) at (orb) {{ {label} }};\n"
    if side_label:
        string +=\
            f"\\node[inner sep=1pt, scale=0.4*{text_scale}, anchor=east] (a) at ($(orb) + ({-1.05*a*k/2*mol_scale:.5f},0)$) {{$a$}};\n" +\
            f"\\node[inner sep=1pt, scale=0.4*{text_scale}, anchor=north] (ka) at ($(orb) + (0,{-1.05*a/2*mol_scale:.5f})$) {{$ka$}};\n"

    string +=\
        f"{orb_circle('H1', c1mag, c1arg, orb_scale)}" +\
        f"{orb_circle('H2', c2mag, c2arg, orb_scale)}" +\
        f"{orb_circle('H3', c3mag, c3arg, orb_scale)}" +\
        f"{orb_circle('H4', c4mag, c4arg, orb_scale)}"

    return string


def plot_orbital_rhombus(x: float, y: float, a: float, k: float,
                         mol_scale: float, text_scale: float, orb_scale: float,
                         cs: Tuple[mpc, mpc, mpc, mpc],
                         label: str, side_label: bool = True):
    [c1, c2, c3, c4] = cs
    c1mag = float(mpmath.sqrt(mpmath.fabs(c1)))
    c1arg = float(mpmath.arg(c1)/(2*mp.pi))
    if c1arg < 0:
        c1arg += 1
    c2mag = float(mpmath.sqrt(mpmath.fabs(c2)))
    c2arg = float(mpmath.arg(c2)/(2*mp.pi))
    if c2arg < 0:
        c2arg += 1
    c3mag = float(mpmath.sqrt(mpmath.fabs(c3)))
    c3arg = float(mpmath.arg(c3)/(2*mp.pi))
    if c3arg < 0:
        c3arg += 1
    c4mag = float(mpmath.sqrt(mpmath.fabs(c4)))
    c4arg = float(mpmath.arg(c4)/(2*mp.pi))
    if c4arg < 0:
        c4arg += 1
    theta = k*np.pi/4
    cos = np.cos(theta)
    sin = np.sin(theta)
    string =\
        f"% new orbital\n" +\
        f"\coordinate (orb) at ({x},{y});\n" +\
        f"\\coordinate (H1) at ($(orb) + ({-a*cos**2*mol_scale:.5f},{a*cos*sin*mol_scale:.5f})$);\n" +\
        f"\\coordinate (H2) at ($(orb) + ({a*sin**2*mol_scale:.5f},{a*sin*cos*mol_scale:.5f})$);\n" +\
        f"\\coordinate (H3) at ($(orb) + ({a*cos**2*mol_scale:.5f},{-a*cos*sin*mol_scale:.5f})$);\n" +\
        f"\\coordinate (H4) at ($(orb) + ({-a*sin**2*mol_scale:.5f},{-a*sin*cos*mol_scale:.5f})$);\n" +\
        f"\draw (H1) -- (H2) -- (H3) -- (H4) -- (H1) -- cycle;\n" +\
        f"\\node[inner sep=1pt, scale=0.5*{text_scale}] (orb) at (orb) {{ {label} }};\n"
    if side_label:
        string +=\
                f"\\node[inner sep=2pt, scale=0.4*{text_scale}, anchor=north, align=center] (a) at ($(orb) + ({a*(cos**2-sin**2)/2*mol_scale:.5f},{-a*sin*cos*mol_scale:.5f})$) {{$a$\\\\ $\\theta={theta*2:.3f}$}};\n"

    string +=\
        f"{orb_circle('H1', c1mag, c1arg, orb_scale)}" +\
        f"{orb_circle('H2', c2mag, c2arg, orb_scale)}" +\
        f"{orb_circle('H3', c3mag, c3arg, orb_scale)}" +\
        f"{orb_circle('H4', c4mag, c4arg, orb_scale)}"

    return string


def plot_orbital_tet(x: float, y: float, a: float, k: float,
                     mol_scale: float, text_scale: float, orb_scale: float,
                     cs: Tuple[mpc, mpc, mpc, mpc],
                     label: str, side_label: bool = True):
    [c1, c2, c3, c4] = cs
    c1mag = float(mpmath.sqrt(mpmath.fabs(c1)))
    c1arg = float(mpmath.arg(c1)/(2*mp.pi))
    if c1arg < 0:
        c1arg += 1
    c2mag = float(mpmath.sqrt(mpmath.fabs(c2)))
    c2arg = float(mpmath.arg(c2)/(2*mp.pi))
    if c2arg < 0:
        c2arg += 1
    c3mag = float(mpmath.sqrt(mpmath.fabs(c3)))
    c3arg = float(mpmath.arg(c3)/(2*mp.pi))
    if c3arg < 0:
        c3arg += 1
    c4mag = float(mpmath.sqrt(mpmath.fabs(c4)))
    c4arg = float(mpmath.arg(c4)/(2*mp.pi))
    if c4arg < 0:
        c4arg += 1
    theta = k*np.pi/2
    d = a/2*(1-1/np.sqrt(2))*np.cos(2*theta) + a/2*(1+1/np.sqrt(2))
    cos = np.cos(theta)
    sin = np.sin(theta)
    cosa = np.cos(np.arctan(2)) # cabinet projection angle
    sina = np.sin(np.arctan(2)) # cabinet projection angle
    string =\
        f"% new orbital\n" +\
        f"\coordinate (orb) at ({x},{y});\n" +\
        f"\\coordinate (H1) at ($(orb) + ({(-a/2*cos-0.5*(-a/2*sin)*cosa)*mol_scale:.5f},{(d/2-0.5*(-a/2*sin)*sina)*mol_scale:.5f})$);\n" +\
        f"\\coordinate (H2) at ($(orb) + ({(a/2*cos-0.5*(a/2*sin)*cosa)*mol_scale:.5f},{(d/2-0.5*(a/2*sin)*sina)*mol_scale:.5f})$);\n" +\
        f"\\coordinate (H3) at ($(orb) + ({+a/2*mol_scale:.5f},{-d/2*mol_scale:.5f})$);\n" +\
        f"\\coordinate (H4) at ($(orb) + ({-a/2*mol_scale:.5f},{-d/2*mol_scale:.5f})$);\n" +\
        f"\draw[gray] (H1) -- (H3);\n" +\
        f"\draw (H1) -- (H2) -- (H3) -- (H4) -- (H1) -- cycle;\n" +\
        f"\draw (H2) -- (H4);\n" +\
        f"\\node[inner sep=1pt, scale=0.5*{text_scale}] (orb) at (orb) {{ {label} }};\n"
    if side_label:
        string +=\
                f"\\node[inner sep=2pt, scale=0.4*{text_scale}, anchor=north, align=center] (a) at ($(H3)!0.5!(H4)$) {{$a$}};\n"

    string +=\
        f"{orb_circle('H1', c1mag, c1arg, orb_scale)}" +\
        f"{orb_circle('H2', c2mag, c2arg, orb_scale)}" +\
        f"{orb_circle('H3', c3mag, c3arg, orb_scale)}" +\
        f"{orb_circle('H4', c4mag, c4arg, orb_scale)}"

    return string
