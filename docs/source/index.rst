.. QCMagic (revised) documentation master file, created by
   sphinx-quickstart on Fri Oct 18 14:02:54 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to analytical-tools's documentation!
============================================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    apidoc/modules.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

References
==========
.. bibliography:: references.bib
