# poly-inspect

`poly-inspect` provides a comprehensive toolkit for the symmetry analysis of single-determinantal and multi-determinantal wavefunctions in any finite point groups.

## Development Goals

`poly-inspect` has the capability to perform symmetry transformation on wavefunctions using the symmetry elements of the underlying point groups, thus allowing it to handle wavefunction degeneracy correctly without the need to confine the analysis to the largest Abelian subgroup as commonly done in many quantum-chemistry packages.

For the time being, character tables have to be manually inputted.
However, one of the goals for the near-future development of `poly-inspect` is to generate character tables automatically based on the point-group information of the systems being analysed.

`poly-inspect` can detect all symmetry _elements_ in any finite point groups.
For infinite point groups, `poly-inspect` can, at the moment, detect all infinitesimal symmetry _generators_.
When the automated generation of character tables has been fully implemented, it will be possible to use these generators to perform symmetry analysis for wavefunctions in infinite point groups as well.

`poly-inspect` is currently written entirely in Python 3.8+ with important vectorisable operations taken care of by `numpy`.
However, as the package expands, other low-level core operations will be rewritten in a faster programming language so as to improve the overall performance of `poly-inspect`.
Rust is my personal prime candidate at the moment owing particularly to its speed and safety.
Plus, it is a cool language.

At all stages during the development, static checks and unit tests will be enforced via the CI/CD pipeline of GitLab to ensure a high standard of code quality.


## Setting Up for Development

A development environment has been set up for `poly-inspect` in `anaconda`.
It is recommended that this environment be used for the development of `poly-inspect`.
The following steps show how this environment can be cloned and activated.

1. Ensure that `conda` is available. This can be done by installing Anaconda (see [here](https://www.anaconda.com/products/individual) for instructions).

2. Clone the development environment by executing the following command from the root directory of `poly-inspect`:

   ```sh
   conda env create -f meta/poly-inspect-devsuite.yml
   ```

3. Verify that the environment `poly-inspect-devsuite` has been successfully created:

   ```sh
   conda env list
   ```

4. Activate the `poly-inspect-devsuite` environment:

   ```sh
   conda activate poly-inspect-devsuite
   ```

5. Generate the API Reference:

   ```sh
   cd docs/
   make html
   ```

   which can be viewed at `docs/build/html/index.html`.


## Usage

A detailed usage guide will be provided as the package becomes more complete.
For the time being, many examples of how the various low-level classes and functions are used can be found in the unit tests located under `tests/`.
Further information can also be found in the API Reference.


## Installation via `pip`

`poly-inspect` can be easily installed via `pip` as an importable package:

```sh
python3 -m pip install git+https://gitlab.com/bangconghuynh/poly-inspect.git
```

This allows all `poly-inspect` classes and functions to be imported via

```python
import polyinspect
```
